<?php
class Common {

    public $dbconn;
    public function __construct(PDO $dbconn){
        $this->dbconn = $dbconn;
    }
    /*Backend API*/
	/*Function to execute the SELECT queries*/
    public function funBckendExeSelectQuery($qry, $qryParams = array(), $fetchtype=''){        
        $prepareQry = $this->dbconn->prepare($qry);
        if (count($qryParams)>0)            // Fetching all records
            $execQry =  $prepareQry->execute($qryParams);
        else                                // Fetching one record
            $execQry = $prepareQry->execute();
        $row_count = $prepareQry->rowCount();
        if ($row_count>0 && $fetchtype=='') {
			$fetch_data = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            return $fetch_data;
        } elseif ($row_count>0 && $fetchtype=='fetch') {
            $fetch_data = $prepareQry->fetch(PDO::FETCH_ASSOC);
            return $fetch_data;
        }
        else {
            return false;
        }
    }   

    /*Function to execute the Insert queries*/
    function funBckendExeInsertRecord($query,$insert_data) {
        $prepareInsertQry = $this->dbconn->prepare($query);
        $ExecPrepInsertQry = $prepareInsertQry->execute($insert_data);
        if($ExecPrepInsertQry) {
            $last_insert_id = $this->dbconn->lastInsertId();
            return $last_insert_id;
        } else {
            return false;
        }
    }

    /*Function to execute the update queries*/
    function funBckendExeUpdateRecord($qry, $qryParams) {
        $PrepUpdateQry =  $this->dbconn->prepare($qry);
        $ExecPrepUpdateQry = $PrepUpdateQry->execute($qryParams);
        if ($ExecPrepUpdateQry) {
            return true;
        } else{
            return false;
        }
    }

    /* Mobile API - Indhiyan */

    // Validation - Check required fields
    public function funCheckRequiredFields ($request, $requiredFieldsArr) {

        $errors = array();
        if (count($requiredFieldsArr) > 0 && count($request) > 0) {
            foreach ($requiredFieldsArr as $field) {               
                if (!array_key_exists($field, $request)) {
                    $errors[$field] = $field." does not exist";
                } else if (empty($request[$field])) {
                    $errors[$field] = $field." cannot be empty";
                }
            }
        }
        return $errors;

    }    

    /*Function to execulte the SELECT single record queries*/
    public function funExeSelectQuery ($reqQryParams, $qryParams = array()) {

        // funExeTwoTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        //create query
        $tableName = $reqQryParams["tableName"];
        $selectCluseCondtn = "*";
        $whereClauseCondtn = ""; 
        $orderByClauseCondtn = ""; 
        $groupByClauseCondtn = "";      

        if (!empty($reqQryParams["selectField"]))
            $selectCluseCondtn = $reqQryParams["selectField"];
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];  

        if (!empty($reqQryParams["groupByCondition"]))
                $groupByClauseCondtn = "GROUP BY  ".$reqQryParams["groupByCondition"];         

        $qry = "SELECT $selectCluseCondtn FROM $tableName $whereClauseCondtn $orderByClauseCondtn $groupByClauseCondtn";
        $prepareQry = $this->dbconn->prepare($qry);
        if (!empty($qryParams) && count($qryParams) > 0) {
            $execQry = $prepareQry->execute($qryParams);
        } else {
            $execQry = $prepareQry->execute();
        }
        $rowCnt = $prepareQry->rowCount();
        $fetchData = array();
        if ($rowCnt > 0 ) {
            if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
            else
                $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
        }
        return $fetchData;
    }    

    /*Function to execulte the Insert queries*/
    function funExeInsertRecord($tblName, $qryParams = array()) {

        if (!empty($qryParams) && count($qryParams) > 0) {
            // Remove ":" string from the key
            $tblFieldsArr = array();
            array_walk($qryParams, function (&$value,$key) use (&$tblFieldsArr) {
                $tblFieldsArr[ str_replace(":","",$key) ] = $value;
            });

            // retrieve the keys of the array (column titles)
            $tblFields = array_keys($tblFieldsArr);
            $tblValues = array_keys($qryParams);
            // Build the query
            $query = "INSERT INTO ".$tblName." (`".implode('`,`', $tblFields)."`) VALUES (".implode(",", $tblValues).")";
            $prepareInsertQry = $this->dbconn->prepare($query);
            $ExecPrepInsertQry = $prepareInsertQry->execute($qryParams);
            $lastInsertId = "";
            if ($ExecPrepInsertQry)
                $lastInsertId = $this->dbconn->lastInsertId();
            return $lastInsertId;
        }
    }

    /*Function to execulte the update queries*/
    function funExeUpdateRecord($reqQryParams, $qryParams = array()) {

        $tableName = $reqQryParams["tableName"];
        $setClauseCondtn = "";
        $whereClauseCondtn = "";

        if (!empty($reqQryParams["setCondtn"]))
            $setClauseCondtn = "SET ".$reqQryParams["setCondtn"]; 
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams)) {
            $qry = "UPDATE $tableName $setClauseCondtn $whereClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $updateRes = false;
            if ($rowCnt > 0 ) {
                $updateRes = true;
            }
            return $updateRes;
        }
    }

    
    /*Function to execulte the delete queries*/
    function funExeDeleteQuery($reqQryParams, $qryParams = array()) {

        //create query
        $tableName = $reqQryParams["tableName"];
        $whereClauseCondtn = ""; 
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        $qry = "DELETE FROM $tableName $whereClauseCondtn";
        $PrepDeleteQry = $this->dbconn->prepare($qry);
        $ExecPrepDeleteQry = $PrepDeleteQry->execute($qryParams);
        if($ExecPrepDeleteQry) {
            return true;
        } else{
            return false;
        }
    } 

    // $field and $seperator are required otherwise both are empty
    public function funParseQryParams ($qryParams, $field = "", $seperator = "") {

        $setWhereCondtn = "";
        if (!empty($qryParams) && count($qryParams) > 1) {
            if (!empty($field) && !empty($seperator)) {
                $i = 1;
                foreach ($qryParams as $qryKey => $qryVal) {       
                    $qryKey = str_replace(":","" ,$qryKey); 
                    $setWhereCondtn .= $qryKey." = :".$qryKey;
                    if ($qryKey != $field)
                        $setWhereCondtn .= " $seperator ";
                    $i++;
                }
            } else {
                return "field and seperator required!";
            }
        } else {
            $i = 1;
            foreach ($qryParams as $qryKey => $qryVal) {       
                $qryKey = str_replace(":","" ,$qryKey); 
                $setWhereCondtn .= $qryKey." = :".$qryKey;
                $i++;
            }
        }
        return $setWhereCondtn;
    }

    /* Function to execulte the Leftjoin query */
    public function funExeTwoTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        if (is_array($reqQryParams) && !empty($reqQryParams)) {

            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";
            

            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];

            if (!empty($reqQryParams["onCondition"]))
                $onClauseCondtn = "ON ".$reqQryParams["onCondition"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];           

            $qry = "SELECT $selectCluseCondtn FROM $tbl1 LEFT JOIN $tbl2 $onClauseCondtn $whereClauseCondtn $orderByClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }


    /* Function to execulte the Leftjoin query */
    public function funExeFourTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        if (is_array($reqQryParams) && !empty($reqQryParams)) {

            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $tbl3 = $reqQryParams["tables"][2];
            $tbl4 = $reqQryParams["tables"][3];
            $onCond1 = "ON ".$reqQryParams["onCondition"][0];
            $onCond2 = "ON ".$reqQryParams["onCondition"][1];
            $onCond3 = "ON ".$reqQryParams["onCondition"][2];
        
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";
            

            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];           

            $qry = "SELECT $selectCluseCondtn FROM $tbl1 INNER JOIN $tbl2 $onCond1 INNER JOIN $tbl3 $onCond2 LEFT JOIN $tbl4 $onCond3 $whereClauseCondtn $orderByClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }

    /* Function to execulte the Leftjoin query */
    public function funGetOrderList($orderId) {

        $orderItemArr = array();
        if (!empty($orderId)) {

            $orderParams = array ( ":order_id" => $orderId);
            $whereCondtn = "a.order_id=:order_id";
            $reqQryParams = array (
                            "fetchType" => "multipleRow",
                            "selectField" => "*",
                            "tables" => array("tbl_order_items as a", "tbl_category_items as b"),
                            "onCondition" => "a.item_id = b.item_id",
                            "orderByCondtn"  => "a.order_id desc",
                            "whereCondition" => $whereCondtn,
                        );
            $orderItemList = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $orderParams);            
            if (!empty($orderItemList)) {
                $j = 0;                
                foreach ($orderItemList as $orderItem) {                    
                    $orderItemArr[$j]["price"] = $orderItem["price"];
                    $orderItemArr[$j]["orderItemId"] = !empty($orderItem["order_item_id"]) ? $orderItem["order_item_id"] : "" ;
                    $orderItemArr[$j]["itemType"] = !empty($orderItem["item_type"]) ? $orderItem["item_type"] : "" ;
                    $orderItemArr[$j]["itemName"] = !empty($orderItem["item_name"]) ? $orderItem["item_name"] : "" ;
                    $orderItemArr[$j]["description"] = !empty($orderItem["short_description"]) ? $orderItem["short_description"] : "" ;
                    $j++;
                }
            }
        }
        return $orderItemArr;
    }
    /* Function to execulte the Leftjoin query */
    public function funGetPackageList($packageId) {

        $packageItemArr = array();
        if (!empty($packageId)) {

            $orderParams = array ( ":packageid" => $packageId);
            $whereCondtn = "a.package_id=:packageid";
            $reqQryParams = array (
                            "fetchType" => "multipleRow",
                            "selectField" => "*",
                            "tables" => array("tbl_packages as a", "tbl_package_items as b","tbl_category_items as c","tbl_category as d"),
                            "onCondition" => array("b.package_id = a.package_id", "c.item_id = b.item_id", "d.category_id=c.category_id"),
                            // "orderByCondtn"  => "a.order_id DESC",
                            "whereCondition" => $whereCondtn,
                        );
            $orderPackageItemList = $this->funExeFourTblLeftJoinQuery($reqQryParams, $orderParams);      
            if (!empty($orderPackageItemList)) {
                $j = 0;
                foreach ($orderPackageItemList as $packageItem) {
                    // funExeSelectQuery
                    $packageItemArr[$j]["price"] = $packageItem["price"];
                    $packageItemArr[$j]["orderItemId"] = !empty($packageItem["package_item_id"]) ? $packageItem["package_item_id"] : "" ;
                    $packageItemArr[$j]["itemType"] = !empty($packageItem["item_type"]) ? $packageItem["item_type"] : "" ;
                    $packageItemArr[$j]["itemName"] = !empty($packageItem["item_name"]) ? $packageItem["item_name"] : "" ;
                    $packageItemArr[$j]["description"] = !empty($packageItem["short_description"]) ? $packageItem["short_description"] : "" ;
                    $j++;
                }
            }
        }
        return $packageItemArr;

    }
    /*Function return JSON results from given Array*/
    public function jsonResponse($jsonEncodeArr){
        if (is_array($jsonEncodeArr)) 
            $responseJson = json_encode($jsonEncodeArr);
        return $responseJson;
    }

    // Convert base64image into image and move to assigned path
    public function base64toImage($base64image,$floderName) {
        if (!empty($base64image)) {
            $uploadDoc = "";
            if (!empty($base64image)) {

                $pos  = strpos($base64image, ';');
                if (!empty($pos)) {
                    $mimeType = explode(':', substr($base64image, 0, $pos))[1];
                    $imgData = str_replace('data:image/'.$mimeType.';base64,', '', $base64image);
                } else {
                    $imgData = str_replace('data:image/png;base64,', '', $base64image);
                }
                $imgData = str_replace(' ', '+', $imgData);
                $imgData = base64_decode($imgData);
                $randNumber = sha1($this->generateRandAlphanumeric(24));
                $genRandFilename = '../uploads/'.$floderName."/".$randNumber. '.png';                    
                $moveImgpath = file_put_contents($genRandFilename, $imgData);
                if ($moveImgpath)
                    $uploadDoc = 'uploads/'.$floderName."/".$randNumber. '.png';
            }
            return $uploadDoc;
        }
    }

    // Convert base64documents into documents and move to assigned path
    public function base64toDocs($base64docs,$floderName) {        
        if (!empty($base64docs)) {
            $uploadDoc = "";
            $type = pathinfo(base64_decode($base64docs), PATHINFO_EXTENSION);
            $imgData = base64_decode($base64docs);
            $randNumber = sha1($this->generateRandAlphanumeric(24));
            $genRandFilename = '../uploads/'.$floderName."/".$randNumber.".".$type;
            if(copy($imgData,$genRandFilename))
                $uploadDoc = 'uploads/'.$floderName."/".$randNumber.".".$type;            
            return $uploadDoc;
        }
    }

    // Select userInfo
    public function getUserInfo($userInfoArr) {

        if (is_array($userInfoArr) && count($userInfoArr) > 0) {

            $selQryParams = array (":user_id" => $userInfoArr["userId"], ":user_type" => $userInfoArr["userType"]);
            $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");
            $reqQryParams = array (
                "fetchType" => "singleRow",
                "selectField" => "*",
                "tableName" => "tbl_users",
                "whereCondition" => $whereCondtn
            );
            $userInfoRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
            return $userInfoRes;
        }
        
    }

    public function generateRandAlphanumeric() {
        // return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        return rand(10,100).sprintf( '%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0C2f ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
        ).rand(10,100);

    }

    public function generateVerficationCode($digits) {
       return $verfication_code=rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    /*Encryption and Decryption Password function start Here*/
    
    var $skey = "a2c4e6g8i0a2c4e6g8i02017"; // change this
 
    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
    
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
 
    public function encode($value){ 
        if (!$value) { return false; }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
    public function decode($value){
        if (!$value) { return false; }
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
    /*Encryption and Decryption Password function end Here*/

    // Send email
    public function sendEmailNotification($mailParams) {

        require '../includes/PHPmailer/PHPMailerAutoload.php';
        
        $mail = new PHPMailer;
        $mail->Host = 'shenll.net;shenll.net'; // Specify main and backup server
        $mail->From = $mailParams['fromAddress'];
        $mail->FromName = 'KazaFood';     
        $mail->addAddress($mailParams['toAddress'], '');
        $mail->isHTML(true);                                
        $mail->Subject = $mailParams['subject'];
        $mail->Body = $mailParams['bodyMsg'];
        $mailStatus = false;
        if($mail->send()){
            $mailStatus = true;
        }
        return $mailStatus;
    }

    // Send push notification Android
    public function sendAndroidPushNotification($gcm_id, $message, $appointment_id, $user_id) {

        /**
        @author:Shenll.
        @param: 1. Registration id from gcm_users table, 2. Task Message, 3. Child ID, 4. Benchmark ID, 5. Schedule ID
        @return: function send pushnotification to all the register id in the given parameter.
        */
        //$gcm_id="APA91bE9zwtuJOxG36LMQJLmoX_PUCicCafPHleD4xUEJsIhmDe8yfl9q-HAR5-ZNwf-MHqEarE0Mc9NdHVQTtfXjmWM5EnEQmUhlP8R2GLgi3yKLnH0RMs";
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
        'registration_ids' => array($gcm_id),
        'data' => array("price" => $message)
        );
        
        $headers = array (
            'Authorization: key=AIzaSyAUNtpvo85pflHod9zg10Hf2NN8RUT3dAI',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        $Arrresult=json_decode($result, true);
        // if ($Arrresult["failure"] == 1 || $result===FALSE) {
        //    // die('Curl failed: ' . curl_error($ch));
        //     $insQryParams = array ( 
        //                         ":user_id" => $user_id,
        //                         ":user_type" => $this->encode($request["password"]),
        //                         ":message" => "",
        //                         ":message_status" => 0,
        //                         ":created_date" => date("Y-m-d h:i:s"),
        //                         ":modified_date" => date("Y-m-d h:i:s")
        //                     );
        //     $insQryResponse = $this->funExeInsertRecord("tbl_pushnotifications", $insQryParams);

        // } else if ($Arrresult["success"] == 1) {
        //     // $update_query   =   "update tbl_appointments set notification_flag='1' where appointment_id='".$appointment_id."'";
        //     $upd_query      =   mysql_query($update_query);
        //     $insQryParams = array ( 
        //                         ":user_id" => $user_id,
        //                         ":user_type" => $this->encode($request["password"]),
        //                         ":message" => "",
        //                         ":message_status" => 1,
        //                         ":created_date" => date("Y-m-d h:i:s"),
        //                         ":modified_date" => date("Y-m-d h:i:s");
        //                     );
        //     $insQryResponse = $this->funExeInsertRecord("tbl_pushnotifications", $insQryParams);
        // }
        // Close connection
        curl_close($ch);
        echo $result;
    }

    //ios push notification
    function sendIospushNotification($deviceToken, $message) {

        // $payload['aps'] = array('alert' => 'This is the alert text', 'badge' => 1, 'sound' => 'default');
        // $payload = json_encode($payload);
        // $apnsHost = 'gateway.sandbox.push.apple.com';
        // $apnsPort = 60;
        // $apnsCert = 'push.pem';
        // $apnsPass = '1234';

        // $streamContext = stream_context_create();
        // stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
        // stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

        // $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);

        // if (!$apns) {
        //     echo "Error: $errorString ($error)";
        // }

        // // Do this for each
        // $deviceToken = '70cf571cee62317f7487277d7d9b2cd3bb3ed0644af6deb0af2c9095c38c3e91';
        // $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
        // $res = fwrite($apns, $apnsMessage);
        // // End do
        
        // socket_close($apns);
        // fclose($apns);
        // print_r($res);exit;

        // Put your device token here (without spaces):
        $deviceToken = '70cf571cee62317f7487277d7d9b2cd3bb3ed0644af6deb0af2c9095c38c3e91';
        // Put your private key's passphrase here:
        $passphrase = '1234';
        ////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert',  'push.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
                //  'book_id' => $book_id,
                //  'strip_id' => $strip_id,
                //  'strip_url' => $strip_url
        );

        //'sound' => 'default'
        //push.caf
        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        
    //  if (!$result)
    //      echo 'Message not delivered' . PHP_EOL;
    //  else
    //      echo 'Message successfully delivered' . PHP_EOL;
        // Close the connection to the server
        fclose($fp);
        prnt_r($result);exit;
    }

    // public function sendIospushNotification($device_token, $message) {
    //     //ob_start();
    //     $chs = curl_init();
    //     $arrs = array();
    //     array_push($arrs, "X-Parse-Application-Id: 1KaQwpPASQPBjuITLcq4ZE1j9Xrp6SDCYZM1bX0G");
    //     array_push($arrs, "X-Parse-REST-API-Key: 48KE3kB9mMhNY0sFxMIjiBRhBAsbL2Ck9Ptq7Wh4");
    //     array_push($arrs, "Content-Type: application/json");
                    
    //     // f583085122c060d0edd9da45e86e9ba9ebfa8ef4b6252503696cce18376918a7   ----- iPad token.
    //     //$alert = "Your tracking number " .$tnt_track_id. " has been ".$tnt_track_status;
                    
    //     curl_setopt($chs, CURLOPT_HTTPHEADER, $arrs);
    //     //curl_setopt($chs, CURLOPT_HEADER, false);
    //     curl_setopt($chs, CURLOPT_URL, 'https://api.parse.com/1/push');
    //     curl_setopt($chs, CURLOPT_POST, true);
    //     curl_setopt($chs, CURLOPT_POSTFIELDS, '{ "where": {"deviceType":"ios","deviceToken":"'.$device_token.'"},"data": { "alert": "'.$message.'","badge":"Increment" } }');
    //     curl_setopt($chs,CURLOPT_RETURNTRANSFER,1);

    //     $result=curl_exec($chs);

    //     $Arrresult=json_decode($result, true);
    //     print_r($Arrresult);exit;
    //     // if ($Arrresult["failure"] == 1 || $result===FALSE) {
    //     //    // die('Curl failed: ' . curl_error($ch));
    //     //     $insQryParams = array ( 
    //     //                         ":user_id" => $user_id,
    //     //                         ":user_type" => $this->encode($request["password"]),
    //     //                         ":message" => "",
    //     //                         ":message_status" => 0,
    //     //                         ":created_date" => date("Y-m-d h:i:s"),
    //     //                         ":modified_date" => date("Y-m-d h:i:s")
    //     //                     );
    //     //     $insQryResponse = $this->funExeInsertRecord("tbl_pushnotifications", $insQryParams);

    //     // } else if ($Arrresult["success"] == 1) {
    //     //     // $update_query   =   "update tbl_appointments set notification_flag='1' where appointment_id='".$appointment_id."'";
    //     //     $upd_query      =   mysql_query($update_query);
    //     //     $insQryParams = array ( 
    //     //                         ":user_id" => $user_id,
    //     //                         ":user_type" => $this->encode($request["password"]),
    //     //                         ":message" => "",
    //     //                         ":message_status" => 1,
    //     //                         ":created_date" => date("Y-m-d h:i:s"),
    //     //                         ":modified_date" => date("Y-m-d h:i:s");
    //     //                     );
    //     //     $insQryResponse = $this->funExeInsertRecord("tbl_pushnotifications", $insQryParams);
    //     // }
    //     curl_close($chs);
    //     echo $result;
    //     //ob_clean();
    // }

    // // Generate random number
    // private function assignRandValue($num) {

    //     // accepts 1 - 36
    //     switch($num) {
    //         case "1"  : $rand_value = "a"; break;
    //         case "2"  : $rand_value = "b"; break;
    //         case "3"  : $rand_value = "c"; break;
    //         case "4"  : $rand_value = "d"; break;
    //         case "5"  : $rand_value = "e"; break;
    //         case "6"  : $rand_value = "f"; break;
    //         case "7"  : $rand_value = "g"; break;
    //         case "8"  : $rand_value = "h"; break;
    //         case "9"  : $rand_value = "i"; break;
    //         case "10" : $rand_value = "j"; break;
    //         case "11" : $rand_value = "k"; break;
    //         case "12" : $rand_value = "l"; break;
    //         case "13" : $rand_value = "m"; break;
    //         case "14" : $rand_value = "n"; break;
    //         case "15" : $rand_value = "o"; break;
    //         case "16" : $rand_value = "p"; break;
    //         case "17" : $rand_value = "q"; break;
    //         case "18" : $rand_value = "r"; break;
    //         case "19" : $rand_value = "s"; break;
    //         case "20" : $rand_value = "t"; break;
    //         case "21" : $rand_value = "u"; break;
    //         case "22" : $rand_value = "v"; break;
    //         case "23" : $rand_value = "w"; break;
    //         case "24" : $rand_value = "x"; break;
    //         case "25" : $rand_value = "y"; break;
    //         case "26" : $rand_value = "z"; break;
    //         case "27" : $rand_value = "0"; break;
    //         case "28" : $rand_value = "1"; break;
    //         case "29" : $rand_value = "2"; break;
    //         case "30" : $rand_value = "3"; break;
    //         case "31" : $rand_value = "4"; break;
    //         case "32" : $rand_value = "5"; break;
    //         case "33" : $rand_value = "6"; break;
    //         case "34" : $rand_value = "7"; break;
    //         case "35" : $rand_value = "8"; break;
    //         case "36" : $rand_value = "9"; break;
    //     }
    //     return $rand_value;
    // }

    // public function generateRandAlphanumeric($length) {
    //     if ($length>0) {
    //         $rand_id="";
    //         for ($i=1; $i<=$length; $i++) {
    //             mt_srand((double)microtime() * 1000000);
    //             $num = mt_rand(1,36);
    //             $rand_id .= $this->assignRandValue($num);
    //         }
    //     }
    //     return $rand_id;
    // }

    // public function generateRandNumbers($length) {
    //     if ($length>0) {
    //         $rand_id="";
    //         for($i=1; $i<=$length; $i++) {
    //             mt_srand((double)microtime() * 1000000);
    //             $num = mt_rand(27,36);
    //             $rand_id .= $this->assignRandValue($num);
    //         }
    //     }
    //     return $rand_id;
    // }

    // public function generateRandLetters($length) {
    //     if ($length>0) {
    //         $rand_id="";
    //         for($i=1; $i<=$length; $i++) {
    //             mt_srand((double)microtime() * 1000000);
    //             $num = mt_rand(1,26);
    //             $rand_id .= $this->assignRandValue($num);
    //         }
    //     }
    //     return $rand_id;
    // }
}
?>
