<?php
header('Content-Type: application/json');

include_once('../includes/configure.php');
include_once('Common.php');
include_once('api.php');

// Initialize FoodAppApi
$foodAppApi = new FoodAppApi($dbconn);

$apiType = isset($_GET["type"]) ? strtolower(trim($_GET["type"])) : "";
$reqMethod = $_SERVER['REQUEST_METHOD'];
$errorReqMethdMsg = array("error" => "Check request method!!");
$apiRouteConfig = json_decode($foodAppApi->apiRouteConfig(), true);
// Check request method and configure API method's are same
// if (!empty($apiType) && array_key_exists($apiType, $apiRouteConfig)) {
if (!empty($apiType)) {
    switch (true) {
    	case ($apiType == "register" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->register($_REQUEST);
            break;
        case ($apiType == "login" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->login($_REQUEST);
            break;
        case ($apiType == "forgetpassword" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->forgetPassword($_REQUEST);
            break;
        case ($apiType == "myorder" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->myOrder($_REQUEST);
            break; 
        case ($apiType == "getvendorlist" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getVendorList($_REQUEST);
            break;
        case ($apiType == "orderhistory" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->orderHistory($_REQUEST);
            break;
        case (($apiType == "createorder" && $reqMethod == $apiRouteConfig[$apiType]) || ($apiType == "updateorderstatus" && $reqMethod == $apiRouteConfig[$apiType])):
            echo $foodAppApi->createUpdateOrder($_REQUEST);
            break;
        case ($apiType == "updateorderratings" && $reqMethod == $apiRouteConfig[$apiType]):
	        echo $foodAppApi->updateOrderRatings($_REQUEST);
	        break;
        case ($apiType == "getcustomerdetail" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getCustomerDetail($_REQUEST);
            break;
        case ($apiType == "createupdatecategoryitem" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->createUpdateCategoryItem($_REQUEST);
            break;
        case ($apiType == "deletecategoryitem" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->deleteCategoryItem($_REQUEST);
            break;
        case ($apiType == "createpackage" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->createPackage($_REQUEST);
            break;
        case ($apiType == "deletepackage" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->deletePackage($_REQUEST);
            break;
        case ($apiType == "updatepackage" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->updatePackage($_REQUEST);
            break;            
        case ($apiType == "getreport" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getReport($_REQUEST);
            break;
        case ($apiType == "getinvoice" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getInvoice($_REQUEST);
            break;
        case ($apiType == "getinvoiceorders" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getInvoiceOrders($_REQUEST);
            break;            
        case ($apiType == "updateinvoice" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->updateInvoice($_REQUEST);
            break;
        case ($apiType == "getcategorylist" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getCategoryList($_REQUEST);
            break;
        case ($apiType == "updateprofile" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->updateProfile($_REQUEST);
            break;
        case ($apiType == "testpushnotification" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->testPushNotification($_REQUEST);
            break;
        case ($apiType == "updatepassword" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->updatePassword($_REQUEST);
            break;
        case ($apiType == "getpackagelist" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->getPackageList($_REQUEST);
            break;
        case ($apiType == "testnotify" && $reqMethod == $apiRouteConfig[$apiType]):
            echo $foodAppApi->testNotify($_REQUEST);
            break;
        default:
            echo $foodAppApi->jsonResponse($errorReqMethdMsg);
    }
} else {
    echo $foodAppApi->jsonResponse(array("error" => "Api doesn't exist!"));
    exit;
}
