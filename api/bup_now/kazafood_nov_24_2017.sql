-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 166.62.8.7
-- Generation Time: Nov 24, 2017 at 06:13 AM
-- Server version: 5.5.43
-- PHP Version: 5.1.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kazafood`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_order` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=292 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` VALUES(1, 1, 6, 'Main dishes', 'Main dishes for Lunch', 'uploads/category/cate_main_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category` VALUES(2, 2, 6, 'Side dishes', 'Side dishes for Lunch', 'uploads/category/cate_side_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category` VALUES(3, 3, 6, 'Vegetarian Only/Halal Only', 'Best Soups after the Lunch', 'uploads/category/cate_soup.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category` VALUES(4, 4, 7, 'Main dishes', 'Main dishes for the Lunch', 'uploads/category/cate_main_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category` VALUES(5, 5, 7, 'Side Dishes', 'Ice creams', 'uploads/category/cate_ice.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category` VALUES(6, 6, 7, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', 'uploads/category/cate_ice.png', '2017-10-24 00:00:00', '2017-10-24 00:00:00');
INSERT INTO `tbl_category` VALUES(7, 1, 17, 'Main Dishes', 'Main Dishes', '', '2017-10-25 02:44:21', '2017-10-25 02:44:21');
INSERT INTO `tbl_category` VALUES(8, 2, 17, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-25 02:44:21', '2017-10-25 02:44:21');
INSERT INTO `tbl_category` VALUES(9, 3, 17, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-25 02:44:21', '2017-10-25 02:44:21');
INSERT INTO `tbl_category` VALUES(10, 1, 18, 'Main Dishes', 'Main Dishes', '', '2017-10-25 02:58:36', '2017-10-25 02:58:36');
INSERT INTO `tbl_category` VALUES(11, 2, 18, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-25 02:58:36', '2017-10-25 02:58:36');
INSERT INTO `tbl_category` VALUES(12, 3, 18, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-25 02:58:36', '2017-10-25 02:58:36');
INSERT INTO `tbl_category` VALUES(13, 1, 19, 'Main Dishes', 'Main Dishes', '', '2017-10-26 01:34:43', '2017-10-26 01:34:43');
INSERT INTO `tbl_category` VALUES(14, 2, 19, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-26 01:34:43', '2017-10-26 01:34:43');
INSERT INTO `tbl_category` VALUES(15, 3, 19, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-26 01:34:43', '2017-10-26 01:34:43');
INSERT INTO `tbl_category` VALUES(16, 1, 20, 'Main Dishes', 'Main Dishes', '', '2017-10-26 02:24:39', '2017-10-26 02:24:39');
INSERT INTO `tbl_category` VALUES(17, 2, 20, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-26 02:24:39', '2017-10-26 02:24:39');
INSERT INTO `tbl_category` VALUES(18, 3, 20, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-26 02:24:39', '2017-10-26 02:24:39');
INSERT INTO `tbl_category` VALUES(19, 1, 21, 'Main Dishes', 'Main Dishes', '', '2017-10-28 03:30:40', '2017-10-28 03:30:40');
INSERT INTO `tbl_category` VALUES(20, 2, 21, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-28 03:30:40', '2017-10-28 03:30:40');
INSERT INTO `tbl_category` VALUES(21, 3, 21, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-28 03:30:40', '2017-10-28 03:30:40');
INSERT INTO `tbl_category` VALUES(22, 1, 22, 'Main Dishes', 'Main Dishes', '', '2017-10-28 03:33:52', '2017-10-28 03:33:52');
INSERT INTO `tbl_category` VALUES(23, 2, 22, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-28 03:33:52', '2017-10-28 03:33:52');
INSERT INTO `tbl_category` VALUES(24, 3, 22, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-28 03:33:52', '2017-10-28 03:33:52');
INSERT INTO `tbl_category` VALUES(25, 1, 23, 'Main Dishes', 'Main Dishes', '', '2017-10-28 03:35:33', '2017-10-28 03:35:33');
INSERT INTO `tbl_category` VALUES(26, 2, 23, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-28 03:35:33', '2017-10-28 03:35:33');
INSERT INTO `tbl_category` VALUES(27, 3, 23, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-28 03:35:33', '2017-10-28 03:35:33');
INSERT INTO `tbl_category` VALUES(28, 1, 24, 'Main Dishes', 'Main Dishes', '', '2017-10-28 04:26:50', '2017-10-28 04:26:50');
INSERT INTO `tbl_category` VALUES(29, 2, 24, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-28 04:26:50', '2017-10-28 04:26:50');
INSERT INTO `tbl_category` VALUES(30, 3, 24, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-28 04:26:50', '2017-10-28 04:26:50');
INSERT INTO `tbl_category` VALUES(31, 1, 25, 'Main Dishes', 'Main Dishes', '', '2017-10-30 02:26:45', '2017-10-30 02:26:45');
INSERT INTO `tbl_category` VALUES(32, 2, 25, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-30 02:26:45', '2017-10-30 02:26:45');
INSERT INTO `tbl_category` VALUES(33, 3, 25, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-30 02:26:45', '2017-10-30 02:26:45');
INSERT INTO `tbl_category` VALUES(34, 1, 26, 'Main Dishes', 'Main Dishes', '', '2017-10-30 05:19:41', '2017-10-30 05:19:41');
INSERT INTO `tbl_category` VALUES(35, 2, 26, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-30 05:19:41', '2017-10-30 05:19:41');
INSERT INTO `tbl_category` VALUES(36, 3, 26, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-30 05:19:41', '2017-10-30 05:19:41');
INSERT INTO `tbl_category` VALUES(37, 1, 27, 'Main Dishes', 'Main Dishes', '', '2017-10-31 04:00:45', '2017-10-31 04:00:45');
INSERT INTO `tbl_category` VALUES(38, 2, 27, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-31 04:00:45', '2017-10-31 04:00:45');
INSERT INTO `tbl_category` VALUES(39, 3, 27, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-31 04:00:45', '2017-10-31 04:00:45');
INSERT INTO `tbl_category` VALUES(40, 1, 28, 'Main Dishes', 'Main Dishes', '', '2017-10-31 08:43:13', '2017-10-31 08:43:13');
INSERT INTO `tbl_category` VALUES(41, 2, 28, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-31 08:43:13', '2017-10-31 08:43:13');
INSERT INTO `tbl_category` VALUES(42, 3, 28, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-31 08:43:13', '2017-10-31 08:43:13');
INSERT INTO `tbl_category` VALUES(43, 1, 29, 'Main Dishes', 'Main Dishes', '', '2017-10-31 08:49:23', '2017-10-31 08:49:23');
INSERT INTO `tbl_category` VALUES(44, 2, 29, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-10-31 08:49:23', '2017-10-31 08:49:23');
INSERT INTO `tbl_category` VALUES(45, 3, 29, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-10-31 08:49:23', '2017-10-31 08:49:23');
INSERT INTO `tbl_category` VALUES(46, 1, 30, 'Main Dishes', 'Main Dishes', '', '2017-11-01 02:46:06', '2017-11-01 02:46:06');
INSERT INTO `tbl_category` VALUES(47, 2, 30, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-01 02:46:06', '2017-11-01 02:46:06');
INSERT INTO `tbl_category` VALUES(48, 3, 30, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-01 02:46:06', '2017-11-01 02:46:06');
INSERT INTO `tbl_category` VALUES(49, 1, 31, 'Main Dishes', 'Main Dishes', '', '2017-11-01 11:50:28', '2017-11-01 11:50:28');
INSERT INTO `tbl_category` VALUES(50, 2, 31, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-01 11:50:28', '2017-11-01 11:50:28');
INSERT INTO `tbl_category` VALUES(51, 3, 31, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-01 11:50:28', '2017-11-01 11:50:28');
INSERT INTO `tbl_category` VALUES(52, 1, 32, 'Main Dishes', 'Main Dishes', '', '2017-11-02 03:13:52', '2017-11-02 03:13:52');
INSERT INTO `tbl_category` VALUES(53, 2, 32, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-02 03:13:52', '2017-11-02 03:13:52');
INSERT INTO `tbl_category` VALUES(54, 3, 32, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-02 03:13:52', '2017-11-02 03:13:52');
INSERT INTO `tbl_category` VALUES(55, 1, 33, 'Main Dishes', 'Main Dishes', '', '2017-11-04 12:32:36', '2017-11-04 12:32:36');
INSERT INTO `tbl_category` VALUES(56, 2, 33, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 12:32:36', '2017-11-04 12:32:36');
INSERT INTO `tbl_category` VALUES(57, 3, 33, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 12:32:36', '2017-11-04 12:32:36');
INSERT INTO `tbl_category` VALUES(58, 1, 34, 'Main Dishes', 'Main Dishes', '', '2017-11-04 12:58:24', '2017-11-04 12:58:24');
INSERT INTO `tbl_category` VALUES(59, 2, 34, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 12:58:24', '2017-11-04 12:58:24');
INSERT INTO `tbl_category` VALUES(60, 3, 34, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 12:58:24', '2017-11-04 12:58:24');
INSERT INTO `tbl_category` VALUES(61, 1, 35, 'Main Dishes', 'Main Dishes', '', '2017-11-04 01:00:29', '2017-11-04 01:00:29');
INSERT INTO `tbl_category` VALUES(62, 2, 35, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 01:00:29', '2017-11-04 01:00:29');
INSERT INTO `tbl_category` VALUES(63, 3, 35, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 01:00:29', '2017-11-04 01:00:29');
INSERT INTO `tbl_category` VALUES(64, 1, 36, 'Main Dishes', 'Main Dishes', '', '2017-11-04 08:53:21', '2017-11-04 08:53:21');
INSERT INTO `tbl_category` VALUES(65, 2, 36, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 08:53:21', '2017-11-04 08:53:21');
INSERT INTO `tbl_category` VALUES(66, 3, 36, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 08:53:21', '2017-11-04 08:53:21');
INSERT INTO `tbl_category` VALUES(67, 1, 37, 'Main Dishes', 'Main Dishes', '', '2017-11-04 08:55:52', '2017-11-04 08:55:52');
INSERT INTO `tbl_category` VALUES(68, 2, 37, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 08:55:52', '2017-11-04 08:55:52');
INSERT INTO `tbl_category` VALUES(69, 3, 37, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 08:55:52', '2017-11-04 08:55:52');
INSERT INTO `tbl_category` VALUES(70, 1, 38, 'Main Dishes', 'Main Dishes', '', '2017-11-04 09:17:01', '2017-11-04 09:17:01');
INSERT INTO `tbl_category` VALUES(71, 2, 38, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 09:17:01', '2017-11-04 09:17:01');
INSERT INTO `tbl_category` VALUES(72, 3, 38, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 09:17:01', '2017-11-04 09:17:01');
INSERT INTO `tbl_category` VALUES(73, 1, 39, 'Main Dishes', 'Main Dishes', '', '2017-11-04 09:24:12', '2017-11-04 09:24:12');
INSERT INTO `tbl_category` VALUES(74, 2, 39, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-04 09:24:12', '2017-11-04 09:24:12');
INSERT INTO `tbl_category` VALUES(75, 3, 39, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-04 09:24:12', '2017-11-04 09:24:12');
INSERT INTO `tbl_category` VALUES(76, 1, 40, 'Main Dishes', 'Main Dishes', '', '2017-11-07 05:37:58', '2017-11-07 05:37:58');
INSERT INTO `tbl_category` VALUES(77, 2, 40, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-07 05:37:58', '2017-11-07 05:37:58');
INSERT INTO `tbl_category` VALUES(78, 3, 40, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-07 05:37:58', '2017-11-07 05:37:58');
INSERT INTO `tbl_category` VALUES(79, 1, 41, 'Main Dishes', 'Main Dishes', '', '2017-11-08 12:44:19', '2017-11-08 12:44:19');
INSERT INTO `tbl_category` VALUES(80, 2, 41, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 12:44:19', '2017-11-08 12:44:19');
INSERT INTO `tbl_category` VALUES(81, 3, 41, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 12:44:19', '2017-11-08 12:44:19');
INSERT INTO `tbl_category` VALUES(82, 1, 42, 'Main Dishes', 'Main Dishes', '', '2017-11-08 01:42:40', '2017-11-08 01:42:40');
INSERT INTO `tbl_category` VALUES(83, 2, 42, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 01:42:40', '2017-11-08 01:42:40');
INSERT INTO `tbl_category` VALUES(84, 3, 42, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 01:42:40', '2017-11-08 01:42:40');
INSERT INTO `tbl_category` VALUES(85, 1, 43, 'Main Dishes', 'Main Dishes', '', '2017-11-08 02:03:49', '2017-11-08 02:03:49');
INSERT INTO `tbl_category` VALUES(86, 2, 43, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 02:03:49', '2017-11-08 02:03:49');
INSERT INTO `tbl_category` VALUES(87, 3, 43, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 02:03:49', '2017-11-08 02:03:49');
INSERT INTO `tbl_category` VALUES(88, 1, 44, 'Main Dishes', 'Main Dishes', '', '2017-11-08 04:37:00', '2017-11-08 04:37:00');
INSERT INTO `tbl_category` VALUES(89, 2, 44, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 04:37:00', '2017-11-08 04:37:00');
INSERT INTO `tbl_category` VALUES(90, 3, 44, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 04:37:00', '2017-11-08 04:37:00');
INSERT INTO `tbl_category` VALUES(91, 1, 45, 'Main Dishes', 'Main Dishes', '', '2017-11-08 06:17:59', '2017-11-08 06:17:59');
INSERT INTO `tbl_category` VALUES(92, 2, 45, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 06:17:59', '2017-11-08 06:17:59');
INSERT INTO `tbl_category` VALUES(93, 3, 45, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 06:17:59', '2017-11-08 06:17:59');
INSERT INTO `tbl_category` VALUES(94, 1, 46, 'Main Dishes', 'Main Dishes', '', '2017-11-08 11:04:53', '2017-11-08 11:04:53');
INSERT INTO `tbl_category` VALUES(95, 2, 46, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-08 11:04:53', '2017-11-08 11:04:53');
INSERT INTO `tbl_category` VALUES(96, 3, 46, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-08 11:04:53', '2017-11-08 11:04:53');
INSERT INTO `tbl_category` VALUES(97, 1, 47, 'Main Dishes', 'Main Dishes', '', '2017-11-09 01:43:51', '2017-11-09 01:43:51');
INSERT INTO `tbl_category` VALUES(98, 2, 47, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-09 01:43:51', '2017-11-09 01:43:51');
INSERT INTO `tbl_category` VALUES(99, 3, 47, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-09 01:43:51', '2017-11-09 01:43:51');
INSERT INTO `tbl_category` VALUES(100, 1, 48, 'Main Dishes', 'Main Dishes', '', '2017-11-09 11:07:39', '2017-11-09 11:07:39');
INSERT INTO `tbl_category` VALUES(101, 2, 48, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-09 11:07:39', '2017-11-09 11:07:39');
INSERT INTO `tbl_category` VALUES(102, 3, 48, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-09 11:07:39', '2017-11-09 11:07:39');
INSERT INTO `tbl_category` VALUES(103, 1, 49, 'Main Dishes', 'Main Dishes', '', '2017-11-09 11:08:31', '2017-11-09 11:08:31');
INSERT INTO `tbl_category` VALUES(104, 2, 49, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-09 11:08:31', '2017-11-09 11:08:31');
INSERT INTO `tbl_category` VALUES(105, 3, 49, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-09 11:08:31', '2017-11-09 11:08:31');
INSERT INTO `tbl_category` VALUES(106, 1, 50, 'Main Dishes', 'Main Dishes', '', '2017-11-10 12:22:48', '2017-11-10 12:22:48');
INSERT INTO `tbl_category` VALUES(107, 2, 50, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-10 12:22:48', '2017-11-10 12:22:48');
INSERT INTO `tbl_category` VALUES(108, 3, 50, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-10 12:22:48', '2017-11-10 12:22:48');
INSERT INTO `tbl_category` VALUES(109, 1, 51, 'Main Dishes', 'Main Dishes', '', '2017-11-10 12:24:03', '2017-11-10 12:24:03');
INSERT INTO `tbl_category` VALUES(110, 2, 51, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-10 12:24:03', '2017-11-10 12:24:03');
INSERT INTO `tbl_category` VALUES(111, 3, 51, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-10 12:24:03', '2017-11-10 12:24:03');
INSERT INTO `tbl_category` VALUES(112, 1, 52, 'Main Dishes', 'Main Dishes', '', '2017-11-10 12:24:19', '2017-11-10 12:24:19');
INSERT INTO `tbl_category` VALUES(113, 2, 52, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-10 12:24:19', '2017-11-10 12:24:19');
INSERT INTO `tbl_category` VALUES(114, 3, 52, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-10 12:24:19', '2017-11-10 12:24:19');
INSERT INTO `tbl_category` VALUES(115, 1, 53, 'Main Dishes', 'Main Dishes', '', '2017-11-10 12:24:39', '2017-11-10 12:24:39');
INSERT INTO `tbl_category` VALUES(116, 2, 53, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-10 12:24:39', '2017-11-10 12:24:39');
INSERT INTO `tbl_category` VALUES(117, 3, 53, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-10 12:24:39', '2017-11-10 12:24:39');
INSERT INTO `tbl_category` VALUES(118, 1, 54, 'Main Dishes', 'Main Dishes', '', '2017-11-10 04:26:44', '2017-11-10 04:26:44');
INSERT INTO `tbl_category` VALUES(119, 2, 54, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-10 04:26:44', '2017-11-10 04:26:44');
INSERT INTO `tbl_category` VALUES(120, 3, 54, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-10 04:26:44', '2017-11-10 04:26:44');
INSERT INTO `tbl_category` VALUES(121, 1, 55, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:22:39', '2017-11-11 06:22:39');
INSERT INTO `tbl_category` VALUES(122, 2, 55, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:22:39', '2017-11-11 06:22:39');
INSERT INTO `tbl_category` VALUES(123, 3, 55, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:22:39', '2017-11-11 06:22:39');
INSERT INTO `tbl_category` VALUES(124, 1, 56, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:23:41', '2017-11-11 06:23:41');
INSERT INTO `tbl_category` VALUES(125, 2, 56, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:23:41', '2017-11-11 06:23:41');
INSERT INTO `tbl_category` VALUES(126, 3, 56, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:23:41', '2017-11-11 06:23:41');
INSERT INTO `tbl_category` VALUES(127, 1, 57, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:28:06', '2017-11-11 06:28:06');
INSERT INTO `tbl_category` VALUES(128, 2, 57, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:28:06', '2017-11-11 06:28:06');
INSERT INTO `tbl_category` VALUES(129, 3, 57, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:28:06', '2017-11-11 06:28:06');
INSERT INTO `tbl_category` VALUES(130, 1, 58, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:37:23', '2017-11-11 06:37:23');
INSERT INTO `tbl_category` VALUES(131, 2, 58, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:37:23', '2017-11-11 06:37:23');
INSERT INTO `tbl_category` VALUES(132, 3, 58, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:37:23', '2017-11-11 06:37:23');
INSERT INTO `tbl_category` VALUES(133, 1, 59, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:39:10', '2017-11-11 06:39:10');
INSERT INTO `tbl_category` VALUES(134, 2, 59, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:39:10', '2017-11-11 06:39:10');
INSERT INTO `tbl_category` VALUES(135, 3, 59, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:39:10', '2017-11-11 06:39:10');
INSERT INTO `tbl_category` VALUES(136, 1, 60, 'Main Dishes', 'Main Dishes', '', '2017-11-11 06:48:52', '2017-11-11 06:48:52');
INSERT INTO `tbl_category` VALUES(137, 2, 60, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-11 06:48:52', '2017-11-11 06:48:52');
INSERT INTO `tbl_category` VALUES(138, 3, 60, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-11 06:48:52', '2017-11-11 06:48:52');
INSERT INTO `tbl_category` VALUES(139, 1, 61, 'Main Dishes', 'Main Dishes', '', '2017-11-12 10:23:25', '2017-11-12 10:23:25');
INSERT INTO `tbl_category` VALUES(140, 2, 61, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 10:23:25', '2017-11-12 10:23:25');
INSERT INTO `tbl_category` VALUES(141, 3, 61, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 10:23:25', '2017-11-12 10:23:25');
INSERT INTO `tbl_category` VALUES(142, 1, 62, 'Main Dishes', 'Main Dishes', '', '2017-11-12 10:25:18', '2017-11-12 10:25:18');
INSERT INTO `tbl_category` VALUES(143, 2, 62, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 10:25:18', '2017-11-12 10:25:18');
INSERT INTO `tbl_category` VALUES(144, 3, 62, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 10:25:18', '2017-11-12 10:25:18');
INSERT INTO `tbl_category` VALUES(145, 1, 63, 'Main Dishes', 'Main Dishes', '', '2017-11-12 10:27:10', '2017-11-12 10:27:10');
INSERT INTO `tbl_category` VALUES(146, 2, 63, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 10:27:10', '2017-11-12 10:27:10');
INSERT INTO `tbl_category` VALUES(147, 3, 63, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 10:27:10', '2017-11-12 10:27:10');
INSERT INTO `tbl_category` VALUES(148, 1, 64, 'Main Dishes', 'Main Dishes', '', '2017-11-12 11:33:47', '2017-11-12 11:33:47');
INSERT INTO `tbl_category` VALUES(149, 2, 64, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 11:33:47', '2017-11-12 11:33:47');
INSERT INTO `tbl_category` VALUES(150, 3, 64, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 11:33:47', '2017-11-12 11:33:47');
INSERT INTO `tbl_category` VALUES(151, 1, 65, 'Main Dishes', 'Main Dishes', '', '2017-11-12 11:54:43', '2017-11-12 11:54:43');
INSERT INTO `tbl_category` VALUES(152, 2, 65, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 11:54:43', '2017-11-12 11:54:43');
INSERT INTO `tbl_category` VALUES(153, 3, 65, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 11:54:43', '2017-11-12 11:54:43');
INSERT INTO `tbl_category` VALUES(154, 1, 66, 'Main Dishes', 'Main Dishes', '', '2017-11-12 11:58:10', '2017-11-12 11:58:10');
INSERT INTO `tbl_category` VALUES(155, 2, 66, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-12 11:58:10', '2017-11-12 11:58:10');
INSERT INTO `tbl_category` VALUES(156, 3, 66, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-12 11:58:10', '2017-11-12 11:58:10');
INSERT INTO `tbl_category` VALUES(157, 1, 67, 'Main Dishes', 'Main Dishes', '', '2017-11-13 04:45:25', '2017-11-13 04:45:25');
INSERT INTO `tbl_category` VALUES(158, 2, 67, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-13 04:45:25', '2017-11-13 04:45:25');
INSERT INTO `tbl_category` VALUES(159, 3, 67, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-13 04:45:25', '2017-11-13 04:45:25');
INSERT INTO `tbl_category` VALUES(160, 1, 68, 'Main Dishes', 'Main Dishes', '', '2017-11-13 06:46:41', '2017-11-13 06:46:41');
INSERT INTO `tbl_category` VALUES(161, 2, 68, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-13 06:46:41', '2017-11-13 06:46:41');
INSERT INTO `tbl_category` VALUES(162, 3, 68, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-13 06:46:41', '2017-11-13 06:46:41');
INSERT INTO `tbl_category` VALUES(163, 1, 69, 'Main Dishes', 'Main Dishes', '', '2017-11-13 11:36:59', '2017-11-13 11:36:59');
INSERT INTO `tbl_category` VALUES(164, 2, 69, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-13 11:36:59', '2017-11-13 11:36:59');
INSERT INTO `tbl_category` VALUES(165, 3, 69, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-13 11:36:59', '2017-11-13 11:36:59');
INSERT INTO `tbl_category` VALUES(166, 1, 70, 'Main Dishes', 'Main Dishes', '', '2017-11-14 07:31:51', '2017-11-14 07:31:51');
INSERT INTO `tbl_category` VALUES(167, 2, 70, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-14 07:31:51', '2017-11-14 07:31:51');
INSERT INTO `tbl_category` VALUES(168, 3, 70, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-14 07:31:51', '2017-11-14 07:31:51');
INSERT INTO `tbl_category` VALUES(169, 1, 71, 'Main Dishes', 'Main Dishes', '', '2017-11-14 11:38:27', '2017-11-14 11:38:27');
INSERT INTO `tbl_category` VALUES(170, 2, 71, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-14 11:38:27', '2017-11-14 11:38:27');
INSERT INTO `tbl_category` VALUES(171, 3, 71, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-14 11:38:27', '2017-11-14 11:38:27');
INSERT INTO `tbl_category` VALUES(172, 1, 72, 'Main Dishes', 'Main Dishes', '', '2017-11-14 11:20:17', '2017-11-14 11:20:17');
INSERT INTO `tbl_category` VALUES(173, 2, 72, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-14 11:20:17', '2017-11-14 11:20:17');
INSERT INTO `tbl_category` VALUES(174, 3, 72, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-14 11:20:17', '2017-11-14 11:20:17');
INSERT INTO `tbl_category` VALUES(175, 1, 73, 'Main Dishes', 'Main Dishes', '', '2017-11-14 11:25:57', '2017-11-14 11:25:57');
INSERT INTO `tbl_category` VALUES(176, 2, 73, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-14 11:25:57', '2017-11-14 11:25:57');
INSERT INTO `tbl_category` VALUES(177, 3, 73, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-14 11:25:57', '2017-11-14 11:25:57');
INSERT INTO `tbl_category` VALUES(178, 1, 74, 'Main Dishes', 'Main Dishes', '', '2017-11-14 11:49:16', '2017-11-14 11:49:16');
INSERT INTO `tbl_category` VALUES(179, 2, 74, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-14 11:49:16', '2017-11-14 11:49:16');
INSERT INTO `tbl_category` VALUES(180, 3, 74, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-14 11:49:16', '2017-11-14 11:49:16');
INSERT INTO `tbl_category` VALUES(181, 1, 75, 'Main Dishes', 'Main Dishes', '', '2017-11-15 12:05:24', '2017-11-15 12:05:24');
INSERT INTO `tbl_category` VALUES(182, 2, 75, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-15 12:05:24', '2017-11-15 12:05:24');
INSERT INTO `tbl_category` VALUES(183, 3, 75, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-15 12:05:24', '2017-11-15 12:05:24');
INSERT INTO `tbl_category` VALUES(190, 1, 78, 'Main Dishes', 'Main Dishes', '', '2017-11-15 06:09:18', '2017-11-15 06:09:18');
INSERT INTO `tbl_category` VALUES(191, 2, 78, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-15 06:09:18', '2017-11-15 06:09:18');
INSERT INTO `tbl_category` VALUES(192, 3, 78, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-15 06:09:18', '2017-11-15 06:09:18');
INSERT INTO `tbl_category` VALUES(193, 1, 79, 'Main Dishes', 'Main Dishes', '', '2017-11-16 01:17:44', '2017-11-16 01:17:44');
INSERT INTO `tbl_category` VALUES(194, 2, 79, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-16 01:17:44', '2017-11-16 01:17:44');
INSERT INTO `tbl_category` VALUES(195, 3, 79, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-16 01:17:44', '2017-11-16 01:17:44');
INSERT INTO `tbl_category` VALUES(196, 1, 80, 'Main Dishes', 'Main Dishes', '', '2017-11-16 02:35:28', '2017-11-16 02:35:28');
INSERT INTO `tbl_category` VALUES(197, 2, 80, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-16 02:35:28', '2017-11-16 02:35:28');
INSERT INTO `tbl_category` VALUES(198, 3, 80, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-16 02:35:28', '2017-11-16 02:35:28');
INSERT INTO `tbl_category` VALUES(199, 1, 81, 'Main Dishes', 'Main Dishes', '', '2017-11-16 02:45:28', '2017-11-16 02:45:28');
INSERT INTO `tbl_category` VALUES(200, 2, 81, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-16 02:45:28', '2017-11-16 02:45:28');
INSERT INTO `tbl_category` VALUES(201, 3, 81, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-16 02:45:28', '2017-11-16 02:45:28');
INSERT INTO `tbl_category` VALUES(202, 1, 82, 'Main Dishes', 'Main Dishes', '', '2017-11-17 12:36:20', '2017-11-17 12:36:20');
INSERT INTO `tbl_category` VALUES(203, 2, 82, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-17 12:36:20', '2017-11-17 12:36:20');
INSERT INTO `tbl_category` VALUES(204, 3, 82, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-17 12:36:20', '2017-11-17 12:36:20');
INSERT INTO `tbl_category` VALUES(205, 1, 83, 'Main Dishes', 'Main Dishes', '', '2017-11-19 10:57:05', '2017-11-19 10:57:05');
INSERT INTO `tbl_category` VALUES(206, 2, 83, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-19 10:57:05', '2017-11-19 10:57:05');
INSERT INTO `tbl_category` VALUES(207, 3, 83, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-19 10:57:05', '2017-11-19 10:57:05');
INSERT INTO `tbl_category` VALUES(208, 1, 84, 'Main Dishes', 'Main Dishes', '', '2017-11-19 11:01:11', '2017-11-19 11:01:11');
INSERT INTO `tbl_category` VALUES(209, 2, 84, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-19 11:01:11', '2017-11-19 11:01:11');
INSERT INTO `tbl_category` VALUES(210, 3, 84, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-19 11:01:11', '2017-11-19 11:01:11');
INSERT INTO `tbl_category` VALUES(211, 1, 85, 'Main Dishes', 'Main Dishes', '', '2017-11-19 11:07:00', '2017-11-19 11:07:00');
INSERT INTO `tbl_category` VALUES(212, 2, 85, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-19 11:07:00', '2017-11-19 11:07:00');
INSERT INTO `tbl_category` VALUES(213, 3, 85, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-19 11:07:00', '2017-11-19 11:07:00');
INSERT INTO `tbl_category` VALUES(214, 1, 86, 'Main Dishes', 'Main Dishes', '', '2017-11-20 12:13:03', '2017-11-20 12:13:03');
INSERT INTO `tbl_category` VALUES(215, 2, 86, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-20 12:13:03', '2017-11-20 12:13:03');
INSERT INTO `tbl_category` VALUES(216, 3, 86, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-20 12:13:03', '2017-11-20 12:13:03');
INSERT INTO `tbl_category` VALUES(217, 1, 87, 'Main Dishes', 'Main Dishes', '', '2017-11-20 12:14:41', '2017-11-20 12:14:41');
INSERT INTO `tbl_category` VALUES(218, 2, 87, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-20 12:14:41', '2017-11-20 12:14:41');
INSERT INTO `tbl_category` VALUES(219, 3, 87, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-20 12:14:41', '2017-11-20 12:14:41');
INSERT INTO `tbl_category` VALUES(220, 1, 88, 'Main Dishes', 'Main Dishes', '', '2017-11-20 04:56:43', '2017-11-20 04:56:43');
INSERT INTO `tbl_category` VALUES(221, 2, 88, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-20 04:56:43', '2017-11-20 04:56:43');
INSERT INTO `tbl_category` VALUES(222, 3, 88, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-20 04:56:43', '2017-11-20 04:56:43');
INSERT INTO `tbl_category` VALUES(223, 1, 89, 'Main Dishes', 'Main Dishes', '', '2017-11-21 03:15:13', '2017-11-21 03:15:13');
INSERT INTO `tbl_category` VALUES(224, 2, 89, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 03:15:13', '2017-11-21 03:15:13');
INSERT INTO `tbl_category` VALUES(225, 3, 89, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 03:15:13', '2017-11-21 03:15:13');
INSERT INTO `tbl_category` VALUES(226, 1, 90, 'Main Dishes', 'Main Dishes', '', '2017-11-21 03:22:17', '2017-11-21 03:22:17');
INSERT INTO `tbl_category` VALUES(227, 2, 90, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 03:22:17', '2017-11-21 03:22:17');
INSERT INTO `tbl_category` VALUES(228, 3, 90, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 03:22:17', '2017-11-21 03:22:17');
INSERT INTO `tbl_category` VALUES(229, 1, 91, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:03:52', '2017-11-21 05:03:52');
INSERT INTO `tbl_category` VALUES(230, 2, 91, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:03:52', '2017-11-21 05:03:52');
INSERT INTO `tbl_category` VALUES(231, 3, 91, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:03:52', '2017-11-21 05:03:52');
INSERT INTO `tbl_category` VALUES(232, 1, 92, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:32:30', '2017-11-21 05:32:30');
INSERT INTO `tbl_category` VALUES(233, 2, 92, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:32:30', '2017-11-21 05:32:30');
INSERT INTO `tbl_category` VALUES(234, 3, 92, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:32:30', '2017-11-21 05:32:30');
INSERT INTO `tbl_category` VALUES(235, 1, 93, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:41:10', '2017-11-21 05:41:10');
INSERT INTO `tbl_category` VALUES(236, 2, 93, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:41:10', '2017-11-21 05:41:10');
INSERT INTO `tbl_category` VALUES(237, 3, 93, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:41:10', '2017-11-21 05:41:10');
INSERT INTO `tbl_category` VALUES(238, 1, 94, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:47:39', '2017-11-21 05:47:39');
INSERT INTO `tbl_category` VALUES(239, 2, 94, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:47:39', '2017-11-21 05:47:39');
INSERT INTO `tbl_category` VALUES(240, 3, 94, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:47:39', '2017-11-21 05:47:39');
INSERT INTO `tbl_category` VALUES(241, 1, 95, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:54:18', '2017-11-21 05:54:18');
INSERT INTO `tbl_category` VALUES(242, 2, 95, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:54:18', '2017-11-21 05:54:18');
INSERT INTO `tbl_category` VALUES(243, 3, 95, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:54:18', '2017-11-21 05:54:18');
INSERT INTO `tbl_category` VALUES(244, 1, 96, 'Main Dishes', 'Main Dishes', '', '2017-11-21 05:58:58', '2017-11-21 05:58:58');
INSERT INTO `tbl_category` VALUES(245, 2, 96, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 05:58:58', '2017-11-21 05:58:58');
INSERT INTO `tbl_category` VALUES(246, 3, 96, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 05:58:58', '2017-11-21 05:58:58');
INSERT INTO `tbl_category` VALUES(247, 1, 97, 'Main Dishes', 'Main Dishes', '', '2017-11-21 06:17:40', '2017-11-21 06:17:40');
INSERT INTO `tbl_category` VALUES(248, 2, 97, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 06:17:40', '2017-11-21 06:17:40');
INSERT INTO `tbl_category` VALUES(249, 3, 97, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 06:17:40', '2017-11-21 06:17:40');
INSERT INTO `tbl_category` VALUES(250, 1, 98, 'Main Dishes', 'Main Dishes', '', '2017-11-21 07:22:32', '2017-11-21 07:22:32');
INSERT INTO `tbl_category` VALUES(251, 2, 98, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 07:22:32', '2017-11-21 07:22:32');
INSERT INTO `tbl_category` VALUES(252, 3, 98, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 07:22:32', '2017-11-21 07:22:32');
INSERT INTO `tbl_category` VALUES(253, 1, 99, 'Main Dishes', 'Main Dishes', '', '2017-11-21 10:32:14', '2017-11-21 10:32:14');
INSERT INTO `tbl_category` VALUES(254, 2, 99, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 10:32:14', '2017-11-21 10:32:14');
INSERT INTO `tbl_category` VALUES(255, 3, 99, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 10:32:14', '2017-11-21 10:32:14');
INSERT INTO `tbl_category` VALUES(256, 1, 100, 'Main Dishes', 'Main Dishes', '', '2017-11-21 11:41:11', '2017-11-21 11:41:11');
INSERT INTO `tbl_category` VALUES(257, 2, 100, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 11:41:11', '2017-11-21 11:41:11');
INSERT INTO `tbl_category` VALUES(258, 3, 100, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 11:41:11', '2017-11-21 11:41:11');
INSERT INTO `tbl_category` VALUES(259, 1, 101, 'Main Dishes', 'Main Dishes', '', '2017-11-21 11:45:18', '2017-11-21 11:45:18');
INSERT INTO `tbl_category` VALUES(260, 2, 101, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-21 11:45:18', '2017-11-21 11:45:18');
INSERT INTO `tbl_category` VALUES(261, 3, 101, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-21 11:45:18', '2017-11-21 11:45:18');
INSERT INTO `tbl_category` VALUES(262, 1, 102, 'Main Dishes', 'Main Dishes', '', '2017-11-22 07:15:36', '2017-11-22 07:15:36');
INSERT INTO `tbl_category` VALUES(263, 2, 102, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 07:15:36', '2017-11-22 07:15:36');
INSERT INTO `tbl_category` VALUES(264, 3, 102, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 07:15:36', '2017-11-22 07:15:36');
INSERT INTO `tbl_category` VALUES(265, 1, 103, 'Main Dishes', 'Main Dishes', '', '2017-11-22 07:29:33', '2017-11-22 07:29:33');
INSERT INTO `tbl_category` VALUES(266, 2, 103, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 07:29:33', '2017-11-22 07:29:33');
INSERT INTO `tbl_category` VALUES(267, 3, 103, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 07:29:33', '2017-11-22 07:29:33');
INSERT INTO `tbl_category` VALUES(268, 1, 104, 'Main Dishes', 'Main Dishes', '', '2017-11-22 07:32:54', '2017-11-22 07:32:54');
INSERT INTO `tbl_category` VALUES(269, 2, 104, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 07:32:54', '2017-11-22 07:32:54');
INSERT INTO `tbl_category` VALUES(270, 3, 104, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 07:32:54', '2017-11-22 07:32:54');
INSERT INTO `tbl_category` VALUES(271, 1, 105, 'Main Dishes', 'Main Dishes', '', '2017-11-22 08:11:39', '2017-11-22 08:11:39');
INSERT INTO `tbl_category` VALUES(272, 2, 105, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 08:11:39', '2017-11-22 08:11:39');
INSERT INTO `tbl_category` VALUES(273, 3, 105, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 08:11:39', '2017-11-22 08:11:39');
INSERT INTO `tbl_category` VALUES(274, 1, 106, 'Main Dishes', 'Main Dishes', '', '2017-11-22 10:07:31', '2017-11-22 10:07:31');
INSERT INTO `tbl_category` VALUES(275, 2, 106, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 10:07:31', '2017-11-22 10:07:31');
INSERT INTO `tbl_category` VALUES(276, 3, 106, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 10:07:31', '2017-11-22 10:07:31');
INSERT INTO `tbl_category` VALUES(277, 1, 107, 'Main Dishes', 'Main Dishes', '', '2017-11-22 10:26:20', '2017-11-22 10:26:20');
INSERT INTO `tbl_category` VALUES(278, 2, 107, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 10:26:20', '2017-11-22 10:26:20');
INSERT INTO `tbl_category` VALUES(279, 3, 107, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 10:26:20', '2017-11-22 10:26:20');
INSERT INTO `tbl_category` VALUES(280, 1, 108, 'Main Dishes', 'Main Dishes', '', '2017-11-22 10:26:42', '2017-11-22 10:26:42');
INSERT INTO `tbl_category` VALUES(281, 2, 108, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 10:26:42', '2017-11-22 10:26:42');
INSERT INTO `tbl_category` VALUES(282, 3, 108, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 10:26:42', '2017-11-22 10:26:42');
INSERT INTO `tbl_category` VALUES(283, 1, 109, 'Main Dishes', 'Main Dishes', '', '2017-11-22 10:29:51', '2017-11-22 10:29:51');
INSERT INTO `tbl_category` VALUES(284, 2, 109, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-22 10:29:51', '2017-11-22 10:29:51');
INSERT INTO `tbl_category` VALUES(285, 3, 109, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-22 10:29:51', '2017-11-22 10:29:51');
INSERT INTO `tbl_category` VALUES(286, 1, 110, 'Main Dishes', 'Main Dishes', '', '2017-11-23 11:15:55', '2017-11-23 11:15:55');
INSERT INTO `tbl_category` VALUES(287, 2, 110, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-23 11:15:55', '2017-11-23 11:15:55');
INSERT INTO `tbl_category` VALUES(288, 3, 110, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-23 11:15:55', '2017-11-23 11:15:55');
INSERT INTO `tbl_category` VALUES(289, 1, 111, 'Main Dishes', 'Main Dishes', '', '2017-11-24 04:40:47', '2017-11-24 04:40:47');
INSERT INTO `tbl_category` VALUES(290, 2, 111, 'Side Dishes (appetizers)', 'Side Dishes (appetizers)', '', '2017-11-24 04:40:47', '2017-11-24 04:40:47');
INSERT INTO `tbl_category` VALUES(291, 3, 111, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', '', '2017-11-24 04:40:47', '2017-11-24 04:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_items`
--

CREATE TABLE `tbl_category_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_order` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_name` varchar(250) NOT NULL,
  `short_description` text NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `tbl_category_items`
--

INSERT INTO `tbl_category_items` VALUES(1, 1, 1, 'item 1', 'This item 1 for Main dishes', 'veg', '', 100.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(2, 2, 1, 'item 2', 'This item 2 for Main dishes', 'veg', '', 120.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(3, 3, 1, 'item 3', 'This item 3 for Main dishes', 'nonveg', '', 150.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(4, 4, 1, 'item 4', 'This item 4 for Main dishes', 'nonveg', '', 130.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(5, 5, 2, 'Item 1', 'side dish item 1', 'veg', 'uploads/category_items/maindish-item1.jpg', 54.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(6, 6, 2, 'Item 2', 'side dish item 2', 'nonveg', 'uploads/category_items/sidedishes-item2.jpg', 86.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(7, 7, 2, 'Item 3', 'side dish item 3', 'nonveg', '', 32.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(8, 8, 3, 'Item 1', 'veg soup', 'Halal', 'uploads/category_items/halal-item1.jpg', 123.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(9, 9, 3, 'Item 2', 'Non-veg soup', 'Halal', 'uploads/category_items/halal-item2.jpg', 233.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(10, 10, 4, 'Item 1', 'item 1', 'nonveg', 'uploads/category_items/maindish-item1.jpg', 234.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(11, 10, 4, 'Item 2', 'item 2', 'nonveg', 'uploads/category_items/maindish-item2.jpg', 102.00, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_category_items` VALUES(12, 11, 5, 'Item 1', 'venilla icecream large cup', 'veg', 'uploads/category_items/sidedishes-item1.jpg', 42.00, '2017-10-09 00:00:00', '2017-10-02 00:00:00');
INSERT INTO `tbl_category_items` VALUES(13, 12, 5, 'Item 2', 'strawberry icecream large cup', 'veg', 'uploads/category_items/sidedishes-item2.jpg', 51.00, '2017-10-09 00:00:00', '2017-10-02 00:00:00');
INSERT INTO `tbl_category_items` VALUES(14, 13, 6, 'Item 1', 'venilla icecream large cup', 'Halal', 'uploads/category_items/halal-item1.jpg', 55.00, '2017-10-24 00:00:00', '2017-10-24 00:00:00');
INSERT INTO `tbl_category_items` VALUES(15, 13, 6, 'Item 2', 'strawberry icecream large cup', 'Halal', 'uploads/category_items/halal-item2.jpg', 55.00, '2017-10-24 00:00:00', '2017-10-24 00:00:00');
INSERT INTO `tbl_category_items` VALUES(16, 1, 28, 'Item 1', 'This item 1 for Main dishes', 'veg', '', 25.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(17, 2, 28, 'Item 2', 'This item 2 for Main dishes', 'veg', '', 25.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(18, 3, 29, 'Item 1', 'This item 1 for Side Dishes', 'veg', '', 25.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(19, 4, 29, 'Item 2', 'This item 2 for Side Dishes', 'veg', '', 25.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(20, 5, 30, 'Item 1', 'This item 1 for Vegetarian Only/Halal Only', 'veg', '', 35.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(21, 5, 30, 'Item 2', 'This item 2 for Vegetarian Only/Halal Only', 'veg', '', 35.00, '2017-10-28 20:29:00', '2017-10-28 20:29:00');
INSERT INTO `tbl_category_items` VALUES(34, 0, 31, 'Other Dish1', 'This is Other Dish 1', 'halal', '', 435.00, '2017-10-31 04:55:03', '2017-11-20 03:27:10');
INSERT INTO `tbl_category_items` VALUES(36, 0, 33, 'Other Dish3', 'This is Other Dish 3', 'nonveg', '', 435.00, '2017-10-31 04:56:04', '2017-11-20 04:19:35');
INSERT INTO `tbl_category_items` VALUES(40, 0, 49, 'Biriyani', 'Veg and no veg', 'nonveg', '', 100.00, '2017-11-02 12:12:02', '2017-11-02 12:12:02');
INSERT INTO `tbl_category_items` VALUES(42, 0, 51, 'dosa v', 'Spl', 'veg', '', 15.00, '2017-11-03 07:52:15', '2017-11-03 07:52:42');
INSERT INTO `tbl_category_items` VALUES(43, 0, 51, 'idly', 'Ghhj ghjjhh sjdjjsjs hsjdjjwdj Hdjdjdjdjdjdkk Hdjdjdjdjdjdkk Hshjsnd Bdjjdkd Hdjkdkdkduififoofofof hdjdjd ', 'veg', '', 5.00, '2017-11-03 07:53:49', '2017-11-03 07:53:49');
INSERT INTO `tbl_category_items` VALUES(44, 0, 50, 'Fish curry', 'Fact ', 'Halal', '', 100.00, '2017-11-03 08:48:53', '2017-11-03 08:48:53');
INSERT INTO `tbl_category_items` VALUES(45, 0, 50, 'chicken fry', 'Chicken fry for home made', 'both', '', 10.00, '2017-11-03 09:23:30', '2017-11-03 09:23:30');
INSERT INTO `tbl_category_items` VALUES(46, 0, 49, 'rice', 'Hdjdjjd jdjjd', 'veg', '', 10.00, '2017-11-03 11:02:39', '2017-11-03 11:02:39');
INSERT INTO `tbl_category_items` VALUES(50, 0, 68, 'Pakora', 'Onion', 'veg', '', 5.00, '2017-11-04 09:06:35', '2017-11-10 11:30:58');
INSERT INTO `tbl_category_items` VALUES(58, 0, 88, 'Chicken', 'Wings of chicken', 'halal', '', 290.00, '2017-11-08 05:29:43', '2017-11-13 02:38:11');
INSERT INTO `tbl_category_items` VALUES(59, 0, 89, 'Pasta', 'Cheese pasta ', 'halal', '', 100.00, '2017-11-08 05:31:06', '2017-11-15 01:57:52');
INSERT INTO `tbl_category_items` VALUES(60, 0, 90, 'Hotdog', 'Chinese hotdog', 'both', '', 300.00, '2017-11-08 05:33:38', '2017-11-08 05:33:38');
INSERT INTO `tbl_category_items` VALUES(61, 0, 88, 'Chilly', 'Hdjdhd ', 'halal', '', 100.00, '2017-11-10 04:11:16', '2017-11-10 04:11:16');
INSERT INTO `tbl_category_items` VALUES(62, 0, 90, 'Fries', 'French fries', 'veg', '', 50.00, '2017-11-10 04:32:33', '2017-11-10 04:32:33');
INSERT INTO `tbl_category_items` VALUES(64, 0, 67, 'Curry', 'Fish curry', 'nonveg', '', 10.00, '2017-11-10 11:31:34', '2017-11-10 11:31:34');
INSERT INTO `tbl_category_items` VALUES(65, 0, 89, 'Olives ', '      Food ', 'halal', '', 45.00, '2017-11-13 02:37:06', '2017-11-13 02:37:06');
INSERT INTO `tbl_category_items` VALUES(67, 0, 68, 'Pakora', 'Onion potato ', 'veg', '', 10.00, '2017-11-13 04:42:13', '2017-11-13 04:42:13');
INSERT INTO `tbl_category_items` VALUES(71, 0, 89, 'Cheese Toast', 'Cheese with bread toast ', 'halal', '', 200.00, '2017-11-16 12:15:41', '2017-11-16 12:15:41');
INSERT INTO `tbl_category_items` VALUES(78, 0, 211, 'Pulav', 'Test', 'veg', '', 36.00, '2017-11-19 11:23:00', '2017-11-19 11:22:01');
INSERT INTO `tbl_category_items` VALUES(86, 0, 32, 'Side Dish', 'Dishhh', 'nonveg', '', 10.00, '2017-11-23 11:00:37', '2017-11-23 11:00:37');
INSERT INTO `tbl_category_items` VALUES(89, 0, 31, 'Hh', 'Gj', 'halal', '', 57.00, '2017-11-24 05:37:28', '2017-11-24 05:37:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daywise_orders`
--

CREATE TABLE `tbl_daywise_orders` (
  `days_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`days_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `tbl_daywise_orders`
--

INSERT INTO `tbl_daywise_orders` VALUES(1, 101, 10, 6, '2017-10-09 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(2, 101, 10, 6, '2017-10-10 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(3, 101, 10, 6, '2017-10-11 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(4, 101, 10, 6, '2017-10-12 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(5, 101, 10, 6, '2017-10-13 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(6, 101, 10, 6, '2017-10-14 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(7, 101, 10, 6, '2017-10-15 00:00:00', 'paid', '2017-11-03 02:57:51', '2017-11-03 02:57:51');
INSERT INTO `tbl_daywise_orders` VALUES(8, 99, 10, 25, '2017-11-03 00:00:00', 'Paid', '2017-11-03 04:06:39', '2017-11-03 04:06:39');
INSERT INTO `tbl_daywise_orders` VALUES(9, 99, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-03 04:06:39', '2017-11-03 04:06:39');
INSERT INTO `tbl_daywise_orders` VALUES(10, 99, 10, 25, '2017-11-05 00:00:00', 'Paid', '2017-11-03 04:06:39', '2017-11-03 04:06:39');
INSERT INTO `tbl_daywise_orders` VALUES(11, 102, 10, 25, '2017-11-03 00:00:00', 'Paid', '2017-11-03 04:10:31', '2017-11-03 04:10:31');
INSERT INTO `tbl_daywise_orders` VALUES(12, 107, 10, 25, '2017-11-03 00:00:00', 'Paid', '2017-11-03 07:11:41', '2017-11-03 07:11:41');
INSERT INTO `tbl_daywise_orders` VALUES(13, 112, 10, 25, '2017-11-03 00:00:00', 'Paid', '2017-11-04 06:01:33', '2017-11-04 06:01:33');
INSERT INTO `tbl_daywise_orders` VALUES(14, 112, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-04 06:01:33', '2017-11-04 06:01:33');
INSERT INTO `tbl_daywise_orders` VALUES(15, 110, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-04 06:01:38', '2017-11-04 06:01:38');
INSERT INTO `tbl_daywise_orders` VALUES(16, 114, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-04 06:15:40', '2017-11-04 06:15:40');
INSERT INTO `tbl_daywise_orders` VALUES(17, 114, 10, 25, '2017-11-05 00:00:00', 'Paid', '2017-11-04 06:15:40', '2017-11-04 06:15:40');
INSERT INTO `tbl_daywise_orders` VALUES(18, 114, 10, 25, '2017-11-06 00:00:00', 'Paid', '2017-11-04 06:15:40', '2017-11-04 06:15:40');
INSERT INTO `tbl_daywise_orders` VALUES(19, 111, 10, 25, '2017-11-03 00:00:00', 'Paid', '2017-11-04 06:15:51', '2017-11-04 06:15:51');
INSERT INTO `tbl_daywise_orders` VALUES(20, 111, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-04 06:15:51', '2017-11-04 06:15:51');
INSERT INTO `tbl_daywise_orders` VALUES(21, 111, 10, 25, '2017-11-05 00:00:00', 'Paid', '2017-11-04 06:15:51', '2017-11-04 06:15:51');
INSERT INTO `tbl_daywise_orders` VALUES(22, 113, 10, 25, '2017-11-04 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(23, 113, 10, 25, '2017-11-05 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(24, 113, 10, 25, '2017-11-06 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(25, 113, 10, 25, '2017-11-07 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(26, 113, 10, 25, '2017-11-08 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(27, 113, 10, 25, '2017-11-09 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(28, 113, 10, 25, '2017-11-10 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(29, 113, 10, 25, '2017-11-11 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(30, 113, 10, 25, '2017-11-12 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(31, 113, 10, 25, '2017-11-13 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(32, 113, 10, 25, '2017-11-14 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(33, 113, 10, 25, '2017-11-15 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(34, 113, 10, 25, '2017-11-16 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(35, 113, 10, 25, '2017-11-17 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(36, 113, 10, 25, '2017-11-18 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(37, 113, 10, 25, '2017-11-19 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(38, 113, 10, 25, '2017-11-20 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(39, 113, 10, 25, '2017-11-21 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(40, 113, 10, 25, '2017-11-22 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(41, 113, 10, 25, '2017-11-23 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(42, 113, 10, 25, '2017-11-24 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(43, 113, 10, 25, '2017-11-25 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(44, 113, 10, 25, '2017-11-26 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(45, 113, 10, 25, '2017-11-27 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(46, 113, 10, 25, '2017-11-28 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(47, 113, 10, 25, '2017-11-29 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(48, 113, 10, 25, '2017-11-30 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(49, 113, 10, 25, '2017-12-01 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(50, 113, 10, 25, '2017-12-02 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(51, 113, 10, 25, '2017-12-03 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(52, 113, 10, 25, '2017-12-04 00:00:00', 'Paid', '2017-11-04 08:31:24', '2017-11-04 08:31:24');
INSERT INTO `tbl_daywise_orders` VALUES(53, 124, 10, 25, '2017-11-06 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(54, 124, 10, 25, '2017-11-07 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(55, 124, 10, 25, '2017-11-08 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(56, 124, 10, 25, '2017-11-09 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(57, 124, 10, 25, '2017-11-10 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(58, 124, 10, 25, '2017-11-11 00:00:00', 'Paid', '2017-11-07 05:34:47', '2017-11-07 05:34:47');
INSERT INTO `tbl_daywise_orders` VALUES(59, 140, 10, 44, '2017-11-12 00:00:00', 'Paid', '2017-11-10 01:08:50', '2017-11-10 01:08:50');
INSERT INTO `tbl_daywise_orders` VALUES(60, 142, 10, 44, '2017-11-10 00:00:00', 'Paid', '2017-11-10 02:24:18', '2017-11-10 02:24:18');
INSERT INTO `tbl_daywise_orders` VALUES(61, 143, 10, 44, '2017-11-10 00:00:00', 'Paid', '2017-11-10 02:34:07', '2017-11-10 02:34:07');
INSERT INTO `tbl_daywise_orders` VALUES(62, 136, 10, 44, '2017-11-11 00:00:00', 'Paid', '2017-11-10 02:35:48', '2017-11-10 02:35:48');
INSERT INTO `tbl_daywise_orders` VALUES(63, 141, 10, 44, '2017-11-11 00:00:00', 'Paid', '2017-11-10 03:03:04', '2017-11-10 03:03:04');
INSERT INTO `tbl_daywise_orders` VALUES(64, 149, 10, 44, '2017-11-11 00:00:00', 'Paid', '2017-11-11 04:47:36', '2017-11-11 04:47:36');
INSERT INTO `tbl_daywise_orders` VALUES(65, 151, 10, 25, '2017-11-11 00:00:00', 'Paid', '2017-11-13 02:20:39', '2017-11-13 02:20:39');
INSERT INTO `tbl_daywise_orders` VALUES(66, 167, 67, 44, '2017-11-14 00:00:00', 'Paid', '2017-11-13 05:51:52', '2017-11-13 05:51:52');
INSERT INTO `tbl_daywise_orders` VALUES(67, 170, 10, 44, '2017-11-13 00:00:00', 'Paid', '2017-11-13 06:29:29', '2017-11-13 06:29:29');
INSERT INTO `tbl_daywise_orders` VALUES(68, 157, 10, 44, '2017-11-11 00:00:00', 'Paid', '2017-11-13 06:29:35', '2017-11-13 06:29:35');
INSERT INTO `tbl_daywise_orders` VALUES(69, 196, 10, 25, '2017-11-14 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(70, 196, 10, 25, '2017-11-15 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(71, 196, 10, 25, '2017-11-16 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(72, 196, 10, 25, '2017-11-17 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(73, 196, 10, 25, '2017-11-18 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(74, 196, 10, 25, '2017-11-19 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(75, 196, 10, 25, '2017-11-20 00:00:00', 'Paid', '2017-11-14 07:18:48', '2017-11-14 07:18:48');
INSERT INTO `tbl_daywise_orders` VALUES(76, 200, 41, 44, '2017-11-18 00:00:00', 'Paid', '2017-11-14 11:43:17', '2017-11-14 11:43:17');
INSERT INTO `tbl_daywise_orders` VALUES(77, 210, 10, 44, '2017-11-15 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(78, 210, 10, 44, '2017-11-16 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(79, 210, 10, 44, '2017-11-17 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(80, 210, 10, 44, '2017-11-18 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(81, 210, 10, 44, '2017-11-19 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(82, 210, 10, 44, '2017-11-20 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(83, 210, 10, 44, '2017-11-21 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(84, 210, 10, 44, '2017-11-22 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(85, 210, 10, 44, '2017-11-23 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(86, 210, 10, 44, '2017-11-24 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(87, 210, 10, 44, '2017-11-25 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(88, 210, 10, 44, '2017-11-26 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(89, 210, 10, 44, '2017-11-27 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(90, 210, 10, 44, '2017-11-28 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(91, 210, 10, 44, '2017-11-29 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(92, 210, 10, 44, '2017-11-30 00:00:00', 'Paid', '2017-11-15 02:24:12', '2017-11-15 02:24:12');
INSERT INTO `tbl_daywise_orders` VALUES(93, 212, 10, 44, '2017-11-15 00:00:00', 'Paid', '2017-11-15 04:18:07', '2017-11-15 04:18:07');
INSERT INTO `tbl_daywise_orders` VALUES(94, 266, 10, 25, '2017-11-22 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(95, 266, 10, 25, '2017-11-23 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(96, 266, 10, 25, '2017-11-24 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(97, 266, 10, 25, '2017-11-25 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(98, 266, 10, 25, '2017-11-26 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(99, 266, 10, 25, '2017-11-27 00:00:00', 'Paid', '2017-11-22 07:05:07', '2017-11-22 07:05:07');
INSERT INTO `tbl_daywise_orders` VALUES(100, 315, 107, 25, '2017-11-23 00:00:00', 'Paid', '2017-11-23 04:31:10', '2017-11-23 04:31:10');
INSERT INTO `tbl_daywise_orders` VALUES(101, 307, 107, 25, '2017-11-23 00:00:00', 'Paid', '2017-11-23 07:25:12', '2017-11-23 07:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(102, 293, 10, 25, '2017-11-23 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(103, 293, 10, 25, '2017-11-24 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(104, 293, 10, 25, '2017-11-25 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(105, 293, 10, 25, '2017-11-26 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(106, 293, 10, 25, '2017-11-27 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');
INSERT INTO `tbl_daywise_orders` VALUES(107, 293, 10, 25, '2017-11-28 00:00:00', 'Paid', '2017-11-23 11:25:12', '2017-11-23 11:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoices`
--

CREATE TABLE `tbl_invoices` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `total_amount` decimal(16,2) NOT NULL,
  `invoice_amount` decimal(16,2) NOT NULL,
  `discount` decimal(16,2) NOT NULL,
  `vendor_invoice_status` varchar(25) NOT NULL,
  `admin_invoice_status` varchar(25) NOT NULL,
  `develop_status` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Dumping data for table `tbl_invoices`
--

INSERT INTO `tbl_invoices` VALUES(34, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(35, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(36, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(37, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(38, 7, '', 0.00, 540.00, 60.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(39, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(40, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(41, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(42, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(43, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(44, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(45, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(46, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(47, 7, '', 0.00, 540.00, 60.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(48, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(49, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(50, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(51, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(52, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(53, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(54, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(55, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(56, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(57, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(58, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(59, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(60, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(61, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(62, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(63, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(64, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(65, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(66, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(67, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(68, 7, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(69, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(70, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(71, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(72, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(73, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(74, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(75, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(76, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(77, 7, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(78, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(79, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(80, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(81, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(82, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(83, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(84, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(85, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(86, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(87, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(88, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(89, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(90, 7, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(91, 7, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(92, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(93, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(94, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(95, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(96, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(97, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(98, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(99, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(100, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_invoices` VALUES(101, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '2017-10-16 01:11:24', '2017-10-16 01:11:24');
INSERT INTO `tbl_invoices` VALUES(102, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '2017-10-16 01:15:59', '2017-10-16 01:15:59');
INSERT INTO `tbl_invoices` VALUES(103, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '2017-10-16 01:17:20', '2017-10-16 01:17:20');
INSERT INTO `tbl_invoices` VALUES(108, 7, 'ACRAF23DB3C4', 0.00, 180.00, 20.00, 'Paid', 'Paid', '', '2017-10-16 03:08:24', '2017-10-16 03:08:24');
INSERT INTO `tbl_invoices` VALUES(109, 6, '', 0.00, 360.00, 40.00, '', '', 'preview', '2017-10-19 22:40:22', '2017-10-19 22:40:22');
INSERT INTO `tbl_invoices` VALUES(142, 7, '', 0.00, 540.00, 60.00, '', '', 'preview', '2017-10-23 04:41:20', '2017-10-23 04:41:20');
INSERT INTO `tbl_invoices` VALUES(143, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '2017-10-23 05:52:45', '2017-10-23 05:52:45');
INSERT INTO `tbl_invoices` VALUES(144, 6, '', 0.00, 270.00, 30.00, '', '', 'preview', '2017-10-23 05:52:53', '2017-10-23 05:52:53');
INSERT INTO `tbl_invoices` VALUES(145, 6, 'ACRAF23DB3C5', 0.00, 270.00, 30.00, 'paid', 'Paid', '', '2017-10-23 05:53:02', '2017-10-23 05:53:02');
INSERT INTO `tbl_invoices` VALUES(146, 7, '', 0.00, 180.00, 20.00, '', '', 'preview', '2017-10-23 05:57:32', '2017-10-23 05:57:32');
INSERT INTO `tbl_invoices` VALUES(147, 7, 'ACRAF23DB3C6', 0.00, 180.00, 20.00, 'Pending', 'Pending', '', '2017-10-23 05:57:46', '2017-10-23 05:57:46');
INSERT INTO `tbl_invoices` VALUES(148, 7, '', 0.00, 360.00, 40.00, '', '', 'preview', '2017-10-23 05:58:29', '2017-10-23 05:58:29');
INSERT INTO `tbl_invoices` VALUES(149, 7, 'ACRAF23DB3C7', 0.00, 360.00, 40.00, 'Pending', 'Pending', '', '2017-10-23 05:58:31', '2017-10-23 05:58:31');
INSERT INTO `tbl_invoices` VALUES(150, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '2017-10-23 07:00:28', '2017-10-23 07:00:28');
INSERT INTO `tbl_invoices` VALUES(151, 6, 'ACRAF23DB3C8', 0.00, 90.00, 10.00, 'Pending', 'Pending', '', '2017-10-24 02:27:55', '2017-10-24 02:27:55');
INSERT INTO `tbl_invoices` VALUES(152, 6, '', 0.00, 90.00, 10.00, '', '', 'preview', '2017-10-24 03:22:40', '2017-10-24 03:22:40');
INSERT INTO `tbl_invoices` VALUES(153, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '2017-10-24 05:09:48', '2017-10-24 05:09:48');
INSERT INTO `tbl_invoices` VALUES(154, 8, '', 0.00, 135.00, 15.00, '', '', 'preview', '2017-10-24 05:10:19', '2017-10-24 05:10:19');
INSERT INTO `tbl_invoices` VALUES(155, 6, 'ACRAF23DB3C9', 0.00, 90.00, 10.00, 'Pending', 'Pending', '', '2017-10-24 07:01:56', '2017-10-24 07:01:56');
INSERT INTO `tbl_invoices` VALUES(156, 25, '', 0.00, 9.00, 1.00, 'Pending', 'Pending', '', '2017-11-02 07:26:56', '2017-11-02 07:26:56');
INSERT INTO `tbl_invoices` VALUES(157, 25, '', 0.00, 45.00, 5.00, '', '', 'preview', '2017-11-10 02:16:35', '2017-11-10 02:16:35');
INSERT INTO `tbl_invoices` VALUES(158, 25, '', 0.00, 45.00, 5.00, 'Pending', 'Pending', '', '2017-11-10 02:59:52', '2017-11-10 02:59:52');
INSERT INTO `tbl_invoices` VALUES(159, 44, '', 0.00, 360.00, 40.00, 'Pending', 'Pending', '', '2017-11-10 03:37:58', '2017-11-10 03:37:58');
INSERT INTO `tbl_invoices` VALUES(160, 44, '', 0.00, 270.00, 30.00, 'Pending', 'Pending', '', '2017-11-10 03:38:35', '2017-11-10 03:38:35');
INSERT INTO `tbl_invoices` VALUES(161, 44, '', 0.00, 346.50, 38.50, 'Pending', 'Pending', '', '2017-11-14 23:22:33', '2017-11-14 23:22:33');
INSERT INTO `tbl_invoices` VALUES(162, 44, '', 0.00, 450.00, 50.00, 'Pending', 'Pending', '', '2017-11-15 03:04:44', '2017-11-15 03:04:44');
INSERT INTO `tbl_invoices` VALUES(163, 44, '', 0.00, 450.00, 50.00, '', '', 'preview', '2017-11-15 03:05:57', '2017-11-15 03:05:57');
INSERT INTO `tbl_invoices` VALUES(164, 44, '', 0.00, 450.00, 50.00, 'Pending', 'Pending', '', '2017-11-15 03:06:05', '2017-11-15 03:06:05');
INSERT INTO `tbl_invoices` VALUES(165, 44, '', 0.00, 317.50, 317.50, 'Pending', 'Pending', '', '2017-11-15 03:18:44', '2017-11-15 03:18:44');
INSERT INTO `tbl_invoices` VALUES(166, 44, '', 0.00, 195.00, 195.00, 'Pending', 'Pending', '', '2017-11-15 03:24:36', '2017-11-15 03:24:36');
INSERT INTO `tbl_invoices` VALUES(167, 44, '', 0.00, 345.00, 345.00, 'Pending', 'Pending', '', '2017-11-15 04:26:36', '2017-11-15 04:26:36');
INSERT INTO `tbl_invoices` VALUES(168, 44, '', 0.00, 375.00, 375.00, 'Pending', 'Pending', '', '2017-11-20 03:41:27', '2017-11-20 03:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_orders`
--

CREATE TABLE `tbl_invoice_orders` (
  `invoice_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `invoice_date` datetime NOT NULL,
  PRIMARY KEY (`invoice_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `tbl_invoice_orders`
--

INSERT INTO `tbl_invoice_orders` VALUES(124, 145, 1, '2017-10-23 05:53:02');
INSERT INTO `tbl_invoice_orders` VALUES(125, 147, 2, '2017-10-23 05:57:46');
INSERT INTO `tbl_invoice_orders` VALUES(126, 149, 5, '2017-10-23 05:58:31');
INSERT INTO `tbl_invoice_orders` VALUES(127, 151, 4, '2017-10-24 02:27:55');
INSERT INTO `tbl_invoice_orders` VALUES(128, 155, 6, '2017-10-24 07:01:56');
INSERT INTO `tbl_invoice_orders` VALUES(129, 156, 89, '2017-11-02 07:26:56');
INSERT INTO `tbl_invoice_orders` VALUES(130, 158, 112, '2017-11-10 02:59:52');
INSERT INTO `tbl_invoice_orders` VALUES(131, 159, 136, '2017-11-10 03:37:58');
INSERT INTO `tbl_invoice_orders` VALUES(132, 160, 140, '2017-11-10 03:38:35');
INSERT INTO `tbl_invoice_orders` VALUES(133, 161, 200, '2017-11-14 23:17:14');
INSERT INTO `tbl_invoice_orders` VALUES(134, 161, 200, '2017-11-14 23:22:26');
INSERT INTO `tbl_invoice_orders` VALUES(135, 161, 200, '2017-11-14 23:22:28');
INSERT INTO `tbl_invoice_orders` VALUES(136, 161, 200, '2017-11-14 23:22:29');
INSERT INTO `tbl_invoice_orders` VALUES(137, 161, 200, '2017-11-14 23:22:33');
INSERT INTO `tbl_invoice_orders` VALUES(138, 162, 210, '2017-11-15 03:04:26');
INSERT INTO `tbl_invoice_orders` VALUES(139, 162, 210, '2017-11-15 03:04:30');
INSERT INTO `tbl_invoice_orders` VALUES(140, 162, 210, '2017-11-15 03:04:32');
INSERT INTO `tbl_invoice_orders` VALUES(141, 162, 210, '2017-11-15 03:04:44');
INSERT INTO `tbl_invoice_orders` VALUES(142, 164, 170, '2017-11-15 03:06:01');
INSERT INTO `tbl_invoice_orders` VALUES(143, 164, 170, '2017-11-15 03:06:05');
INSERT INTO `tbl_invoice_orders` VALUES(144, 165, 169, '2017-11-15 03:18:44');
INSERT INTO `tbl_invoice_orders` VALUES(145, 166, 167, '2017-11-15 03:24:36');
INSERT INTO `tbl_invoice_orders` VALUES(146, 167, 212, '2017-11-15 04:26:36');
INSERT INTO `tbl_invoice_orders` VALUES(147, 168, 149, '2017-11-20 03:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE `tbl_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `delivery_time` time DEFAULT NULL,
  `price` decimal(16,2) NOT NULL,
  `ratings` float NOT NULL,
  `order_type` varchar(10) NOT NULL,
  `status` varchar(25) NOT NULL,
  `paypal_transaction_id` varchar(100) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `payment_date` datetime NOT NULL,
  `reason` varchar(200) DEFAULT NULL,
  `feedback_msg` varchar(200) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=329 ;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` VALUES(1, 2, 6, 1, '2017-10-06', '2017-10-10', NULL, 300.00, 3.5, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 12:20:26');
INSERT INTO `tbl_orders` VALUES(2, 2, 7, 2, '2017-10-06', '2017-10-09', NULL, 200.00, 4.3, 'veg', 'Pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 07:18:19', '2017-10-06 10:21:21');
INSERT INTO `tbl_orders` VALUES(3, 3, 8, 2, '2017-10-06', '2017-10-10', NULL, 150.00, 2.5, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 12:20:26');
INSERT INTO `tbl_orders` VALUES(4, 4, 6, 1, '2017-10-06', '2017-10-09', NULL, 100.00, 4, 'veg', 'Pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 07:18:19', '2017-10-06 10:21:21');
INSERT INTO `tbl_orders` VALUES(5, 5, 7, 1, '2017-10-06', '2017-10-10', NULL, 400.00, 3, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 07:10:06');
INSERT INTO `tbl_orders` VALUES(6, 2, 6, 2, '2017-10-22', '2017-10-28', NULL, 100.00, 4, 'veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-23 19:26:43', '2017-10-23 19:26:47');
INSERT INTO `tbl_orders` VALUES(7, 2, 6, 1, '2017-10-23', '2017-10-31', NULL, 100.00, 4, 'veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-23 19:26:43', '2017-10-23 19:26:47');
INSERT INTO `tbl_orders` VALUES(35, 2, 6, 0, '2017-10-28', '2017-10-30', '12:00:00', 26.00, 5, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 12:23:24', '2017-10-28 12:23:24');
INSERT INTO `tbl_orders` VALUES(36, 17, 6, 0, '2017-10-26', '2017-10-28', '10:11:00', 123.00, 5, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 12:29:16', '2017-10-28 12:29:16');
INSERT INTO `tbl_orders` VALUES(37, 17, 6, 1, '2017-10-26', '2017-10-28', '10:11:00', 123.00, 5, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 12:30:04', '2017-10-28 12:30:04');
INSERT INTO `tbl_orders` VALUES(75, 10, 6, 0, '2017-10-28', '2017-10-28', '19:23:00', 370.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-10-28 06:53:23', '2017-11-04 03:46:33');
INSERT INTO `tbl_orders` VALUES(76, 10, 6, 1, '2017-10-28', '2017-10-28', '19:25:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 06:56:02', '2017-10-28 06:56:02');
INSERT INTO `tbl_orders` VALUES(77, 10, 6, 1, '2017-10-28', '2017-10-28', '19:29:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 06:59:53', '2017-10-28 06:59:53');
INSERT INTO `tbl_orders` VALUES(78, 10, 6, 1, '2017-10-28', '2017-10-28', '20:09:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 07:39:26', '2017-10-28 07:39:26');
INSERT INTO `tbl_orders` VALUES(79, 10, 24, 3, '2017-10-28', '2017-10-28', '20:44:00', 4.00, 5, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-10-28 08:15:03', '2017-10-31 04:38:42');
INSERT INTO `tbl_orders` VALUES(80, 10, 24, 4, '2017-10-30', '2017-10-30', '17:00:00', 4.00, 3.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-10-28 08:24:41', '2017-10-31 04:36:33');
INSERT INTO `tbl_orders` VALUES(81, 17, 24, 3, '2017-10-30', '2017-10-30', '17:00:00', 4.00, 0, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-28 08:50:14', '2017-10-28 08:50:14');
INSERT INTO `tbl_orders` VALUES(82, 17, 6, 0, '2017-10-30', '2017-10-30', '17:00:00', 100.00, 3, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, 'Feedback msg', '2017-10-28 08:51:20', '2017-10-31 03:47:37');
INSERT INTO `tbl_orders` VALUES(83, 10, 6, 0, '2017-10-30', '2017-10-30', '17:00:00', 640.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_orders` VALUES(84, 10, 6, 0, '2017-10-29', '2017-10-29', '17:00:00', 500.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-29 11:31:11', '2017-10-29 11:31:11');
INSERT INTO `tbl_orders` VALUES(85, 10, 6, 1, '2017-10-29', '2017-10-29', '17:00:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-10-29 11:39:13', '2017-11-02 06:00:15');
INSERT INTO `tbl_orders` VALUES(86, 10, 6, 1, '2017-10-30', '2017-10-30', '17:00:00', 20.00, 5, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-10-30 12:03:46', '2017-11-01 03:08:49');
INSERT INTO `tbl_orders` VALUES(87, 26, 6, 1, '2017-10-30', '2017-10-30', '17:00:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-30 05:33:25', '2017-10-30 05:33:25');
INSERT INTO `tbl_orders` VALUES(88, 26, 6, 0, '2017-10-31', '2017-10-31', '17:00:00', 220.00, 4.3, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-10-31 12:59:19', '2017-10-31 12:59:19');
INSERT INTO `tbl_orders` VALUES(89, 10, 25, 12, '2017-11-02', '2017-11-05', '12:39:00', 10.00, 4.8, 'Package', 'Cancel', '', '', '2017-11-03 01:05:09', '', '', '2017-11-01 11:37:23', '2017-11-03 01:05:09');
INSERT INTO `tbl_orders` VALUES(90, 10, 6, 0, '2017-11-02', '2017-11-02', '17:00:00', 370.00, 4.3, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-02 01:52:13', '2017-11-02 01:52:13');
INSERT INTO `tbl_orders` VALUES(91, 10, 6, 1, '2017-11-02', '2017-11-02', '17:00:00', 20.00, 4.3, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-02 04:33:37', '2017-11-02 04:33:37');
INSERT INTO `tbl_orders` VALUES(92, 10, 6, 0, '2017-11-02', '2017-11-02', '17:00:00', 100.00, 5, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-02 04:34:09', '2017-11-02 06:14:09');
INSERT INTO `tbl_orders` VALUES(93, 10, 6, 2, '2017-11-02', '2017-11-02', '18:53:00', 30.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-02 06:23:34', '2017-11-02 06:23:34');
INSERT INTO `tbl_orders` VALUES(94, 10, 25, 12, '2017-11-03', '2017-11-14', '13:56:00', 10.00, 4.8, 'Package', 'Cancel', '', '', '2017-11-03 01:27:56', 'Due to power Off', NULL, '2017-11-03 01:26:39', '2017-11-03 01:27:56');
INSERT INTO `tbl_orders` VALUES(95, 10, 25, 12, '2017-11-03', '2017-11-05', '14:00:00', 10.00, 4.8, 'Package', 'Started', '12345', 'completed', '2017-11-11 03:46:06', '', NULL, '2017-11-03 01:30:40', '2017-11-11 03:46:06');
INSERT INTO `tbl_orders` VALUES(96, 10, 25, 0, '2017-11-03', '2017-11-06', '14:00:00', 295.00, 4.8, 'Normal', 'Cancel', '', '', '2017-11-03 01:33:15', 'power', NULL, '2017-11-03 01:31:02', '2017-11-03 01:33:15');
INSERT INTO `tbl_orders` VALUES(97, 10, 25, 0, '2017-11-03', '2017-11-03', '14:44:00', 173.00, 4.8, 'Normal', 'Cancel', '', '', '2017-11-03 02:34:34', '', NULL, '2017-11-03 02:14:43', '2017-11-03 02:34:34');
INSERT INTO `tbl_orders` VALUES(98, 10, 25, 18, '2017-11-03', '2017-11-08', '14:45:00', 700.00, 4.8, 'Package', 'Started', '', '', '2017-11-03 03:22:37', '', NULL, '2017-11-03 02:15:10', '2017-11-03 03:22:37');
INSERT INTO `tbl_orders` VALUES(99, 10, 25, 18, '2017-11-03', '2017-11-05', '15:15:00', 700.00, 4.8, 'Package', 'Started', '', '', '2017-11-03 04:07:23', '', NULL, '2017-11-03 02:45:21', '2017-11-03 04:07:23');
INSERT INTO `tbl_orders` VALUES(100, 10, 25, 0, '2017-11-03', '2017-11-05', '15:15:00', 173.00, 4.8, 'Normal', 'Started', '', '', '2017-11-03 03:24:24', '', NULL, '2017-11-03 02:45:39', '2017-11-03 03:24:24');
INSERT INTO `tbl_orders` VALUES(101, 10, 6, 0, '2017-10-09', '2017-10-15', '17:00:00', 123.00, 5, 'Normal', 'cancel', 'abd2331', 'success', '2017-11-03 03:07:58', 'power cut', NULL, '2017-11-03 02:56:46', '2017-11-03 03:07:58');
INSERT INTO `tbl_orders` VALUES(102, 10, 25, 18, '2017-11-03', '2017-11-03', '16:39:00', 700.00, 4.8, 'Package', 'Started', '', '', '2017-11-03 04:11:19', '', NULL, '2017-11-03 04:09:34', '2017-11-03 04:11:19');
INSERT INTO `tbl_orders` VALUES(103, 10, 25, 18, '2017-11-03', '2017-11-03', '17:27:00', 700.00, 4.8, 'Package', 'Cancel', '', '', '2017-11-03 06:43:11', 'Resource lack', NULL, '2017-11-03 04:57:39', '2017-11-03 06:43:11');
INSERT INTO `tbl_orders` VALUES(104, 10, 6, 0, '2017-11-03', '2017-11-03', '17:44:00', 510.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_orders` VALUES(105, 10, 6, 1, '2017-11-03', '2017-11-03', '17:46:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-03 05:16:24', '2017-11-03 05:16:24');
INSERT INTO `tbl_orders` VALUES(106, 10, 25, 18, '2017-11-03', '2017-11-15', '20:10:00', 700.00, 4.8, 'Package', 'Cancel', '', '', '2017-11-03 07:09:21', 'Not cooking today', NULL, '2017-11-03 07:08:15', '2017-11-03 07:09:21');
INSERT INTO `tbl_orders` VALUES(107, 10, 25, 0, '2017-11-03', '2017-11-03', '20:12:00', 173.00, 4.8, 'Normal', 'Paid', '', '', '2017-11-03 07:11:41', '', NULL, '2017-11-03 07:10:22', '2017-11-03 07:11:41');
INSERT INTO `tbl_orders` VALUES(108, 10, 31, 0, '2017-11-03', '2017-11-05', '21:55:00', 200.00, 0, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-03 08:53:27', '2017-11-03 08:53:27');
INSERT INTO `tbl_orders` VALUES(109, 10, 25, 0, '2017-11-04', '2017-11-04', '14:01:00', 233.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-04 01:31:37', '2017-11-04 01:31:37');
INSERT INTO `tbl_orders` VALUES(110, 10, 25, 0, '2017-11-04', '2017-11-04', '16:53:00', 233.00, 4.8, 'Normal', 'Paid', '', '', '2017-11-04 06:01:38', '', NULL, '2017-11-04 04:23:07', '2017-11-04 06:01:38');
INSERT INTO `tbl_orders` VALUES(111, 10, 25, 0, '2017-11-03', '2017-11-05', '20:56:00', 123.00, 4.8, 'Normal', 'Paid', '', '', '2017-11-04 06:15:51', '', NULL, '2017-11-04 05:55:54', '2017-11-04 06:15:51');
INSERT INTO `tbl_orders` VALUES(112, 10, 25, 0, '2017-11-03', '2017-11-04', '19:00:00', 50.00, 4.8, 'Normal', 'Delivery', '', 'Completed', '2017-11-07 06:46:20', '', NULL, '2017-11-04 05:58:29', '2017-11-07 06:46:20');
INSERT INTO `tbl_orders` VALUES(113, 10, 25, 12, '2017-11-04', '2017-12-04', '22:15:00', 67.00, 4.8, 'Package', 'Delivery', '', 'Completed', '2017-11-04 08:31:59', '', NULL, '2017-11-04 06:11:40', '2017-11-04 08:31:59');
INSERT INTO `tbl_orders` VALUES(114, 10, 25, 0, '2017-11-04', '2017-11-06', '20:16:00', 122.00, 4.8, 'Normal', 'Delivery', '', 'Completed', '2017-11-04 06:48:28', '', NULL, '2017-11-04 06:14:26', '2017-11-04 06:48:28');
INSERT INTO `tbl_orders` VALUES(115, 10, 25, 27, '2017-11-04', '2017-11-04', '20:04:00', 126.00, 4.8, 'Package', 'Confirmed', '', '', '2017-11-04 07:37:54', '', NULL, '2017-11-04 07:34:39', '2017-11-04 07:37:54');
INSERT INTO `tbl_orders` VALUES(116, 10, 25, 19, '2017-11-06', '2017-11-06', '15:59:00', 67.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 02:57:16', '2017-11-06 02:57:16');
INSERT INTO `tbl_orders` VALUES(117, 10, 25, 12, '2017-11-06', '2017-11-11', '16:29:00', 10.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 03:27:31', '2017-11-06 03:27:31');
INSERT INTO `tbl_orders` VALUES(118, 10, 25, 0, '2017-11-06', '2017-11-06', '16:36:00', 233.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 03:34:59', '2017-11-06 03:34:59');
INSERT INTO `tbl_orders` VALUES(119, 10, 25, 12, '2017-11-06', '2017-11-11', '16:50:00', 10.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 03:48:15', '2017-11-06 03:48:15');
INSERT INTO `tbl_orders` VALUES(120, 10, 25, 0, '2017-11-06', '2017-11-06', '16:50:00', 173.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 03:48:37', '2017-11-06 03:48:37');
INSERT INTO `tbl_orders` VALUES(121, 10, 6, 0, '2017-11-06', '2017-11-06', '17:00:00', 370.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 04:51:13', '2017-11-06 04:51:13');
INSERT INTO `tbl_orders` VALUES(122, 10, 6, 0, '2017-11-06', '2017-11-06', '17:00:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 04:51:43', '2017-11-06 04:51:43');
INSERT INTO `tbl_orders` VALUES(123, 10, 25, 0, '2017-11-06', '2017-11-06', '17:59:00', 173.00, 4.8, 'Normal', 'Cancel', '', '', '2017-11-07 05:33:57', 'No food available', NULL, '2017-11-06 04:57:16', '2017-11-07 05:33:57');
INSERT INTO `tbl_orders` VALUES(124, 10, 25, 12, '2017-11-06', '2017-11-11', '17:59:00', 10.00, 4.8, 'Package', 'Paid', '', '', '2017-11-07 05:34:47', '', NULL, '2017-11-06 04:57:36', '2017-11-07 05:34:47');
INSERT INTO `tbl_orders` VALUES(125, 10, 6, 0, '2017-11-06', '2017-11-06', '18:10:00', 672.00, 4.6, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-06 05:09:24', '2017-11-06 05:09:52');
INSERT INTO `tbl_orders` VALUES(126, 10, 6, 1, '2017-11-06', '2017-11-09', '18:13:00', 20.00, 5, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-06 05:11:26', '2017-11-07 04:03:56');
INSERT INTO `tbl_orders` VALUES(127, 10, 6, 2, '2017-11-06', '2017-11-11', '19:57:00', 10.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 07:27:47', '2017-11-06 07:27:47');
INSERT INTO `tbl_orders` VALUES(128, 10, 6, 0, '2017-11-06', '2017-11-06', '20:03:00', 1028.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_orders` VALUES(129, 10, 6, 2, '2017-11-07', '2017-11-12', '15:32:00', 10.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-07 04:03:03', '2017-11-07 04:03:03');
INSERT INTO `tbl_orders` VALUES(130, 43, 6, 0, '2017-11-08', '2017-11-08', '01:03:00', 120.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-08 03:33:37', '2017-11-08 03:33:37');
INSERT INTO `tbl_orders` VALUES(131, 43, 6, 0, '2017-11-23', '2017-11-23', '04:30:00', 120.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-08 03:42:07', '2017-11-08 03:42:07');
INSERT INTO `tbl_orders` VALUES(132, 43, 7, 0, '2017-11-08', '2017-11-08', '10:00:00', 429.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-08 03:44:17', '2017-11-08 03:44:17');
INSERT INTO `tbl_orders` VALUES(133, 43, 25, 0, '2017-11-08', '2017-11-08', '16:20:00', 167.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-08 03:51:27', '2017-11-08 03:51:27');
INSERT INTO `tbl_orders` VALUES(134, 43, 24, 0, '2017-11-18', '2017-11-18', '01:10:00', 100.00, 1, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-08 03:52:53', '2017-11-08 04:05:49');
INSERT INTO `tbl_orders` VALUES(135, 10, 44, 0, '2017-11-08', '2017-11-08', '06:00:00', 300.00, 0, 'Normal', 'Cancel', '', '', '2017-11-08 05:56:56', 'No paitence ', NULL, '2017-11-08 05:35:59', '2017-11-08 05:56:56');
INSERT INTO `tbl_orders` VALUES(136, 10, 44, 0, '2017-11-11', '2017-11-11', '18:15:00', 400.00, 3.5, 'Normal', 'Delivery', '', '', '2017-11-10 04:08:05', '', '', '2017-11-08 05:55:01', '2017-11-10 04:08:05');
INSERT INTO `tbl_orders` VALUES(137, 10, 44, 0, '2017-11-08', '2017-11-08', '18:40:00', 100.00, 1.3, 'Normal', 'Cancel', '', '', '2017-11-08 06:11:58', '    ', NULL, '2017-11-08 06:10:48', '2017-11-08 06:11:58');
INSERT INTO `tbl_orders` VALUES(138, 10, 0, 1, '2017-11-09', '2017-11-09', '17:00:00', 0.00, 3.6, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-09 02:23:47', '2017-11-10 06:01:29');
INSERT INTO `tbl_orders` VALUES(139, 10, 0, 1, '2017-11-09', '2017-11-12', '14:54:00', 0.00, 3.1, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-09 02:25:06', '2017-11-10 04:02:48');
INSERT INTO `tbl_orders` VALUES(140, 10, 44, 0, '2017-11-12', '2017-11-12', '00:18:00', 300.00, 1.6, 'Normal', 'Delivery', '', 'completed', '2017-11-10 01:14:38', '', NULL, '2017-11-10 12:49:23', '2017-11-10 01:14:38');
INSERT INTO `tbl_orders` VALUES(141, 10, 44, 0, '2017-11-11', '2017-11-11', '13:52:00', 600.00, 5, 'Normal', 'Delivery', '', '', '2017-11-10 04:08:02', '', '', '2017-11-10 01:23:16', '2017-11-13 02:27:36');
INSERT INTO `tbl_orders` VALUES(142, 10, 44, 0, '2017-11-10', '2017-11-10', '14:42:00', 300.00, 1.6, 'Normal', 'Paid', '', '', '2017-11-10 02:24:18', '', NULL, '2017-11-10 02:12:18', '2017-11-10 02:24:18');
INSERT INTO `tbl_orders` VALUES(143, 10, 44, 0, '2017-11-10', '2017-11-10', '15:02:00', 300.00, 2.8, 'Normal', 'Delivery', '', '', '2017-11-10 04:07:58', '', '', '2017-11-10 02:32:26', '2017-11-13 01:36:10');
INSERT INTO `tbl_orders` VALUES(144, 54, 44, 0, '2017-11-11', '2017-11-11', '04:00:00', 500.00, 1.6, 'Normal', 'Cancel', '', '', '2017-11-10 05:22:10', '    ', NULL, '2017-11-10 04:28:17', '2017-11-10 05:22:10');
INSERT INTO `tbl_orders` VALUES(145, 10, 0, 2, '2017-11-10', '2017-11-10', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-10 06:29:18', '2017-11-10 06:29:18');
INSERT INTO `tbl_orders` VALUES(146, 10, 0, 2, '2017-11-10', '2017-11-10', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-10 06:29:29', '2017-11-10 06:29:29');
INSERT INTO `tbl_orders` VALUES(147, 10, 6, 1, '2017-11-10', '2017-11-13', '18:59:00', 40.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-10 06:29:40', '2017-11-10 06:29:40');
INSERT INTO `tbl_orders` VALUES(148, 10, 25, 12, '2017-11-10', '2017-11-15', '20:06:00', 10.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-10 07:36:53', '2017-11-10 07:36:53');
INSERT INTO `tbl_orders` VALUES(149, 10, 44, 0, '2017-11-11', '2017-11-11', '18:08:00', 750.00, 1.6, 'Normal', 'Paid', '12345', 'completed', '2017-11-11 04:47:36', '', NULL, '2017-11-11 04:38:12', '2017-11-11 04:47:36');
INSERT INTO `tbl_orders` VALUES(150, 10, 44, 0, '2017-11-11', '2017-11-11', '17:26:00', 200.00, 1.6, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-11 04:56:14', '2017-11-11 04:56:14');
INSERT INTO `tbl_orders` VALUES(151, 10, 25, 0, '2017-11-11', '2017-11-11', '17:46:00', 173.00, 4.8, 'Normal', 'Paid', '12345', 'completed', '2017-11-13 02:20:39', '', NULL, '2017-11-11 05:15:31', '2017-11-13 02:20:39');
INSERT INTO `tbl_orders` VALUES(152, 10, 44, 0, '2017-11-12', '2017-11-12', '17:47:00', 100.00, 1.6, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-11 05:16:09', '2017-11-11 05:16:09');
INSERT INTO `tbl_orders` VALUES(153, 10, 6, 0, '2017-11-11', '2017-11-11', '18:31:00', 270.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-11 06:01:06', '2017-11-11 06:01:06');
INSERT INTO `tbl_orders` VALUES(154, 10, 6, 1, '2017-11-21', '2017-11-24', '18:33:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-11 06:02:57', '2017-11-20 02:35:23');
INSERT INTO `tbl_orders` VALUES(155, 10, 25, 12, '2017-11-13', '2017-11-18', '21:35:00', 10.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-11 06:05:03', '2017-11-11 06:05:03');
INSERT INTO `tbl_orders` VALUES(156, 10, 7, 0, '2017-11-12', '2017-11-12', '18:43:00', 234.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-11 06:12:06', '2017-11-11 06:12:06');
INSERT INTO `tbl_orders` VALUES(157, 10, 44, 0, '2017-11-11', '2017-11-11', '19:18:00', 400.00, 1.6, 'Normal', 'Paid', '12345', 'completed', '2017-11-13 06:29:35', '', NULL, '2017-11-11 06:47:23', '2017-11-13 06:29:35');
INSERT INTO `tbl_orders` VALUES(158, 10, 0, 1, '2017-11-14', '2017-11-17', '17:12:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 02:26:15', '2017-11-13 02:26:15');
INSERT INTO `tbl_orders` VALUES(159, 10, 0, 1, '2017-11-17', '2017-11-20', '15:10:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 02:40:35', '2017-11-13 02:40:35');
INSERT INTO `tbl_orders` VALUES(160, 52, 0, 1, '2017-11-13', '2017-11-13', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 04:12:50', '2017-11-13 04:12:50');
INSERT INTO `tbl_orders` VALUES(161, 67, 6, 1, '2017-11-19', '2017-11-22', '23:26:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 04:55:28', '2017-11-13 04:55:28');
INSERT INTO `tbl_orders` VALUES(162, 52, 0, 1, '2017-11-13', '2017-11-13', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 05:03:39', '2017-11-13 05:03:39');
INSERT INTO `tbl_orders` VALUES(163, 52, 0, 1, '2017-11-13', '2017-11-13', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 05:03:39', '2017-11-13 05:03:39');
INSERT INTO `tbl_orders` VALUES(164, 52, 0, 1, '2017-11-13', '2017-11-13', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 05:03:40', '2017-11-13 05:03:40');
INSERT INTO `tbl_orders` VALUES(165, 67, 6, 2, '2017-11-21', '2017-11-26', '23:16:00', 10.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 05:46:04', '2017-11-13 05:46:04');
INSERT INTO `tbl_orders` VALUES(166, 67, 6, 2, '2017-11-13', '2017-11-18', '18:17:00', 10.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 05:46:37', '2017-11-13 05:46:37');
INSERT INTO `tbl_orders` VALUES(167, 67, 44, 0, '2017-11-14', '2017-11-14', '19:17:00', 390.00, 4.3, 'Normal', 'Delivery', '12345', 'completed', '2017-11-13 05:52:06', '', '', '2017-11-13 05:46:48', '2017-11-13 05:53:10');
INSERT INTO `tbl_orders` VALUES(168, 67, 24, 0, '2017-11-13', '2017-11-13', '18:43:00', 85.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 06:12:12', '2017-11-13 06:12:12');
INSERT INTO `tbl_orders` VALUES(169, 67, 44, 0, '2017-11-13', '2017-11-13', '19:05:00', 635.00, 2.2, 'Normal', 'Confirm', '12345', 'completed', '2017-11-13 06:18:14', '', NULL, '2017-11-13 06:17:10', '2017-11-13 06:18:14');
INSERT INTO `tbl_orders` VALUES(170, 10, 44, 0, '2017-11-13', '2017-11-13', '18:58:00', 500.00, 2.2, 'Normal', 'Paid', '12345', 'completed', '2017-11-13 06:29:29', '', NULL, '2017-11-13 06:27:11', '2017-11-13 06:29:29');
INSERT INTO `tbl_orders` VALUES(171, 10, 25, 12, '2017-11-13', '2017-11-18', '19:31:00', 10.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 06:28:17', '2017-11-13 06:28:17');
INSERT INTO `tbl_orders` VALUES(172, 10, 0, 1, '2017-11-20', '2017-11-23', '15:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-13 08:55:55', '2017-11-13 08:55:55');
INSERT INTO `tbl_orders` VALUES(173, 50, 0, 2, '2017-11-14', '2017-11-19', '13:49:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 01:19:21', '2017-11-14 01:19:21');
INSERT INTO `tbl_orders` VALUES(174, 10, 6, 1, '2017-11-29', '2017-12-02', '14:16:00', 20.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-14 01:46:34', '2017-11-20 02:35:11');
INSERT INTO `tbl_orders` VALUES(175, 10, 24, 4, '2017-11-14', '2017-11-18', '14:19:00', 4.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 01:48:27', '2017-11-14 01:48:27');
INSERT INTO `tbl_orders` VALUES(176, 10, 6, 2, '2017-11-14', '2017-11-19', '14:20:00', 10.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 01:49:26', '2017-11-14 01:49:26');
INSERT INTO `tbl_orders` VALUES(177, 50, 0, 2, '2017-11-14', '2017-11-19', '14:29:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 01:59:26', '2017-11-14 01:59:26');
INSERT INTO `tbl_orders` VALUES(178, 10, 25, 0, '2017-11-14', '2017-11-14', '14:41:00', 173.00, 4.8, 'Normal', 'Cancel', '12345', 'completed', '2017-11-14 01:42:27', 'Due to power cut', NULL, '2017-11-14 01:38:08', '2017-11-14 01:42:27');
INSERT INTO `tbl_orders` VALUES(179, 10, 25, 12, '2017-11-14', '2017-11-19', '14:41:00', 10.00, 4.8, 'Package', 'Cancel', '12345', 'completed', '2017-11-14 01:40:06', 'Selected dish not available', NULL, '2017-11-14 01:38:23', '2017-11-14 01:40:06');
INSERT INTO `tbl_orders` VALUES(180, 10, 25, 0, '2017-11-14', '2017-11-14', '14:41:00', 359.00, 4.8, 'Normal', 'Cancel', '12345', 'completed', '2017-11-14 02:42:57', 'No package availbale', NULL, '2017-11-14 01:38:41', '2017-11-14 02:42:57');
INSERT INTO `tbl_orders` VALUES(181, 50, 0, 2, '2017-11-14', '2017-11-19', '14:46:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 02:16:36', '2017-11-14 02:16:36');
INSERT INTO `tbl_orders` VALUES(182, 50, 0, 30, '2017-11-14', '2017-11-19', '14:51:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 02:21:35', '2017-11-14 02:21:35');
INSERT INTO `tbl_orders` VALUES(183, 50, 0, 31, '2017-11-14', '2017-11-19', '14:56:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 02:26:23', '2017-11-14 02:26:23');
INSERT INTO `tbl_orders` VALUES(184, 50, 0, 1, '2017-11-14', '2017-11-17', '15:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 02:30:25', '2017-11-14 02:30:25');
INSERT INTO `tbl_orders` VALUES(185, 10, 25, 0, '2017-11-18', '2017-11-18', '15:12:00', 233.00, 0.2, 'Normal', 'Cancel', '12345', 'completed', '2017-11-14 02:54:37', 'No cooking today', '', '2017-11-14 02:41:43', '2017-11-16 05:12:02');
INSERT INTO `tbl_orders` VALUES(186, 10, 25, 0, '2017-11-14', '2017-11-14', '15:12:00', 345512.00, 4.8, 'Normal', 'Cancel', '12345', 'completed', '2017-11-14 02:55:39', 'Leave today', NULL, '2017-11-14 02:41:57', '2017-11-14 02:55:39');
INSERT INTO `tbl_orders` VALUES(187, 50, 0, 1, '2017-11-14', '2017-11-17', '15:21:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 02:51:00', '2017-11-14 02:51:00');
INSERT INTO `tbl_orders` VALUES(188, 52, 0, 1, '2017-11-14', '2017-11-17', '15:39:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 03:08:57', '2017-11-14 03:08:57');
INSERT INTO `tbl_orders` VALUES(189, 50, 0, 19, '2017-11-14', '2017-11-20', '17:59:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 05:29:36', '2017-11-14 05:29:36');
INSERT INTO `tbl_orders` VALUES(190, 50, 0, 12, '2017-11-14', '2017-11-19', '18:09:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 05:39:10', '2017-11-14 05:39:10');
INSERT INTO `tbl_orders` VALUES(191, 50, 0, 4, '2017-11-14', '2017-11-18', '18:24:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 05:54:58', '2017-11-14 05:54:58');
INSERT INTO `tbl_orders` VALUES(192, 50, 0, 2, '2017-11-14', '2017-11-19', '18:28:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 05:59:25', '2017-11-14 05:59:25');
INSERT INTO `tbl_orders` VALUES(193, 50, 0, 25, '2017-11-14', '2017-11-20', '18:32:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 06:02:19', '2017-11-14 06:02:19');
INSERT INTO `tbl_orders` VALUES(194, 50, 0, 2, '2017-11-14', '2017-11-19', '18:46:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 06:16:16', '2017-11-14 06:16:16');
INSERT INTO `tbl_orders` VALUES(195, 10, 6, 0, '2017-11-14', '2017-11-14', '18:55:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 06:24:45', '2017-11-14 06:24:45');
INSERT INTO `tbl_orders` VALUES(196, 10, 25, 19, '2017-11-14', '2017-11-20', '19:48:00', 57.00, 4.8, 'Package', 'Started', '12345', 'completed', '2017-11-14 07:19:30', '', NULL, '2017-11-14 07:17:44', '2017-11-14 07:19:30');
INSERT INTO `tbl_orders` VALUES(197, 10, 0, 1, '2017-11-16', '2017-11-19', '04:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 11:08:17', '2017-11-14 11:08:17');
INSERT INTO `tbl_orders` VALUES(198, 10, 0, 1, '2017-11-16', '2017-11-19', '07:28:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 11:10:39', '2017-11-14 11:10:39');
INSERT INTO `tbl_orders` VALUES(199, 10, 0, 1, '2017-11-15', '2017-11-18', '11:41:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-14 11:11:23', '2017-11-14 11:11:23');
INSERT INTO `tbl_orders` VALUES(200, 41, 44, 0, '2017-11-18', '2017-11-18', '12:41:00', 385.00, 5, 'Normal', 'Delivery', '12345', 'completed', '2017-11-14 11:46:14', '', '', '2017-11-14 11:37:00', '2017-11-15 12:28:02');
INSERT INTO `tbl_orders` VALUES(201, 41, 7, 0, '2017-11-15', '2017-11-15', '13:04:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 12:33:17', '2017-11-15 12:33:17');
INSERT INTO `tbl_orders` VALUES(202, 50, 0, 1, '2017-11-15', '2017-11-18', '13:32:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:02:15', '2017-11-15 01:02:15');
INSERT INTO `tbl_orders` VALUES(203, 50, 0, 1, '2017-11-15', '2017-11-18', '13:40:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:10:20', '2017-11-15 01:10:20');
INSERT INTO `tbl_orders` VALUES(204, 50, 0, 1, '2017-11-15', '2017-11-18', '13:43:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:13:07', '2017-11-15 01:13:07');
INSERT INTO `tbl_orders` VALUES(205, 10, 25, 0, '2017-11-15', '2017-11-15', '13:51:00', 173.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:20:40', '2017-11-15 01:20:40');
INSERT INTO `tbl_orders` VALUES(206, 50, 0, 2, '2017-11-15', '2017-11-20', '14:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:30:34', '2017-11-15 01:30:34');
INSERT INTO `tbl_orders` VALUES(207, 10, 25, 0, '2017-11-15', '2017-11-15', '14:03:00', 173.00, 4.8, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:32:40', '2017-11-15 01:32:40');
INSERT INTO `tbl_orders` VALUES(208, 50, 0, 1, '2017-11-15', '2017-11-18', '14:05:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 01:35:05', '2017-11-15 01:35:05');
INSERT INTO `tbl_orders` VALUES(210, 10, 44, 45, '2017-11-15', '2017-11-30', '19:00:00', 500.00, 2.3, 'Package', 'Delivery', '12345', 'completed', '2017-11-15 02:53:29', '', NULL, '2017-11-15 02:23:44', '2017-11-15 02:53:29');
INSERT INTO `tbl_orders` VALUES(211, 10, 6, 0, '2017-11-15', '2017-11-15', '16:46:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 04:15:23', '2017-11-15 04:15:23');
INSERT INTO `tbl_orders` VALUES(212, 10, 44, 0, '2017-11-15', '2017-11-15', '16:48:00', 690.00, 2.3, 'Normal', 'Delivery', '12345', 'completed', '2017-11-15 04:28:59', '', NULL, '2017-11-15 04:17:47', '2017-11-15 04:28:59');
INSERT INTO `tbl_orders` VALUES(213, 51, 0, 4, '2017-11-16', '2017-11-20', '11:34:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 11:04:49', '2017-11-15 11:04:49');
INSERT INTO `tbl_orders` VALUES(214, 10, 0, 1, '2017-11-17', '2017-11-20', '16:29:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-15 11:42:08', '2017-11-15 11:42:08');
INSERT INTO `tbl_orders` VALUES(215, 50, 0, 3, '2017-11-16', '2017-11-19', '12:59:00', 0.00, 4.1, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-16 12:29:46', '2017-11-16 12:38:35');
INSERT INTO `tbl_orders` VALUES(216, 10, 0, 45, '2017-11-16', '2017-11-16', '17:00:00', 0.00, 2.3, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-16 04:40:18', '2017-11-16 04:40:18');
INSERT INTO `tbl_orders` VALUES(217, 10, 0, 12, '2017-11-16', '2017-11-21', '18:10:00', 0.00, 4.8, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-16 04:48:29', '2017-11-16 04:48:29');
INSERT INTO `tbl_orders` VALUES(218, 10, 0, 45, '2017-11-16', '2017-11-16', '17:00:00', 0.00, 2.3, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-16 04:54:57', '2017-11-16 04:54:57');
INSERT INTO `tbl_orders` VALUES(219, 10, 0, 45, '2017-11-16', '2017-11-16', '17:00:00', 0.00, 0.7, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-16 05:08:16', '2017-11-16 05:08:58');
INSERT INTO `tbl_orders` VALUES(220, 10, 0, 45, '2017-11-16', '2017-12-01', '20:15:00', 0.00, 2.3, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-16 05:15:03', '2017-11-16 05:25:57');
INSERT INTO `tbl_orders` VALUES(221, 10, 0, 3, '2017-11-16', '2017-11-16', '17:00:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-16 05:29:32', '2017-11-16 05:29:32');
INSERT INTO `tbl_orders` VALUES(222, 10, 0, 4, '2017-11-16', '2017-11-16', '17:00:00', 0.00, 10.2, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-16 05:30:00', '2017-11-16 06:07:40');
INSERT INTO `tbl_orders` VALUES(223, 10, 0, 1, '2017-11-17', '2017-11-20', '15:28:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-16 06:19:52', '2017-11-16 06:19:52');
INSERT INTO `tbl_orders` VALUES(224, 10, 6, 1, '2017-11-16', '2017-11-19', '18:50:00', 40.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-16 06:19:56', '2017-11-16 06:48:52');
INSERT INTO `tbl_orders` VALUES(225, 50, 0, 0, '2017-11-17', '2017-11-17', '12:51:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 12:21:32', '2017-11-17 12:21:32');
INSERT INTO `tbl_orders` VALUES(226, 50, 0, 0, '2017-11-17', '2017-11-17', '12:55:00', 100.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 12:25:13', '2017-11-17 12:25:13');
INSERT INTO `tbl_orders` VALUES(227, 50, 0, 0, '2017-11-17', '2017-11-17', '13:00:00', 316.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 12:30:26', '2017-11-17 12:30:26');
INSERT INTO `tbl_orders` VALUES(228, 50, 0, 0, '2017-11-17', '2017-11-17', '17:38:00', 100.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:08:18', '2017-11-17 05:08:18');
INSERT INTO `tbl_orders` VALUES(229, 50, 0, 3, '2017-11-17', '2017-11-20', '16:42:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:12:30', '2017-11-17 05:12:30');
INSERT INTO `tbl_orders` VALUES(230, 10, 7, 0, '2017-11-17', '2017-11-17', '17:43:00', 93.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:12:58', '2017-11-17 05:12:58');
INSERT INTO `tbl_orders` VALUES(231, 50, 0, 3, '2017-11-17', '2017-11-17', '17:00:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:15:15', '2017-11-17 05:15:15');
INSERT INTO `tbl_orders` VALUES(232, 50, 0, 0, '2017-11-17', '2017-11-17', '17:00:00', 50.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:47:32', '2017-11-17 05:47:32');
INSERT INTO `tbl_orders` VALUES(233, 50, 0, 1, '2017-11-17', '2017-11-17', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 05:48:09', '2017-11-17 05:48:09');
INSERT INTO `tbl_orders` VALUES(234, 50, 0, 2, '2017-11-17', '2017-11-17', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-17 05:50:09', '2017-11-17 06:07:16');
INSERT INTO `tbl_orders` VALUES(235, 50, 0, 1, '2017-11-17', '2017-11-17', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 06:33:16', '2017-11-17 06:33:16');
INSERT INTO `tbl_orders` VALUES(236, 50, 0, 1, '2017-11-17', '2017-11-17', '17:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 06:33:59', '2017-11-17 06:33:59');
INSERT INTO `tbl_orders` VALUES(237, 50, 0, 3, '2017-11-17', '2017-11-20', '20:18:00', 0.00, 2.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 06:48:36', '2017-11-17 06:48:36');
INSERT INTO `tbl_orders` VALUES(238, 50, 0, 2, '2017-11-17', '2017-11-22', '16:50:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 07:00:45', '2017-11-17 07:00:45');
INSERT INTO `tbl_orders` VALUES(239, 50, 0, 0, '2017-11-17', '2017-11-17', '17:00:00', 150.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 07:02:29', '2017-11-17 07:02:29');
INSERT INTO `tbl_orders` VALUES(240, 50, 0, 0, '2017-11-22', '2017-11-22', '22:38:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-17 07:08:04', '2017-11-17 07:08:04');
INSERT INTO `tbl_orders` VALUES(241, 50, 0, 1, '2017-11-18', '2017-11-21', '13:42:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-18 12:40:09', '2017-11-18 12:40:09');
INSERT INTO `tbl_orders` VALUES(242, 50, 0, 0, '2017-11-20', '2017-11-20', '14:01:00', 0.00, 0, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-19 11:31:18', '2017-11-20 05:01:49');
INSERT INTO `tbl_orders` VALUES(243, 50, 0, 0, '2017-11-21', '2017-11-21', '15:09:00', 36.00, 0, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-19 11:39:30', '2017-11-19 11:39:30');
INSERT INTO `tbl_orders` VALUES(244, 10, 25, 12, '2017-11-20', '2017-11-25', '12:52:00', 10.00, 4.7, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-20 12:21:46', '2017-11-20 12:21:46');
INSERT INTO `tbl_orders` VALUES(245, 10, 25, 0, '2017-11-20', '2017-11-20', '12:53:00', 233.00, 4.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-20 12:22:56', '2017-11-20 01:37:05');
INSERT INTO `tbl_orders` VALUES(246, 10, 85, 0, '2017-11-21', '2017-11-21', '12:57:00', 36.00, 0, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-20 12:26:24', '2017-11-20 12:26:24');
INSERT INTO `tbl_orders` VALUES(247, 10, 0, 0, '2017-11-23', '2017-11-23', '16:45:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-20 02:16:36', '2017-11-20 02:16:36');
INSERT INTO `tbl_orders` VALUES(248, 10, 0, 12, '2017-11-20', '2017-11-25', '18:47:00', 0.00, 4.7, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-20 02:18:38', '2017-11-20 02:42:48');
INSERT INTO `tbl_orders` VALUES(249, 10, 0, 0, '2017-11-20', '2017-11-20', '15:01:00', 290.00, 0.5, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-20 02:32:59', '2017-11-20 03:37:46');
INSERT INTO `tbl_orders` VALUES(250, 50, 0, 0, '2017-11-20', '2017-11-20', '17:35:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-20 05:05:33', '2017-11-20 05:05:33');
INSERT INTO `tbl_orders` VALUES(251, 10, 0, 0, '2017-11-21', '2017-11-21', '15:46:00', 285.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-21 03:17:30', '2017-11-21 03:17:30');
INSERT INTO `tbl_orders` VALUES(252, 10, 0, 0, '2017-11-21', '2017-11-21', '18:08:00', 393.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-21 04:39:30', '2017-11-21 04:39:30');
INSERT INTO `tbl_orders` VALUES(253, 10, 0, 0, '2017-11-23', '2017-11-23', '06:25:00', 32.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-21 10:42:17', '2017-11-21 10:42:17');
INSERT INTO `tbl_orders` VALUES(254, 10, 0, 1, '2017-11-23', '2017-11-26', '04:19:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-21 10:49:07', '2017-11-21 10:49:07');
INSERT INTO `tbl_orders` VALUES(255, 50, 0, 1, '2017-11-22', '2017-11-25', '12:00:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-21 11:30:21', '2017-11-22 12:27:40');
INSERT INTO `tbl_orders` VALUES(256, 50, 0, 2, '2017-11-22', '2017-11-27', '12:23:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-21 11:53:43', '2017-11-21 11:53:43');
INSERT INTO `tbl_orders` VALUES(257, 50, 0, 0, '2017-11-22', '2017-11-22', '12:36:00', 220.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:06:37', '2017-11-22 12:06:37');
INSERT INTO `tbl_orders` VALUES(258, 50, 0, 0, '2017-11-22', '2017-11-22', '12:54:00', 100.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:24:42', '2017-11-22 12:24:42');
INSERT INTO `tbl_orders` VALUES(259, 50, 0, 0, '2017-11-22', '2017-11-22', '12:55:00', 172.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:25:52', '2017-11-22 12:25:52');
INSERT INTO `tbl_orders` VALUES(260, 10, 0, 0, '2017-11-22', '2017-11-22', '13:00:00', 25.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:30:45', '2017-11-22 12:30:45');
INSERT INTO `tbl_orders` VALUES(261, 10, 0, 0, '2017-11-22', '2017-11-22', '13:07:00', 234.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:37:46', '2017-11-22 12:37:46');
INSERT INTO `tbl_orders` VALUES(262, 10, 0, 0, '2017-11-22', '2017-11-22', '13:12:00', 484.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 12:42:58', '2017-11-22 12:42:58');
INSERT INTO `tbl_orders` VALUES(263, 10, 0, 0, '2017-11-22', '2017-11-22', '15:15:00', 100.00, 4.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, '', '2017-11-22 12:45:04', '2017-11-22 12:46:25');
INSERT INTO `tbl_orders` VALUES(264, 10, 25, 0, '2017-11-22', '2017-11-22', '13:20:00', 608.00, 4.7, 'Normal', 'Cancel', '12345', 'completed', '2017-11-22 07:03:15', 'I am available today', NULL, '2017-11-22 12:49:51', '2017-11-22 07:03:15');
INSERT INTO `tbl_orders` VALUES(265, 10, 25, 0, '2017-11-22', '2017-11-22', '13:33:00', 730.00, 4.7, 'Normal', 'Cancel', '12345', 'completed', '2017-11-22 07:02:28', 'No cooking today bcc rain', NULL, '2017-11-22 01:02:52', '2017-11-22 07:02:28');
INSERT INTO `tbl_orders` VALUES(266, 10, 25, 12, '2017-11-22', '2017-11-27', '13:36:00', 10.00, 4.7, 'Package', 'Paid', '12345', 'completed', '2017-11-22 07:05:07', '', NULL, '2017-11-22 01:05:02', '2017-11-22 07:05:07');
INSERT INTO `tbl_orders` VALUES(267, 79, 0, 0, '2017-11-22', '2017-11-22', '13:37:00', 50.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:07:05', '2017-11-22 01:07:05');
INSERT INTO `tbl_orders` VALUES(268, 50, 0, 0, '2017-11-22', '2017-11-22', '13:43:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:13:31', '2017-11-22 01:13:31');
INSERT INTO `tbl_orders` VALUES(269, 50, 0, 0, '2017-11-22', '2017-11-22', '13:52:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:22:32', '2017-11-22 01:22:32');
INSERT INTO `tbl_orders` VALUES(270, 79, 0, 0, '2017-11-22', '2017-11-22', '13:57:00', 276.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:27:17', '2017-11-22 01:27:17');
INSERT INTO `tbl_orders` VALUES(271, 79, 0, 0, '2017-11-22', '2017-11-22', '14:05:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:35:03', '2017-11-22 01:35:03');
INSERT INTO `tbl_orders` VALUES(272, 79, 7, 0, '2017-11-22', '2017-11-22', '14:10:00', 276.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:40:52', '2017-11-22 01:40:52');
INSERT INTO `tbl_orders` VALUES(273, 79, 24, 0, '2017-11-22', '2017-11-22', '14:17:00', 50.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:47:33', '2017-11-22 01:47:33');
INSERT INTO `tbl_orders` VALUES(274, 79, 6, 1, '2017-11-22', '2017-11-25', '14:17:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 01:48:00', '2017-11-22 01:48:00');
INSERT INTO `tbl_orders` VALUES(275, 79, 6, 2, '2017-11-22', '2017-11-27', '15:13:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 02:43:05', '2017-11-22 02:43:05');
INSERT INTO `tbl_orders` VALUES(276, 50, 7, 0, '2017-11-22', '2017-11-22', '15:15:00', 336.00, 3.7, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 02:45:54', '2017-11-22 02:45:54');
INSERT INTO `tbl_orders` VALUES(277, 50, 6, 1, '2017-11-20', '2017-11-23', '16:09:00', 0.00, 4.4, 'Package', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 03:39:39', '2017-11-22 03:39:39');
INSERT INTO `tbl_orders` VALUES(278, 10, 24, 0, '2017-11-22', '2017-11-22', '19:25:00', 50.00, 2.4, 'Normal', 'pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 06:54:09', '2017-11-22 06:54:09');
INSERT INTO `tbl_orders` VALUES(279, 0, 6, 1, '2017-11-22', '2017-11-25', '19:38:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:08:12', '2017-11-22 07:08:12');
INSERT INTO `tbl_orders` VALUES(280, 0, 24, 0, '2017-11-22', '2017-11-22', '19:38:00', 50.00, 2.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:08:34', '2017-11-22 07:08:34');
INSERT INTO `tbl_orders` VALUES(281, 102, 6, 1, '2017-11-22', '2017-11-25', '19:47:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:17:15', '2017-11-22 07:17:15');
INSERT INTO `tbl_orders` VALUES(282, 102, 6, 0, '2017-11-22', '2017-11-22', '19:47:00', 100.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:17:31', '2017-11-22 07:17:31');
INSERT INTO `tbl_orders` VALUES(283, 0, 6, 0, '2017-11-30', '2017-11-30', '17:43:00', 100.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:23:47', '2017-11-22 07:23:47');
INSERT INTO `tbl_orders` VALUES(284, 104, 6, 0, '2017-11-22', '2017-11-22', '20:05:00', 154.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:34:09', '2017-11-22 07:34:09');
INSERT INTO `tbl_orders` VALUES(285, 104, 6, 1, '2017-11-22', '2017-11-25', '20:07:00', 20.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:36:47', '2017-11-22 07:36:47');
INSERT INTO `tbl_orders` VALUES(286, 103, 6, 0, '2017-11-22', '2017-11-22', '20:34:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:04:11', '2017-11-22 08:04:11');
INSERT INTO `tbl_orders` VALUES(287, 105, 6, 0, '2017-11-22', '2017-11-22', '20:45:00', 100.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:15:36', '2017-11-22 08:15:36');
INSERT INTO `tbl_orders` VALUES(288, 10, 6, 0, '2017-11-22', '2017-11-22', '20:52:00', 795.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_orders` VALUES(289, 105, 6, 0, '2017-11-22', '2017-11-22', '21:00:00', 1028.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:31:00', '2017-11-22 08:31:00');
INSERT INTO `tbl_orders` VALUES(290, 105, 6, 0, '2017-11-22', '2017-11-22', '21:02:00', 1028.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:32:29', '2017-11-22 08:32:29');
INSERT INTO `tbl_orders` VALUES(291, 105, 6, 0, '2017-11-22', '2017-11-22', '21:05:00', 672.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 08:35:57', '2017-11-22 08:35:57');
INSERT INTO `tbl_orders` VALUES(292, 10, 24, 3, '2017-11-23', '2017-11-26', '08:12:00', 4.00, 2.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 07:41:09', '2017-11-22 07:41:09');
INSERT INTO `tbl_orders` VALUES(293, 10, 25, 12, '2017-11-23', '2017-11-28', '11:59:00', 10.00, 4.7, 'Package', 'Paid', '12345', 'completed', '2017-11-23 11:25:12', '', NULL, '2017-11-22 10:55:44', '2017-11-23 11:25:12');
INSERT INTO `tbl_orders` VALUES(294, 10, 25, 0, '2017-11-23', '2017-11-23', '11:59:00', 558.00, 4.7, 'Normal', 'Confirm', '', '', '2017-11-23 06:24:01', '', NULL, '2017-11-22 10:56:27', '2017-11-23 06:24:01');
INSERT INTO `tbl_orders` VALUES(295, 10, 25, 0, '2017-11-23', '2017-11-23', '12:00:00', 608.00, 4.7, 'Normal', 'Cancel', '12345', 'completed', '2017-11-22 11:15:51', '295', NULL, '2017-11-22 10:57:06', '2017-11-22 11:15:51');
INSERT INTO `tbl_orders` VALUES(296, 107, 6, 0, '2017-11-21', '2017-11-21', '12:42:00', 500.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 11:40:13', '2017-11-22 11:40:13');
INSERT INTO `tbl_orders` VALUES(297, 107, 6, 0, '2017-11-21', '2017-11-21', '12:47:00', 370.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-22 11:45:39', '2017-11-22 11:45:39');
INSERT INTO `tbl_orders` VALUES(298, 107, 6, 0, '2017-11-21', '2017-11-21', '13:18:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:16:13', '2017-11-23 12:16:13');
INSERT INTO `tbl_orders` VALUES(299, 107, 6, 0, '2017-11-21', '2017-11-21', '13:19:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:16:51', '2017-11-23 12:16:51');
INSERT INTO `tbl_orders` VALUES(300, 107, 6, 1, '2017-11-21', '2017-11-24', '13:26:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:24:27', '2017-11-23 12:24:27');
INSERT INTO `tbl_orders` VALUES(301, 107, 6, 0, '2017-11-23', '2017-11-23', '13:41:00', 370.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:39:13', '2017-11-23 12:39:13');
INSERT INTO `tbl_orders` VALUES(302, 107, 7, 0, '2017-11-23', '2017-11-23', '13:45:00', 429.00, 3.7, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:43:34', '2017-11-23 12:43:34');
INSERT INTO `tbl_orders` VALUES(303, 107, 24, 0, '2017-11-24', '2017-11-24', '15:00:00', 85.00, 2.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:47:50', '2017-11-23 12:47:50');
INSERT INTO `tbl_orders` VALUES(304, 107, 7, 0, '2017-11-23', '2017-11-23', '13:55:00', 539.00, 3.7, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_orders` VALUES(305, 50, 6, 0, '2017-11-23', '2017-11-23', '16:40:00', 500.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 01:38:32', '2017-11-23 01:38:32');
INSERT INTO `tbl_orders` VALUES(306, 107, 6, 0, '2017-11-23', '2017-11-23', '16:00:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 02:58:29', '2017-11-23 02:58:29');
INSERT INTO `tbl_orders` VALUES(307, 107, 25, 0, '2017-11-23', '2017-11-23', '18:00:00', 1221.00, 4.7, 'Normal', 'Paid', '123456', 'completed', '2017-11-23 07:25:12', '', NULL, '2017-11-23 03:04:43', '2017-11-23 07:25:12');
INSERT INTO `tbl_orders` VALUES(308, 107, 6, 0, '2017-11-23', '2017-11-23', '16:10:00', 856.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 03:07:54', '2017-11-23 03:07:54');
INSERT INTO `tbl_orders` VALUES(309, 107, 6, 0, '2017-11-23', '2017-11-23', '16:29:00', 500.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 03:27:10', '2017-11-23 03:27:10');
INSERT INTO `tbl_orders` VALUES(310, 107, 7, 0, '2017-11-23', '2017-11-23', '20:20:00', 539.00, 3.7, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 03:54:50', '2017-11-23 03:54:50');
INSERT INTO `tbl_orders` VALUES(311, 107, 25, 0, '2017-11-23', '2017-11-23', '17:00:00', 664.00, 4.7, 'Normal', 'Cancel', '12345', 'completed', '2017-11-23 04:32:36', 'Nig', NULL, '2017-11-23 03:56:49', '2017-11-23 04:32:36');
INSERT INTO `tbl_orders` VALUES(312, 107, 6, 0, '2017-11-23', '2017-11-23', '17:14:00', 500.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 04:11:44', '2017-11-23 04:11:44');
INSERT INTO `tbl_orders` VALUES(313, 107, 6, 0, '2017-11-23', '2017-11-23', '17:15:00', 100.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 04:13:16', '2017-11-23 04:13:16');
INSERT INTO `tbl_orders` VALUES(314, 107, 6, 1, '2017-11-23', '2017-11-26', '17:16:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 04:13:58', '2017-11-23 04:13:58');
INSERT INTO `tbl_orders` VALUES(315, 107, 25, 0, '2017-11-23', '2017-11-23', '19:19:00', 608.00, 4.7, 'Normal', 'Started', '12345', 'completed', '2017-11-23 04:32:15', '', NULL, '2017-11-23 04:16:57', '2017-11-23 04:32:15');
INSERT INTO `tbl_orders` VALUES(316, 107, 6, 2, '2017-11-23', '2017-11-28', '18:18:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 05:48:25', '2017-11-23 05:48:25');
INSERT INTO `tbl_orders` VALUES(317, 107, 6, 1, '2017-11-23', '2017-11-26', '18:26:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 05:56:38', '2017-11-23 05:56:38');
INSERT INTO `tbl_orders` VALUES(318, 110, 6, 0, '2017-11-25', '2017-11-25', '05:25:00', 118.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 11:17:28', '2017-11-23 11:17:28');
INSERT INTO `tbl_orders` VALUES(319, 110, 6, 1, '2017-11-30', '2017-12-03', '14:49:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 11:20:14', '2017-11-23 11:20:14');
INSERT INTO `tbl_orders` VALUES(320, 109, 6, 0, '2017-11-24', '2017-11-24', '12:12:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 11:43:00', '2017-11-23 11:43:00');
INSERT INTO `tbl_orders` VALUES(321, 109, 6, 1, '2017-11-24', '2017-11-27', '12:13:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 11:44:03', '2017-11-23 11:44:03');
INSERT INTO `tbl_orders` VALUES(322, 109, 6, 2, '2017-11-24', '2017-11-29', '12:14:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-23 11:44:57', '2017-11-23 11:44:57');
INSERT INTO `tbl_orders` VALUES(323, 10, 6, 0, '2017-11-24', '2017-11-24', '13:11:00', 370.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-24 12:40:51', '2017-11-24 12:40:51');
INSERT INTO `tbl_orders` VALUES(324, 10, 25, 55, '2017-11-24', '2017-11-29', '14:34:00', 100.00, 4.7, 'Package', 'Cancel', '', '', '2017-11-24 02:39:23', 'ffdh', NULL, '2017-11-24 02:03:46', '2017-11-24 02:39:23');
INSERT INTO `tbl_orders` VALUES(325, 10, 25, 0, '2017-11-24', '2017-11-24', '15:27:00', 435.00, 4.7, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-24 02:23:37', '2017-11-24 02:23:37');
INSERT INTO `tbl_orders` VALUES(326, 10, 6, 0, '2017-11-24', '2017-11-24', '15:33:00', 220.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-24 02:31:26', '2017-11-24 02:31:26');
INSERT INTO `tbl_orders` VALUES(327, 109, 6, 0, '2017-11-24', '2017-11-24', '16:27:00', 1028.00, 4.4, 'Normal', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_orders` VALUES(328, 110, 6, 1, '2017-11-28', '2017-12-01', '17:26:00', 0.00, 4.4, 'Package', 'Pending', '', '', '0000-00-00 00:00:00', NULL, NULL, '2017-11-24 03:35:54', '2017-11-24 03:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_items`
--

CREATE TABLE `tbl_order_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=276 ;

--
-- Dumping data for table `tbl_order_items`
--

INSERT INTO `tbl_order_items` VALUES(1, 3, 1, 6, 150.00, '2017-10-06 10:24:24', '2017-10-06 10:24:24');
INSERT INTO `tbl_order_items` VALUES(2, 4, 1, 2, 150.00, '2017-10-06 08:21:25', '2017-10-06 11:28:27');
INSERT INTO `tbl_order_items` VALUES(3, 5, 3, 3, 200.00, '2017-10-06 10:24:24', '2017-10-06 10:24:24');
INSERT INTO `tbl_order_items` VALUES(4, 3, 2, 2, 150.00, '2017-10-06 08:21:25', '2017-10-06 11:28:27');
INSERT INTO `tbl_order_items` VALUES(5, 4, 5, 4, 50.00, '2017-10-06 10:24:24', '2017-10-06 10:24:24');
INSERT INTO `tbl_order_items` VALUES(6, 5, 5, 4, 50.00, '2017-10-06 08:21:25', '2017-10-06 11:28:27');
INSERT INTO `tbl_order_items` VALUES(7, 5, 4, 7, 200.00, '2017-10-06 10:24:24', '2017-10-06 10:24:24');
INSERT INTO `tbl_order_items` VALUES(8, 4, 4, 5, 200.00, '2017-10-06 08:21:25', '2017-10-06 11:28:27');
INSERT INTO `tbl_order_items` VALUES(9, 11, 4, 1, 18.00, '2017-10-26 05:05:32', '2017-10-26 05:05:32');
INSERT INTO `tbl_order_items` VALUES(10, 11, 5, 2, 18.00, '2017-10-26 05:05:32', '2017-10-26 05:05:32');
INSERT INTO `tbl_order_items` VALUES(11, 26, 12, 12323, 18.00, '2017-10-27 05:03:18', '2017-10-27 05:03:18');
INSERT INTO `tbl_order_items` VALUES(12, 26, 12, 12323, 18.00, '2017-10-27 05:03:18', '2017-10-27 05:03:18');
INSERT INTO `tbl_order_items` VALUES(13, 27, 1, 1, 18.00, '2017-10-27 05:04:45', '2017-10-27 05:04:45');
INSERT INTO `tbl_order_items` VALUES(14, 27, 1, 2, 18.00, '2017-10-27 05:04:45', '2017-10-27 05:04:45');
INSERT INTO `tbl_order_items` VALUES(15, 35, 12, 3, 18.00, '2017-10-28 12:23:24', '2017-10-28 12:23:24');
INSERT INTO `tbl_order_items` VALUES(16, 35, 12, 4, 18.00, '2017-10-28 12:23:24', '2017-10-28 12:23:24');
INSERT INTO `tbl_order_items` VALUES(17, 36, 1, 1, 18.00, '2017-10-28 12:29:16', '2017-10-28 12:29:16');
INSERT INTO `tbl_order_items` VALUES(18, 36, 1, 2, 18.00, '2017-10-28 12:29:16', '2017-10-28 12:29:16');
INSERT INTO `tbl_order_items` VALUES(19, 37, 1, 1, 18.00, '2017-10-28 12:30:04', '2017-10-28 12:30:04');
INSERT INTO `tbl_order_items` VALUES(20, 37, 1, 2, 18.00, '2017-10-28 12:30:04', '2017-10-28 12:30:04');
INSERT INTO `tbl_order_items` VALUES(21, 51, 5, 12, 42.00, '2017-10-28 01:34:42', '2017-10-28 01:34:42');
INSERT INTO `tbl_order_items` VALUES(22, 52, 5, 12, 42.00, '2017-10-28 01:35:13', '2017-10-28 01:35:13');
INSERT INTO `tbl_order_items` VALUES(23, 54, 5, 12, 42.00, '2017-10-28 01:37:12', '2017-10-28 01:37:12');
INSERT INTO `tbl_order_items` VALUES(24, 74, 1, 1, 100.00, '2017-10-28 06:17:09', '2017-10-28 06:17:09');
INSERT INTO `tbl_order_items` VALUES(25, 74, 1, 2, 120.00, '2017-10-28 06:17:09', '2017-10-28 06:17:09');
INSERT INTO `tbl_order_items` VALUES(26, 74, 1, 3, 150.00, '2017-10-28 06:17:09', '2017-10-28 06:17:09');
INSERT INTO `tbl_order_items` VALUES(27, 74, 1, 4, 130.00, '2017-10-28 06:17:09', '2017-10-28 06:17:09');
INSERT INTO `tbl_order_items` VALUES(28, 75, 1, 1, 100.00, '2017-10-28 06:53:23', '2017-10-28 06:53:23');
INSERT INTO `tbl_order_items` VALUES(29, 75, 1, 2, 120.00, '2017-10-28 06:53:23', '2017-10-28 06:53:23');
INSERT INTO `tbl_order_items` VALUES(30, 75, 1, 3, 150.00, '2017-10-28 06:53:23', '2017-10-28 06:53:23');
INSERT INTO `tbl_order_items` VALUES(31, 82, 1, 1, 100.00, '2017-10-28 08:51:20', '2017-10-28 08:51:20');
INSERT INTO `tbl_order_items` VALUES(32, 83, 1, 1, 100.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(33, 83, 1, 2, 120.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(34, 83, 1, 3, 150.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(35, 83, 1, 4, 130.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(36, 83, 2, 5, 54.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(37, 83, 2, 6, 86.00, '2017-10-29 10:50:41', '2017-10-29 10:50:41');
INSERT INTO `tbl_order_items` VALUES(38, 84, 1, 1, 100.00, '2017-10-29 11:31:11', '2017-10-29 11:31:11');
INSERT INTO `tbl_order_items` VALUES(39, 84, 1, 2, 120.00, '2017-10-29 11:31:11', '2017-10-29 11:31:11');
INSERT INTO `tbl_order_items` VALUES(40, 84, 1, 3, 150.00, '2017-10-29 11:31:11', '2017-10-29 11:31:11');
INSERT INTO `tbl_order_items` VALUES(41, 84, 1, 4, 130.00, '2017-10-29 11:31:11', '2017-10-29 11:31:11');
INSERT INTO `tbl_order_items` VALUES(42, 88, 1, 1, 100.00, '2017-10-31 12:59:19', '2017-10-31 12:59:19');
INSERT INTO `tbl_order_items` VALUES(43, 88, 1, 2, 120.00, '2017-10-31 12:59:19', '2017-10-31 12:59:19');
INSERT INTO `tbl_order_items` VALUES(44, 90, 1, 1, 100.00, '2017-11-02 01:52:13', '2017-11-02 01:52:13');
INSERT INTO `tbl_order_items` VALUES(45, 90, 1, 2, 120.00, '2017-11-02 01:52:13', '2017-11-02 01:52:13');
INSERT INTO `tbl_order_items` VALUES(46, 90, 1, 3, 150.00, '2017-11-02 01:52:13', '2017-11-02 01:52:13');
INSERT INTO `tbl_order_items` VALUES(47, 92, 1, 1, 100.00, '2017-11-02 04:34:09', '2017-11-02 04:34:09');
INSERT INTO `tbl_order_items` VALUES(48, 96, 32, 23, 122.00, '2017-11-03 01:31:02', '2017-11-03 01:31:02');
INSERT INTO `tbl_order_items` VALUES(49, 96, 31, 37, 123.00, '2017-11-03 01:31:02', '2017-11-03 01:31:02');
INSERT INTO `tbl_order_items` VALUES(50, 96, 31, 38, 50.00, '2017-11-03 01:31:02', '2017-11-03 01:31:02');
INSERT INTO `tbl_order_items` VALUES(51, 97, 31, 37, 123.00, '2017-11-03 02:14:43', '2017-11-03 02:14:43');
INSERT INTO `tbl_order_items` VALUES(52, 97, 31, 38, 50.00, '2017-11-03 02:14:43', '2017-11-03 02:14:43');
INSERT INTO `tbl_order_items` VALUES(53, 100, 31, 37, 123.00, '2017-11-03 02:45:39', '2017-11-03 02:45:39');
INSERT INTO `tbl_order_items` VALUES(54, 100, 31, 38, 50.00, '2017-11-03 02:45:39', '2017-11-03 02:45:39');
INSERT INTO `tbl_order_items` VALUES(55, 101, 1, 1, 100.00, '2017-11-03 02:56:46', '2017-11-03 02:56:46');
INSERT INTO `tbl_order_items` VALUES(56, 101, 1, 2, 120.00, '2017-11-03 02:56:46', '2017-11-03 02:56:46');
INSERT INTO `tbl_order_items` VALUES(57, 104, 1, 1, 100.00, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_order_items` VALUES(58, 104, 1, 2, 120.00, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_order_items` VALUES(59, 104, 1, 3, 150.00, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_order_items` VALUES(60, 104, 2, 5, 54.00, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_order_items` VALUES(61, 104, 2, 6, 86.00, '2017-11-03 05:14:48', '2017-11-03 05:14:48');
INSERT INTO `tbl_order_items` VALUES(62, 107, 31, 37, 123.00, '2017-11-03 07:10:22', '2017-11-03 07:10:22');
INSERT INTO `tbl_order_items` VALUES(63, 107, 31, 38, 50.00, '2017-11-03 07:10:22', '2017-11-03 07:10:22');
INSERT INTO `tbl_order_items` VALUES(64, 108, 49, 40, 100.00, '2017-11-03 08:53:27', '2017-11-03 08:53:27');
INSERT INTO `tbl_order_items` VALUES(65, 108, 50, 44, 100.00, '2017-11-03 08:53:27', '2017-11-03 08:53:27');
INSERT INTO `tbl_order_items` VALUES(66, 109, 31, 37, 123.00, '2017-11-04 01:31:37', '2017-11-04 01:31:37');
INSERT INTO `tbl_order_items` VALUES(67, 109, 31, 38, 50.00, '2017-11-04 01:31:37', '2017-11-04 01:31:37');
INSERT INTO `tbl_order_items` VALUES(68, 109, 31, 41, 60.00, '2017-11-04 01:31:37', '2017-11-04 01:31:37');
INSERT INTO `tbl_order_items` VALUES(69, 110, 31, 37, 123.00, '2017-11-04 04:23:07', '2017-11-04 04:23:07');
INSERT INTO `tbl_order_items` VALUES(70, 110, 31, 38, 50.00, '2017-11-04 04:23:07', '2017-11-04 04:23:07');
INSERT INTO `tbl_order_items` VALUES(71, 110, 31, 41, 60.00, '2017-11-04 04:23:07', '2017-11-04 04:23:07');
INSERT INTO `tbl_order_items` VALUES(72, 111, 31, 37, 123.00, '2017-11-04 05:55:54', '2017-11-04 05:55:54');
INSERT INTO `tbl_order_items` VALUES(73, 112, 31, 38, 50.00, '2017-11-04 05:58:29', '2017-11-04 05:58:29');
INSERT INTO `tbl_order_items` VALUES(74, 114, 32, 23, 122.00, '2017-11-04 06:14:26', '2017-11-04 06:14:26');
INSERT INTO `tbl_order_items` VALUES(75, 118, 31, 37, 123.00, '2017-11-06 03:34:59', '2017-11-06 03:34:59');
INSERT INTO `tbl_order_items` VALUES(76, 118, 31, 38, 50.00, '2017-11-06 03:34:59', '2017-11-06 03:34:59');
INSERT INTO `tbl_order_items` VALUES(77, 118, 31, 41, 60.00, '2017-11-06 03:34:59', '2017-11-06 03:34:59');
INSERT INTO `tbl_order_items` VALUES(78, 120, 31, 37, 123.00, '2017-11-06 03:48:37', '2017-11-06 03:48:37');
INSERT INTO `tbl_order_items` VALUES(79, 120, 31, 38, 50.00, '2017-11-06 03:48:37', '2017-11-06 03:48:37');
INSERT INTO `tbl_order_items` VALUES(80, 121, 1, 1, 100.00, '2017-11-06 04:51:13', '2017-11-06 04:51:13');
INSERT INTO `tbl_order_items` VALUES(81, 121, 1, 2, 120.00, '2017-11-06 04:51:13', '2017-11-06 04:51:13');
INSERT INTO `tbl_order_items` VALUES(82, 121, 1, 3, 150.00, '2017-11-06 04:51:13', '2017-11-06 04:51:13');
INSERT INTO `tbl_order_items` VALUES(83, 122, 1, 1, 100.00, '2017-11-06 04:51:43', '2017-11-06 04:51:43');
INSERT INTO `tbl_order_items` VALUES(84, 122, 1, 2, 120.00, '2017-11-06 04:51:43', '2017-11-06 04:51:43');
INSERT INTO `tbl_order_items` VALUES(85, 123, 31, 37, 123.00, '2017-11-06 04:57:16', '2017-11-06 04:57:16');
INSERT INTO `tbl_order_items` VALUES(86, 123, 31, 38, 50.00, '2017-11-06 04:57:16', '2017-11-06 04:57:16');
INSERT INTO `tbl_order_items` VALUES(87, 125, 1, 1, 100.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(88, 125, 1, 2, 120.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(89, 125, 1, 3, 150.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(90, 125, 1, 4, 130.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(91, 125, 2, 5, 54.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(92, 125, 2, 6, 86.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(93, 125, 2, 7, 32.00, '2017-11-06 05:09:24', '2017-11-06 05:09:24');
INSERT INTO `tbl_order_items` VALUES(94, 128, 1, 1, 100.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(95, 128, 1, 2, 120.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(96, 128, 1, 3, 150.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(97, 128, 1, 4, 130.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(98, 128, 2, 5, 54.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(99, 128, 2, 6, 86.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(100, 128, 2, 7, 32.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(101, 128, 3, 8, 123.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(102, 128, 3, 9, 233.00, '2017-11-06 07:33:11', '2017-11-06 07:33:11');
INSERT INTO `tbl_order_items` VALUES(103, 130, 1, 2, 120.00, '2017-11-08 03:33:37', '2017-11-08 03:33:37');
INSERT INTO `tbl_order_items` VALUES(104, 131, 1, 2, 120.00, '2017-11-08 03:42:07', '2017-11-08 03:42:07');
INSERT INTO `tbl_order_items` VALUES(105, 132, 4, 10, 234.00, '2017-11-08 03:44:17', '2017-11-08 03:44:17');
INSERT INTO `tbl_order_items` VALUES(106, 132, 4, 11, 102.00, '2017-11-08 03:44:17', '2017-11-08 03:44:17');
INSERT INTO `tbl_order_items` VALUES(107, 132, 5, 12, 42.00, '2017-11-08 03:44:17', '2017-11-08 03:44:17');
INSERT INTO `tbl_order_items` VALUES(108, 132, 5, 13, 51.00, '2017-11-08 03:44:17', '2017-11-08 03:44:17');
INSERT INTO `tbl_order_items` VALUES(109, 133, 32, 23, 122.00, '2017-11-08 03:51:27', '2017-11-08 03:51:27');
INSERT INTO `tbl_order_items` VALUES(110, 133, 32, 32, 45.00, '2017-11-08 03:51:27', '2017-11-08 03:51:27');
INSERT INTO `tbl_order_items` VALUES(111, 134, 28, 16, 25.00, '2017-11-08 03:52:53', '2017-11-08 03:52:53');
INSERT INTO `tbl_order_items` VALUES(112, 134, 28, 17, 25.00, '2017-11-08 03:52:53', '2017-11-08 03:52:53');
INSERT INTO `tbl_order_items` VALUES(113, 134, 29, 18, 25.00, '2017-11-08 03:52:53', '2017-11-08 03:52:53');
INSERT INTO `tbl_order_items` VALUES(114, 134, 29, 19, 25.00, '2017-11-08 03:52:53', '2017-11-08 03:52:53');
INSERT INTO `tbl_order_items` VALUES(115, 135, 88, 58, 200.00, '2017-11-08 05:35:59', '2017-11-08 05:35:59');
INSERT INTO `tbl_order_items` VALUES(116, 135, 89, 59, 100.00, '2017-11-08 05:35:59', '2017-11-08 05:35:59');
INSERT INTO `tbl_order_items` VALUES(117, 136, 89, 59, 100.00, '2017-11-08 05:55:01', '2017-11-08 05:55:01');
INSERT INTO `tbl_order_items` VALUES(118, 136, 90, 60, 300.00, '2017-11-08 05:55:01', '2017-11-08 05:55:01');
INSERT INTO `tbl_order_items` VALUES(119, 137, 89, 59, 100.00, '2017-11-08 06:10:48', '2017-11-08 06:10:48');
INSERT INTO `tbl_order_items` VALUES(120, 140, 88, 58, 200.00, '2017-11-10 12:49:23', '2017-11-10 12:49:23');
INSERT INTO `tbl_order_items` VALUES(121, 140, 89, 59, 100.00, '2017-11-10 12:49:23', '2017-11-10 12:49:23');
INSERT INTO `tbl_order_items` VALUES(122, 141, 88, 58, 200.00, '2017-11-10 01:23:16', '2017-11-10 01:23:16');
INSERT INTO `tbl_order_items` VALUES(123, 141, 89, 59, 100.00, '2017-11-10 01:23:16', '2017-11-10 01:23:16');
INSERT INTO `tbl_order_items` VALUES(124, 141, 90, 60, 300.00, '2017-11-10 01:23:16', '2017-11-10 01:23:16');
INSERT INTO `tbl_order_items` VALUES(125, 142, 88, 58, 200.00, '2017-11-10 02:12:18', '2017-11-10 02:12:18');
INSERT INTO `tbl_order_items` VALUES(126, 142, 89, 59, 100.00, '2017-11-10 02:12:18', '2017-11-10 02:12:18');
INSERT INTO `tbl_order_items` VALUES(127, 143, 88, 58, 200.00, '2017-11-10 02:32:26', '2017-11-10 02:32:26');
INSERT INTO `tbl_order_items` VALUES(128, 143, 89, 59, 100.00, '2017-11-10 02:32:26', '2017-11-10 02:32:26');
INSERT INTO `tbl_order_items` VALUES(129, 144, 88, 58, 200.00, '2017-11-10 04:28:17', '2017-11-10 04:28:17');
INSERT INTO `tbl_order_items` VALUES(130, 144, 90, 60, 300.00, '2017-11-10 04:28:17', '2017-11-10 04:28:17');
INSERT INTO `tbl_order_items` VALUES(131, 149, 88, 58, 200.00, '2017-11-11 04:38:12', '2017-11-11 04:38:12');
INSERT INTO `tbl_order_items` VALUES(132, 149, 89, 59, 100.00, '2017-11-11 04:38:12', '2017-11-11 04:38:12');
INSERT INTO `tbl_order_items` VALUES(133, 149, 90, 60, 300.00, '2017-11-11 04:38:12', '2017-11-11 04:38:12');
INSERT INTO `tbl_order_items` VALUES(134, 149, 88, 61, 100.00, '2017-11-11 04:38:12', '2017-11-11 04:38:12');
INSERT INTO `tbl_order_items` VALUES(135, 149, 90, 62, 50.00, '2017-11-11 04:38:12', '2017-11-11 04:38:12');
INSERT INTO `tbl_order_items` VALUES(136, 150, 88, 58, 200.00, '2017-11-11 04:56:14', '2017-11-11 04:56:14');
INSERT INTO `tbl_order_items` VALUES(137, 151, 31, 37, 123.00, '2017-11-11 05:15:31', '2017-11-11 05:15:31');
INSERT INTO `tbl_order_items` VALUES(138, 151, 31, 38, 50.00, '2017-11-11 05:15:31', '2017-11-11 05:15:31');
INSERT INTO `tbl_order_items` VALUES(139, 152, 88, 61, 100.00, '2017-11-11 05:16:09', '2017-11-11 05:16:09');
INSERT INTO `tbl_order_items` VALUES(140, 153, 1, 2, 120.00, '2017-11-11 06:01:06', '2017-11-11 06:01:06');
INSERT INTO `tbl_order_items` VALUES(141, 153, 1, 3, 150.00, '2017-11-11 06:01:06', '2017-11-11 06:01:06');
INSERT INTO `tbl_order_items` VALUES(142, 156, 4, 10, 234.00, '2017-11-11 06:12:06', '2017-11-11 06:12:06');
INSERT INTO `tbl_order_items` VALUES(143, 157, 88, 58, 200.00, '2017-11-11 06:47:23', '2017-11-11 06:47:23');
INSERT INTO `tbl_order_items` VALUES(144, 157, 89, 59, 100.00, '2017-11-11 06:47:23', '2017-11-11 06:47:23');
INSERT INTO `tbl_order_items` VALUES(145, 157, 88, 61, 100.00, '2017-11-11 06:47:23', '2017-11-11 06:47:23');
INSERT INTO `tbl_order_items` VALUES(146, 167, 88, 58, 290.00, '2017-11-13 05:46:48', '2017-11-13 05:46:48');
INSERT INTO `tbl_order_items` VALUES(147, 167, 88, 61, 100.00, '2017-11-13 05:46:48', '2017-11-13 05:46:48');
INSERT INTO `tbl_order_items` VALUES(148, 168, 28, 16, 25.00, '2017-11-13 06:12:12', '2017-11-13 06:12:12');
INSERT INTO `tbl_order_items` VALUES(149, 168, 29, 19, 25.00, '2017-11-13 06:12:12', '2017-11-13 06:12:12');
INSERT INTO `tbl_order_items` VALUES(150, 168, 30, 21, 35.00, '2017-11-13 06:12:12', '2017-11-13 06:12:12');
INSERT INTO `tbl_order_items` VALUES(151, 169, 88, 58, 290.00, '2017-11-13 06:17:10', '2017-11-13 06:17:10');
INSERT INTO `tbl_order_items` VALUES(152, 169, 90, 60, 300.00, '2017-11-13 06:17:10', '2017-11-13 06:17:10');
INSERT INTO `tbl_order_items` VALUES(153, 169, 89, 65, 45.00, '2017-11-13 06:17:10', '2017-11-13 06:17:10');
INSERT INTO `tbl_order_items` VALUES(154, 170, 89, 59, 100.00, '2017-11-13 06:27:11', '2017-11-13 06:27:11');
INSERT INTO `tbl_order_items` VALUES(155, 170, 90, 60, 300.00, '2017-11-13 06:27:11', '2017-11-13 06:27:11');
INSERT INTO `tbl_order_items` VALUES(156, 170, 88, 61, 100.00, '2017-11-13 06:27:11', '2017-11-13 06:27:11');
INSERT INTO `tbl_order_items` VALUES(157, 178, 31, 37, 123.00, '2017-11-14 01:38:08', '2017-11-14 01:38:08');
INSERT INTO `tbl_order_items` VALUES(158, 178, 31, 38, 50.00, '2017-11-14 01:38:08', '2017-11-14 01:38:08');
INSERT INTO `tbl_order_items` VALUES(159, 180, 31, 37, 123.00, '2017-11-14 01:38:41', '2017-11-14 01:38:41');
INSERT INTO `tbl_order_items` VALUES(160, 180, 31, 38, 50.00, '2017-11-14 01:38:41', '2017-11-14 01:38:41');
INSERT INTO `tbl_order_items` VALUES(161, 180, 31, 41, 60.00, '2017-11-14 01:38:41', '2017-11-14 01:38:41');
INSERT INTO `tbl_order_items` VALUES(162, 180, 31, 48, 126.00, '2017-11-14 01:38:41', '2017-11-14 01:38:41');
INSERT INTO `tbl_order_items` VALUES(163, 185, 31, 37, 123.00, '2017-11-14 02:41:43', '2017-11-14 02:41:43');
INSERT INTO `tbl_order_items` VALUES(164, 185, 31, 38, 50.00, '2017-11-14 02:41:43', '2017-11-14 02:41:43');
INSERT INTO `tbl_order_items` VALUES(165, 185, 31, 41, 60.00, '2017-11-14 02:41:43', '2017-11-14 02:41:43');
INSERT INTO `tbl_order_items` VALUES(166, 186, 32, 23, 122.00, '2017-11-14 02:41:57', '2017-11-14 02:41:57');
INSERT INTO `tbl_order_items` VALUES(167, 186, 32, 32, 45.00, '2017-11-14 02:41:57', '2017-11-14 02:41:57');
INSERT INTO `tbl_order_items` VALUES(168, 186, 32, 33, 345345.00, '2017-11-14 02:41:57', '2017-11-14 02:41:57');
INSERT INTO `tbl_order_items` VALUES(169, 195, 1, 1, 100.00, '2017-11-14 06:24:45', '2017-11-14 06:24:45');
INSERT INTO `tbl_order_items` VALUES(170, 195, 1, 2, 120.00, '2017-11-14 06:24:45', '2017-11-14 06:24:45');
INSERT INTO `tbl_order_items` VALUES(171, 200, 88, 58, 290.00, '2017-11-14 11:37:00', '2017-11-14 11:37:00');
INSERT INTO `tbl_order_items` VALUES(172, 200, 90, 62, 50.00, '2017-11-14 11:37:00', '2017-11-14 11:37:00');
INSERT INTO `tbl_order_items` VALUES(173, 200, 89, 65, 45.00, '2017-11-14 11:37:00', '2017-11-14 11:37:00');
INSERT INTO `tbl_order_items` VALUES(174, 201, 4, 10, 234.00, '2017-11-15 12:33:17', '2017-11-15 12:33:17');
INSERT INTO `tbl_order_items` VALUES(175, 201, 4, 11, 102.00, '2017-11-15 12:33:17', '2017-11-15 12:33:17');
INSERT INTO `tbl_order_items` VALUES(176, 205, 31, 37, 123.00, '2017-11-15 01:20:40', '2017-11-15 01:20:40');
INSERT INTO `tbl_order_items` VALUES(177, 205, 31, 38, 50.00, '2017-11-15 01:20:40', '2017-11-15 01:20:40');
INSERT INTO `tbl_order_items` VALUES(178, 207, 31, 37, 123.00, '2017-11-15 01:32:40', '2017-11-15 01:32:40');
INSERT INTO `tbl_order_items` VALUES(179, 207, 31, 38, 50.00, '2017-11-15 01:32:40', '2017-11-15 01:32:40');
INSERT INTO `tbl_order_items` VALUES(180, 211, 1, 1, 100.00, '2017-11-15 04:15:23', '2017-11-15 04:15:23');
INSERT INTO `tbl_order_items` VALUES(181, 211, 1, 2, 120.00, '2017-11-15 04:15:23', '2017-11-15 04:15:23');
INSERT INTO `tbl_order_items` VALUES(182, 212, 88, 58, 290.00, '2017-11-15 04:17:47', '2017-11-15 04:17:47');
INSERT INTO `tbl_order_items` VALUES(183, 212, 90, 60, 300.00, '2017-11-15 04:17:47', '2017-11-15 04:17:47');
INSERT INTO `tbl_order_items` VALUES(184, 212, 88, 61, 100.00, '2017-11-15 04:17:47', '2017-11-15 04:17:47');
INSERT INTO `tbl_order_items` VALUES(185, 230, 5, 12, 42.00, '2017-11-17 05:12:58', '2017-11-17 05:12:58');
INSERT INTO `tbl_order_items` VALUES(186, 230, 5, 13, 51.00, '2017-11-17 05:12:58', '2017-11-17 05:12:58');
INSERT INTO `tbl_order_items` VALUES(187, 245, 31, 37, 123.00, '2017-11-20 12:22:56', '2017-11-20 12:22:56');
INSERT INTO `tbl_order_items` VALUES(188, 245, 31, 38, 50.00, '2017-11-20 12:22:56', '2017-11-20 12:22:56');
INSERT INTO `tbl_order_items` VALUES(189, 245, 31, 41, 60.00, '2017-11-20 12:22:56', '2017-11-20 12:22:56');
INSERT INTO `tbl_order_items` VALUES(190, 246, 211, 78, 36.00, '2017-11-20 12:26:24', '2017-11-20 12:26:24');
INSERT INTO `tbl_order_items` VALUES(191, 264, 31, 34, 435.00, '2017-11-22 12:49:51', '2017-11-22 12:49:51');
INSERT INTO `tbl_order_items` VALUES(192, 264, 31, 37, 123.00, '2017-11-22 12:49:51', '2017-11-22 12:49:51');
INSERT INTO `tbl_order_items` VALUES(193, 264, 31, 38, 50.00, '2017-11-22 12:49:51', '2017-11-22 12:49:51');
INSERT INTO `tbl_order_items` VALUES(194, 265, 32, 23, 122.00, '2017-11-22 01:02:52', '2017-11-22 01:02:52');
INSERT INTO `tbl_order_items` VALUES(195, 265, 31, 34, 435.00, '2017-11-22 01:02:52', '2017-11-22 01:02:52');
INSERT INTO `tbl_order_items` VALUES(196, 265, 31, 37, 123.00, '2017-11-22 01:02:52', '2017-11-22 01:02:52');
INSERT INTO `tbl_order_items` VALUES(197, 265, 31, 38, 50.00, '2017-11-22 01:02:52', '2017-11-22 01:02:52');
INSERT INTO `tbl_order_items` VALUES(198, 271, 4, 10, 234.00, '2017-11-22 01:35:03', '2017-11-22 01:35:03');
INSERT INTO `tbl_order_items` VALUES(199, 272, 4, 10, 234.00, '2017-11-22 01:40:52', '2017-11-22 01:40:52');
INSERT INTO `tbl_order_items` VALUES(200, 273, 28, 16, 25.00, '2017-11-22 01:47:33', '2017-11-22 01:47:33');
INSERT INTO `tbl_order_items` VALUES(201, 276, 4, 10, 234.00, '2017-11-22 02:45:54', '2017-11-22 02:45:54');
INSERT INTO `tbl_order_items` VALUES(202, 278, 28, 16, 25.00, '2017-11-22 06:54:09', '2017-11-22 06:54:09');
INSERT INTO `tbl_order_items` VALUES(203, 278, 28, 17, 25.00, '2017-11-22 06:54:09', '2017-11-22 06:54:09');
INSERT INTO `tbl_order_items` VALUES(204, 280, 28, 16, 25.00, '2017-11-22 07:08:34', '2017-11-22 07:08:34');
INSERT INTO `tbl_order_items` VALUES(205, 282, 1, 1, 100.00, '2017-11-22 07:17:31', '2017-11-22 07:17:31');
INSERT INTO `tbl_order_items` VALUES(206, 283, 1, 1, 100.00, '2017-11-22 07:23:47', '2017-11-22 07:23:47');
INSERT INTO `tbl_order_items` VALUES(207, 284, 1, 1, 100.00, '2017-11-22 07:34:09', '2017-11-22 07:34:09');
INSERT INTO `tbl_order_items` VALUES(208, 284, 2, 5, 54.00, '2017-11-22 07:34:09', '2017-11-22 07:34:09');
INSERT INTO `tbl_order_items` VALUES(209, 286, 1, 1, 100.00, '2017-11-22 08:04:11', '2017-11-22 08:04:11');
INSERT INTO `tbl_order_items` VALUES(210, 287, 1, 1, 100.00, '2017-11-22 08:15:36', '2017-11-22 08:15:36');
INSERT INTO `tbl_order_items` VALUES(211, 288, 1, 1, 100.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(212, 288, 1, 2, 120.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(213, 288, 1, 3, 150.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(214, 288, 1, 4, 130.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(215, 288, 2, 5, 54.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(216, 288, 2, 6, 86.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(217, 288, 2, 7, 32.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(218, 288, 3, 8, 123.00, '2017-11-22 08:21:19', '2017-11-22 08:21:19');
INSERT INTO `tbl_order_items` VALUES(219, 289, 1, 1, 100.00, '2017-11-22 08:31:00', '2017-11-22 08:31:00');
INSERT INTO `tbl_order_items` VALUES(220, 290, 1, 1, 100.00, '2017-11-22 08:32:29', '2017-11-22 08:32:29');
INSERT INTO `tbl_order_items` VALUES(221, 291, 1, 1, 100.00, '2017-11-22 08:35:57', '2017-11-22 08:35:57');
INSERT INTO `tbl_order_items` VALUES(222, 294, 31, 34, 435.00, '2017-11-22 10:56:27', '2017-11-22 10:56:27');
INSERT INTO `tbl_order_items` VALUES(223, 294, 31, 37, 123.00, '2017-11-22 10:56:27', '2017-11-22 10:56:27');
INSERT INTO `tbl_order_items` VALUES(224, 295, 31, 34, 435.00, '2017-11-22 10:57:06', '2017-11-22 10:57:06');
INSERT INTO `tbl_order_items` VALUES(225, 295, 31, 37, 123.00, '2017-11-22 10:57:06', '2017-11-22 10:57:06');
INSERT INTO `tbl_order_items` VALUES(226, 295, 31, 38, 50.00, '2017-11-22 10:57:06', '2017-11-22 10:57:06');
INSERT INTO `tbl_order_items` VALUES(227, 296, 1, 1, 100.00, '2017-11-22 11:40:13', '2017-11-22 11:40:13');
INSERT INTO `tbl_order_items` VALUES(228, 297, 1, 1, 100.00, '2017-11-22 11:45:39', '2017-11-22 11:45:39');
INSERT INTO `tbl_order_items` VALUES(229, 298, 1, 1, 100.00, '2017-11-23 12:16:13', '2017-11-23 12:16:13');
INSERT INTO `tbl_order_items` VALUES(230, 299, 1, 2, 120.00, '2017-11-23 12:16:51', '2017-11-23 12:16:51');
INSERT INTO `tbl_order_items` VALUES(231, 301, 1, 1, 100.00, '2017-11-23 12:39:13', '2017-11-23 12:39:13');
INSERT INTO `tbl_order_items` VALUES(232, 302, 4, 10, 234.00, '2017-11-23 12:43:34', '2017-11-23 12:43:34');
INSERT INTO `tbl_order_items` VALUES(233, 303, 28, 17, 25.00, '2017-11-23 12:47:50', '2017-11-23 12:47:50');
INSERT INTO `tbl_order_items` VALUES(234, 304, 4, 10, 234.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(235, 304, 4, 11, 102.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(236, 304, 5, 12, 42.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(237, 304, 5, 13, 51.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(238, 304, 6, 14, 55.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(239, 304, 6, 15, 55.00, '2017-11-23 12:51:46', '2017-11-23 12:51:46');
INSERT INTO `tbl_order_items` VALUES(240, 305, 1, 1, 100.00, '2017-11-23 01:38:32', '2017-11-23 01:38:32');
INSERT INTO `tbl_order_items` VALUES(241, 306, 1, 1, 100.00, '2017-11-23 02:58:29', '2017-11-23 02:58:29');
INSERT INTO `tbl_order_items` VALUES(242, 307, 31, 37, 123.00, '2017-11-23 03:04:43', '2017-11-23 03:04:43');
INSERT INTO `tbl_order_items` VALUES(243, 308, 1, 1, 100.00, '2017-11-23 03:07:54', '2017-11-23 03:07:54');
INSERT INTO `tbl_order_items` VALUES(244, 310, 4, 10, 234.00, '2017-11-23 03:54:50', '2017-11-23 03:54:50');
INSERT INTO `tbl_order_items` VALUES(245, 311, 31, 34, 435.00, '2017-11-23 03:56:49', '2017-11-23 03:56:49');
INSERT INTO `tbl_order_items` VALUES(246, 311, 31, 37, 123.00, '2017-11-23 03:56:49', '2017-11-23 03:56:49');
INSERT INTO `tbl_order_items` VALUES(247, 311, 31, 38, 50.00, '2017-11-23 03:56:49', '2017-11-23 03:56:49');
INSERT INTO `tbl_order_items` VALUES(248, 311, 31, 85, 56.00, '2017-11-23 03:56:49', '2017-11-23 03:56:49');
INSERT INTO `tbl_order_items` VALUES(249, 312, 1, 1, 100.00, '2017-11-23 04:11:44', '2017-11-23 04:11:44');
INSERT INTO `tbl_order_items` VALUES(250, 312, 1, 2, 120.00, '2017-11-23 04:11:44', '2017-11-23 04:11:44');
INSERT INTO `tbl_order_items` VALUES(251, 312, 1, 3, 150.00, '2017-11-23 04:11:44', '2017-11-23 04:11:44');
INSERT INTO `tbl_order_items` VALUES(252, 312, 1, 4, 130.00, '2017-11-23 04:11:44', '2017-11-23 04:11:44');
INSERT INTO `tbl_order_items` VALUES(253, 313, 1, 1, 100.00, '2017-11-23 04:13:16', '2017-11-23 04:13:16');
INSERT INTO `tbl_order_items` VALUES(254, 315, 31, 34, 435.00, '2017-11-23 04:16:57', '2017-11-23 04:16:57');
INSERT INTO `tbl_order_items` VALUES(255, 315, 31, 37, 123.00, '2017-11-23 04:16:57', '2017-11-23 04:16:57');
INSERT INTO `tbl_order_items` VALUES(256, 315, 31, 38, 50.00, '2017-11-23 04:16:57', '2017-11-23 04:16:57');
INSERT INTO `tbl_order_items` VALUES(257, 318, 2, 6, 86.00, '2017-11-23 11:17:28', '2017-11-23 11:17:28');
INSERT INTO `tbl_order_items` VALUES(258, 318, 2, 7, 32.00, '2017-11-23 11:17:28', '2017-11-23 11:17:28');
INSERT INTO `tbl_order_items` VALUES(259, 320, 1, 1, 100.00, '2017-11-23 11:43:00', '2017-11-23 11:43:00');
INSERT INTO `tbl_order_items` VALUES(260, 320, 1, 2, 120.00, '2017-11-23 11:43:00', '2017-11-23 11:43:00');
INSERT INTO `tbl_order_items` VALUES(261, 323, 1, 1, 100.00, '2017-11-24 12:40:51', '2017-11-24 12:40:51');
INSERT INTO `tbl_order_items` VALUES(262, 323, 1, 2, 120.00, '2017-11-24 12:40:51', '2017-11-24 12:40:51');
INSERT INTO `tbl_order_items` VALUES(263, 323, 1, 3, 150.00, '2017-11-24 12:40:51', '2017-11-24 12:40:51');
INSERT INTO `tbl_order_items` VALUES(264, 325, 31, 34, 435.00, '2017-11-24 02:23:37', '2017-11-24 02:23:37');
INSERT INTO `tbl_order_items` VALUES(265, 326, 1, 1, 100.00, '2017-11-24 02:31:26', '2017-11-24 02:31:26');
INSERT INTO `tbl_order_items` VALUES(266, 326, 1, 2, 120.00, '2017-11-24 02:31:26', '2017-11-24 02:31:26');
INSERT INTO `tbl_order_items` VALUES(267, 327, 1, 1, 100.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(268, 327, 1, 2, 120.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(269, 327, 1, 3, 150.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(270, 327, 1, 4, 130.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(271, 327, 2, 5, 54.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(272, 327, 2, 6, 86.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(273, 327, 2, 7, 32.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(274, 327, 3, 8, 123.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');
INSERT INTO `tbl_order_items` VALUES(275, 327, 3, 9, 233.00, '2017-11-24 03:24:59', '2017-11-24 03:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE `tbl_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_days_count` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `package_type` varchar(50) NOT NULL,
  `rate` decimal(16,2) NOT NULL,
  `status` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` VALUES(1, 6, 'pack 1', 3, '', 'both', 20.00, 'active', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_packages` VALUES(2, 6, 'pack 2', 5, '', 'veg', 10.00, 'active', '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_packages` VALUES(3, 24, 'Fast Food', 3, '', 'both', 4.00, 'Active', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_packages` VALUES(4, 24, 'The Tulip', 4, '', 'both', 4.00, 'Active', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `tbl_packages` VALUES(17, 31, 'New Pack', 3, '', 'Halal', 3.00, '', '2017-11-02 12:18:02', '2017-11-02 12:18:02');
INSERT INTO `tbl_packages` VALUES(20, 31, 'Test Gh', 2, '', 'Halal', 5.00, '', '2017-11-03 08:46:22', '2017-11-03 08:46:22');
INSERT INTO `tbl_packages` VALUES(21, 31, 'Non Veg', 2, '', 'nonveg', 125.00, '', '2017-11-03 08:50:32', '2017-11-03 08:50:32');
INSERT INTO `tbl_packages` VALUES(30, 25, 'Pacakge 11', 5, '', 'nonveg', 466.00, '', '2017-11-05 10:33:34', '2017-11-05 10:33:34');
INSERT INTO `tbl_packages` VALUES(43, 37, 'Package 1', 7, '', 'Nonveg', 100.00, '', '2017-11-13 04:44:14', '2017-11-13 04:44:14');
INSERT INTO `tbl_packages` VALUES(55, 25, 'Package 12', 5, '', 'veg', 100.00, '', '2017-11-22 11:02:52', '2017-11-22 11:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_items`
--

CREATE TABLE `tbl_package_items` (
  `package_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`package_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `tbl_package_items`
--

INSERT INTO `tbl_package_items` VALUES(1, 1, 1, 1, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_package_items` VALUES(2, 1, 2, 6, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_package_items` VALUES(3, 2, 5, 12, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_package_items` VALUES(4, 2, 5, 13, '2017-10-09 00:00:00', '2017-10-09 00:00:00');
INSERT INTO `tbl_package_items` VALUES(5, 3, 1, 1, '2017-10-28 20:37:36', '2017-10-28 20:37:40');
INSERT INTO `tbl_package_items` VALUES(6, 3, 1, 2, '2017-10-28 20:37:36', '2017-10-28 20:37:40');
INSERT INTO `tbl_package_items` VALUES(7, 4, 5, 12, '2017-10-28 20:37:36', '2017-10-28 20:37:40');
INSERT INTO `tbl_package_items` VALUES(8, 4, 5, 13, '2017-10-28 20:37:36', '2017-10-28 20:37:40');
INSERT INTO `tbl_package_items` VALUES(11, 17, 49, 40, '2017-11-02 12:18:02', '2017-11-02 12:18:02');
INSERT INTO `tbl_package_items` VALUES(19, 20, 49, 40, '2017-11-03 08:46:22', '2017-11-03 08:46:22');
INSERT INTO `tbl_package_items` VALUES(20, 21, 49, 40, '2017-11-03 08:50:32', '2017-11-03 08:50:32');
INSERT INTO `tbl_package_items` VALUES(21, 21, 50, 44, '2017-11-03 08:50:32', '2017-11-03 08:50:32');
INSERT INTO `tbl_package_items` VALUES(45, 30, 31, 37, '2017-11-05 10:33:34', '2017-11-05 10:33:34');
INSERT INTO `tbl_package_items` VALUES(46, 30, 31, 38, '2017-11-05 10:33:34', '2017-11-05 10:33:34');
INSERT INTO `tbl_package_items` VALUES(67, 43, 68, 50, '2017-11-13 04:44:14', '2017-11-13 04:44:14');
INSERT INTO `tbl_package_items` VALUES(68, 43, 68, 67, '2017-11-13 04:44:14', '2017-11-13 04:44:14');
INSERT INTO `tbl_package_items` VALUES(89, 55, 31, 34, '2017-11-22 11:02:52', '2017-11-22 11:02:52');
INSERT INTO `tbl_package_items` VALUES(90, 55, 31, 37, '2017-11-22 11:02:52', '2017-11-22 11:02:52');
INSERT INTO `tbl_package_items` VALUES(91, 55, 32, 23, '2017-11-22 11:02:52', '2017-11-22 11:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(100) NOT NULL,
  `setting_type` varchar(100) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` VALUES(1, 'discount', 'percent', '50', '2017-10-15 23:47:54', '2017-10-15 23:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `login_type` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `country` varchar(50) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `user_img` text,
  `upload_documents` text NOT NULL,
  `vendor_description` text,
  `status` varchar(25) NOT NULL,
  `verification_code` varchar(10) DEFAULT NULL,
  `device_token` varchar(100) NOT NULL,
  `user_platform` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` VALUES(1, 'Kaza', 'admin@kaza.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'Admin', NULL, '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '324222', '543534534533', '', '', NULL, 'Active', NULL, '', '', '2017-10-05 09:22:22', '2017-10-05 08:24:25');
INSERT INTO `tbl_users` VALUES(2, 'karuna', 'karuna.shenll@gmail.com', '6MmgwVCPRh2H-1YgT9P9kxxMw2O4vwJAulGMDMNRhwY', 'customer', NULL, '26/1, Mahathma Gandhi Road, Chennai  600034.', 'indian', '635737', '7833453456', 'uploads/user_profile/7ee2b269a8d636be0bf9067d7a76faebbf644ebe.png', '', NULL, 'Active', '20425', 'testss', 'IOS', '2017-10-06 09:22:24', '2017-10-26 05:23:38');
INSERT INTO `tbl_users` VALUES(3, 'vicky', 'vigneshkumar.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '746373', '3232324322', '', '', NULL, 'Active', '17575', '1234', 'Android', '2017-10-06 08:20:24', '2017-11-16 02:46:41');
INSERT INTO `tbl_users` VALUES(4, 'indhiyan', 'indhiyan.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'No 6 AMC Enclave,  Nungambakkam, Chennai - 600034, ', 'India', '324522', '43432322322', '', 'uploads/user_documents/auntya.pdf', NULL, 'active', NULL, '', '', '2017-10-06 10:24:33', '2017-10-06 09:24:27');
INSERT INTO `tbl_users` VALUES(5, 'thiyagu', 'thiyagu.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'No 6 AMC Enclave,KK.Nagar, Chennai - 600034, ', 'India', '324522', '43432322322', '', '', NULL, 'Active', NULL, '', '', '2017-10-06 10:24:33', '2017-10-06 09:24:27');
INSERT INTO `tbl_users` VALUES(6, 'Saravanan', 'saravanakumar.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', NULL, '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '626765', '9095905095', '', 'uploads/user_documents/auntya.pdf', NULL, 'active', NULL, '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19');
INSERT INTO `tbl_users` VALUES(7, 'Alan', 'alan@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', NULL, '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '626555', '9095905065', '', 'uploads/user_documents/auntya.pdf', NULL, 'active', NULL, '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19');
INSERT INTO `tbl_users` VALUES(8, 'ino', 'ino@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, '26/1, Mahathma Gandhi Road, Chennai 600034.', 'India', '626555', '9095905065', '', 'uploads/user_documents/auntya.pdf', NULL, 'active', NULL, '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19');
INSERT INTO `tbl_users` VALUES(9, 'Shenll', 'shenllemail@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'Chennai', 'India', '354637', '1234567894', '', '', NULL, 'block', NULL, '643834834834346384363', 'IOS', '2017-10-12 05:03:00', '2017-10-12 05:03:00');
INSERT INTO `tbl_users` VALUES(10, 'hai123', 'test@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, '', '', ' ', '123456', 'uploads/user_profile/d8899d2c0c2beaa60801d6782e55c8304f153d40.png', '', NULL, 'Active', '46487', '1234', 'IOS', '2017-10-23 01:22:56', '2017-11-24 02:50:11');
INSERT INTO `tbl_users` VALUES(11, 'preethi', 'test1@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Address', 'india', '606096', '12323445', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 02:22:18', '2017-10-23 02:22:18');
INSERT INTO `tbl_users` VALUES(12, 'testing', 'testing@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Address', 'india', '606907', '12345', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 02:23:31', '2017-10-23 02:23:31');
INSERT INTO `tbl_users` VALUES(13, 'Testing1', 'testing1@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'AddressAddress', 'india', '600000', '1234567', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 02:30:25', '2017-10-23 03:08:08');
INSERT INTO `tbl_users` VALUES(14, 'Testing', 'testing1@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Addressnew', 'india', '606060', '1234567890', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 03:05:22', '2017-10-23 03:05:22');
INSERT INTO `tbl_users` VALUES(15, 'Testing12', 'testing13@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Addressnew3', 'india', '606060', '1234567893', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 03:06:12', '2017-10-23 03:06:12');
INSERT INTO `tbl_users` VALUES(16, 'Testing123', 'testing135@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Addresss', 'india', '606909', '1234567833', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-23 03:06:42', '2017-10-23 03:06:42');
INSERT INTO `tbl_users` VALUES(17, 'madhavan s', 'madhavan.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'Address ', 'country', '334', '1234567', '', '', NULL, 'Active', '96171', '1234', 'IOS', '2017-10-25 02:44:21', '2017-11-04 07:02:30');
INSERT INTO `tbl_users` VALUES(18, 'Usus', 'asd@asd.cc', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', NULL, 'Address', 'jdjfj', '5252', '2442563636', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-25 02:58:36', '2017-10-25 02:58:36');
INSERT INTO `tbl_users` VALUES(19, 'Iostest', 'iostest@test.com', 'hHg_QRwFwWWrMJKYKxqpyXjBb6GHvaa8-9UedfTtgsY', 'customer', NULL, 'New', 'india', '678888', '1234567890', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-26 01:34:43', '2017-10-26 01:34:43');
INSERT INTO `tbl_users` VALUES(20, 'iOS.Test', 'testios@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'New address', 'india', '565675', '1235456655', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-26 02:24:39', '2017-10-26 02:24:39');
INSERT INTO `tbl_users` VALUES(24, 'karuna', 'karan@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', NULL, 'Chennai', 'India', '354637', '1234567894', NULL, '', NULL, 'Active', NULL, '643834834834346384363', 'IOS', '2017-10-28 04:26:50', '2017-10-28 04:26:50');
INSERT INTO `tbl_users` VALUES(25, 'stsss', 'testvendor@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', NULL, '', '', '', '2646465467', 'uploads/user_profile/078020df326dfb9e7d5f6873bc1eabc449c0d0e8.png', '', NULL, 'active', '95455', '1234', 'Android', '2017-10-30 02:26:45', '2017-11-24 05:23:05');
INSERT INTO `tbl_users` VALUES(26, 'Preeti ', 'preetirnigam@Gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', NULL, 'Toronto', 'Canada', '85055', '4162755437', NULL, '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-30 05:19:41', '2017-11-10 05:22:43');
INSERT INTO `tbl_users` VALUES(27, 'sts', 'karuna.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'fb', 'test address', 'india', '354637', '78474774747', '', '', 'vendor description', 'Active', NULL, '643834834834346384363', 'IOS', '2017-10-31 04:00:45', '2017-10-31 04:00:45');
INSERT INTO `tbl_users` VALUES(28, 'Manoj Shanmugam', 'manoj.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'gplus', '', '', '', '', 'uploads/user_profile/66cc0bd6cddafe939673f5d3b0d2522a8434ccc4.png', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-31 08:43:13', '2017-10-31 08:43:13');
INSERT INTO `tbl_users` VALUES(29, 'Manoj Sh', 'manoj.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'fb', '', '', '', '', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-10-31 08:49:23', '2017-10-31 08:49:23');
INSERT INTO `tbl_users` VALUES(30, 'Manoj Shanmugam', 'manoj.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'gplus', '', '', '', '', 'uploads/user_profile/1a03f519f1db2803c1642dda5236f3185069f173.png', '', ' ', 'Active', NULL, '1234', 'IOS', '2017-11-01 02:46:06', '2017-11-01 02:46:06');
INSERT INTO `tbl_users` VALUES(31, 'st', 'shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'test test address', 'indian', '123455', '2646465466', '', 'uploads/user_profile/afb46da767d0d567a05a18141a32a8cd5108baf3.jpeg', 'test vendor description', 'active', NULL, '1234', 'IOS', '2017-11-01 11:50:28', '2017-11-05 05:16:10');
INSERT INTO `tbl_users` VALUES(32, 'Manoj Sh', 'manoj.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'fb', '', '', '', '', '', '', ' ', 'pending', NULL, '1234', 'IOS', '2017-11-02 03:13:52', '2017-11-02 03:13:52');
INSERT INTO `tbl_users` VALUES(33, 'IOS Tester', 'testmail@test.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'email', 'STS', 'India', '600000', '9988776655', '', '', NULL, 'pending', NULL, '1234', 'IOS', '2017-11-04 12:32:36', '2017-11-04 12:32:36');
INSERT INTO `tbl_users` VALUES(34, 'customertesting', 'customer@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New address', 'India', '123486', '1234567890', '', '', NULL, 'pending', NULL, '1234', 'IOS', '2017-11-04 12:58:24', '2017-11-04 12:58:24');
INSERT INTO `tbl_users` VALUES(35, 'test', 'vendor@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'vendor', 'email', 'New', 'India', '6899', '8689009988', '', '', 'Specialist in veg', 'pending', NULL, '1234', 'IOS', '2017-11-04 01:00:29', '2017-11-04 01:00:29');
INSERT INTO `tbl_users` VALUES(36, 'Appteam', 'customer@Appteam.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'email', 'STS', 'India', '123456', '9988776655', '', '', NULL, 'pending', NULL, '1234', 'IOS', '2017-11-04 08:53:21', '2017-11-10 05:46:26');
INSERT INTO `tbl_users` VALUES(37, 'Appteam', 'vendor@Appteam.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'vendor', 'email', 'STS', 'India', '123456', '9988776655', '', '', 'Service with good quality', 'active', NULL, '1234', 'IOS', '2017-11-04 08:55:52', '2017-11-10 10:02:17');
INSERT INTO `tbl_users` VALUES(38, 'Testing', 'vendor1@test.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'vendor', 'email', 'STS', 'India', '123456', '9988776655', '', '', 'Testing new vendor', 'pending', NULL, '1234', 'IOS', '2017-11-04 09:17:01', '2017-11-04 09:17:01');
INSERT INTO `tbl_users` VALUES(39, 'Testing vendor', 'test@testing.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'vendor', 'email', 'Sts', 'India', '123456', '9876543211', '', '', 'Testing vendor', 'pending', NULL, '1234', 'IOS', '2017-11-04 09:24:12', '2017-11-04 09:24:12');
INSERT INTO `tbl_users` VALUES(40, 'sts', 'karuna.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'test address', 'india', '354637', '78474774747', 'uploads/user_profile/28a8fdcf2503fa373f89215281672795c4019dd3.png', '', 'test vendor description', 'Active', NULL, '643834834834346384363', 'Android', '2017-11-07 05:37:58', '2017-11-07 05:37:58');
INSERT INTO `tbl_users` VALUES(41, 'Akash', 'akash18.shenll@gmail.com', 'zm8iVdJAhZcQDTQrSTaF2FOnGS4xdWLDoR_IUQQ9_oo', 'customer', 'email', '', '', '', '9840223293', 'uploads/user_profile/b2f6de112713c5c8d225056d8fe5bcfa2df3d2f0.png', '', NULL, 'Active', '35480', '1234', 'IOS', '2017-11-08 12:44:19', '2017-11-14 11:28:47');
INSERT INTO `tbl_users` VALUES(42, '         ', 'akash18.shenll@gmail.com', 'XE-HpFnQnuox7zWrvMhW9ndgWHu4hQERmd5MxjZIgO8', 'vendor', 'email', '     ', '     ', '51716', '61727', '', '', '123', 'pending', NULL, '1234', 'IOS', '2017-11-08 01:42:40', '2017-11-08 01:42:40');
INSERT INTO `tbl_users` VALUES(43, '       ', 'shenlltester121@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'fb', '', '', '', '1234567890', 'uploads/user_profile/e5e08950924626bd83638e8a8d037e92c593b100.png', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-08 02:03:49', '2017-11-08 02:33:46');
INSERT INTO `tbl_users` VALUES(44, 'swiggy', 'shenlltester121@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'vendor', 'email', '', '', '', '123456789', 'uploads/user_profile/53024fbec1e556a2bbcf4a9c56114a10b10536ff.png', '', '     ', 'active', NULL, '1234', 'IOS', '2017-11-08 04:37:00', '2017-11-20 04:16:09');
INSERT INTO `tbl_users` VALUES(45, 'Dhd', 'akash@gmail.com', 'u8fvH6tmy9dLGJd9rSIo64NLNlUeZNub1IpXM5rul8k', 'customer', 'email', 'Dfff', 'Chfg', '356', '46634', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-08 06:17:59', '2017-11-08 06:17:59');
INSERT INTO `tbl_users` VALUES(46, 'testingteam', 'test@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'vendor', 'email', 'Testing', 'India', '6000089', '9840223293', '', '', 'Tester', 'pending', NULL, '1234', 'IOS', '2017-11-08 11:04:53', '2017-11-08 11:04:53');
INSERT INTO `tbl_users` VALUES(47, 'Testing ', 'tester@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'KKnagar', 'India ', '600078', '1234567890', '', '', 'Tester', 'pending', NULL, '1234', 'IOS', '2017-11-09 01:43:51', '2017-11-09 01:43:51');
INSERT INTO `tbl_users` VALUES(48, 'Testing', 'testing@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Adddress', 'India', '606060', '123456', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-09 11:07:39', '2017-11-09 11:07:39');
INSERT INTO `tbl_users` VALUES(49, 'Testing', 'testing@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'vendor', 'email', 'New Address', 'India', '5656565', '1234567890', '', '', 'Specialist in veg', 'pending', NULL, '1234', 'IOS', '2017-11-09 11:08:31', '2017-11-09 11:08:31');
INSERT INTO `tbl_users` VALUES(50, 'Gnanasekar N', 'gnanasekar.shenll@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'gplus', '', '', ' ', '75624', 'uploads/user_profile/73a1f19de95a4ae20c9bf52c47496a803385ebbd.png', '', '', 'Active', NULL, '', 'Android', '2017-11-10 12:22:48', '2017-11-21 01:39:24');
INSERT INTO `tbl_users` VALUES(51, 'Android Test', 'test.andro.shenll@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'gplus', '', '', '', '', '', '', '', 'Active', NULL, '', 'Android', '2017-11-10 12:24:03', '2017-11-10 12:24:03');
INSERT INTO `tbl_users` VALUES(52, 'Preethi Ios', 'preethi.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'fb', '', '', '', '1234567890', 'uploads/user_profile/dd48e01cbabbe9b1ba8359eb5260ae41f0440066.png', '', '', 'Active', '94550', '', 'Android', '2017-11-10 12:24:19', '2017-11-22 12:04:50');
INSERT INTO `tbl_users` VALUES(53, 'Df', 'abc@fdg..com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'email', 'Dads', 'Dsf', 'ds', '6576757556', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-10 12:24:39', '2017-11-10 12:24:39');
INSERT INTO `tbl_users` VALUES(54, 'Kazatest', 'kazatest@gmail.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'email', 'KK nagar ', 'India', '600045', '2134567890', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-10 04:26:44', '2017-11-10 04:26:44');
INSERT INTO `tbl_users` VALUES(55, 'Ios', 'abc@abc.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '660880', '2356789999', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-11 06:22:39', '2017-11-11 06:22:39');
INSERT INTO `tbl_users` VALUES(56, 'iOS', 'abc@ios.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'Address New', 'India', '608907', '1458999876', '', '', '     ', 'pending', NULL, '1234', 'IOS', '2017-11-11 06:23:41', '2017-11-11 06:23:41');
INSERT INTO `tbl_users` VALUES(57, 'Testiny', 'anc@abc.von', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '707548', '2457789987', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-11 06:28:06', '2017-11-11 06:28:06');
INSERT INTO `tbl_users` VALUES(58, 'New', 'ey@ry.ch', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '36789', '2467999000', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-11 06:37:23', '2017-11-11 06:37:23');
INSERT INTO `tbl_users` VALUES(59, 'Vendor', 'sbc@act.con', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Volony', 'India', '3568', '256589000', '', '', '     ', 'pending', NULL, '1234', 'IOS', '2017-11-11 06:39:10', '2017-11-11 06:39:10');
INSERT INTO `tbl_users` VALUES(60, 'Ios ios', 'qe@wrt.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'Affhh', 'India', '5790', '3677900084', '', '', '    Veg only', 'pending', NULL, '1234', 'IOS', '2017-11-11 06:48:52', '2017-11-11 06:48:52');
INSERT INTO `tbl_users` VALUES(61, 'New Customer', 'ios@iOS.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Colony', 'India', '606903', '1234567899', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-12 10:23:25', '2017-11-12 10:23:25');
INSERT INTO `tbl_users` VALUES(62, 'New Vendor', 'ios@io.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Colony', 'India', '1234566', '1345889975', '', '', '       New vendor', 'pending', NULL, '1234', 'IOS', '2017-11-12 10:25:18', '2017-11-12 10:25:18');
INSERT INTO `tbl_users` VALUES(63, 'New Customer 1', 'iOS@io.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Colony', 'India', '1235888', '2748959595', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-12 10:27:10', '2017-11-12 10:27:10');
INSERT INTO `tbl_users` VALUES(64, 'Food Panda ', 'food.panda@gmail.com', 'zm8iVdJAhZcQDTQrSTaF2FOnGS4xdWLDoR_IUQQ9_oo', 'customer', 'email', 'Chennai', 'India', '600119', '9876543210', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-12 11:33:47', '2017-11-12 11:46:56');
INSERT INTO `tbl_users` VALUES(65, 'food app', 'food.app@gmail.com', 'GQIMha-_oa7FIW28_ikvV0LnpWArcE-A7J3_YVsIxjE', 'customer', 'email', 'Chennai', 'India', '119', '7896543574', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-12 11:54:43', '2017-11-12 11:54:43');
INSERT INTO `tbl_users` VALUES(66, 'Hshshs', 'Bshshdh@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'Hshshhd', 'Hshshs', 'hsjs', '627272', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-12 11:58:10', '2017-11-12 11:58:10');
INSERT INTO `tbl_users` VALUES(67, '   Kiran', 'k@k.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'Chennai', 'Usa', '12345566', '9566060228', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-13 04:45:25', '2017-11-13 05:43:55');
INSERT INTO `tbl_users` VALUES(68, 'Mew', 'test.vendor@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Address', 'India', '680064', '1457900075', '', '', 'No veg specialist', 'pending', NULL, '1234', 'IOS', '2017-11-13 06:46:41', '2017-11-13 06:46:41');
INSERT INTO `tbl_users` VALUES(69, 'Muthukumaresan T', 'muthukumaresan.shenll@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'gplus', '', '', '', '', '', '', '', 'Active', NULL, '', 'Android', '2017-11-13 11:36:59', '2017-11-13 11:36:59');
INSERT INTO `tbl_users` VALUES(70, 'Customer1', 'em@gmail.con', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'Add New Address', 'India', '456799', '9367899553', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-14 07:31:51', '2017-11-14 07:31:51');
INSERT INTO `tbl_users` VALUES(71, 'Customer2', 'test@cust.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '12567', '1235678990', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-14 11:38:27', '2017-11-14 11:39:01');
INSERT INTO `tbl_users` VALUES(72, 'Customer3', 'Test@customer.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '123456', '9897889234', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-14 11:20:17', '2017-11-14 11:20:17');
INSERT INTO `tbl_users` VALUES(73, 'Customer4', 'testcustomer@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '123456', '9768676767', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-14 11:25:57', '2017-11-14 11:27:02');
INSERT INTO `tbl_users` VALUES(74, 'Vendor5', 'vendor5@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Address', 'India', '23455', '3243456745', '', '', 'Cooking master', 'active', NULL, '1234', 'IOS', '2017-11-14 11:49:16', '2017-11-21 06:05:09');
INSERT INTO `tbl_users` VALUES(75, 'Vendor5', 'vendor5@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Address', 'India', '123234', '546456456', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-15 12:05:24', '2017-11-15 12:05:24');
INSERT INTO `tbl_users` VALUES(78, 'Hshshs', 'gahs@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'Hshshs', 'Shhshs', '6272', '8282', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-15 06:09:18', '2017-11-15 06:09:18');
INSERT INTO `tbl_users` VALUES(79, 'Muthu Kumarshan', 'muthukumaresan235@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'fb', '', '', ' ', '56', 'uploads/user_profile/3f6d6263ef518522829ab06dcfc51a4893df9165.png', '', '', 'Active', NULL, '', 'Android', '2017-11-16 01:17:44', '2017-11-21 02:15:10');
INSERT INTO `tbl_users` VALUES(80, 'Sandyy Shenll', 'qatestforfb@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'fb', '', '', '', '', '', '', '', 'Active', NULL, '', 'Android', '2017-11-16 02:35:28', '2017-11-16 02:35:28');
INSERT INTO `tbl_users` VALUES(81, 'Ionic Shenll', 'ionic.shenll@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'gplus', '', '', '', '', '', '', '', 'Active', NULL, '', 'Android', '2017-11-16 02:45:28', '2017-11-16 02:45:28');
INSERT INTO `tbl_users` VALUES(82, 'Gnanasekar N', 'gnanasekar.shenll@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'vendor', 'gplus', '', '', '', '', '', '', '', 'pending', NULL, '', 'Android', '2017-11-17 12:36:20', '2017-11-17 12:36:20');
INSERT INTO `tbl_users` VALUES(83, 'KazaAkash', 'kazaakash@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'customer', 'email', '', '', '', '9809676745', 'uploads/user_profile/880797f3d0adfb348026e1c2bd164f4265389838.png', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-19 10:57:05', '2017-11-20 03:56:10');
INSERT INTO `tbl_users` VALUES(84, 'Kaza shenll', 'kazashenll@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'vendor', 'email', 'India Chennai', 'India ', '600045', '9867456734', '', '', '      ðŸ˜—', 'pending', NULL, '1234', 'IOS', '2017-11-19 11:01:11', '2017-11-19 11:01:11');
INSERT INTO `tbl_users` VALUES(85, 'Akashshenll', 'akashshenll@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'vendor', 'email', 'Testershenll', 'India', '600045', '9866758556', '', '', '     ()', 'active', NULL, '1234', 'IOS', '2017-11-19 11:07:00', '2017-11-20 12:28:42');
INSERT INTO `tbl_users` VALUES(86, 'madhavan s', 'madhavanrmk123@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'customer', 'gplus', '', '', '', '', '', '', '', 'Active', NULL, '', 'Android', '2017-11-20 12:13:03', '2017-11-20 12:13:03');
INSERT INTO `tbl_users` VALUES(87, 'madhavan s', 'madhavanrmk123@gmail.com', 'r2CJo1WcFCCiR0528zTRoHuOdLsBLwbIYQFKzbyOrEM', 'vendor', 'gplus', '', '', '', '', '', '', '', 'pending', NULL, '', 'Android', '2017-11-20 12:14:41', '2017-11-20 12:14:41');
INSERT INTO `tbl_users` VALUES(88, 'Hsjsh', 'fb@gmail.com', 'b16-grzBa0GFzdgDtddSrsYGOr62nm8UpBysCUjywO0', 'customer', 'email', 'Hshshhd', 'Gshshsbshsua', ')/â‚¹/!!/', '7256826725', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-20 04:56:43', '2017-11-20 04:56:43');
INSERT INTO `tbl_users` VALUES(89, 'kaza', 'kaza@maily.com', 'jCEJJ6jVtT-yAPocNU-WVvDZkMMQHFUaXRNo6IYoMVk', 'customer', 'email', 'test', 'test', 'test', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 03:15:13', '2017-11-21 03:15:13');
INSERT INTO `tbl_users` VALUES(90, 'test', 'testvdkckdkck', 'jCEJJ6jVtT-yAPocNU-WVvDZkMMQHFUaXRNo6IYoMVk', 'customer', 'email', 'gdjxkdj', 'gxbkd', 'gdjdkd', '1246', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 03:22:17', '2017-11-21 03:22:17');
INSERT INTO `tbl_users` VALUES(91, 'Test', 'test', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', 'test', 'test', 'test', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 05:03:52', '2017-11-21 05:03:52');
INSERT INTO `tbl_users` VALUES(92, 'kaza', 'kazatest123@maily.com', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', 'test', 'test', 'test', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 05:32:30', '2017-11-21 05:32:30');
INSERT INTO `tbl_users` VALUES(93, 'Test', 'kazagfmdkdbxnckdjsj', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', 'test', 'test', 'test', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 05:41:10', '2017-11-21 05:41:10');
INSERT INTO `tbl_users` VALUES(94, 'Test', 'kazafjkdjkdgkfnl', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', 'tesy', 'tedt', 'tedt', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-21 05:47:39', '2017-11-21 05:47:39');
INSERT INTO `tbl_users` VALUES(95, 'tyj', 'dhlgsndlkdhdj', 'jCEJJ6jVtT-yAPocNU-WVvDZkMMQHFUaXRNo6IYoMVk', 'customer', 'email', '', '', ' ', '566', 'uploads/user_profile/09e6ad54cd2c8d550f22dc10e6555540301d3460.png', '', '', 'Active', NULL, '', 'Android', '2017-11-21 05:54:18', '2017-11-21 05:54:54');
INSERT INTO `tbl_users` VALUES(96, 'Preethi G', 'preethi.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'gplus', '', '', '', '', 'uploads/user_profile/ab28bc0bbe90ae4d407a42eadabfff63903a72e1.png', '', NULL, 'Active', '94550', '1234', 'IOS', '2017-11-21 05:58:58', '2017-11-21 05:58:58');
INSERT INTO `tbl_users` VALUES(97, 'Preethi Ios', 'preethi.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'fb', '', '', '', '', 'uploads/user_profile/92ae8e63469645d92838d0352ac8fc447b15a22f.png', '', ' ', 'pending', NULL, '1234', 'IOS', '2017-11-21 06:17:40', '2017-11-21 06:17:40');
INSERT INTO `tbl_users` VALUES(98, 'HaHa HaHa', 'iostest.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'fb', '', '', '', '', 'uploads/user_profile/36ac66bb27a63fa29cabfc2bb7a08b98f7289031.png', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-21 07:22:32', '2017-11-21 07:22:32');
INSERT INTO `tbl_users` VALUES(99, 'Preethi', 'preethi@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Colony', 'India', '600000', '123456789', '', '', 'New vendor', 'pending', NULL, '1234', 'IOS', '2017-11-21 10:32:14', '2017-11-21 10:32:14');
INSERT INTO `tbl_users` VALUES(100, 'Preethi', 'preethi.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'email', 'New Colony', 'India', '606066', '1234567890', '', '', 'New vendor', 'pending', NULL, '1234', 'IOS', '2017-11-21 11:41:11', '2017-11-21 11:41:11');
INSERT INTO `tbl_users` VALUES(101, 'Preethi G', 'preethi.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'vendor', 'gplus', '', '', '', '1232345678', 'uploads/user_profile/6fee381703337a877cf19bf87e685a1ea66ca975.png', '', ' ', 'pending', NULL, '1234', 'IOS', '2017-11-21 11:45:18', '2017-11-22 12:06:28');
INSERT INTO `tbl_users` VALUES(102, 'akila', 'akila@maily.com', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', '', '', ' ', '1234567', 'uploads/user_profile/23feaa6253bb626fd7fa9ed183329b320fda11ad.png', '', '', 'Active', NULL, '1234', 'Android', '2017-11-22 07:15:36', '2017-11-22 07:16:58');
INSERT INTO `tbl_users` VALUES(103, 'kiran', 'kiran@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'chennai', 'india', 'tn002', '123456', '', '', '', 'Active', NULL, '1234', 'Android', '2017-11-22 07:29:33', '2017-11-22 10:23:50');
INSERT INTO `tbl_users` VALUES(104, 'Customernew', 'custonernew@gma.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'New Addres', 'India', '689939', '1234567890', '', '', NULL, 'Active', NULL, '1234', 'IOS', '2017-11-22 07:32:54', '2017-11-22 07:32:54');
INSERT INTO `tbl_users` VALUES(105, 'kaza', 'kazakaza@maily.com', 'nnkPi-LhBdkVaYWC7rVNLKCXcaRkF6J2dtCg5r91SnM', 'customer', 'email', '', '', ' ', '123456', 'uploads/user_profile/dd5fb28a7a0ed233418c1d4c175c4e198ace6619.png', '', '', 'Active', NULL, '1234', 'Android', '2017-11-22 08:11:39', '2017-11-22 10:23:16');
INSERT INTO `tbl_users` VALUES(106, 't', 'q', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'q', 'e', 'q', '1', '', '', '', 'Active', NULL, '', 'Android', '2017-11-22 10:07:31', '2017-11-22 10:07:31');
INSERT INTO `tbl_users` VALUES(107, 'nithya', 'nithya@maily.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'test', 'india', 'tn007', '123456', '', '', '', 'Active', NULL, '1234', 'Android', '2017-11-22 10:26:20', '2017-11-23 11:11:41');
INSERT INTO `tbl_users` VALUES(108, 'nithya', 'nithya1@maily.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'test', 'india', 'tn007', '123456', '', '', '', 'Active', NULL, '', 'Android', '2017-11-22 10:26:42', '2017-11-22 10:26:42');
INSERT INTO `tbl_users` VALUES(109, 'muthu', 'muthu@maily.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', '', '', ' ', '123456', 'uploads/user_profile/f153505fb43c04a8fa08014b57ca6c2df61490a2.png', '', '', 'Active', NULL, '1234', 'Android', '2017-11-22 10:29:51', '2017-11-24 03:24:13');
INSERT INTO `tbl_users` VALUES(110, 'kirankumar', 'k@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', '', '', ' ', '9566060228', 'uploads/user_profile/a092af181c8a1d955f3bb6e4111fa8ba45e46b22.png', '', '', 'Active', '89166', '1234', 'Android', '2017-11-23 11:15:55', '2017-11-24 03:35:30');
INSERT INTO `tbl_users` VALUES(111, 'Akila', 'akila. shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'email', 'chennai', 'India', 'tnoo2', '1234567990', '', '', '', 'Active', '62910', '', 'Android', '2017-11-24 04:40:47', '2017-11-24 04:40:47');
