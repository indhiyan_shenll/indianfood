<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
// include_once("header.php");

// $Page = 1;$RecordsPerPage = 25;
// $TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendor 	 = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $itemType 	 = !empty($orderSearch["item_type"]) ? $orderSearch["item_type"] : "" ;
    $itemId 	 = !empty($orderSearch["item_name"]) ? $orderSearch["item_name"] : "" ;
    // if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
    //     $Page = $orderSearch['HdnPage'];
}
?>
<style>
	.itemRow {
		display: none;
	}
</style>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<!-- <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> -->

<div class="portlet-body vendor-category-portlet-body" >
    <table class="table table-striped table-bordered table-hover" id="vendor-catgory-item-list">
        <thead>
            <tr>
                <th>#</th>
                <th>Category name</th>
                <th>Image</th>
            </tr>
        </thead>
        <tbody>
    		<?php
			$Arrvendrcatgry= array();$itemcondn="";
            if ($vendor) {
                $itemcondn.= (!empty($itemType))?" and cateitm.item_type = :item_type":"";
                $itemcondn.= (!empty($itemId))?" and cateitm.item_id = :item_id":"";
                $vendcategryitmsQry = "SELECT *,cate.image as cateimage,cateitm.image as itemimage FROM `tbl_category` as cate JOIN tbl_category_items as cateitm on cate.category_id = cateitm.category_id where cate.vendor_id = :vendor_id $itemcondn order by cate.category_id,cate.category_order";
                $qryParams[':vendor_id'] = $vendor;
                if(!empty($itemType))
                    $qryParams[':item_type'] = $itemType;
                if(!empty($itemId))
                	$qryParams[':item_id'] = $itemId;                
                $getVendcatgryitms = $foodAppApi->funBckendExeSelectQuery($vendcategryitmsQry,$qryParams);
    			if(count($getVendcatgryitms,COUNT_RECURSIVE)>1) {
    				foreach($getVendcatgryitms as $fetchVendcatgryitms) {
    					$category_id 		=  $fetchVendcatgryitms['category_id'];
    					$category_name 		=  $fetchVendcatgryitms['category_name']; 
    					$description 		=  $fetchVendcatgryitms['description'];// Category description
    					$item_name 			=  $fetchVendcatgryitms['item_name'];
                        $short_description  =  $fetchVendcatgryitms['short_description'];
                        $item_type       	=  $fetchVendcatgryitms['item_type'];
                        $price          	=  $fetchVendcatgryitms['price']; // Item price
                        $categoryimage      =  $fetchVendcatgryitms['cateimage'];
                        $itemimage     		=  $fetchVendcatgryitms['itemimage'];                        
                        $Arrvendrcatgry[$category_id] = $category_id;
                        $Arrvendrcatgryname[$category_id][$category_name] = $category_name;
                        $Arrvendrcatgryimg[$category_id][$category_name] = $categoryimage;
    					$Arrvendrcatitms[$category_id][$category_name][$item_name]['item_type'] =  $item_type;
    					$Arrvendrcatitms[$category_id][$category_name][$item_name]['price'] =  $price;
    					$Arrvendrcatitms[$category_id][$category_name][$item_name]['itemimage'] =  $itemimage;
    				}
    			}
            }
            // print_r($Arrvendrcatitms);exit;
            if (count($Arrvendrcatgry)>0) {
            	$vendcatgryitmlist="";$i=1;
        		foreach($Arrvendrcatgry as $categoryid) {
        			foreach($Arrvendrcatgryname[$categoryid] as $category_name) {
        				$vendcatgryitmlist.="<tr>";
        				$vendcatgryitmlist.="<td width='14.5%'>".$i++."</td>";
        				$vendcatgryitmlist.="<td><a data-id=".$categoryid." data-href=cate_".$categoryid." class='cate'>".$category_name."</a></td>";
                        $catgryimg = (!empty($Arrvendrcatgryimg[$categoryid][$category_name]))?"../".$Arrvendrcatgryimg[$categoryid][$category_name]:"../uploads/category/no_food.png";
        				$vendcatgryitmlist.="<td colspan='1'><img width='75px' height='50px' src='".$catgryimg."'></td>";
        				$vendcatgryitmlist.="</tr>";
        				$vendcatgryitmlist.="<tr class='itemRow' id=item_".$categoryid.">
        					<td colspan='3'>
        						<div>";	
        				// echo $category_name."==>";
        				$vendcatgryitmlist.="<table class='table table-striped table-bordered table-hover hiddenRow' id=cate_".$categoryid.">
        					<thead>
                    			<tr>
	                                <th width='14%'>Item</th>
	                                <th style='text-align:center;'>Type</th>
	                                <th style='text-align:center;'>Image</th>
	                                <th style='text-align:center;'>Price</th>
	                            </tr>
	                            </thead><tbody>";
        				foreach($Arrvendrcatitms[$categoryid][$category_name] as $items=>$Arritems) {
        					$item_typeimg    =   "";                                            
                            $item_typeimg    =  ($Arritems['item_type']=='veg')?"<img src='../assets/layouts/layout2/img/veg.png'>":"<img src='../assets/layouts/layout2/img/non-veg.png'>";
                            $itmimg = (!empty($Arritems['itemimage']))?"../".$Arritems['itemimage']:"../uploads/category_items/no_food.png";
        					$vendcatgryitmlist.="<tr>
				                                <td>".$items."</td>
				                                <td align='center'>".$item_typeimg."  ".$Arritems['item_type']."</td>
				                                <td align='center'><img width='75px' height='50px' src='".$itmimg."'></td>
				                                <td align='center'>".$Arritems['price']."</td>
				                            </tr>";
        				}
        				$vendcatgryitmlist.="</tbody></table></div></td></tr>";
        			}
        		}
        		echo $vendcatgryitmlist;
        	} else {
        		echo "<tr><td></td><td align='center'>No Item(s) found</td><td></td></tr>";
        	}
    		?>
        </tbody>
    </table>
</div>
<!-- <script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script> -->
<script type="text/javascript">    
    $(document).ready(function(){
        $(".cate").on("click",function(){  
        	$(".itemRow").slideUp();
        	var refid = $(this).attr("data-href");
        	var itemrowid = $(this).attr("data-id");
        	if ($("#"+refid).is(':visible')) {
        		$("#"+refid).slideUp();
        		$("#item_"+itemrowid).slideUp();
        		return false;
        	}
        	$(".hiddenRow").slideUp();
        	$("#"+refid).slideDown();
        	// $(".itemRow").show();
        	$("#item_"+itemrowid).slideDown();
        });        
    });   
</script>