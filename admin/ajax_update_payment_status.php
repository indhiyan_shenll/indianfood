<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
// error_reporting(E_ALL);

$foodAppApi = new Common($dbconn);

$payment_status = trim($_REQUEST['payment_status']);
$invoice_id = trim($_REQUEST['invoice_id']);
$modified_date = date("Y-m-d H:i:s");


if(trim($_REQUEST['payment_status'])!="") {
	$updateqry = "UPDATE tbl_invoices SET admin_invoice_status = :payment_status  WHERE invoice_id = :invoice_id";
	$qryParams['payment_status'] = $payment_status;
	$qryParams['invoice_id'] = $invoice_id;
	$getvendors = $foodAppApi->funBckendExeUpdateRecord($updateqry,$qryParams);
	echo $getvendors;
}
?>