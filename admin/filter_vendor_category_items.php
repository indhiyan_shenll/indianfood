<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');

$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendor 	 = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $itemType 	 = !empty($orderSearch["item_type"]) ? $orderSearch["item_type"] : "" ;
    $itemId 	 = !empty($orderSearch["item_name"]) ? $orderSearch["item_name"] : "" ;
}
?>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<div class="portlet-body vendor-category-portlet-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered " id="vendor-catgory-item-list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Category Name</th>
                    <th>Item Name</th>
                    <th>Image</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
        	<?php
                $itemcondn="";
                $qryParams=array();
                if ($vendor) {
                    $itemcondn.= (!empty($itemType))?" and cateitm.item_type = :item_type":"";
                    $itemcondn.= (!empty($itemId))?" and cateitm.item_id = :item_id":"";
                    $vendcategryitmsQry = "SELECT *,cate.image as cateimage,cateitm.image as itemimage FROM `tbl_category` as cate JOIN tbl_category_items as cateitm on cate.category_id = cateitm.category_id where  cateitm.status=:status and cate.vendor_id = :vendor_id $itemcondn order by cate.category_id,cate.category_order";
                    $qryParams[':vendor_id'] = $vendor;
                    $qryParams[':status'] = 'active';

                    if(!empty($itemType))
                        $qryParams[':item_type'] = $itemType;
                    if(!empty($itemId))
                        $qryParams[':item_id'] = $itemId;  
                    $getVendcatgryitms = $foodAppApi->funBckendExeSelectQuery($vendcategryitmsQry,$qryParams);

                    if(count($getVendcatgryitms,COUNT_RECURSIVE)>1) {
                        foreach ($getVendcatgryitms as $fetchVendcatgryitms) {
                            $category_id        =  $fetchVendcatgryitms['category_id'];
                            $category_name      =  $fetchVendcatgryitms['category_name']; 
                            $description        =  $fetchVendcatgryitms['description'];// Category description
                            $item_name          =  $fetchVendcatgryitms['item_name'];
                            $short_description  =  $fetchVendcatgryitms['short_description'];
                            $item_type          =  $fetchVendcatgryitms['item_type'];
                            $price              =  $fetchVendcatgryitms['price']; // Item price
                            $categoryimage      =  $fetchVendcatgryitms['cateimage'];
                            $itemimage          =  $fetchVendcatgryitms['itemimage']; 

                                                   
                            $Arrvendrcatgry[$category_id] = $category_id;
                            $Arrvendrcatgryname[$category_id][$category_name] = $category_name;
                            $Arrvendrcatgryimg[$category_id][$category_name] = $categoryimage;
                            $Arrvendrcatitms[$category_id][$category_name][$item_name]['item_type'] =  $item_type;
                            $Arritemcount[$category_id][]=  $item_name;
                            $Arrvendrcatitms[$category_id][$category_name][$item_name]['price'] =  $price;
                            $Arrvendrcatitms[$category_id][$category_name][$item_name]['itemimage'] =  $itemimage;
                        }    
                    }
                }
                //print_r($Arritemcount);
     
                if (count($Arrvendrcatgry)>0) {
                    $vendcatgryitmlist="";
                    $i=1;
                    foreach ($Arrvendrcatgry as $categoryid) {
                        //$vendcatgryitmlist="";
                        foreach ($Arrvendrcatgryname[$categoryid] as $category_name) {
                            $rowspan=count($Arritemcount[$categoryid]);
                            $vendcatgryitmlist.="<tr>";
                            $vendcatgryitmlist.="
                                                <td rowspan='".$rowspan."' class='verticalmiddle'>".$i++."</td>
                                                <td rowspan='".$rowspan."' class='verticalmiddle'>".$category_name."</td>";
                            foreach($Arrvendrcatitms[$categoryid][$category_name] as $items=>$Arritems) {
                                $item_typeimg    =   "";                                            
                                $itmimg = (!empty($Arritems['itemimage']))?"../".$Arritems['itemimage']:"../uploads/category_items/no_food.png";
                                $vendcatgryitmlist.="
                                                    <td class='verticalmiddle'>".$items."</td>
                                                    <td align='center' class='verticalmiddle'><img width='75px' height='50px' src='".$itmimg."'></td>
                                                    <td align='center' class='verticalmiddle'>"."$".$Arritems['price']."</td>";
                                $vendcatgryitmlist.="</tr>";                    
                                        
                            }
                            
                            
                        }
                    }
                    echo $vendcatgryitmlist;
                } else {
                     echo "<tr><td align='center' colspan='5'>No Item(s) found</td></tr>";
                }
        		?>
            </tbody>
        </table>
    </div>
</div>