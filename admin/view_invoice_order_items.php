<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
$foodAppApi = new Common($dbconn);
if ($_REQUEST["invoiceID"] && $_REQUEST["vendorID"]) {
    $invoiceID=$_REQUEST["invoiceID"];
    $vendorID=$_REQUEST["vendorID"];
    $CatagoryQry="SELECT cate.category_id as category_id,cate.category_name,cateItem.item_id,cateItem.item_name,cateItem.image,cateItem.price FROM tbl_category AS cate INNER JOIN tbl_category_items AS cateItem ON cateItem.category_id=cate.category_id WHERE cate. vendor_id=:vendor_id";
    $qryCateParam[":vendor_id"]=$vendorID;
    $getcateResArr= $foodAppApi->funBckendExeSelectQuery($CatagoryQry,$qryCateParam);
    if (count($getcateResArr,COUNT_RECURSIVE)>1) {
        foreach($getcateResArr as $getCatgoryData) {
            $cateid=$getCatgoryData["category_id"];
            $ArrCategoryDetails[$cateid][$getCatgoryData["item_id"]]["categoryName"]=$getCatgoryData["category_name"];
            $ArrCategoryDetails[$cateid][$getCatgoryData["item_id"]]["itemName"]=$getCatgoryData["item_name"];
            $ArrCategoryDetails[$cateid][$getCatgoryData["item_id"]]["images"]=$getCatgoryData["image"];
            $ArrCategoryDetails[$cateid][$getCatgoryData["item_id"]]["price"]=$getCatgoryData["price"];
        }
        //echo"<pre>";print_r($ArrCategoryDetails); exit;
    }
    $packageQry="SELECT pack.package_id,pack.package_name,pack.rate,packItem.category_id,packItem.item_id FROM tbl_packages AS pack INNER JOIN tbl_package_items AS packItem ON packItem.package_id=pack.package_id WHERE pack.vendor_id=:vendor_id";
    $qryPackParam[":vendor_id"]=$vendorID;
    $getpackResArr= $foodAppApi->funBckendExeSelectQuery($packageQry,$qryPackParam);
    if (count($getpackResArr,COUNT_RECURSIVE)>1) {
        foreach($getpackResArr as $getPackageData) {
            $packageid=$getPackageData["package_id"];
            $categoryid=$getPackageData["category_id"];
            $itemid=$getPackageData["item_id"];
            $ArrpackageDetails[$packageid]["packageName"]=$getPackageData["package_name"];
            $ArrpackageDetails[$packageid]["price"]=$getPackageData["rate"];
            $ArrpackageDetails[$packageid]["category_id"]=$categoryid;
            $ArrpackageDetails[$packageid]["itemid"]=$itemid;
        }
        //echo"<pre>";print_r($ArrpackageDetails); exit;
    }
}
// echo"<pre>";
// print_r($ArrpackageDetails); exit;
?>
<div class="col-md-8 col-sm-10 col-xs-8 viewodercate">
    <span><strong> Invoice #</strong> : <span><?php echo $invoiceID; ?></span></span><br>
</div>
<?php
if (file_exists("../invoices/invoice_pdf/Invoice_".$invoiceID.".pdf")) {
    $displayIcon="block";
} else {
    $displayIcon="none";
}
?>
<div class="pdf_icon" style="display:<?php echo $displayIcon; ?>">
   <a id="download_order_items_pdf" ><img class="order_items_pdf" src="../assets/layouts/layout2/img/download.png" onclick="document.location='download_invoice.php?id=<?php echo $foodAppApi->encode($invoiceID);?>'"></a>
</div>

<div class="portlet-body">
    <div class="table-responsive" style="overflow-x: initial;">
        <table class="table table-bordered table-striped table-condensed" id="tbl_order_item">
            <thead class="">
                <tr>
                    <th width="10%">Order #</th>
                    <th width="10%">Item Id</th>
                    <th width="10%">Image</th>
                    <th width="20%">Category Name</th>
                    <th width="20%">Item Name</th>
                    <th width="10%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if (!empty($invoiceID) && !empty($vendorID)) {
                   $Qry="SELECT *, ordr.order_id as order_id,ordritms.price as itemPrice FROM tbl_invoices as invce JOIN tbl_invoice_orders as invceordr ON invce.invoice_id = invceordr.invoice_id JOIN tbl_orders as ordr ON invceordr.order_id = ordr.order_id LEFT JOIN tbl_order_items as ordritms ON ordritms.order_id = ordr.order_id WHERE invce.vendor_id = :vendor_id and invce.invoice_id =:invoice_id";
                    $qryParams[":invoice_id"]=$invoiceID;
                    $qryParams[":vendor_id"]=$vendorID;
                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    //echo "<pre>";print_r($getResCnt);exit;
                    $i=1;
                    $category_name="";
                    $invoiceOrdersPackageArr = array();
                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                        // echo "<pre>";print_r($getResCnt);exit;
                        foreach($getResCnt as $getinvoiceOrder) {
                            $category_id=$getinvoiceOrder["category_id"];
                            $service_charge_percentage=$getinvoiceOrder["service_charge_percentage"];
                            $item_id=$getinvoiceOrder["item_id"];
                            $order_id=$getinvoiceOrder["order_id"];
                            $packageId=$getinvoiceOrder["package_id"];
                            $countItem[$order_id][]=$item_id;
                            if (strtolower($getinvoiceOrder["order_type"])=="normal" && $getinvoiceOrder["package_id"]==0){
                                $invoiceOrdersArr[$order_id][$item_id]["order_id"]=(!empty($getinvoiceOrder["order_id"]))?$getinvoiceOrder["order_id"]:"-";
                                $invoiceOrdersArr[$order_id][$item_id]["item_id"]=(!empty($getinvoiceOrder["item_id"]))?$getinvoiceOrder["item_id"]:"-";
                                $invoiceOrdersArr[$order_id][$item_id]["price"]=(!empty($getinvoiceOrder["itemPrice"]))?$getinvoiceOrder["itemPrice"]:"-";
                                // $invoiceOrdersArr[$order_id][$item_id]["price"]=(!empty($getinvoiceOrder["price"]))?$getinvoiceOrder["price"]:"-";
                                $invoiceOrdersArr[$order_id][$item_id]["category_name"]=$ArrCategoryDetails[$category_id][$item_id]["categoryName"];
                                $invoiceOrdersArr[$order_id][$item_id]["item_name"]=$ArrCategoryDetails[$category_id][$item_id]["itemName"];
                                $invoiceOrdersArr[$order_id][$item_id]["item_image"]=($ArrCategoryDetails[$category_id][$item_id]["images"]=="")?"../uploads/category_items/no_food.png":"../".$ArrCategoryDetails[$category_id][$item_id]["images"];
                                  // $item_price=$ArrCategoryDetails[$category_id][$item_id]["price"];
                            }
                            $prevOrderId = '';
                            if((strtolower($getinvoiceOrder["order_type"])=="package") && ($getinvoiceOrder["package_id"]>0 && $order_id != $prevOrderId)) {
                                $prevOrderId = $order_id;
                                 // Package order
                                $packageId=$getinvoiceOrder["package_id"];
                                $invoiceOrdersArr[$order_id][$packageId] = $ArrpackageDetails[$packageId];


                                // $k = 0;
                                // foreach($ArrpackageDetails as $packid=>$Packages) { 

                                //     $invoiceOrdersPackageArr[$order_id][$packageId][$k]["order_id"]=(!empty($getinvoiceOrder["order_id"]))?$getinvoiceOrder["order_id"]:"-";
                                //     $packcatgry = $ArrCategoryDetails[$Packages["category_id"]][$Packages["item_id"]]["category_name"];
                                //     $packitems = $ArrCategoryDetails[$Packages["category_id"]][$Packages["item_id"]]["item_name"];
                                //     $packcatgry =  (!empty($packcatgry))? $packcatgry: "";
                                //     $packitems =  (!empty($packitems))? $packitems: "";
                                //     $invoiceOrdersPackageArr[$order_id][$packageId][$k]["package_name"]  = $Packages["packageName"];
                                //     $invoiceOrdersPackageArr[$order_id][$packageId][$k]["categoryName"]  = $packcatgry;
                                //     $invoiceOrdersPackageArr[$order_id][$packageId][$k]["itemName"]   = $packitems;
                                //     $invoiceOrdersPackageArr[$order_id][$packageId][$k]["price"] = $Packages["rate"];  
                                //     $k++;
                                // }   
                                    
                            }
                            
                            $i++;
                        }
                    }
                    if(count($invoiceOrdersArr)>0){
                        foreach($invoiceOrdersArr as $order_id=>$invoiceitemsdetails ) {                            
                            $rowspan= "rowspan='".count($invoiceitemsdetails)."'";
                            $orderow = "<td class='viewordermiddle' $rowspan>".$order_id."</td>";
                            foreach($invoiceitemsdetails as $invoicedetails) {
                                $rowspanCount[]=$invoicedetails['item_id'];
                                $itemid=($invoicedetails['item_id']=="")?"-":$invoicedetails['item_id'];
                                $item_image=($invoicedetails['item_image']=="")?"../uploads/category_items/no_food.png":$invoicedetails['item_image'];
                                $categoryname=(empty($invoicedetails['category_name']))?$invoicedetails["packageName"]:$invoicedetails['category_name'];
                                $itemname=(empty($invoicedetails['item_name']))?"-":$invoicedetails['item_name'];
                                $ArrayPrice[]=$invoicedetails['price'];
                                
                                echo "<tr>".$orderow."                                    
                                    <td class='viewordermiddle'>".$itemid."</td>
                                    <td><img width='75px' height='50px' src='".$item_image."'></td>
                                    <td class='viewordermiddle'>".$categoryname."</td>
                                    <td class='viewordermiddle'>".$itemname."</td>
                                    <td class='viewordermiddle'>$".number_format($invoicedetails['price'],2)."</td></tr>";
                                    $i++;
                                $rowspan="";
                                $orderow="";
                            }
                                
                        }

                        $subTotalAmt=array_sum($ArrayPrice);
                        $discount = ($subTotalAmt * $service_charge_percentage)/100;
                        $discountText = $service_charge_percentage."%";
                        $totalAmount = $subTotalAmt - $discount;
                        
                    
                   ?>
                    <tr>
                        <td colspan="5" class="viewordermiddle" style="text-align: right;"><b>Sub Total</b></td>
                        <td class="viewordermiddle"><b><?php echo "$".number_format($subTotalAmt,2);?></b></td>
                    </tr>
                    <tr>
                         <td colspan="5" class="viewordermiddle" style="text-align: right;"><b>Service Charge (<?php echo $discountText;?>)</b></td>
                        <td class="viewordermiddle"><b><?php echo "$".number_format($discount,2);?></b></td>
                    </tr>
                    <tr>
                         <td colspan="5" class="viewordermiddle" style="text-align: right;"><b>Total Amount</b></td>
                        <td class="viewordermiddle"><b><?php echo "$".number_format($totalAmount,2);?></b></td>
                    </tr>
                    <?php
                    } else {
                    
                      echo "<tr>
                            <td colspan='6'>No item order(s) found</td>
                        </tr>";
                        }  


                
                 } else {
                    echo "<tr>
                            <td colspan='6'>No item order(s) found</td>
                        </tr>";
                }    
             ?>
            </tbody>
        </table>
    </div>
</div> 