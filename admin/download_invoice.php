<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
//include_once('../includes/session_check.php');
$foodAppApi = new Common($dbconn);

ob_start();
if(isset($_GET['id'])) {
    $InvoiceId = $foodAppApi->decode($_GET['id']);
}

$InvoiceQuery = "select * from tbl_invoices where invoice_id = :invoice_id";
$qryParams[":invoice_id"]=$InvoiceId;
$InvoiceRow = $foodAppApi->funBckendExeSelectQuery($InvoiceQuery,$qryParams);
$invoice_id = $InvoiceRow[0]['invoice_id'];

if(!empty($invoice_id)){
	$DownloadFile = '../invoices/invoice_pdf/Invoice_'.$invoice_id.'.pdf';
}

if (file_exists($DownloadFile)) 
{
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($DownloadFile));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($DownloadFile));
    ob_clean();
    flush();
    readfile($DownloadFile);
    exit();
}
?>