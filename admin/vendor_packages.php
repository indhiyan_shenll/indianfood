<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
include("header.php");

$foodAppApi = new Common($dbconn);
$emptycondn = array();
$pstflg = false;
if (isset($_GET['vendor']))
    //$vendor = trim(base64_decode($_GET['vendor']));
    $vendor =$foodAppApi->decode($_GET['vendor']);
if(isset($_POST['package_type'])) {
	$package_type 	= trim($_POST['package_type']);
	$pstflg     = true;
}
?>
<style>
 	.custom_height {
    	min-height: 500px !important;
    }
</style>
<form name="vendorpackage_form" id="vendorpackage_form" method="post" action="">
<input type="hidden" name="HdnVendor" id="HdnVendor" value="<?php echo $vendor; ?>">
	<!-- <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
	<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>"> -->
	<div class="page-content" id="vendor-package-content">
	    <div class="row food-orders">
	        <div class="col-md-12">
	            <!-- BEGIN EXAMPLE TABLE PORTLET-->
	            <!-- <div class="page-bar">
	            </div> -->
	            <div class="portlet light customlistminheight">
	                <div class="portlet-title" >
	                    <div class="caption font-dark">
	                        <i class="icon-settings font-dark"></i>
	                        <span class="caption-subject bold uppercase">Package</span>
	                    </div>
	                    <div class="tools"> </div>
	                </div>
	                <div class="portlet-body vendor-package-portlet-body">
	                    <div class="row">                    
	                        <div class="col-md-12 col-sm-12 col-xs-12 remove-left-right-padding">

	                        	<div class="col-md-6 col-sm-6 col-xs-12 remove-left-right-padding">
	                                <div class="col-md-6 col-sm-6 col-xs-12">
	                                    <label>Package:</label>
		                                <select name="package_name" id="package_name" class="form-control">
		                                    <option value="">Select</option>
		                                    <?php
		                                        $Qry1="SELECT * FROM tbl_packages where vendor_id=:vendor_id and status=:status order by package_name asc";
		                                        $qryParams1[":vendor_id"]   =   $vendor; 
		                                        $qryParams1[":status"]   ='active';   
		                                        $getpackages = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
		                                        foreach ($getpackages as $getpackagesData) {
		                                            echo "<option value=".$getpackagesData["package_id"].">".$getpackagesData["package_name"]."</option>";
		                                        }
		                                    ?>
		                                </select>		                                
	                                </div>
	                                <div class="col-md-6 col-sm-6 col-xs-12">
	                                    <label>Food Type:</label>
		                                <select name="package_type" id="package_type" class="form-control">
		                                    <option value="">Select</option>
		                                    <?php
		                                         $Qry2="SELECT * FROM tbl_packages where vendor_id=:vendor_id and status=:status group by package_type order by package_name asc";
		                                        $qryPara[":vendor_id"]   =   $vendor; 
		                                        $qryPara[":status"]   ='active';
		                                        $getFoodType = $foodAppApi->funBckendExeSelectQuery($Qry2,$qryPara);
		                                        foreach ($getFoodType as $getpackagesFoodData) {
		                                            echo "<option value=".$getpackagesFoodData["package_type"].">".$getpackagesFoodData["package_type"]."</option>";
		                                        }
		                                    ?>
		                                </select>                           
	                                </div>
	                            </div>
	                            <div class="col-md-6 col-sm-6 col-xs-12 search-orderlist-btns remove-left-right-padding" id="vndrpckfltr">
	                                <div class="col-md-12 col-sm-12 col-xs-12">
	                                    <a type="button" class="btn yellow custombtn" id="searchbtn"><i class="fa fa-search"></i> Search</a>
                                        <a type="button" class="btn red custombtn" id="resetbtn"><i class="fa fa-times-circle"></i> Reset</a>
                                        <a class="btn dark custombtn" id="bckbtn" href="vendors_listing.php"><i class="fa fa-arrow-left"></i> Back</a>
	                                </div>
	                            </div>
	                        </div>
						</div>
	                </div>
	                <!-- testing table -->
	                <div class="loadingreportsection">
	                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
	                </div>               
	                <!-- testing table ends here -->
	                <!-- AJAX response will be appending here -->
	                <div id="vendorPackagesTable"></div>
	            </div>
	        </div>
	    </div>
	</div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/custom_vendor_packages.js" type="text/javascript"></script>