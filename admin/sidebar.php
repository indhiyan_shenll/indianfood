<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <?php 
            $navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
        ?>

        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo ($navigationURL=='dashboard.php')?'active':''; ?> open side-menu-logo">
                <a href="dashboard.php" class="side-menu nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='customer_listing.php')?'active':''; ?> side-menu-logo">
                <a href="customer_listing.php" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Customers</span>
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='vendors_listing.php' || $navigationURL=='vendor_orders_listing.php' || $navigationURL=='vendor_category_items.php' || $navigationURL=='vendor_packages.php')?'active':'';?> side-menu-logo">
                <a href="vendors_listing.php" class="nav-link nav-toggle">
                    <i class="icon-user-female"></i>
                    <span class="title">Aunties</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='reports.php')?'active':'';?> side-menu-logo">
                <a href="reports.php" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Reports</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='invoice.php' || $navigationURL=='invoice_order.php')?'active':'';?> side-menu-logo">
                <a href="invoice.php" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Invoices</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL=='setting.php')?'active':'';?> side-menu-logo">
                <a href="setting.php" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Settings</span>            
                    <span class="selected"></span>
                    <!-- <span class="arrow open"></span> -->
                </a>                            
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->