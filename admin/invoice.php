<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');

if (isset($_POST["vendor"])) {
    $vendorname=trim($_POST["vendor"]);
}

if (isset($_POST["status"])) {
    $status=trim($_POST["status"]);
}
	/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/
include("header.php");
$foodAppApi = new Common($dbconn);
?>
<form name="invoice_list_form" id="invoice_list_form" method="post" action="">
<!-- BEGIN CONTENT BODY -->
<!-- <div id="AddPaymentDetails" class="modal fade" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="opacity: .8; "></button>
                <h4 class="modal-title">Update Payment Status</h4>
            </div>
            <div class="col-md-11 col-sm-12 col-xs-12 remove-left-right-padding" id="paymentStatusNotificationMsg">
                <div class="alert alert-success fade in alert-dismissable customsuccessalert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Status update successfully!</strong>
                </div>
            </div>
            <div class="modal-body clearfix">
                <input type="hidden" name="hnd_invoice_id" id="hnd_invoice_id" />
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12 margintop10">
                        <div class="col-md-4">
                            <div class="control-label label_name text-box">Admin Status :<l style="color:red;">*</l></div>
                        </div>
                        <div class="col-md-8">
                            <select name="payment_status" id="payment_status" class="form-control input-large">
                                <option value="">-- Select --</option>
                                <option value="Paid">Paid</option>
                                <option value="Pending">Pending</option>
                            </select>
                            <label class="error" id="payment_status_err" >Please select admin status</label>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="modal-footer" style="margin-top:15px;">
                <button type="button" class="btn green custombtn customupdate" id="SavePaymentDetails"> <i class="fa fa-floppy-o"></i> Update</button>
                <button type="button"  id="close_btn" class="btn red custombtn reset save-note" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</button>                
            </div>
        </div>
    </div>
</div> -->
<div class="page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light custom_height">
                <div class="portlet-title" style="margin-bottom: 10px;">

                    <div class="caption font-dark">
                        <i class="icon-docs font-dark"></i>
                        <span class="caption-subject bold uppercase">Invoices</span>
                    </div>
                    <div class="pull-right">  <a href="invoice_order.php" class="btn dark invoicecustombtn customcreate"><i class="fa fa-file-pdf-o"></i> Create Invoice</a> </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch" id="customerlistresponsive">
                            <div class="col-md-8 col-sm-8 col-xs-12 remove-left-right-padding">
                                 <div class="col-md-5 col-sm-5 col-xs-12">
                                    <label>Aunty:</label>
                                     <input type="text" name="vendor" id="vendor" class="form-control" placeholder="Aunty Name" value="<?php echo $vendorname ?>">
                                </div>
                                
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <label>Status:</label>
                                    <select  name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="paid">Paid</option>
                                        <option value="pending">Pending</option>
                                    </select>
                                    <script>$("#status").val("<?php echo $status;?>")</script>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 remove-left-right-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                    <button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-search"></i> Search</button>
                                    <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <!-- AJAX response will be appending here -->
                <div id="invoicelist">
                    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                    
                </div>

                <!-- <div class="portlet-body flip-scroll" id="invoicelist">
                <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">

                </div> -->
                
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
</div>
<div class="modal fade bs-modal-lg in"  data-toggle="modal" data-backdrop="static" data-keyboard="false" id="invoiceviweorderitem" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
                        <h4 class="modal-title">View Order Items</h4>
                    </div>
                    <div class="modal-body">
                        <div id="invoiceviweorderitemList">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn red custombtn" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <!-- <button type="button" class="btn green">Save changes</button> -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/invoice.js" type="text/javascript"></script>