<?php    
    include_once('../includes/configure.php');
    include_once('../api/Common.php');
    $foodAppApi = new Common($dbconn);
    $loginerror_message="";
    if(isset($_POST['txtbx_email'])&&($_POST['txtbx_email'])!="" && isset($_POST['txtbx_pass']) && $_POST['txtbx_pass']!="")
    {
        //Posted values of the user name and Password.
        $login_username =addslashes(trim($_POST['txtbx_email']));
        $login_password =trim($_POST['txtbx_pass']);
        //Find wether given login name and password exists in the database.
        $AdminloginQuery="select * from tbl_users where email = :login_username and password = :login_password and user_type = :user_type and status = :status";
        $qryParams[":login_username"] = $login_username;
        $qryParams[":login_password"] = $login_password;
        $qryParams[":user_type"] = "admin";
        $qryParams[":status"] = "active";
        $getAdmin = $foodAppApi->funBckendExeSelectQuery($AdminloginQuery,$qryParams);        
        if(count($getAdmin,COUNT_RECURSIVE)>1) {            
            foreach($getAdmin as $fetchAdmin) {        
                //SETTING THE DETAILS ABOUT THE LOGGED IN USER IN THE SESSION                
                $_SESSION['admin_id']    =   $fetchAdmin['user_id'];
                $_SESSION['admin_name']  =   $fetchAdmin['full_name'];
                header("Location:dashboard.php");
                exit;
            }
        }
        else
            $loginerror_message = "Login failed invalid email or password";
    }
?>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Kaza | <?php echo (is_numeric(strpos($_SERVER['REQUEST_URI'], "admin"))?"Admin":"User"); ?> Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/global/css/login-4.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/> 
    </head>
    <!-- END HEAD -->
    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a>
                <img src="../assets/layouts/layout2/img/dish.png" alt="" /> 
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form action="index.php" name="frm_login" id="frm_login" method="post">
                <h3 class="form-title"><?php echo (is_numeric(strpos($_SERVER['REQUEST_URI'], "admin"))?"Admin Login":"User Login"); ?></h3>
                <div class="alert alert-danger <?php echo (empty($loginerror_message))?'display-hide':'' ?>">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo $loginerror_message;?></span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->                    
                    <label class="control-label visible-ie8 visible-ie9" for="txtbx_email">Email:</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>                       
                        <input class="form-control placeholder-no-fix" type="text" id="txtbx_email" name="txtbx_email" placeholder="Email" autocomplete="off" class="email-field" />
                    </div>
                </div>
                <div class="form-group">                    
                    <label class="control-label visible-ie8 visible-ie9" for="txtbx_pass">Password:</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>                        
                        <input class="form-control placeholder-no-fix" type="password" id="txtbx_pass" name="txtbx_pass" value="" placeholder="Password" autocomplete="off" class="password-field"/>
                    </div>
                </div>
                <div class="form-actions">                    
                    <button type="submit" class="btn green pull-right" id="login-button"> Login </button>
                </div>
                        
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> <?php echo date("Y")?> &copy; Kaza food - Admin</div>
        <!-- END COPYRIGHT -->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/global/scripts/login-4.min.js" type="text/javascript"></script>       
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
</html>



<script>
    $(document).ready(function(){
        var disp="";
        var validator=$("#frm_login").validate({          
            rules: {             
              txtbx_email:  "required",
              txtbx_pass:   "required",             
            },
            messages: {
                txtbx_email:    "Please enter Email",
                txtbx_pass:     "Please enter Password",                
            },
            submitHandler: function(form) {
                form.submit();                
            }
        });
        $(".error").css("color","red");
    });
</script>
<style type="text/css">
    .error {
        color: red !important;
    }
</style>