<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$customername="";
$email="";
$status="";
if (isset($_POST["customer"])) {
    $customername=trim($_POST["customer"]);
}
if (isset($_POST["email"])) {
    $email=trim($_POST["email"]);
}
if (isset($_POST["status"])) {
    $status=trim($_POST["status"]);
}
/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/
include("header.php");
$foodAppApi = new Common($dbconn);
?>
<form name="customerlist_form" id="customerlist_form" method="post" action="">
<!-- BEGIN CONTENT BODY -->
<div class="page-content" id="customer-list-page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light customlistminheight">
                <div class="portlet-title" >
                    <div class="caption font-dark caption-new">
                        <i class="icon-users font-dark"></i>
                        <span class="caption-subject bold uppercase">Customers</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch" id="customerlistresponsive">
                            <div class="col-md-8 col-sm-8 col-xs-12 remove-left-right-padding">
                                 <div class="col-md-6 col-sm-4 col-xs-12">
                                    <label>Customer:</label>
                                     <input type="text" name="customer" id="customer" class="form-control" placeholder="Customer Name" value="<?php echo $customername ?>">
                                </div>
                                <div class="col-md-6 col-sm-4 col-xs-12">
                                    <label>Email:</label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email Address" value="<?php echo $email ?>">
                                </div>
                                <!-- <div class="col-md-4 col-sm-4 col-xs-12"> -->
                                    <!-- <label>Status:</label>
                                    <select  name="status" id="status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">In-Active</option> -->                                        
                                    <!-- </select> <script>$("#status").val("<?php echo $status;?>")</script>-->
                                    
                                <!-- </div> -->
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 remove-left-right-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                    <button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-search"></i> Search</button>
                                    <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 alertmsg">
                    <div class="successmsg"></div>
                </div>
                <!-- AJAX response will be appending here -->
                <div id="customerListTable"> 
                    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

    </div>
    <div class="modal fade in" id="CustomerStatus" tabindex="-1" role="basic" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Customer Status</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-10 col-md-offset-2 col-sm-offset-2 col-xs-offset-1 popupdropdown">
                            <input type="hidden" name="customerID" id="customerID">
                            <label>Status:</label>
                            <select  name="popstatus" id="popstatus" class="form-control">
                                <option value="">Select</option>
                                <option value="Active">Active</option>
                                <option value="InActive">InActive</option>
                                <option value="Block">Block</option>
                            </select>
                            <div class="error statusmsg" style="display:none;">Please select status</div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn red custombtn" data-dismiss="modal">Close</button>
                    <button type="button" class="btn green custombtn" id="PopupStatus">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/customer.js" type="text/javascript"></script>