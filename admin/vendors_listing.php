<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/
include_once('../includes/session_check.php');

$foodAppApi = new Common($dbconn);
$emptycondn = array();

$vendors="";
$email="";
$status_fltr="";
if (isset($_POST["vendors"])) {
    $vendors=trim($_POST["vendors"]);
}
if (isset($_POST["vendor_mail"])) {
    $email=trim($_POST["vendor_mail"]);
}
if (isset($_POST["status_fltr"])) {
    $status_fltr=trim($_POST["status_fltr"]);
}
include("header.php");
?>
<form name="vendorlist_form" id="vendorlist_form" method="post" action="">
<!-- BEGIN CONTENT BODY -->
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<div id="vendorstatus" class="modal fade" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="opacity: .8; "><img src="../assets/layouts/layout2/img/close.png" title="close"></button>
                <h4 class="modal-title">Aunty Status</h4>
            </div>
            <div class="modal-body clearfix">
                <input type="hidden" name="hnd_invoice_id" id="hnd_invoice_id" />
                <div class="col-md-12 col-sm-12 col-xs-12">                    
                    <div class="col-md-12 col-sm-12 col-xs-12 margintop10">
                        <div class="col-md-4">
                            <div class="control-label label_name text-box">Status:<l style="color:red;">*</l></div>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="HdnVendor" id="HdnVendor" value="<?php echo $vendorid;?>">
                            <select name="status" id="status" class="form-control input-large">
                                <option value="">Select</option>
                                <option value="active">Active</option>
                                <option value="pending">Pending</option>
                                <option value="block">Block</option>
                            </select>
                            <!-- <label class="error" id="payment_type_err" >Please select payment type</label> -->
                        </div>
                    </div>  
                </div>
            </div>
            <div class="modal-footer" style="margin-top:15px;">
                <a type="button" class="btn green custombtn customupdate" id="Savevendorstatus"><i class="fa fa-floppy-o"></i> Update</a>
                <a type="button" class="btn red custombtn" id="close_btn" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</a>
            </div>
        </div>
    </div>
</div>   

<div class="page-content" id="vendor-list-page-content">
    <div class="row food-orders">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="page-bar">
            </div>
            <div class="portlet light customlistminheight">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Aunties</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 alertmsg">                    
                    <div class="alert-success"></div>
                </div>
                <?php
                    $Qry1="SELECT * FROM tbl_users where user_type=:user_type order by full_name asc";
                    $qryParams1[":user_type"]   =   "vendor";       
                    $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
                ?>
                <div class="portlet-body search-body vendor-list-portlet-body">
                    <div class="row">                        
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-8 col-sm-8 col-xs-12 remove-left-right-padding">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Aunty:</label>
                                    <input class="form-control" type="text" name="vendors" id="vendors" value="<?php echo $vendors?>">                      
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Email:</label>
                                    <input class="form-control" type="text" name="vendor_mail" id="vendor_mail" value="<?php echo $email ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Status:</label>
                                    <select name="status_fltr" id="status_fltr" class="form-control">
                                        <option value="">Select</option>                               
                                        <option value="active">Active</option>
                                        <option value="pending">Pending</option>
                                        <option value="block">Block</option>
                                    </select> 
                                   <script>$("#status_fltr").val("<?php echo $status_fltr;?>")</script>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a type="button" class="btn yellow custombtn" id="searchbtn"><i class="fa fa-search"></i> Search</a>
                                    <a type="button" class="btn red custombtn" id="resetbtn"><i class="fa fa-times-circle"></i> Reset</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>                
                <!-- AJAX response will be appending here -->
                <div id="vendorListingTable">
                    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/custom_vendor_listing.js" type="text/javascript"></script>
<!-- <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> -->