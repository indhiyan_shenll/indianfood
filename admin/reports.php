<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$customer_id = "";
$vendors_id = "";
$start_date = "";
$end_date = "";
$pstflg = false;
if (isset($_GET["cusid"])) {
  //$customer_id=base64_decode($_GET["cusid"]); 
   $customer_id=$foodAppApi->decode($_GET["cusid"]);
}

if(isset($_GET["vendor"])){
	$vendors_id=$foodAppApi->decode($_GET["vendor"]);
}

if (isset($_POST["customer"]))	{
	$customer_id=$_POST["customer"];
	 $pstflg     = true;
} 

if (isset($_POST["vendors"])) {
	$vendors_id=$_POST["vendors"];
	 $pstflg     = true;
}
if (isset($_POST["start_date"])) {
	$start_date=$_POST["start_date"];
	 $pstflg     = true;
}
if (isset($_POST["end_date"]))	{
	$end_date=$_POST["end_date"];
	 $pstflg     = true;
}
if (isset($_POST["status"]))	{
	$status=$_POST["status"];
	 $pstflg     = true;
}
if (isset($_POST["dashboard_widget_type"]))	{

	function getWeek($week, $year) {
	  $dto = new DateTime();
	  $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
	  $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
	  return $result;
	}

	if ($_POST["dashboard_widget_type"]=='daily') {
		$start_date=date("m/d/Y");
		$end_date=date("m/d/Y");
		$pstflg = true;
	} else if ($_POST["dashboard_widget_type"]=='weekly') {
		$signupdate=date("Y-m-d");
		$signupweek=date("W",strtotime($signupdate));
		$year=date("Y",strtotime($signupdate));
		$currentweek = date("W");
		$result = getWeek($currentweek,$year);
		// print_r($result);
		$start_date=date('m/d/Y',strtotime($result['start']));
		$end_date=date('m/d/Y',strtotime($result['end']));
		$pstflg = true;
	} else if ($_POST["dashboard_widget_type"]=='monthly') {
		$first_day_this_month = date('m/01/Y'); // hard-coded '01' for first day
		$last_day_this_month  = date('m/t/Y');	
		$start_date=$first_day_this_month;
		$end_date=$last_day_this_month;
		$pstflg = true;
	} else{
		$start_date="";
		$end_date="";
	}
}

if(!empty($vendors_id)){
	$CreateInvoiceHref="invoice_order.php?vendor=".$foodAppApi->encode($vendors_id);
} else {
	$CreateInvoiceHref="invoice_order.php";
}
/****Paging ***/
$Page=1;$RecordsPerPage=1;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;
/*End of paging*/
// echo $Page; exit
include("header.php");
//echo $customer_id."dfhdhf";
?>

<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTENT BODY -->
<form name="reportlist_form" id="reportlist_form" method="post" action="" >
<div class="page-content" id="reports-page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light customlistminheight">
                <div class="portlet-title">
                	<div class="caption font-dark caption-new">
                		<i class="icon-layers font-dark"></i>
	                    <span class="caption-subject bold uppercase">Reports</span>
                    </div>
                    <div class="pull-right"><a href="<?php echo $CreateInvoiceHref; ?>" class="btn dark invoicecustombtn customcreate" id="appendURL"><i class="fa fa-file-pdf-o"></i> Create Invoice</a> </div>
                </div>
                <div class="portlet-body">
                	<div class="row">
	                    <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch" id="reportsearch">
	                    	<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 remove-left-right-padding">
	                    		<div class="col-md-3 col-sm-3 col-xs-12">
			                		<label>Customer:</label>
			                		<select name="customer" id="customer" class="form-control">
			                			<option value="">Select</option>
			                			<?php
				                			$Qry2="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
				                            $qryParams2[":user_type"]="customer";
				                            $qryParams2[":status"]="Active";
				                            $getCustomers = $foodAppApi->funBckendExeSelectQuery($Qry2,$qryParams2);
				                            if (count($getCustomers,COUNT_RECURSIVE)>1) {
					                            foreach ($getCustomers as $getCustomerData) {
					                            	$datauserid=base64_encode($getCustomerData["user_id"]);
					                            	if($getCustomerData["user_id"]==$customer_id)
					                            		$selected="selected";
					                            	else
					                            		$selected="";
					                            	echo "<option value=".$getCustomerData["user_id"]." data-userid=".$datauserid." ".$selected." >".$getCustomerData["full_name"]."</option>";
					                            }
					                        }
			                			?>
			                		</select>
								</div>
	                    		<div class="col-md-3 col-sm-3 col-xs-12">
			                		<label>Aunty</label>
			                		<select name="vendors" id="vendors" class="form-control">
			                			<option value="">Select</option>
			                			<?php
				                			$Qry1="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
				                            $qryParams1[":user_type"]="vendor";
				                            $qryParams1[":status"]="Active";
				                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
				                            if (count($getvendors,COUNT_RECURSIVE)>1) {
					                            foreach ($getvendors as $getVendorsData) {
					                            	if($getVendorsData["user_id"]==$vendors_id)
					                            		$selected="selected";
					                            	else
					                            		$selected="";
					                            	echo "<option value=".$getVendorsData["user_id"]." data-id=".$foodAppApi->encode($getVendorsData["user_id"])." ".$selected.">".$getVendorsData["full_name"]."</option>";
					                            }
					                        }
			                			?>
			                		</select>
								</div>
	                    		<div class="col-md-2 col-sm-2 col-xs-12">
			                		<label>From Date:</label>
			                		<input type="text" name="start_date" id="start_date" class="form-control form-control-inline  date-picker" placeholder="From Date" value="<?php echo $start_date; ?>">
								</div>
								<div class="col-md-2 col-sm-2 col-xs-12">
			                		<label>To Date:</label>
			                		<input type="text" name="end_date" id="end_date" class="form-control form-control-inline  date-picker" placeholder="To Date" value="<?php echo $end_date; ?>">
								</div>
								<div class="col-md-2 col-sm-2 col-xs-12">
			                		<label>Status</label>
			                		<select name="status" id="status" class="form-control">
			                			<option value="">Select</option>
			                			<?php
				                			$Qry1 = "SELECT DISTINCT(status) FROM tbl_orders where status <> '' order by order_id desc";
				                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
				                            if (count($getvendors,COUNT_RECURSIVE)>1) {
					                            foreach ($getvendors as $getVendorsData) {
					                            	echo "<option value=".ucfirst($getVendorsData["status"]).">".ucfirst($getVendorsData["status"])."</option>";
					                            }
					                        }
			                			?>
			                		</select>
			                		<script>$("#status").val("<?php echo $status ?>")</script>
								</div>
							</div>
	                    	<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
	                    		<!-- <div class="col-md-2 col-sm-2 col-xs-12 remove-left-right-padding">
	                    		</div> -->
	                    		<div class="col-md-12 col-sm-12 col-xs-12 remove-left-right-padding pull-right">
		                    		<button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-search"></i> Search</button>
			                		<?php
			                		if (isset($_GET["cusid"])) {
			                			$custid=$_GET["cusid"];
			                		?>
			                		<button type="button"  data-id="Reset" value="<?php echo $custid;?>" class="btn red custombtn reset" id="reportReset"><i class="fa fa-times-circle"></i> Reset</button>
			                	    <a class="btn dark custombtn" href="customer_listing.php"><i class="fa fa-arrow-left"></i> Back</a>
			                		<?php
			                		} else{
			                		?>
			                		<button type="button" class="btn red custombtn reset" id="reportReset"><i class="fa fa-times-circle"></i> Reset</button>
			                		<?php
									}
			                		?>
			                	</div>
	                    	</div>
		                	
						</div>
					</div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <div id="reportsListTable"> 
                	<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
				</div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <div class="modal fade bs-modal-lg in"  data-toggle="modal" data-backdrop="static" data-keyboard="false" id="viweorderitem" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
                        <h4 class="modal-title">View Order Items</h4>
                    </div>
                    <div class="modal-body">
                    	<div id="viweorderitemList">
                    	</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn red custombtn" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        <!-- <button type="button" class="btn green">Save changes</button> -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
	</div>
</div>
</form>
<?php include("footer.php");?>
<script src="../assets/layouts/layout2/scripts/reports.js" type="text/javascript"></script>