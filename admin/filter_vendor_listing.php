<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
// include_once("header.php");

$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendorId = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $vendorMail = !empty($orderSearch["vendor_mail"]) ? trim($orderSearch["vendor_mail"]) : "" ;
    $statusFltr = !empty($orderSearch["status_fltr"]) ? $orderSearch["status_fltr"] : "" ;
    if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
        $Page = $orderSearch['HdnPage'];
}
?>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
            
<div class="portlet-body" style="padding-top: 0px;">
    <table class="table table-striped table-bordered table-hover" id="food-vendor-list">
        <thead>
            <tr>
                <th>#&nbsp;&nbsp;</th>
                <th>Aunty</th>
                <th>Email</th>
                <th>Address</th>
                <th>Mobile number</th>                      
                <th>Ratings</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        		<?php
        			$qryParams = array();
                	$orderTypeCodtn = $vendorCondtn = "";
                	$whereCondtn = "";
                    if (!empty($vendorId)) {
                        $whereCondtn.= " and full_name like :full_name";
                    } 
                    if(!empty($vendorMail)) {
                        $whereCondtn.= " and email like :email";
                    }
                    if (!empty($statusFltr)) {
                        $whereCondtn.= " and usr.status = :status";
                    }
        			$Arrvendor_id = array();
                    // Overall ratings
        			$vendrQry = "SELECT *,if(TRUNCATE(avg(ratings),1)>0,TRUNCATE(avg(ratings),1),0) as overall_ratings,usr.status as usrstatus FROM `tbl_users` as usr left JOIN tbl_orders as ordr ON user_id = vendor_id where user_type = :user_type $whereCondtn group by user_id order by user_id desc";
        			$qryParams[':user_type'] 	= 'vendor';
        			if(!empty($vendorId))
                        $qryParams[':full_name'] 	= "%".$vendorId."%";
                    if(!empty($vendorMail))
                        $qryParams[':email'] 	= "%".$vendorMail."%";
                    if(!empty($statusFltr))
                        $qryParams[':status'] 	= $statusFltr;                    
        			$i=1;$vendorlist="";
        			$getVendordetails = $foodAppApi->funBckendExeSelectQuery($vendrQry,$qryParams);
        			if(count($getVendordetails,COUNT_RECURSIVE)>1) {
        				$TotalPages=ceil(count($getVendordetails)/$RecordsPerPage);
                        $Start=($Page-1)*$RecordsPerPage;
                        $sno=$Start+1;
                        $vendrQry.=" limit $Start,$RecordsPerPage";
                   		$getVendors = $foodAppApi->funBckendExeSelectQuery($vendrQry,$qryParams);
        				foreach($getVendors as $fetchVendors) {
        					$user_id 		= $fetchVendors['user_id'];
        					$full_name 		= $fetchVendors['full_name'];
        					$email 			= $fetchVendors['email'];
        					$address 		= $fetchVendors['address'];
        					$mobile_number 	= $fetchVendors['mobile_number'];
                            $overall_ratings = $fetchVendors['overall_ratings']; // Average rating for particular vendors
                            $status         = $fetchVendors['usrstatus'];
                            $documents      = $fetchVendors['upload_documents'];
        					$vendorid ="";
                            $document_link  = (!empty($documents))?"../".$documents:"javascript:void(0);";
        					$vendorid = $foodAppApi->encode($user_id);
        					$vendorlist.="<tr>";
        					$vendorlist.="<td>".$sno."</td>";
        					$vendorlist.="<td>".$full_name."</td>";
        					$vendorlist.="<td>".$email."</td>";
        					$vendorlist.="<td class='vendor-address'>".$address."</td>";
        					$vendorlist.="<td>".$mobile_number."</td>";
        					if ($status=="active")
        						$statusimg="<img data-status='active' class='vendor_status' src='../assets/layouts/layout2/img/active.png'>";
        					elseif($status=="pending")
        						$statusimg="<img data-status='pending' class='vendor_status' src='../assets/layouts/layout2/img/waiting.png'>";
        					elseif($status=="block")
        						$statusimg="<img data-status='block' class='vendor_status' src='../assets/layouts/layout2/img/block.png'>";
        					$vendorlist.="<td><div title=".$overall_ratings." id='overallratings_$sno'></div></td>";
                            $vendorlist.="<td><a id='status_".$user_id."' class='vendor_status' data-id='".$user_id."'>".$statusimg."</a></td>";
        					$rating_id = "#overallratings_$sno";
        					$vendorlist.="<script>$('".$rating_id."').rateYo({rating:$overall_ratings,readOnly: true,starWidth: '15px'});</script>";
        					$vendorlist.= "<td  class='vendor-actions'>
											    <div class='btn-group vendor-list-action'>
											        <button class='btn btn-xs default dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'> Actions
											            <i class='fa fa-angle-down'></i>
											        </button>
											        <ul class='dropdown-menu pull-right dropdown-menu-new' role='menu'>                          
											            <li>
											                <a href='vendor_category_items.php?vendor=$vendorid'>
											                    <i class='icon-tag'></i> Category & items 
											                </a>
											            </li>
                                                        <li>
                                                            <a href='reports.php?vendor=$vendorid'>
                                                                <i class='fa fa-file-pdf-o'></i> Create Invoice
                                                            </a>
                                                        </li> 
											            <li>
											                <a href='vendor_packages.php?vendor=$vendorid'>
											                    <i class='icon-layers'></i> Packages 
											                </a>
											            </li>
                                                        <li>
                                                            <a target='_blank' href='$document_link'>
                                                                <i class='icon-docs'></i> View Documents
                                                            </a>
                                                        </li>
                                                          
											        </ul>
											    </div>
											</td>";
                            $sno++;
        				}
        				echo $vendorlist;
        			} 
        		?>                        		
        </tbody>
    </table>
</div>
<?php
if ($TotalPages > 1) {
    echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
    $FormName = "vendorlist_form";
    require_once ("paging.php");
    echo "</td></tr>";
}
?>
<script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>
<!-- <script src="../assets/layouts/layout2/scripts/filter_vendor_listing.js" type="text/javascript"></script> -->
<script>
	$(document).ready(function(){
		$(".vendor_status").on("click",function(){	        
	        $("#vendorstatus").modal('show');
	        var vendorid ="";
	        vendorid = $(this).attr('data-id');
	        $("#HdnVendor").val(vendorid);
            var curentstatus = $(this).find(">img").attr("data-status")
            if(curentstatus!="undefined" || curentstatus!="")
                $("#status").val(curentstatus);
	    });	
	});
</script>