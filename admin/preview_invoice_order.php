<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$ArrInvoiceId=array();

if(isset($_REQUEST["ArrInvoiceJson"])) {
	$ArrInvoiceJson=$_REQUEST["ArrInvoiceJson"];
	$InvoiceJson=json_decode($ArrInvoiceJson);
	foreach($InvoiceJson as $getInvoiceId) {
	    $ArrInvoiceId[]=$getInvoiceId;
	}
	$vendorid = $_REQUEST["vendorid"];
	$created_date = date("Y-m-d H:i:s");
}
?>
<form name="filter_invoice_order_form" id="filter_invoice_order_form" method="post" action="">
<!-- BEGIN CONTENT BODY -->
<div class="">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_invoice_list">
                        <thead class="flip-content">
                            <tr>
                                <th class="text-center">S.No</th>
                                <th class="text-center">Order #</th>
                                <th class="text-center">Package ?</th>
                                <!-- <th></th> -->
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $qryParams=array();
                        if (count($ArrInvoiceId)>0) {
                        	$DiscountQry="SELECT * FROM tbl_settings WHERE setting_name='service_charge'";
                    		// $DiscountqryParams['order_id'] = $ArrInvoiceValue;
							$getResDiscount = $foodAppApi->funBckendExeSelectQuery($DiscountQry);
                        	$discountVal = $getResDiscount[0]['setting_value'];
                        	$discountType = $getResDiscount[0]['setting_type'];
                        	// $ArrOreders = explode(ArrInvoiceId, string)
                        	$subTot=0;$invoice_amount=0;$discount_amount=0;$sno=1;$invoiceHTML = "";
                        	foreach ($ArrInvoiceId as $ArrInvoicekey => $ArrInvoiceValue) {
                        		$Qry="SELECT order_id,package_id,price from tbl_orders where order_id = :order_id";
                        		$qryParams['order_id'] = $ArrInvoiceValue;
								$getResInvoice = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
								// print_r($getResInvoice);
									
								$proCount = count($getResInvoice);
								$subTotAmt=0;$discountText="";$discount=0;
								foreach ($getResInvoice as $key => $OrderDetails) {
									$invoiceHTML .= "<tr>"; 
									$OrderDetails["package_id"] = empty($OrderDetails["package_id"])?"No":"Yes";

									$invoiceHTML .= "<td class='text-center'>".$sno."</td>";
									$invoiceHTML .= "<td class='text-center'>".$OrderDetails["order_id"]."</td>";
									$invoiceHTML .= "<td class='text-center'>".$OrderDetails["package_id"]."</td>";
							   		// $invoiceHTML .= "<td></td>";
							   		$invoiceHTML .= "<td>$".$OrderDetails["price"]."</td>";
							   		$invoiceHTML .= "</tr>";
							   		$subTotAmt += $OrderDetails["price"];
							   		// $proCount++;
							   		$sno++;
								}
								$subTot += $subTotAmt;
                        	}
                        	if ($discountType=='percent') {
								$discount = ($subTot * $discountVal)/100;
								$discountText = $discountVal."%";
							} else {
								$discount = $discountVal;
								$discountText = "$ ".number_format($discountVal);
							}
							$totalAmount = $subTot - $discount;

							$invoiceHTML .= "<tr><td colspan='3' class='customalign'>SubTotal</td><td class='customalign'>$ ".number_format($subTot,2)."</td></tr>";
                        	$invoiceHTML .= "<tr><td colspan='3' class='customalign'>Service Charge (".$discountText.")</td><td class='customalign'>$ ".number_format($discount,2)."</td></tr>";
							$invoiceHTML .= "<tr><td colspan='3' class='customalign'>Total</td><td class='customalign'>$ ".number_format($totalAmount,2)."</td></tr>";
							echo $invoiceHTML;
                        	$develop_status = "preview";
							$InsertPreviewQry="INSERT INTO tbl_invoices (vendor_id, total_amount, invoice_amount, service_charge, service_charge_percentage,develop_status, created_date, modified_date) VALUES (:vendor_id,:total_amount, :invoice_amount, :service_charge, :service_charge_percentage, :develop_status, :created_date, :modified_date);";
							$InsertPreviewParams['vendor_id'] = $vendorid;
							$InsertPreviewParams['total_amount'] = $subTot;
							$InsertPreviewParams['invoice_amount'] = $totalAmount;
							$InsertPreviewParams['service_charge'] = number_format($discount,2);
							$InsertPreviewParams['service_charge_percentage'] = $discountVal;
							$InsertPreviewParams['develop_status'] = $develop_status;
							$InsertPreviewParams['created_date'] = $created_date;
							$InsertPreviewParams['modified_date'] = $created_date;
							$getPreviewId = $foodAppApi->funBckendExeInsertRecord($InsertPreviewQry,$InsertPreviewParams);
							// print_r($getPreviewId);exit;
						} else {
                            echo "<tr><td colspan='5' style='text-align:center;'>No order(s) found </td></tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <input type="hidden" name="preview_id" id="preview_id" value="<?php echo $getPreviewId; ?>">
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>
</form>