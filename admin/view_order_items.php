<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
$foodAppApi = new Common($dbconn);
if ($_REQUEST["orderID"] || $_REQUEST["packageID"]) {
    $orderId=$_REQUEST["orderID"];
	$packageId=$_REQUEST["packageID"];
}
if ((!empty($orderId) || $orderId!="0") && $packageId=="0") {
?>
<div class="col-md-8 col-sm-10 col-xs-8 viewodercate">
    <input type="hidden" name="orderId" id="orderId" value="<?php echo $orderId; ?>">
    <input type="hidden" name="packageId" id="packageId" value="<?php echo $packageId; ?>">
    <span><strong>Order #</strong> : <span><?php echo $orderId; ?></span></span><br>
</div>
<div class="pdf_icon">
   <a id="download_order_items_pdf" ><img class="order_items_pdf" src="../assets/layouts/layout2/img/download.png"></a>
</div>
<div class="portlet-body">
    <div class="table-responsive" style="overflow-x: initial;">
    	<table class="table table-bordered table-striped table-condensed" id="tbl_order_item">
    		<thead class="">
                <tr>
                    <th width="10%">Item #</th>
                	<th width="10%">Image</th>
                    <th width="20%">Category Name</th>
                    <th width="20%">Item Name</th>
                    <th width="20%">Amount</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
            	   $Qry="SELECT orders.order_id, orderItem.order_id, orderItem.category_id, orderItem.item_id, category.category_name,cateitems.item_name, cateitems.item_type,cateitems.image,orderItem.price FROM tbl_orders AS orders INNER JOIN tbl_order_items AS orderItem ON orderItem.order_id=orders.order_id INNER JOIN tbl_category AS category ON category.category_id=orderItem.category_id INNER JOIN tbl_category_items AS cateitems ON cateitems.item_id=orderItem.item_id where orders.order_id=:order_id";
                   $qryParams[":order_id"]=$orderId;
                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    $i=1;
                    $category_name="";
                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                        foreach($getResCnt as $getOrderData) {
                            $item_id=(!empty($getOrderData["item_id"]))?$getOrderData["item_id"]:"-";
                            $category_name=(!empty($getOrderData["category_name"]))?$getOrderData["category_name"]:"-";
                            $item_name=(!empty($getOrderData["item_name"]))?$getOrderData["item_name"]:"-";
                            $item_image=(!empty($getOrderData["image"]))?"../".$getOrderData["image"]:"../uploads/category_items/no_food.png";
                            $item_price=(!empty($getOrderData["price"]))?$getOrderData["price"]:"-";
                            $ArrayPrice[]=$item_price;
                            $totalAmt=array_sum($ArrayPrice);
                    ?>
                    <tr>
                        <td class="viewordermiddle"><?php echo $item_id?></td>
                        <td><img width="75px" height="50px" src="<?php echo $item_image ?>"?></td>
                        <td class="viewordermiddle"><?php echo $category_name ?></td>
                        <td class="viewordermiddle"><?php echo $item_name ?></td>
                        <td class="viewordermiddle"><?php echo "$".number_format($item_price,2) ?></td>
                    </tr>
                    <?php
                      }
                      ?>
                    <tr>
                        <td colspan="4" class="viewordermiddle" style="text-align: right;"><b>Total Amount</b></td>
                        <td class="viewordermiddle"><b><?php echo "$".number_format($totalAmt,2);?></b></td>
                    </tr>
                    <?php
                    }else {
                        echo "<tr>
                            <td colspan='5'>No item order(s) found</td>
                        </tr>";
                    }
                ?>
            </tbody>
    	</table>
    </div>
</div> 
<?php
}
if ($packageId!="0" || !empty($packageId)) {
?>
<div class="col-md-8 col-sm-10 col-xs-8 viewodercate">
    <input type="hidden" name="orderId" id="orderId" value="<?php echo $orderId; ?>">
    <input type="hidden" name="packageId" id="packageId" value="<?php echo $packageId; ?>">
    <span><strong>Order #</strong> : <span><?php echo $orderId; ?></span></span><br>
    <span><strong>Package Name</strong> : <span class="orderPackname">-</span></span><br>
</div>
<div class="pdf_icon">
   <a id="download_order_items_pdf" ><img class="order_items_pdf" src="../assets/layouts/layout2/img/download.png"></a>
</div>
<div class="portlet-body">
    <div class="table-responsive" style="overflow-x: initial;">
    	<table class="table table-bordered table-striped table-condensed " id="tbl_package_item">
    		<thead class="">
                <tr>
                    <th width="20%">Image</th>
                    <th width="20%">Category Name</th>
                    <th width="20%">Item Name</th>
                    <th width="20%">Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
                  $Qry="SELECT orders.order_id,orders.package_id, packages.package_name,packages.rate,packageitems.item_id,cateitems.item_name,cateitems.image,cat.category_name FROM tbl_orders AS orders  INNER JOIN tbl_packages AS packages ON packages.package_id=orders.package_id INNER JOIN tbl_package_items AS packageitems ON packageitems.package_id=packages.package_id INNER JOIN tbl_category_items AS cateitems ON cateitems.item_id=packageitems.item_id left join tbl_category as cat on cat.category_id=cateitems.category_id  where orders.order_id=:orderid";
                    $qryParam[":orderid"]=$orderId;
                    //$qryParam[":status"]='active'; packages.status=:status and
                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParam);
                    $i=1;
                    $package_name="";
                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                        foreach($getResCnt as $getOrderData) {
                            $category_name=(!empty($getOrderData["category_name"]))?$getOrderData["category_name"]:"-";
                            $package_name=(!empty($getOrderData["package_name"]))?trim($getOrderData["package_name"]):"";
                            $item_name=(!empty($getOrderData["item_name"]))?$getOrderData["item_name"]:"-";
                            $item_image=(!empty($getOrderData["image"]))?"../".$getOrderData["image"]:"../uploads/category_items/no_food.png";
                            $item_price="-";
                            $totalAmt=(!empty($getOrderData["rate"]))?$getOrderData["rate"]:"-";
                    ?>
                    <tr>
                        <td><img width="75px" height="50px" src="<?php echo $item_image ?>"?></td>
                        <td class="viewordermiddle"><?php echo $category_name ?></td>
                        <td class="viewordermiddle"><?php echo $item_name ?></td>
                        <td class="viewordermiddle"><?php echo $item_price?></td>
                    </tr>
                    <?php
                      }
                      ?>
                    <tr>
                        <td colspan="3" style="text-align: right;"><b>Total Amount</b></td>
                        <td class="viewordermiddle"><b><?php echo "$".number_format($totalAmt,2);?></b></td>
                    </tr>
                    <?php
                    } else {
                        echo "<tr>
                            <td colspan='4'>No item order(s) found</td>
                        </tr>";
                    }
                ?>
            </tbody>
    	</table>
    </div>
</div>
<?php
}
?>
<script>
$(document).ready(function () {
    var package="<?php echo (!empty($package_name))?$package_name:"";?>";
    if (package!="") {
        $(".orderPackname").empty().append(package);
    } else{
        $(".orderPackname").empty().append("-");
    }
});

$(document).on("click",".order_items_pdf",function() {

    $(".download_order_items_pdf").attr("href", '');
    $(".download_order_items_pdf").attr("download", '');

    var orderId = $('#orderId').val();
    var packageId = $('#packageId').val();
    if (orderId!="") {
        $.ajax({
            url:"order_pdf.php",
            method:'GET',
            data:"orderId="+orderId+"&packageId="+packageId,
            async: false,
            success:function(data) {                 
                $("#download_order_items_pdf").attr("href", data);
                $("#download_order_items_pdf").attr("download", '');
                $("#download_order_items_pdf").trigger("click");
                // $('#download_order_items_pdf')[0].click();
            }
        });            
    } 
});
</script>