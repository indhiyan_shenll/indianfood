<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
$foodAppApi = new Common($dbconn);
$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["reportsSearchCriteria"])) {
    $reportSearch = json_decode($_POST["reportsSearchCriteria"], true);
    $customer_id  = !empty($reportSearch["customer_id"]) ? $reportSearch["customer_id"] : "" ;    
    $vendors_id   = !empty($reportSearch["vendors_id"]) ? $reportSearch["vendors_id"] : "" ;
    $start_date   = !empty($reportSearch["start_date"]) ? $reportSearch["start_date"] : "" ;
    $end_date     = !empty($reportSearch["end_date"]) ? $reportSearch["end_date"] : "" ;
    $status     = !empty($reportSearch["status"]) ? $reportSearch["status"] : "" ;
    if (isset($reportSearch['HdnPage']) && is_numeric($reportSearch['HdnPage']))
        $Page = $reportSearch['HdnPage'];
}  
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<table class="table table-bordered table-striped table-condensed flip-content" id="tbl_reports_list">
    <div class="portlet-body flip-scroll" id="sample">
        <thead class="flip-content">
            <tr>
            	<th width="10%">Order #</th>
                <th nowrap>Aunty</th>
                <th>Customer</th>                
                <th>Start Date</th>
                <th>End Date</th>
                <th>Rating</th>
                <th>Status</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php
                $Qrycondition="";   
                $qryParams=array();

                if (!empty($customer_id)) {
                    $Qrycondition.=" and orders.customer_id=:customerid";
                    $qryParams[":customerid"]=$customer_id;
                }
                if (!empty($vendors_id)) {
                    $Qrycondition.=" and orders.vendor_id=:vendorid";
                    $qryParams[":vendorid"]=$vendors_id;
                }
                if ( ( !empty($start_date) && !empty($end_date) ) && ( trim($start_date) == trim($end_date) ) ) {
                    $Qrycondition.=" and DATE_FORMAT(orders.start_date, '%Y-%m-%d')=:startdate OR DATE_FORMAT(orders.end_date, '%Y-%m-%d')=:enddate";
                    $qryParams[":startdate"]=date("Y-m-d",strtotime($start_date));
                    $qryParams[":enddate"]=date("Y-m-d",strtotime($end_date));
                } else {

                    if (!empty($start_date) && empty($end_date)) {
                        $Qrycondition.=" and :startdate >=date_format(orders.start_date,'%m-%d-%Y')"; 
                        $qryParams[":startdate"]=date("Y-m-d",strtotime($start_date));
                    } elseif (empty($start_date) && !empty($end_date)) {
                        $Qrycondition.=" and DATE_FORMAT(orders.end_date, '%m-%d-%Y')<=:enddate";
                        $qryParams[":enddate"]=date("Y-m-d",strtotime($end_date));
                    } elseif (!empty($start_date) && !empty($end_date)) {
                        $Qrycondition.=" and DATE_FORMAT(orders.start_date, '%Y-%m-%d')>=:startdate and DATE_FORMAT(orders.end_date, '%Y-%m-%d')<=:enddate";
                        $qryParams[":startdate"]=date("Y-m-d",strtotime($start_date));
                        $qryParams[":enddate"]=date("Y-m-d",strtotime($end_date));
                    }
                }
                if (!empty($status)) {
                    $Qrycondition .= " AND orders.status='$status'";
                }

                $Qry="SELECT users.full_name,orders.order_id,orders.vendor_id,orders.customer_id,orders.package_id,orders.start_date,orders.end_date,orders.price,orders.ratings,orders.order_type,orders.status FROM tbl_orders as orders INNER JOIN tbl_users as users ON users.user_id=orders.vendor_id where orders.order_id !='' ".$Qrycondition." order by orders.order_id desc";
                $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                    
                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                    $Start=($Page-1)*$RecordsPerPage;
                    $sno=$Start+1;
                    $Qry.=" limit $Start,$RecordsPerPage";
               		$getReports = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
           		    //$sno=1;
			   	    if (count($getReports)>0) {
                        
			   		    foreach ($getReports as $reportsListData) {
                           $customer_id=$reportsListData["customer_id"];
                           $package_id=$reportsListData["package_id"];
                           // echo "pack_id =>".$package_id."<br>";
                           $customerQry ="Select * from tbl_users where user_id=:userid";
                           $qryParam[":userid"]=$customer_id;
                           $getRescustomer = $foodAppApi->funBckendExeSelectQuery($customerQry,$qryParam,'fetch');
                           $customer_name = $getRescustomer["full_name"];

                            if (strtolower($reportsListData["status"])=="pending")
                                $statusColor="info";
                            if (strtolower($reportsListData["status"])=="confirmed")
                                $statusColor="warning";
                            if (strtolower($reportsListData["status"])=="started")
                                $statusColor="purple";
                            if (strtolower($reportsListData["status"])=="paid")
                                $statusColor="success";
                            if (strtolower($reportsListData["status"])=="cancel")
                                $statusColor="danger";
                            if (strtolower($reportsListData["status"])=="completed")
                                $statusColor="default";
                            
                           $rating_id = "#reports_$sno";
                           $ratingvalue = $reportsListData['ratings'];
                           $ratingsdiv = "<div id='reports_$sno' title='".$ratingvalue."' style='width:15px;height:15px;'></div>";
                           $ratings="<script>$( document ).ready(function() { $('".$rating_id."').rateYo({rating:$ratingvalue,readOnly: true,starWidth: '15px'});});</script>";
			?>
			       <tr>
			       		<td><?php echo $reportsListData["order_id"];?></td>
			       		<td><?php echo $reportsListData["full_name"];?></td>
			       		<td><?php echo $customer_name;?></td>
			       		<td><?php echo date("m/d/Y",strtotime($reportsListData["start_date"]));?></td>
			       		<td><?php echo date("m/d/Y",strtotime($reportsListData["end_date"]));?></td>
			       		<td><?php echo $ratingsdiv.$ratings; ?></td>
                        <td> <span class="label label-<?php echo $statusColor;?>"><?php echo ucfirst($reportsListData["status"]);?></span></td>
                        <?php if ($package_id!="" && $package_id!=0) { ?>
                            <td><?php echo "$".$reportsListData["price"];?><img src="../assets/layouts/layout2/img/pack.png" class="pack_image"></td>
                        <?php } else {?>
					    <td><?php echo "$".$reportsListData["price"];?><img src="../assets/layouts/layout2/img/pack_empty.png" class="pack_image" ></td>
                        <?php } ?>
                        <td><a href="javascript:;" class="btn actionvieworder customgrey view-order-items" dataorder-id="<?php echo $reportsListData["order_id"]?>" datapackage-id="<?php echo $package_id;?>" id="vieworderitems"><i class="fa fa-book"></i> View Order Items</a></td>			       		
			       </tr>
            <?php
			    $sno++;
		            }
				}
            }
            ?>
        </tbody>
    </table>
</div>
<div>
    <?php
        if ($TotalPages > 1) {

            echo "<table style='text-align:center;width:478px;margin:auto;'><tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
            $FormName = "reportlist_form";
            require_once ("paging.php");
            echo "</td></tr></table>";
        }
    ?>
</div>
<script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>