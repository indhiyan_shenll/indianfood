<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');

$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["customerSearchCriteria"])) {
    $customSearch  = json_decode($_POST["customerSearchCriteria"], true);
    $customername  = trim(!empty($customSearch["customername"])) ? trim($customSearch["customername"]) : "" ;    
    $emailAddress  = trim(!empty($customSearch["email"])) ? trim($customSearch["email"]) : "" ;
    $status        = trim(!empty($customSearch["status"])) ? trim($customSearch["status"]) : "" ;
    if (isset($customSearch['HdnPage']) && is_numeric($customSearch['HdnPage']))
        $Page = $customSearch['HdnPage'];
}  
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
<div class="portlet-body">
    <table class="table table-striped table-bordered table-hover" id="tbl_customer_list">
        <thead>
            <tr>
            	<th>#</th>
                <th>Customer</th>
                <th>Email</th>
                <th>Phone</th>
                <th class="customer-address">Address</th>
                <!-- <th>Status</th> -->
                <th class="customer-action">Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php
                $qryParams = array();
                $QryCondition="";
                if (!empty($customername)) {
                    $QryCondition.=" and full_name like :full_name";
                    $qryParams[":full_name"]= "%".$customername."%" ;
                }
                if (!empty($emailAddress)) {
                    $QryCondition.=" and email like :email";
                    $qryParams[":email"]= "%".$emailAddress."%" ;
                }
                if (!empty($status)) {
                    $QryCondition.=" and status=:status";
                    $qryParams[":status"]=$status;
                }
                if(empty($customerId) && empty($emailAddress) && empty($status) ){
                    $QryCondition.=" and status=:status";
                    $qryParams[":status"]='Active';
                }
                $Qry="SELECT * FROM tbl_users where user_type=:user_type ".$QryCondition." order by user_id desc";
                $qryParams[":user_type"]="customer";
                $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                if (count($getResCnt,COUNT_RECURSIVE)>1) {
                    $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                    $Start=($Page-1)*$RecordsPerPage;
                    $sno=$Start+1;
                    $Qry.=" limit $Start,$RecordsPerPage";
               		$getCustomer = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
			   	    if (count($getCustomer)>0) {
			   		    foreach ($getCustomer as $customerListData) {
                            if(strtolower($customerListData["status"])=="active")
			   			       $statusColor="../assets/layouts/layout2/img/active.png";
                            if(strtolower($customerListData["status"])=="inactive")
                                $statusColor="../assets/layouts/layout2/img/inactive.png";
                            if(strtolower($customerListData["status"])=="block")
                                $statusColor="../assets/layouts/layout2/img/block.png";

			 ?>
			 <tr>
		       		<td><?php echo $sno;?></td>
		       		<td><?php echo $customerListData["full_name"];?></td>
		       		<td><?php echo $customerListData["email"];?></td>
		       		<td><?php echo $customerListData["mobile_number"];?></td>
		       		<td style="text-align:left"><?php echo $customerListData["address"];?></td>
		       		<!-- <td class="status_<?php echo $customerListData["user_id"]?>"><span data-id="<?php echo $customerListData["user_id"]?>" id="popupStatus"><img src="<?php echo $statusColor; ?>"></span></td> -->
                    <!-- <td ><span><img src="<?php echo $statusColor; ?>"></span></td> -->
                    <!-- <td><span class="label label-info"><?php echo ucfirst($customerListData["status"]);?></span></td> -->
                    
                    <td><a href="reports.php?cusid=<?php echo $foodAppApi->encode($customerListData["user_id"]);?>" class="btn actionvieworder customgrey view-order-history" ><i class="fa fa-book"></i> Order History</a></td>
		       </tr>
            <?php   
			    $sno++;		
		            }
				}
            }
            ?>
            
        </tbody>
    </table>
</div>
<div>
    <?php
        if ($TotalPages > 1) {

            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
            $FormName = "customerlist_form";
            require_once ("paging.php");
            echo "</td></tr>";
        }
    ?>
</div>
<script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>