<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
// include_once("header.php");
// $Page = 1;$RecordsPerPage = 25;
// $TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["orderSearchCriteria"])) {
    $orderSearch = json_decode($_POST["orderSearchCriteria"], true);
    $vendor 	 = !empty($orderSearch["vendor_id"]) ? $orderSearch["vendor_id"] : "" ;    
    $packageName = !empty($orderSearch["package_name"]) ? $orderSearch["package_name"] : "" ;
    $packageType = !empty($orderSearch["package_type"]) ? $orderSearch["package_type"] : "" ;
    // if (isset($orderSearch['HdnPage']) && is_numeric($orderSearch['HdnPage']))
    //     $Page = $orderSearch['HdnPage'];
}
?>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<div class="portlet-body">
    <div class="table-responsive">
       <table class="table table-striped table-bordered" id="food-packages-list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Package</th>
                    <th>Number Of Days</th>
                    <th>Rate</th>
                    <th>Category</th>
                    <th>Item Name</th>
                    <th>Image</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
        		<?php
        		$Arrpackage =array();$foodtypcondn="";
                if ($vendor) {
                    $foodtypcondn.= (!empty($packageType))?" and package_type = :package_type":"";
                    $foodtypcondn.= (!empty($packageName))?" and pck.package_id = :package_name":"";
                    $vendpackageQry = "SELECT *,catitm.image as itemimage FROM `tbl_packages` as pck JOIN tbl_package_items as pckitm ON pck.package_id = pckitm.package_id JOIN tbl_category_items as catitm ON catitm.item_id = pckitm.item_id JOIN tbl_category AS catgry ON catgry.category_id = catitm.category_id WHERE pck.status=:status and pck.vendor_id = :vendor_id $foodtypcondn";
                    $qryParams[':vendor_id'] = $vendor;
                    $qryParams[':status']='active';
                    if(!empty($packageType))
                        $qryParams[':package_type'] = $packageType;
                    if(!empty($packageName))
                        $qryParams[':package_name'] = $packageName;                
                    $getVendorpackage = $foodAppApi->funBckendExeSelectQuery($vendpackageQry,$qryParams);
        			$i=1;$vendorderlist="";
        			if(count($getVendorpackage,COUNT_RECURSIVE)>1) {
        				foreach($getVendorpackage as $fetchVendorpackage) {
        					$package_name 			=  	$fetchVendorpackage['package_name'];
        					$package_days_count 	= 	$fetchVendorpackage['package_days_count'];
        					$package_type 			=  	$fetchVendorpackage['package_type'];
                            $rate     				=  	$fetchVendorpackage['rate'];
                            $item_name       		=  	$fetchVendorpackage['item_name'];
                            $package_id 			= 	$fetchVendorpackage['package_id'];
                            $item_type 				= 	$fetchVendorpackage['item_type'];
                            $price 					= 	$fetchVendorpackage['price'];
                            $itemimage              =   $fetchVendorpackage['itemimage'];
                            $category_name          =   $fetchVendorpackage['category_name'];

                            $Arrpackage[$package_id]= 	$package_id;
                            $Arrpackagename[$package_id][$package_name] =  $package_name;
                            $Arritemcount[$package_id][]=  $item_name;
                            $Arrpackagedetails[$package_id][$package_name]['count'] = $package_days_count;
                            $Arrpackagedetails[$package_id][$package_name]['type'] = $package_type;
                            $Arrpackagedetails[$package_id][$package_name]['rate'] = $rate;
                            $Arrpackageitems[$package_id][$package_name][$item_name]['item_type'] = $item_type;
                            $Arrpackageitems[$package_id][$package_name][$item_name]['price'] = $price;
                            $Arrpackageitems[$package_id][$package_name][$item_name]['itemimage'] =  $itemimage;
                            $Arrpackageitems[$package_id][$package_name][$item_name]['category'] = $category_name;
                            $Arrpackcategory[$package_id][$package_name][$category_name][] = $item_name;
        				}
        			}
                }            
                if (count($Arrpackage)>0) {
                	$vendpackagelist="";$i=1;
            		foreach($Arrpackage as $package_id) {
            			foreach($Arrpackagename[$package_id] as $package_name) {
                            $rowspan=count($Arritemcount[$package_id]);
            				$vendpackagelist.="<tr>";
            				$vendpackagelist.="<td rowspan=".$rowspan." class='verticalmiddle'>".$i++."</td>";
            				$vendpackagelist.="<td rowspan=".$rowspan." class='verticalmiddle'>".$package_name."</td>";
            				$vendpackagelist.="<td rowspan=".$rowspan." class='verticalmiddle'>" .$Arrpackagedetails[$package_id][$package_name]['count']."</td>";
            				$vendpackagelist.="<td rowspan=".$rowspan." class='verticalmiddle'>" ."$".$Arrpackagedetails[$package_id][$package_name]['rate']."</td>";
                            $prevcategory="";
            				foreach($Arrpackageitems[$package_id][$package_name] as $items=>$Arritems) {
                                $itmimg = (!empty($Arritems['itemimage']))?"../".$Arritems['itemimage']:"../uploads/category_items/no_food.png";
                                $currentcategory = $Arritems['category'];
                                $catgryspan = count($Arrpackcategory[$package_id][$package_name][$currentcategory]);
                                if (empty($prevcategory) || $prevcategory!=$currentcategory)
                                   $vendpackagelist.="<td rowspan=".$catgryspan." class='verticalmiddle'>".$Arritems['category']."</td>";
            					$vendpackagelist.="<td class='verticalmiddle'>".$items."</td>
    				                                <td align='center' class='verticalmiddle'><img width='75px' height='50px' src='".$itmimg."'></td>
    				                                <td align='center' class='verticalmiddle'>"."$".$Arritems['price']."</td>
    				                            </tr>";
                                $prevcategory = $Arritems['category'];
            				}        				
            			}
            		}
            		echo $vendpackagelist;
                }  else {
                	echo "<tr><td colspan='8' class='txt_center'>No Package(s) found</td></tr>";
                }
        		?>
            </tbody>
        </table>
    </div>
</div>
<script>
	$(document).ready(function(){
	 	$(".pack").on("click",function(){        	
	    	var refid = $(this).attr("data-href");
	    	if ($("#"+refid).is(':visible')) {
	    		$("#"+refid).slideUp();
	    		return false;
	    	}
	    	$(".hiddenRow").slideUp();
	    	$("#"+refid).slideDown();
	    });
    });
</script>