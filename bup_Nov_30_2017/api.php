<?php
class FoodAppApi extends Common {

	public $dbconn;
	public $currentDate;
	public function __construct(PDO $dbconn){
		$this->dbconn = $dbconn;
		$this->currentDate = date("Y-m-d h:i:s");
	}
	public function apiRouteConfig() {
		$apiReqMethod = array ( 
							"register" => "POST",
							"login" => "POST",							
							"forgetpassword" => "POST",								
							"getvendorlist" => "GET",						
							"myorder" => "POST",
							"orderhistory" => "POST",
							"createorder" => "POST",
							"updateorderstatus" => "POST",
							"updateorderratings" => "POST",
							"getcustomerdetail" => "POST",
							"createupdatecategoryitem" => "POST",
							"deletecategoryitem" => "POST",
							"createpackage" => "POST",
							"deletepackage" => "POST",
							"getreport" => "POST",
							"getinvoice" => "POST",
							"updateinvoice" => "POST",
							"getcategorylist" => "POST",
							"updateprofile" => "POST",
							"testpushnotification" => "GET",
							"updatepassword"=>"POST",
							"getpackagelist" => "POST"
						);
		return $this->jsonResponse($apiReqMethod);
	}

	// Register vendor/customer
	public function register($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array('fullName', 'email', 'password','loginType','userType');
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			$currentDate = $this->currentDate;
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			
			$validemail = emailValidate($request['email']);
			if ($validemail) {
				if (strtolower($request["loginType"]) == "email" || strtolower($request["loginType"]) == "fb" || strtolower($request["loginType"]) == "gplus") {
					// Check vendor/customer already exist
				$selQryParams = array ( 
									":email" => $request["email"],
									":login_type" => strtolower($request["loginType"]),
									":user_type" => strtolower($request["userType"])
								);
			    $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");   
			    $reqQryParams = array (
									"fetchType" => "singleRow",
									"selectField" => "count(email) as countRows,user_id,user_img,upload_documents,status",
									"tableName" => "tbl_users",
									"whereCondition" => $whereCondtn
								);
				$chekEmailExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
				$responseDate = array();
				if ($chekEmailExistRes["countRows"] == 0) {

					$uploadImg = $request["userImg"];
					$userImg = "";
					if (!empty($uploadImg)) {
						// Convert base64image to image
						$floderName="user_profile";
						$userImg = $this->base64toImage($uploadImg,$floderName);
					}
					$userDocs="";
					// Register vendor/customer
					$status="";
					$status = (strtolower($request["userType"]) == "vendor")?"pending":"Active";
					$insQryParams = array ( 
										":full_name" => $request["fullName"],
										":email" => $request["email"],
										":password" => $this->encode($request["password"]),
										":user_type" => strtolower($request["userType"]),
										":login_type" => strtolower($request["loginType"]),
										":address" => $request["address"],
										":country" => $request["country"],
										":zip_code" => $request["zipCode"],
										":mobile_number" => trim($request["mobileNumber"]),
										":user_img" => $userImg,
										":upload_documents" => $userDocs,
										":status" => $status,
										":vendor_description" => $request["vendorDescription"],
										":device_token" => $request["deviceToken"],
										":user_platform" => $request["userPlatform"],
										":created_date" => $currentDate,
										":modified_date" => $currentDate
									);
					$insQryResponse = $this->funExeInsertRecord("tbl_users", $insQryParams);
					if (!empty($insQryResponse))	 {

						// Register Default Category
						$insQryParams = array ( 
											":category_order" => 1,
											":vendor_id" => $insQryResponse,
											":category_name" => "Main Dishes",
											":description" => "Main Dishes",
											":image" => "",
											":created_date" => $currentDate,
											":modified_date" => $currentDate
										);
						$insCatQryResponse = $this->funExeInsertRecord("tbl_category", $insQryParams);

						$insQryParams = array ( 
											":category_order" => 2,
											":vendor_id" => $insQryResponse,
											":category_name" => "Side Dishes (appetizers)",
											":description" => "Side Dishes (appetizers)",
											":image" => "",
											":created_date" => $currentDate,
											":modified_date" => $currentDate
										);
						$insCatQryResponse = $this->funExeInsertRecord("tbl_category", $insQryParams);					

						$selInsQryParams = array(
								                 ":email" => $request["email"],
								                 ":login_type" => strtolower($request["loginType"]),
												 ":user_type" => strtolower($request["userType"]) 
											);
					    // Select last inserted vendor/customer
					    $selInsWhereCondtn = $this->funParseQryParams($selInsQryParams, "user_type", "AND");
					    $reqQryParams = array (
											"fetchType" => "singleRow",
											"selectField" => "",
											"tableName" => "tbl_users",
											"whereCondition" => $selInsWhereCondtn
										);
						$selQryResponse = $this->funExeSelectQuery($reqQryParams, $selInsQryParams);
						// Send email to Admin
						$mailParams = array(
										"fromAddress" => "food@kazafood.com",
				                        "toAddress" => "karuna.shenll@gmail.com",
				                        "customerName" => "Kazafood",
				                        "subject" => "User registration",
				                        "bodyMsg" =>'<html>
	                                            <body>
		                                            <table width="600px" style="font-family: verdana;font-size:12px;">
		                                             	<tr>
													   		<td>Dear Admin,</td>
													    </tr>
													    <tr style="height:10px"><td></td></tr>
													    <tr>
													   		<td>Congratulations! The following person has just downloaded your food App and registered as a new '.strtolower($request["userType"]).'!</td>
													    </tr>
														<tr style="height:10px"><td></td></tr>
														<tr>
															<td>Name: '.$request["fullName"].'</td>
														</tr>
														<tr style="height:10px"><td></td></tr>
														<tr>
															<td>User Type: '.$request["userType"].'</td>
														</tr>
														<tr style="height:10px"><td></td></tr>
														<tr>
															<td>User Email: '.$request["email"].'</td>
														</tr>
														<tr style="height:20px"><td></td></tr>
														<tr>
															<td>Best Regards,<br>KazaFood support Team</td>
														</tr>
													</table>
	                                            </body>
	                                            </html>'
				                    );
						$sendEmail = $this->sendEmailNotification($mailParams);
						if ($sendEmail) {
							$responseDate["status"] = 1;
							$responseDate["message"] = "Registered successfully";
							$responseDate["userid"] = !empty($selQryResponse["user_id"]) ? $selQryResponse["user_id"] : "" ;
							$responseDate["userImg"] = !empty($selQryResponse["user_img"]) ? KAZA_SYSTEM_PATH.$selQryResponse["user_img"] : "" ;
							$responseDate["userdocs"] = !empty($selQryResponse["upload_documents"]) ? KAZA_SYSTEM_PATH.$selQryResponse["upload_documents"] : "" ;
							$responseDate["isActive"] = (strtolower($status)=="active")?"true":"false";
						}
					} else {
						$responseDate["status"] = 0;
						$responseDate["message"] = "Something Issue in Registration!";
						$responseDate["userid"] = "";
					}				
				} else {
					$empty_docs="";
	          		$responseDate["status"] 	= (strtolower($request["loginType"]) == "email" )? 0 : 1;
				  	$responseDate["message"] 	= (strtolower($request["loginType"]) == "email" )?"Email already exists!":"Registered successfully";
				  	$responseDate["userid"] 	= (strtolower($request["loginType"]) == "email" )?"":$chekEmailExistRes["user_id"];
				  	$responseDate["userImg"] 	= (strtolower($request["loginType"]) == "email" )?"":((!empty($chekEmailExistRes['user_img']))? KAZA_SYSTEM_PATH.$chekEmailExistRes['user_img'] : "");
				  	$responseDate["userdocs"] 	= (strtolower($request["loginType"]) == "email" )?"":((!empty($chekEmailExistRes['upload_documents']))? KAZA_SYSTEM_PATH.$chekEmailExistRes['upload_documents'] : "");
				 	$responseDate["isActive"] 	= (strtolower($chekEmailExistRes["status"])=="active")?"true":"false";
				}	
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Invalid Email!";
			}	
		}
			return $this->jsonResponse($responseDate);
		}
	}

	// Login
	public function login($request) {		
		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array('email', 'password', 'userType');
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			$currentDate = $this->currentDate;
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}			
			// Select User
			$validemail = emailValidate($request['email']);
			if ($validemail) {
				$selQryParams = array (	
									":email" => $request["email"],
									":password" => $this->encode($request["password"]),
									":user_type" => $request["userType"]
								);
			    $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");
			    $reqQryParams = array (
									"fetchType" => "singleRow",
									"selectField" => "",
									"tableName" => "tbl_users",
									"whereCondition" => $whereCondtn
								);		    
				$selQryResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);			
				$responseDate = array();
				if (count($selQryResponse) > 0) {
					$responseDate["status"] = 1;
					$responseDate["message"] = "Logged successfully!";
					$responseDate["userid"]  = $selQryResponse["user_id"];
					$responseDate["fullName"] = $selQryResponse["full_name"];
					$responseDate["mobileNumber"] = $selQryResponse["mobile_number"];
					$responseDate["userImg"] = (!empty($selQryResponse["user_img"]))?KAZA_SYSTEM_PATH.$selQryResponse["user_img"]:"";
					$responseDate["userdocs"] = (!empty($selQryResponse["upload_documents"]))?KAZA_SYSTEM_PATH.$selQryResponse["upload_documents"]:"";
					$responseDate["email"] = $selQryResponse["email"];
					$responseDate["isActive"] = (strtolower($selQryResponse["status"])=="active")?"true":"false";
					// Update device info
					// $updateQryParams = array ( 
					// 						":device_token" => $request["deviceToken"], 
					// 						":user_platform" => $request["userPlatform"],
					// 						":modified_date" => $currentDate,
					// 						":user_id" => $selQryResponse["user_id"]
					// 					);
					$updateQryParams = array ( 
											":device_token" => $request["deviceToken"], 
											":user_platform" => $request["userPlatform"],
											":modified_date" => $currentDate,
										);
					$setCondtn = $this->funParseQryParams($updateQryParams, "modified_date", ",");
					$updateQryParams[":user_id"] = $selQryResponse["user_id"];
				    $reqQryParams = array (
										"tableName" => "tbl_users",
										"setCondtn" =>$setCondtn,
										"whereCondition" => "user_id=:user_id"
									);
					$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $updateQryParams);
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "User doesn't exists! OR email and password doesn't match";
					$responseDate["userid"] = "";
					$responseDate["isActive"] = "";
				}
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Invalid Email!";				
			}
			return $this->jsonResponse($responseDate);
		}		
	}	

	// ForgetPassword
	public function forgetPassword($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array('email', 'userType');
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			$currentDate = $this->currentDate;
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$validemail = emailValidate($request['email']);
			if ($validemail) {
				//generate verification code
	            $digits="5";
				$verification_code=$this->generateVerficationCode($digits);
				$qryParam = array ( ":verification_code" => $verification_code);
	            $setCondtn = $this->funParseQryParams($qryParam, "verification_code", ",");
	            //echo $setCondtn;
			    $reqQryParams = array (
						"tableName" => "tbl_users",
						"setCondtn" =>$setCondtn,
						"whereCondition" => "email=:email and user_type=:user_type"
					);

			    $updateQryParams = array ( 
									":email" => $request["email"], 
									":user_type" => $request["userType"],
									":verification_code" => $verification_code
								);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $updateQryParams);
	   
				// Check vendor/customer already exist
				$selQryParams = array ( 
									":email" => $request["email"], 
									":user_type" => $request["userType"]
								);
			    $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND"); 
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "*",
					"tableName" => "tbl_users",
					"whereCondition" => $whereCondtn
				);
				$userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams); 
				$responseDate = array();
				if (is_array($userInfo) && count($userInfo) > 0) {

					// Send email to Customer // <p>Password for your account is: ".$this->decode($userInfo["password"])."</p>
					$mailParams = array(
									"fromAddress" => "food@kazafood.com",
			                        "toAddress" => $userInfo["email"],
			                        "customerName" => "Indhiyan",
			                        "subject" => "Forget password",
	                                "bodyMsg" =>'<html>
	                                            <body>
		                                            <table width="600px" style="font-family: verdana;font-size:12px;">
		                                             	<tr>
													   		<td>Dear '.$userInfo["full_name"].',</td>
													    </tr>
													    <tr style="height:10px"><td></td></tr>
													    <tr>
													   		<td>User Email: '.$userInfo["email"].'</td>
													    </tr>
														<tr style="height:10px"><td></td></tr>
														<tr>
															<td>Verification Code: '.$userInfo["verification_code"].'</td>
														</tr>
														<tr style="height:20px"><td></td></tr>
														<tr>
															<td>Best Regards,<br>KazaFood support Team</td>
														</tr>
													</table>
	                                            </body>
	                                            </html>'
			                    );
					$sendEmail = $this->sendEmailNotification($mailParams);
					if ($sendEmail) {
						$responseDate["status"] = 1;
						$responseDate["message"] = "We have sent you verification code, Please check your email for further instructions.";
						$responseDate["userId"] =$userInfo["user_id"];
						$responseDate["verificationCode"] =$userInfo["verification_code"];
					}
				} else {				 
					$responseDate["status"] = 0;
					$responseDate["message"] = "Email not exists!";
				}
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Invalid Email!";
			}
			return $this->jsonResponse($responseDate);
		}
	}	

	// Fetch list of orders that customer ordered till now
	public function getVendorList($request) {

		if (is_array($request) && count($request) > 0) {

			// Select all vendors
			$selVendorQryParams = array(":user_type" => "vendor" );
			$selVendorWhereCondtn = $this->funParseQryParams($selVendorQryParams);
			$reqQryParams = array (
					"fetchType" => "multipleRow",
					"selectField" => "",
					"tableName" => "tbl_users",
					"whereCondition" => $selVendorWhereCondtn
				);
			$vendorsResponse = $this->funExeSelectQuery($reqQryParams, $selVendorQryParams);
			$categoryRes = $responseDate = $categoryItems = array();
			if (!empty($vendorsResponse)) {
				$i = 0;
				foreach ($vendorsResponse as $vendor) {

					// Fetch/Get Category and Category items
					$selCatQryParams = array(":vendor_id" => $vendor["user_id"]);
					$selCatWhereCondtn = "a.vendor_id=:vendor_id";
					$reqQryParams = array (
										"fetchType" => "multipleRow",
										"selectField" => "*, a.category_id as cat_category_id, a.created_date as cat_created_date, a.modified_date as cat_modified_date, b.category_id as catitem_category_id, b.created_date as catitem_created_date, b.modified_date as catitem_modified_date",
										"tables" => array("tbl_category as a", "tbl_category_items as b"),
										"onCondition" => "a.category_id = b.category_id",
										"whereCondition" => $selCatWhereCondtn,
										// "orderByCondtn" => "b.category_id asc"
									);
					$categoryRes = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $selCatQryParams);
					$orderParams = array ( ":vendor_id" => $vendor["user_id"]);								
					$oderWhereCondtn = $this->funParseQryParams($orderParams);
					$reqOrderQryParams = array (
							"fetchType" => "singleRow",
							"selectField" => "AVG(ratings) as ratings",
							"tableName" => "tbl_orders",
							"whereCondition" => $oderWhereCondtn
						);
					$orderRatingsResponse = $this->funExeSelectQuery($reqOrderQryParams, $orderParams);
					$categoryItems[$i]["vendorId"] = $vendor["user_id"];
					$categoryItems[$i]["vendorName"] = $vendor["full_name"];
					$categoryItems[$i]["address"] = $vendor["address"];
					$categoryItems[$i]["vendorDescription"] = (!empty($vendor["vendor_description"]))?$vendor["vendor_description"]:"";
					$categoryItems[$i]["country"] = $vendor["country"];
					$categoryItems[$i]["zipCode"] = (!empty($vendor["zipcode"]))?$vendor["zipcode"]:"";
					$categoryItems[$i]["mobileNumber"] = $vendor["mobile_number"];
					$categoryItems[$i]["uploadDoc"] = !empty($vendor["upload_documnets"]) ? KAZA_SYSTEM_PATH.$vendor["upload_documnets"] : "" ;
					$categoryItems[$i]["status"] = $vendor["status"];

					$categoryItems[$i]["ratings"] = isset($orderRatingsResponse["ratings"]) && !empty($orderRatingsResponse["ratings"]) ? round($orderRatingsResponse["ratings"],1) : 0 ;

					// $catItemsDetails = array();
					$catItemsListDetails = array();
					if (!empty($categoryRes) && count($categoryRes > 0)) {
						$k = $j = 0;
						$prevCatId = "";
						foreach ($categoryRes as $category) {

							if ($prevCatId != $category["cat_category_id"]) {
								$catItemsListDetails[$category["cat_category_id"]]["categoryId"] = $category["cat_category_id"]; 
								$catItemsListDetails[$category["cat_category_id"]]["categoryName"] = $category["category_name"]; 
								$catItemsListDetails[$category["cat_category_id"]]["description"] = $category["description"]; 
								$catItemsListDetails[$category["cat_category_id"]]["image"] = $category["image"];

								$prevCatId = $category["cat_category_id"];
								$k = 0;								
							}							

							if (!empty($category["item_id"])) {

								// $catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["catid"] = $category["cat_category_id"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["categoryId"] = $category["category_id"];		
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemId"] = $category["item_id"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemName"] = $category["item_name"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemDescription"] = $category["short_description"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemType"] = $category["item_type"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["image"] = !empty($category["image"]) ? KAZA_SYSTEM_PATH.$category["image"] : "" ;
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["price"] = $category["price"];
							} else {
								$catItemsListDetails[$category["cat_category_id"]]['itemList'] = array();
							}
							$j++;
							$k++;							
						}
					}

					// Fetch/Get Packages and Package items
					$reqQryParams = array (
										"fetchType" => "multipleRow",
										"selectField" => "*, a.package_id as pack_package_id, a.created_date as cat_created_date, a.modified_date as cat_modified_date, b.package_id as pack_item_id, b.created_date as catitem_created_date, b.modified_date as catitem_modified_date",
										"tables" => array("tbl_packages as a", "tbl_package_items as b"),
										"onCondition" => "a.package_id = b.package_id",
										"whereCondition" => $selCatWhereCondtn." order by b.package_id asc"
									);
					$packageRes = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $selCatQryParams);					
					$packItemsDetails = array();
					if (!empty($packageRes) && count($packageRes > 0)) {
						$k = $j = 0;
						$prevPackId = "";
						foreach ($packageRes as $package) {

							if ($prevPackId != $package["pack_package_id"]) {
								$packItemsDetails[$package["pack_package_id"]]["packageId"] = $package["pack_package_id"];
								$packItemsDetails[$package["pack_package_id"]]["packageName"] = $package["package_name"]; 
								$packItemsDetails[$package["pack_package_id"]]["image"] = $package["image"];
								$packItemsDetails[$package["pack_package_id"]]["packageType"] = $package["package_type"];
								$packItemsDetails[$package["pack_package_id"]]["price"] = $package["rate"];
								$packItemsDetails[$package["pack_package_id"]]["status"] = $package["status"];
								$packItemsDetails[$package["pack_package_id"]]["packageDaysCount"] = $package["package_days_count"];

								// $packItemsDetails[$j]["packageId"] = $package["pack_package_id"]; 
								// $packItemsDetails[$j]["packageName"] = $package["package_name"]; 
								// $packItemsDetails[$j]["image"] = $package["image"];
								// $packItemsDetails[$j]["packageType"] = $package["package_type"];
								// $packItemsDetails[$j]["price"] = $package["rate"];
								// $packItemsDetails[$j]["status"] = $package["status"];
								// $packItemsDetails[$j]["packageDaysCount"] = $package["package_days_count"];

								$prevPackId = $package["pack_package_id"];
								$k = 0;								
							}
							// $packItemsDetails[$j]["packageId"] = $package["pack_package_id"]; 
							// $packItemsDetails[$j]["packageName"] = $package["package_name"]; 
							// $packItemsDetails[$j]["image"] = $package["image"];
							// $packItemsDetails[$j]["packageType"] = $package["package_type"];
							// $packItemsDetails[$j]["price"] = $package["rate"];
							// $packItemsDetails[$j]["status"] = $package["status"];
							// $packItemsDetails[$j]["packageDaysCount"] = $package["package_days_count"];

							if (!empty($package["item_id"])) {
								
								$vendorParams = array ( ":item_id" => $package["item_id"]);								
								$vendorWhereCondtn = " item_id = :item_id";
								$reqQryParams = array (
										"fetchType" => "singleRow",
										"selectField" => "",
										"tableName" => "tbl_category_items",
										"whereCondition" => $vendorWhereCondtn
									);
								$vendorDetails = $this->funExeSelectQuery($reqQryParams, $vendorParams);
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["packageItemId"] = $package["item_id"];
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemId"] = $vendorDetails["item_id"];
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemName"] = $vendorDetails["item_name"];
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemDescription"] = $vendorDetails["short_description"];
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemType"] = $vendorDetails["item_type"];
								$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["image"] = !empty($vendorDetails["image"]) ? KAZA_SYSTEM_PATH.$vendorDetails["image"] : "" ;

								// $packItemsDetails[$j]['itemList'][$k]["packageItemId"] = $package["item_id"];
								// $packItemsDetails[$j]['itemList'][$k]["itemId"] = $vendorDetails["item_id"];
								// $packItemsDetails[$j]['itemList'][$k]["itemName"] = $vendorDetails["item_name"];
								// $packItemsDetails[$j]['itemList'][$k]["itemDescription"] = $vendorDetails["short_description"];
								// $packItemsDetails[$j]['itemList'][$k]["itemType"] = $vendorDetails["item_type"];
								// $packItemsDetails[$j]['itemList'][$k]["image"] = !empty($vendorDetails["image"]) ? KAZA_SYSTEM_PATH.$vendorDetails["image"] : "" ;
							} else {
								$packItemsDetails[$package["pack_package_id"]]['itemList'] = array();
							}
							$j++;
							$k++;
						}
					}
					$categoryItems[$i]["category"] = array_values($catItemsListDetails);
					$categoryItems[$i]["package"] = array_values($packItemsDetails);
					$i++;					
				}
				// $catItemsDetails['itemList'] = $catItemsListDetails;
				// print_r($catItemsListDetails);exit;
				if (!empty($categoryItems) && count($categoryItems > 0)) {
					$responseDate["status"] = 1;
					$responseDate["message"] = "success";
					$responseDate["vendor"] = $categoryItems;
				}
			}
			return $this->jsonResponse($responseDate);
		}
	}

	// Fetch list of orders that customer ordered till now
	public function myOrder($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {

			// Check required fields
			$requiredFields = array("userId", "userType");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}

			// Check order exisits
			if (strtolower($request["userType"]) != "vendor") {
				/*$selQryParams = array ( ":customer_id" => $request["userId"], ":status" => "pending");
			    $whereCondtn = $this->funParseQryParams($selQryParams, "status", "AND");*/
			    $selQryParams = array ( 
			    	":customer_id" => $request["userId"],
			    );
			    $whereCondtn = $this->funParseQryParams($selQryParams,"customer_id");
			    $whereCondtn = $whereCondtn." AND DATE_FORMAT(start_date, '%Y-%m-%d') >='".date("Y-m-d")."'";
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(customer_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn 
				);
			} else {
				/*$selQryParams = array ( ":vendor_id" => $request["userId"], ":status" => "pending");
			    $whereCondtn = $this->funParseQryParams($selQryParams, "status", "AND");*/
			    $selQryParams = array ( ":vendor_id" => $request["userId"]);
			    $whereCondtn = $this->funParseQryParams($selQryParams);
			    $whereCondtn = $whereCondtn." AND DATE_FORMAT(start_date, '%Y-%m-%d') >='".date("Y-m-d")."'";
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(vendor_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn
				);					    
			}

			$chekOrderExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekOrderExistRes["countRows"]) && $chekOrderExistRes["countRows"] > 0) {
			    $reqQryParams = array (
					"fetchType" => "multipleRow",
					"selectField" => "",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn,
					"orderByCondtn" =>" FIELD(status,'pending','confirmed','started','paid','cancel','completed','delivery') ASC, order_id DESC"
				);
				$orderResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);				
				$orderArr = array();
				if (!empty($orderResponse)) {
					$i = $k = 0;
					// $prevOrderId = "";
					// $prevVendorId = "";
					foreach ($orderResponse as $order) {

						// if ($prevOrderId != $order["order_id"]) {
						// 	$prevOrderId = $order["order_id"];
						// 	// $prevVendorId = $order["vendor_id"];
						// 	$k = 0;								
						// }

						$package_id=$order["package_id"];
						// if customer seeing my order then list by vendorname
						// if vendor seeing my order then list by customername
						$userid="";$userfullname="";
						if (strtolower($request["userType"]) != "vendor") { // for customer
							$userid = $order["vendor_id"];
							$userfullname = "vendorName";
						}
						else {
							$userid = $order["customer_id"];  //for vendor
							$userfullname = "customerName";
						}

						$vendorParams = array ( ":user_id" => $userid);
						$vendorWhereCondtn = $this->funParseQryParams($vendorParams);						
						$reqQryParams = array (
							"fetchType" => "singleRow",
							"selectField" => "",
							"tableName" => "tbl_users",
							"whereCondition" => $vendorWhereCondtn
						);
						$vendorDetails = $this->funExeSelectQuery($reqQryParams, $vendorParams);
						$orderItemArr = $this->funGetOrderList($order["order_id"]);

						$ArrPrice = array_map(function ($value) {
					        return  $value['price'];
					    }, $orderItemArr);

					    $totalAmt = number_format(array_sum($ArrPrice),2);

						if (($package_id != "0") || (!empty($package_id))) {
							$packageParams = array ( ":package_id" => $package_id);
							$packageWhereCondtn = $this->funParseQryParams($packageParams);
							$reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "",
								"tableName" => "tbl_packages",
								"whereCondition" => $packageWhereCondtn
							);
							$packageDetails = $this->funExeSelectQuery($reqQryParams, $packageParams);
							$totalAmt = number_format($packageDetails["rate"],2);

							$orderItemArr = $this->funGetPackageList($package_id);
						} 

						// $orderParams = array ( ":order_id" => $order["order_id"]);
						// // $whereCondtn = $this->funParseQryParams($orderParams);
						// $whereCondtn = "a.order_id=:order_id";
						// $reqQryParams = array (
						// 				"fetchType" => "multipleRow",
						// 				"selectField" => "*",
						// 				"tables" => array("tbl_order_items as a", "tbl_category_items as b"),
						// 				"onCondition" => "a.item_id = b.item_id",
						// 				"whereCondition" => $whereCondtn,
						// 				// "orderByCondtn" => "b.item_id asc"
						// 			);
						// $orderItemList = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $orderParams);
						// $orderItemArr = array();
						// if (!empty($orderItemList)) {
						// 	$j = 0;
						// 	foreach ($orderItemList as $orderItem) {
						// 		$orderItemArr[$j]["price"] = $orderItem["price"];
						// 		$orderItemArr[$j]["itemType"] = !empty($orderItem["item_type"]) ? $orderItem["item_type"] : "" ;
						// 		$orderItemArr[$j]["itemName"] = !empty($orderItem["item_name"]) ? $orderItem["item_name"] : "" ;
						// 		$orderItemArr[$j]["description"] = !empty($orderItem["short_description"]) ? $orderItem["short_description"] : "" ;
						// 		$j++;
						// 	}
						// }
						$orderArr[$k]["orderId"] = $order["order_id"];						
						$orderArr[$k][$userfullname] = $vendorDetails["full_name"];
						$orderArr[$k]["rating"] = $order["ratings"];
						$orderArr[$k]["status"] = $order["status"];
						if(strtolower($order["status"])=='cancel')
							$orderArr[$k]["reason"] = $order["reason"];
                        $orderArr[$k]["totalAmt"] = $totalAmt;
						$orderArr[$k]["startDate"] = (!empty($order["start_date"]))?$order["start_date"]:"";
						$orderArr[$k]["endDate"] = (!empty($order["end_date"]))?$order["end_date"]:"";
						$orderArr[$k]["deliveryTime"] = (!empty($order["delivery_time"]))?date('H:i',strtotime($order["delivery_time"])):"";
						$orderArr[$k]["orderType"]    = $order["order_type"];
						$orderArr[$k]["orderItemList"] = $orderItemArr;
						$i++;
						$k++;
					}
				}
				$responseDate["status"] = 1;
				$responseDate["message"] = "success";
				$responseDate["orders"] = $orderArr;
			} else {				 
				$responseDate["status"] = 0;
				$responseDate["message"] = "No order(s) found!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Fetch list of orders that customer ordered till now
	public function orderHistory($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {

			// Check required fields
			$requiredFields = array("userId", "userType");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}

			// Check order exisits
			if (strtolower($request["userType"]) != "vendor") {
				$selQryParams = array ( ":customer_id" => $request["userId"]);
			    $whereCondtn = $this->funParseQryParams($selQryParams);
			    $whereCondtn = $whereCondtn." AND DATE_FORMAT(end_date, '%Y-%m-%d') < '".date("Y-m-d")."' AND status in ('delivery','cancel','pending')";			    
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(customer_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn
				);
			} else {
				$selQryParams = array ( ":vendor_id" => $request["userId"]);
			    $whereCondtn = $this->funParseQryParams($selQryParams);
			    $whereCondtn = $whereCondtn." AND DATE_FORMAT(end_date, '%Y-%m-%d') < '".date("Y-m-d")."' AND status in ('delivery','cancel','pending')";
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(vendor_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn
				);					    
			}
			$chekOrderExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekOrderExistRes["countRows"]) && $chekOrderExistRes["countRows"] > 0) {

			    $reqQryParams = array (
					"fetchType" => "multipleRow",
					"selectField" => "",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn,
					"orderByCondtn" =>" order_id DESC"
				);
				$orderResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);
				$orderArr = array();
				if (!empty($orderResponse)) {
					$i = $k = 0;
					foreach ($orderResponse as $order) {
						// if ($prevOrderId != $order["vendor_id"]) {
						// 	$prevOrderId = $order["vendor_id"];
						// 	$k = 0;								
						// }
						$package_id=$order["package_id"];
						// if customer seeing my order then list by vendorname
						// if vendor seeing my order then list by customername
						$userid="";$userfullname="";
						if (strtolower($request["userType"]) != "vendor") { // for customer
							$userid = $order["vendor_id"];
							$userfullname = "vendorName";
						}
						else {
							$userid = $order["customer_id"];  //for vendor
							$userfullname = "customerName";
						}

						$vendorParams = array ( ":user_id" => $userid);
						$vendorWhereCondtn = $this->funParseQryParams($vendorParams);
						$reqQryParams = array (
							"fetchType" => "singleRow",
							"selectField" => "",
							"tableName" => "tbl_users",
							"whereCondition" => $vendorWhereCondtn
						);
						$vendorDetails = $this->funExeSelectQuery($reqQryParams, $vendorParams);
						$orderItemArr = $this->funGetOrderList($order["order_id"]);

						$ArrPrice = array_map(function ($value) {
					        return  $value['price'];
					        }, $orderItemArr);
					    $totalAmt = number_format(array_sum($ArrPrice),2);

						if (($package_id != "0") || (!empty($package_id))) {
							$packageParams = array ( ":package_id" => $package_id);
							$packageWhereCondtn = $this->funParseQryParams($packageParams);
							$reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "",
								"tableName" => "tbl_packages",
								"whereCondition" => $packageWhereCondtn
							);
							$packageDetails = $this->funExeSelectQuery($reqQryParams, $packageParams);
							$totalAmt = number_format($packageDetails["rate"],2);
							$orderItemArr = $this->funGetPackageList($package_id);
						} 
						// $orderParams = array ( ":order_id" => $order["order_id"]);
						// $whereCondtn = $this->funParseQryParams($orderParams);
						// $reqQryParams = array (
						// 				"fetchType" => "multipleRow",
						// 				"selectField" => "*",
						// 				"tables" => array("tbl_order_items as a", "tbl_category_items as b"),
						// 				"onCondition" => "a.item_id = b.item_id",
						// 				"whereCondition" => $whereCondtn,
						// 				// "orderByCondtn" => "b.item_id asc"
						// 			);
						// $orderItemList = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $orderParams);
						// $orderItemArr = array();
						// if (!empty($orderItemList)) {
						// 	$i = 0;
						// 	foreach ($orderItemList as $orderItem) {
						// 		$orderItemArr[$i]["price"] = $orderItem["price"];
						// 		$orderItemArr[$i]["itemType"] = !empty($orderItem["item_type"]) ? $orderItem["item_type"] : "" ;
						// 		$orderItemArr[$i]["itemName"] = !empty($orderItem["item_name"]) ? $orderItem["item_name"] : "" ;
						// 		$orderItemArr[$i]["description"] = !empty($orderItem["short_description"]) ? $orderItem["short_description"] : "" ;
						// 		$i++;
						// 	}
						// }
						$orderArr[$k]["orderId"] = $order["order_id"];
						$orderArr[$k][$userfullname] = $vendorDetails["full_name"];
						$orderArr[$k]["rating"] = $order["ratings"];
						$orderArr[$k]["status"] = $order["status"];
						if(strtolower($order["status"])=='cancel')
							$orderArr[$k]["reason"] = $order["reason"];
						$orderArr[$k]["totalAmt"] = $totalAmt;
						$orderArr[$k]["startDate"] = (!empty($order["start_date"]))?$order["start_date"]:"";
						$orderArr[$k]["endDate"] = (!empty($order["end_date"]))?$order["end_date"]:"";
						$orderArr[$k]["deliveryTime"] = (!empty($order["delivery_time"]))?date('H:i',strtotime($order["delivery_time"])):"";
						$orderArr[$k]["orderType"]    = $order["order_type"];
						$orderArr[$k]["orderItemList"] = $orderItemArr;
						$i++;
						$k++;
					}
				}
				$responseDate["status"] = 1;
				$responseDate["message"] = "success";
				$responseDate["orders"] = $orderArr;
			} else {				 
				$responseDate["status"] = 0;
				$responseDate["message"] = "No order(s) found!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Create and update orderDetails
	public function createUpdateOrder($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {

			$currentDate = $this->currentDate;
			$responseDate = array();
					
			if (!array_key_exists("orderId", $request)) {   // Create Order

				// Check required fields
				$requiredFields = array("userId");
				$errors = $this->funCheckRequiredFields($request, $requiredFields);

				
				if (count($errors) > 0) {
					return $this->jsonResponse($errors);
				}
				if((date('y-m-d',strtotime($request["startDate"])) >= date('y-m-d',strtotime($currentDate))) && (date('y-m-d',strtotime($request["endDate"])) >= date('y-m-d',strtotime($currentDate)))) { 
					// Checking the order date should be current date and future date
					$qryParams = array (
								// ":order_id" => $request["orderId"],
								":customer_id" => $request["userId"],
								":vendor_id" => $request["vendorId"],
								":package_id" => $request["packageId"],
								":start_date" => (empty($request["startDate"]))?$currentDate:date("Y-m-d h:i:s", strtotime($request["startDate"])),
								":end_date" => (empty($request["endDate"]))?$currentDate:date("Y-m-d h:i:s", strtotime($request["endDate"])),
								":delivery_time" => date("H:i:s", strtotime($request["deliveryTime"])),
								":price" => $request["price"],
								":ratings" => $request["ratings"],
								// ":order_type" => $request["orderType"],
								":order_type" => !empty($request["packageId"]) ? "Package" : "Normal",
								":status" => "Pending",
								":created_date" => $currentDate,
								":modified_date" => $currentDate
							);
					// Create/Insert order
					$insQryResponse = $this->funExeInsertRecord("tbl_orders", $qryParams);
					$orderId = $insQryResponse;
					// $orderItemList = json_decode($request["orderItemList"], TRUE);
					// if (is_array($orderItemList) && count($orderItemList) > 0) {
					// 		foreach ($orderItemList as $orderDet) {
					// 			$orderItemParams = array ( 
					// 					// ":order_item_id" => 24,	
					// 					":order_id" => $orderId,
					// 					":category_id" => $orderDet["categoryId"],
					// 					":item_id" => $orderDet["itemId"],
					// 					":price" => $orderDet["price"],
					// 					":created_date" => $currentDate,
					// 					":modified_date" => $currentDate
					// 				);
					// 			$insOrderItemQryResponse = $this->funExeInsertRecord("tbl_order_items", $orderItemParams);
					// 		}			
					// }
					$ArritemIds[] = $request["itemIds"];
					$itemIds = implode(",", $ArritemIds);
					$reqQryParams = array (
						"fetchType" => "multipleRow",
						"selectField" => "*",
						"tableName" => "tbl_category_items",
						"whereCondition" =>" item_id in (".$itemIds.")"
					);
					
					$orderItemList = $this->funExeSelectQuery($reqQryParams, $selQryParams);	
	                if (is_array($orderItemList) && count($orderItemList) > 0) {
							foreach ($orderItemList as $orderDet) {
								$orderItemParams = array ( 
										":order_id" => $orderId,
										":category_id" => $orderDet["category_id"],
										":item_id" => $orderDet["item_id"],
										":price" => $orderDet["price"],
										":created_date" => $currentDate,
										":modified_date" => $currentDate
									);
								$insOrderItemQryResponse = $this->funExeInsertRecord("tbl_order_items", $orderItemParams);
							}			
					}
					$responseDate["status"] = 1;
					$responseDate["message"] = "Order created successfully";
					$responseDate["orderId"] = !empty($insQryResponse) ? $insQryResponse : "" ;
				}  else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Invalid date!";					
				}
			} else {	// Update order status				
				// Check required fields
				$requiredFields = array("orderId");
				$errors = $this->funCheckRequiredFields($request, $requiredFields);
				if (count($errors) > 0) {
					return $this->jsonResponse($errors);
				}		
				$orderId = $request["orderId"];
				$qryParams = array ( 	
							":order_id" => $request["orderId"],
							":status" => $request["status"],
							":reason" => $request["reason"],
							":paypal_transaction_id" => $request["paypalTransactionId"],
							":payment_status" => $request["paymentStatus"],
							":payment_date" => $currentDate,
							":modified_date" => $currentDate
						);
				// Update order
				// Check given order exists ?
				$selQryParams = array ( ":order_id" => $orderId);
				$whereCondtn = $this->funParseQryParams($selQryParams);
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(order_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn
				);
				$chekOrderExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);	    
				if (isset($chekOrderExistRes["countRows"]) && $chekOrderExistRes["countRows"] > 0) {
					$setCondtn = $this->funParseQryParams($qryParams, "modified_date", ",");
				    $reqQryParams = array (
							"tableName" => "tbl_orders",
							"setCondtn" =>$setCondtn,
							"whereCondition" => "order_id=:order_id"
						);
					$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $qryParams);
					if (trim(strtolower($request["status"])) == "paid") {

						$reqQryParams = array (
							"fetchType" => "singleRow",
							"selectField" => "*",
							"tableName" => "tbl_orders",
							"whereCondition" => $whereCondtn
						);
						$getOrderDetails = $this->funExeSelectQuery($reqQryParams, $selQryParams);
						$dayinSec = 86400; // Day in seconds
						$startDate = strtotime($getOrderDetails["start_date"]);
						$endDate = strtotime($getOrderDetails["end_date"]);
						$diff = $endDate - $startDate;
						$diff_in_days = floor($diff/(60*60*24)) + 1;
						for ($d = 0; $d < $diff_in_days; $d++) {
							$orderItemParams = array (
									// ":order_item_id" => 24,
									":order_id" => $orderId,
									":customer_id" => $getOrderDetails["customer_id"],
									":vendor_id" => $getOrderDetails["vendor_id"],
									":order_date" => date("Y-m-d", ($startDate + ($d * $dayinSec))),
									":status" => $request["status"],
									":created_date" => $currentDate,
									":modified_date" => $currentDate
								);
							$insOrderItemQryResponse = $this->funExeInsertRecord("tbl_daywise_orders", $orderItemParams);
						}
					}

					// Get order
				    $reqQryParams = array (
						"fetchType" => "singleRow",
						"selectField" => "*",
						"tableName" => "tbl_orders",
						"whereCondition" => $whereCondtn
					);
					$orderInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams);
					$responseDate["status"] = 1;
					$responseDate["message"] = "Order status updated successfully";
					$responseDate["orderStatus"] = (isset($orderInfo["status"]) && !empty($orderInfo["status"])) ? $orderInfo["status"] : "" ;
					if (trim($request["reason"]) == "cancel")
						$responseDate["reason"] = $request["reason"];
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Order not exists!";
				}
			}
			return $this->jsonResponse($responseDate);
		}
	}

	// Create and update orderRatings
	public function updateOrderRatings($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {

			// Check required fields
			$requiredFields = array("orderId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			$responseDate = array();
			$orderId = $request["orderId"];
			$qryParams = array ( 	
						":order_id" => $request["orderId"],
						":ratings" => $request["ratings"],
						":feedback_msg" => $request["feedBackMsg"],
						":modified_date" => $currentDate
					);
			// Update order
			// Check given order exists ?
			$selQryParams = array ( ":order_id" => $orderId);
			$whereCondtn = $this->funParseQryParams($selQryParams);
		    $reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "*",
				"tableName" => "tbl_orders",
				"whereCondition" => $whereCondtn
			);
			$checkOrderExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);	
			if (is_array($checkOrderExistRes) && count($checkOrderExistRes) > 0) {
				$setCondtn = $this->funParseQryParams($qryParams, "modified_date", ",");
			    $reqQryParams = array (
						"tableName" => "tbl_orders",
						"setCondtn" =>$setCondtn,
						"whereCondition" => "order_id=:order_id"
					);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $qryParams);
				// Get vendor info to send Email
				$vendorInfoArr = array("userId" => $checkOrderExistRes["vendor_id"], "userType" => "vendor");
				$getVendorInfo = $this->getUserInfo($vendorInfoArr);
				// Get vendor info
				$custInfoArr = array("userId" => $checkOrderExistRes["customer_id"], "userType" => "customer");
				$getCustInfo = $this->getUserInfo($custInfoArr);
				if (count($getVendorInfo) > 0 && count($getCustInfo) > 0) {
					// Send Email 
					$mailParams = array(
										"fromAddress" => "food@kazafood.com",
				                        "toAddress" => $getVendorInfo["email"],
				                        "customerName" => $getCustInfo["full_name"],
				                        "subject" => "Update order ratings",
				                        "bodyMsg" =>'<html>
                                            <body>
	                                            <table width="600px" style="font-family: verdana;font-size:12px;">
	                                             	<tr>
												   		<td>Dear '.$getVendorInfo["full_name"].',</td>
												    </tr>
												    <tr style="height:10px"><td></td></tr>
												    <tr>
												   		<td>The '.$getCustInfo["full_name"].' has updated status</td>
												    </tr>
													<tr style="height:10px"><td></td></tr>
													<tr>
														<td>orderID: '.$orderId.'</td>
													</tr>
													<tr style="height:20px"><td></td></tr>
													<tr>
														<td>Best Regards,<br>KazaFood support Team</td>
													</tr>
												</table>
                                            </body>
                                            </html>'
				                    );
					$sendEmail = $this->sendEmailNotification($mailParams);
				}

				$responseDate["status"] = 1;
				$responseDate["message"] = "Rating updated successfully";
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Order not exists!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Fetch list of orders that customer ordered till now
	public function getCustomerDetail($request) {
		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			// Check required fields
			$requiredFields = array("userId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			// Check order exisits
			$selQryParams = array ( ":vendor_id" => $request["userId"]);
		    $whereCondtn = $this->funParseQryParams($selQryParams);

		    $reqQryParams = array (  
				"fetchType" => "singleRow",
				"selectField" => "count(customer_id) as countRows",
				"tableName" => "tbl_orders",
				"whereCondition" => $whereCondtn
			);
			// Getting unique customers for particular vendor
			$reqCustomQryParams = array (
				"fetchType" => "multipleRow",
				"selectField" => "customer_id",
				"tableName" => "tbl_orders",
				"whereCondition" => $whereCondtn,
				"groupByCondition" => " customer_id "
			);			
			$chekOrderExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$chekOrderExistCustom = $this->funExeSelectQuery($reqCustomQryParams, $selQryParams);

			$responseDate = array();$orderArr = array();$i = 0;
			if (isset($chekOrderExistRes["countRows"]) && $chekOrderExistRes["countRows"] > 0) {				
				foreach($chekOrderExistCustom as $customers) {					
					$userParams = array ( ":user_id" => $customers["customer_id"]);
					$userWhereCondtn = $this->funParseQryParams($userParams);
					// Below query for take the customer name
					$reqQryParams = array (
						"fetchType" => "singleRow",
						"selectField" => "",
						"tableName" => "tbl_users",
						"whereCondition" => $userWhereCondtn
					);
					$customerDetails = $this->funExeSelectQuery($reqQryParams, $userParams);

					// Below query for take the order list for particular customer
					$customerParams = array ( ":customer_id" => $customers["customer_id"],":vendor_id" => $request["userId"]);
					$customerWhereCondtn = $this->funParseQryParams($customerParams, "vendor_id", "AND");
					$reqOrderQryParams = array (
						"fetchType" => "multipleRow",
						"selectField" => "",
						"tableName" => "tbl_orders",
						"whereCondition" => $customerWhereCondtn
					);		

					$orderResponse = $this->funExeSelectQuery($reqOrderQryParams, $customerParams);	

					$orderArr[$i]["customerName"] = $customerDetails["full_name"];
					
					if (!empty($orderResponse)) {
						$orderItemArr = array(); $overallratingsArr = array();						
						foreach ($orderResponse as $order) {							
							if(count($this->funGetOrderList($order["order_id"]))>0) {
								$orderItemArr[]= $this->funGetOrderList($order["order_id"]);
							}		
							$overallratingsArr[] = $order["ratings"];	// Overall ratings for all order of individual customer			
						}							
						$orderArr[$i]["rating"] =  number_format((array_sum($overallratingsArr)/count($overallratingsArr)), 1, '.', '');	
						
						if (count($orderItemArr)>0) {
							foreach($orderItemArr as $key=>$orderItems) {
								foreach($orderItems as $OrdersItems) {
									$orderArr[$i]["orderItemList"][] = $OrdersItems;						
								}
							}
						}
						$i++;
					}
				}
				/*if (!empty($orderResponse)) {
					$i = 0;
					// $prevOrderId = "";
					// $prevVendorId = "";
					// foreach ($orderResponse as $order) {

						// if ($prevOrderId != $order["order_id"]) {
						// 	$prevOrderId = $order["order_id"];
						// 	// $prevVendorId = $order["vendor_id"];
						// 	$k = 0;								
						// }											
						
					   	
						// $orderParams = array ( ":order_id" => $order["order_id"]);
						// $whereCondtn = "a.order_id=:order_id";
						// $reqQryParams = array (
						// 				"fetchType" => "multipleRow",
						// 				"selectField" => "*",
						// 				"tables" => array("tbl_order_items as a", "tbl_category_items as b"),
						// 				"onCondition" => "a.item_id = b.item_id",
						// 				"whereCondition" => $whereCondtn,
						// 			);
						// $orderItemList = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $orderParams);
						// $orderItemArr = array();
						// if (!empty($orderItemList)) {
						// 	$j = 0;
						// 	foreach ($orderItemList as $orderItem) {
						// 		$orderItemArr[$j]["price"] = $orderItem["price"];
						// 		$orderItemArr[$j]["itemType"] = !empty($orderItem["item_type"]) ? $orderItem["item_type"] : "" ;
						// 		$orderItemArr[$j]["itemName"] = !empty($orderItem["item_name"]) ? $orderItem["item_name"] : "" ;
						// 		$orderItemArr[$j]["description"] = !empty($orderItem["short_description"]) ? $orderItem["short_description"] : "" ;
						// 		$j++;
						// 	}
						// }
						$orderArr[$i]["orderId"] = $order["order_id"];
						$orderArr[$i]["customerName"] = $customerDetails["full_name"];
						$orderArr[$i]["rating"] = $order["ratings"];
						$orderArr[$i]["status"] = $order["status"];
						$orderArr[$i]["orderItemList"] = $orderItemArr;
						$i++;
					// }
				} */
				$responseDate["status"] = 1;
				$responseDate["message"] = "success";
				$responseDate["orders"] = $orderArr;
			} else {				 
				$responseDate["status"] = 1;
				$responseDate["message"] = "No order(s) found!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Create and update category items
	public function createUpdateCategoryItem($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("categoryId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			$qryParams = array ( 
							":item_id" => isset($request["itemId"]) ? $request["itemId"] : "",
							":item_order" => $request["itemOrder"],	
							":category_id" => $request["categoryId"],
							":item_name" => $request["itemName"],
							":short_description" => $request["itemDescription"],
							":item_type" => $request["itemType"],
							":image" => $request["itemImage"],
							":price" => $request["itemPrice"],
							":created_date" => $currentDate,
							":modified_date" => $currentDate
						);
			$responseDate = array();
			$itemId = $qryParams[":item_id"];		
			if (empty($itemId)) {

				unset($qryParams[':item_id']);
				// Create category item					
				$insQryResponse = $this->funExeInsertRecord("tbl_category_items", $qryParams);
				$itemId = $insQryResponse;
				$responseDate["message"] = "Category created successfully";	

			} else {			

				unset($qryParams[':created_date']);
				// Check given category item exists ?										
				$selQryParams = array ( ":item_id" => $itemId);
			    $whereCondtn = $this->funParseQryParams($selQryParams);	
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(item_id) as countRows",
					"tableName" => "tbl_category_items",
					"whereCondition" => $whereCondtn
				);
				$chekCatItemExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);	    
				if (isset($chekCatItemExistRes["countRows"]) && $chekCatItemExistRes["countRows"] > 0) {
					// Update category items
					$setCondtn = $this->funParseQryParams($qryParams, "modified_date", ",");
				    $reqQryParams = array (
							"tableName" => "tbl_category_items",
							"setCondtn" =>$setCondtn,
							"whereCondition" => $this->funParseQryParams($selQryParams)
						);
					$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $qryParams);

					$responseDate["message"] = "Category updated successfully";
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Category item not exists!";
				}
			}

			if (!empty($itemId)) {
				$selInsQryParams = array(":item_id" => $itemId );
				$selInsWhereCondtn = $this->funParseQryParams($selInsQryParams);
			    // Select last inserted category item
			    $reqQryParams = array (
						"fetchType" => "singleRow",
						"selectField" => "",
						"tableName" => "tbl_category_items",
						"whereCondition" => $selInsWhereCondtn
					);
				$selQryResponse = $this->funExeSelectQuery($reqQryParams, $selInsQryParams);
				$responseDate["status"] = 1;					
				$responseDate["categoryItemId"] = !empty($selQryResponse["item_id"]) ? $selQryResponse["item_id"] : "" ;
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Something Issue in creating category!";
				$responseDate["categoryItemId"] = "";
			}
			return $this->jsonResponse($responseDate);
		}
	}

	// Create and update category items
	public function deleteCategoryItem($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("itemId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			$itemId = isset($request["itemId"]) ? $request["itemId"] : "";
			$qryParams = array ( ":item_id" => $itemId);		

			$responseDate = array();					
			if (!empty($itemId)) {

				$reqestParams = array ( ":item_id" => $itemId);
			    $whereCondtn = $this->funParseQryParams($reqestParams);	
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(item_id) as countRows",
					"tableName" => "tbl_category_items",
					"whereCondition" => $whereCondtn
				);				
				$chekCatItemExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
				if (isset($chekCatItemExistRes["countRows"]) && $chekCatItemExistRes["countRows"] > 0) {

					// Delete category item
				    $delWhereCondtn = $this->funParseQryParams($reqestParams);	
				    $reqQryParams = array (
						"tableName" => "tbl_category_items",
						"whereCondition" => $delWhereCondtn
					);
					$delCatItemRes = $this->funExeDeleteQuery($reqQryParams, $reqestParams);
					$responseDate["status"] = 1;
					$responseDate["message"] = "Category item deleted successfully";	
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Category item not exists!";
				}
			}		
			return $this->jsonResponse($responseDate);
		}
	}

	// Create package and package items
	public function createPackage($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("vendorId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			// Create package
			$qryParams = array ( 
							":vendor_id" => $request["vendorId"],
							":package_name" => $request["packageName"],	
							":package_days_count" => $request["daysCount"],
							":image" => $request["image"],
							":package_type" => $request["packageType"],
							":rate" => $request["price"],
							":created_date" => $currentDate,
							":modified_date" => $currentDate
						);
			$insPackageQryResponse = $this->funExeInsertRecord("tbl_packages", $qryParams);
			$responseDate = array();
			// Create package item
			if (!empty($insPackageQryResponse)) {
				$packageItemList = json_decode($request["packageItemList"], TRUE);
				foreach ($packageItemList as $package) {
					$catItem = explode(",",$package["itemId"]);					
					foreach ($catItem as $catKey => $catVal) {
						$packItemParams = array ( 
							":package_id" => $insPackageQryResponse,
							":category_id" => $package["categoryId"],
							":item_id" => $catVal,
							":created_date" => $currentDate,
							":modified_date" => $currentDate
						);
						$insPackageItemQryResponse = $this->funExeInsertRecord("tbl_package_items", $packItemParams);
					}
				}
				$responseDate["status"] = 1;
				$responseDate["message"] = "Package created successfully";
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "Something Issue in creating package!";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	// Delete package
	public function deletePackage($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("packageId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$packageId = isset($request["packageId"]) ? $request["packageId"] : "";
			$qryParams = array ( ":package_id" => $packageId);			

			$responseDate = array();					
			if (!empty($packageId)) {

				$reqestParams = array ( ":package_id" => $packageId);
			    $whereCondtn = $this->funParseQryParams($reqestParams);	
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(package_id) as countRows",
					"tableName" => "tbl_packages",
					"whereCondition" => $whereCondtn
				);				
				$chekCatItemExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
				if (isset($chekCatItemExistRes["countRows"]) && $chekCatItemExistRes["countRows"] > 0) {

					// Delete package
				    $delWhereCondtn = $this->funParseQryParams($reqestParams);	
				    $reqQryParams = array (
						"tableName" => "tbl_packages",
						"whereCondition" => $delWhereCondtn
					);
					$delCatItemRes = $this->funExeDeleteQuery($reqQryParams, $reqestParams);

					// Check package item exists
				    $reqQryParams = array (
						"fetchType" => "singleRow",
						"selectField" => "count(package_id) as countRows",
						"tableName" => "tbl_package_items",
						"whereCondition" => $whereCondtn
					);				
					$chekPackItemExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);

					if (isset($chekPackItemExistRes["countRows"]) && $chekPackItemExistRes["countRows"] > 0) {
						// Delete package item
					    $delWhereCondtn = $this->funParseQryParams($reqestParams);	
					    $reqQryParams = array (
							"tableName" => "tbl_package_items",
							"whereCondition" => $delWhereCondtn
						);
						$delCatItemRes = $this->funExeDeleteQuery($reqQryParams, $reqestParams);
					}

					$responseDate["status"] = 1;
					$responseDate["message"] = "Package deleted successfully";	
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "Package not exists!";
				}
			}		
			return $this->jsonResponse($responseDate);
		}
	}

	// Get list of orders for customer
	public function getReport($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("userId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			$userId = isset($request["userId"]) ? $request["userId"] : "";

			$responseDate = array();					
			if (!empty($userId)) {
				$reqestParams = array ( 
							":vendor_id" => $userId,
							":start_date" =>  $request["startDate"],
							":end_date" =>  $request["endDate"]
						);

			    // $whereCondtn = $this->funParseQryParams($reqestParams);
			    $whereCondtn = "vendor_id=:vendor_id AND ";
			    if (!empty($request["startDate"]) && !empty($request["endDate"])) {
			    	$whereCondtn .= "DATE_FORMAT(start_date, '%Y-%m-%d') >= :start_date AND DATE_FORMAT(end_date, '%Y-%m-%d') <= :end_date";
			    } elseif (!empty($request["startDate"])) {
			    	$whereCondtn .= "DATE_FORMAT(start_date, '%Y-%m-%d') >= :start_date";
			    	unset($reqestParams[':end_date']);
			    } elseif (!empty($request["endDate"])) {
			    	$whereCondtn .= "DATE_FORMAT(end_date, '%Y-%m-%d') <= :end_date";
			    	unset($reqestParams[':start_date']);
			    } else {
			    	unset($reqestParams[':start_date'],  $reqestParams[':end_date']);
			    }
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(vendor_id) as countRows",
					"tableName" => "tbl_orders",
					"whereCondition" => $whereCondtn
				);			
				$chekVendorExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
				if (isset($chekVendorExistRes["countRows"]) && $chekVendorExistRes["countRows"] > 0) {
					// Check package item exists
				    $reqQryParams = array (
						"fetchType" => "multipleRow",
						"selectField" => "*, SUM(price) as custTotPrice",
						"tableName" => "tbl_orders",
						"whereCondition" => $whereCondtn,
						"groupByCondition" => "customer_id"
					);
					$customerListRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
					$customersArr = array();
					$venTotOrderPrice = 0;
					if (is_array($customerListRes) && count($customerListRes) > 0) {
						$i = 0;
						foreach ($customerListRes as $customer) {

							$userParams = array(":user_id" =>$customer["customer_id"]);
							$reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "*",
								"tableName" => "tbl_users",
								"whereCondition" => "user_id=:user_id"
							);			
							$customerInfo = $this->funExeSelectQuery($reqQryParams, $userParams);
							$customersArr[$i]["customerId"] = $customer["customer_id"];
							$customersArr[$i]["customerName"] = isset($customerInfo["full_name"]) ? $customerInfo["full_name"] : "" ;
							$customersArr[$i]["price"] = $customer["custTotPrice"];
							$venTotOrderPrice += $customer["custTotPrice"];
							$i++;
						}	
					}
					$responseDate["status"] = 1;
					$responseDate["message"] = "Success";
					$responseDate["customerList"] = $customersArr;
					$responseDate["totalPrice"] = $venTotOrderPrice;

				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "No report(s) found!";
				}
			}		
			return $this->jsonResponse($responseDate);
		}
	}

	// Get list of orders for customer
	public function getInvoice($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("userId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			$currentDate = $this->currentDate;
			$userId = isset($request["userId"]) ? $request["userId"] : "";
			$responseDate = array();					
			if (!empty($userId)) {

				$reqestParams = array ( ":vendor_id" => $userId );
			    $whereCondtn = $this->funParseQryParams($reqestParams);
			    $reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "count(vendor_id) as countRows",
					"tableName" => "tbl_invoices",
					"whereCondition" => $whereCondtn,
					"groupByCondition" => "vendor_id"
				);			
				$chekVendorExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
				if (isset($chekVendorExistRes["countRows"]) && $chekVendorExistRes["countRows"] > 0) {					

					// Check invoice order exists
				    $reqQryParams = array (
						"fetchType" => "multipleRow",
						"selectField" => "*",
						"tableName" => "tbl_invoices",
						"whereCondition" => $whereCondtn,
						"groupByCondition" => "invoice_id"
					);				
					$invoiceListRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
					$invoiceOrdersArr = array();
					if (is_array($invoiceListRes) && count($invoiceListRes) > 0) {
						$i = 0;
						foreach ($invoiceListRes as $invoice) {
							$invoiceOrdersArr[$i]["invoiceId"] = $invoice["invoice_id"];
							$invoiceOrdersArr[$i]["invoiceStatus"] = $invoice["vendor_invoice_status"];
							$invoiceOrdersArr[$i]["invoiceAmt"] = $invoice["invoice_amount"];
							$i++;
						}	
					}
					$responseDate["status"] = 1;
					$responseDate["message"] = "Success";
					$responseDate["invoiceOrderList"] = $invoiceOrdersArr;
				} else {
					$responseDate["status"] = 0;
					$responseDate["message"] = "No invoice(s) found!";
				}
			}		
			return $this->jsonResponse($responseDate);
		}
	}

	// Update invoice
	public function updateInvoice($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			$responseDate = array();
					
			// Check required fields
			$requiredFields = array("userId", "invoiceId");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}	

			// Update invoice
			// Check invoice exists ?
			$currentDate = $this->currentDate;
			$selQryParams = array ( ":invoice_id" => $request["invoiceId"]);
			$whereCondtn = $this->funParseQryParams($selQryParams);
		    $reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "count(invoice_id) as countRows",
				"tableName" => "tbl_invoices",
				"whereCondition" => $whereCondtn
			);
			$chekInvoiceExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);	    
			if (isset($chekInvoiceExistRes["countRows"]) && $chekInvoiceExistRes["countRows"] > 0) {

				$setInvoiceParams = array ( 
						":vendor_invoice_status" => $request["invoiceStatus"],
						":modified_date" => $currentDate
					);
				$invoiceParams = array_merge($selQryParams, $setInvoiceParams);
				$setCondtn = $this->funParseQryParams($setInvoiceParams,"modified_date", ",");
				$whereCondtn = $this->funParseQryParams($selQryParams);
			    $reqQryParams = array (
						"tableName" => "tbl_invoices",
						"setCondtn" =>$setCondtn,
						"whereCondition" => $whereCondtn
					);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $invoiceParams);
				$responseDate["status"] = 1;
				$responseDate["message"] = "Invoice updated successfully";
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "No invoice(s) found!";
			}
			return $this->jsonResponse($responseDate);
		}
	}

	// Get list of categories for customer
	public function getCategoryList($request) {
	    $requiredFields = array('userId');
		$errors = $this->funCheckRequiredFields($request, $requiredFields);
		$currentDate = $this->currentDate;
		if (count($errors) > 0) {
			return $this->jsonResponse($errors);
		}

		$selVendorQryParams = array(
			                    ":user_id" => $request["userId"],
								":user_type" => "vendor" 
							);
		$whereCondtn = $this->funParseQryParams($selVendorQryParams, "user_type", "AND");
		$reqQryParams = array (
				"fetchType" => "multipleRow",
				"selectField" => "",
				"tableName" => "tbl_users",
				"whereCondition" => $whereCondtn
			);
		//print_r($reqQryParams);
		$vendorsResponse = $this->funExeSelectQuery($reqQryParams, $selVendorQryParams);
		$categoryRes = $responseDate = $categoryItems = array();
		if (!empty($vendorsResponse)) {
			foreach ($vendorsResponse as $vendor) {
				// Fetch/Get Category and Category items
				$selCatQryParams = array(":vendor_id" => $vendor["user_id"]);
				$selCatWhereCondtn = "a.vendor_id=:vendor_id";
				if (is_array($request) && count($request) > 0) {
					$i = 0;
					$reqQryParams = array (
										"fetchType" => "multipleRow",
										"selectField" => "*, a.category_id as cat_category_id, a.created_date as cat_created_date, a.modified_date as cat_modified_date, b.category_id as catitem_category_id, b.created_date as catitem_created_date, b.modified_date as catitem_modified_date",
										"tables" => array("tbl_category as a", "tbl_category_items as b"),
										"onCondition" => "a.category_id = b.category_id",
										"whereCondition" => $selCatWhereCondtn,
										// "orderByCondtn" => "b.category_id asc"
									);
					$categoryRes = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $selCatQryParams);
					
					$catItemsListDetails = array();
					if (!empty($categoryRes) && count($categoryRes > 0)) {
						$k = $j = 0;
						$prevCatId = "";
						foreach ($categoryRes as $category) {

							if ($prevCatId != $category["cat_category_id"]) {
								$catItemsListDetails[$category["cat_category_id"]]["categoryId"] = $category["cat_category_id"]; 
								$catItemsListDetails[$category["cat_category_id"]]["categoryName"] = $category["category_name"]; 
								$catItemsListDetails[$category["cat_category_id"]]["description"] = $category["description"]; 
								$catItemsListDetails[$category["cat_category_id"]]["image"] = $category["image"];

								$prevCatId = $category["cat_category_id"];
								$k = 0;
							}							

							if (!empty($category["item_id"])) {

								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["categoryId"] = $category["category_id"];		
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemId"] = $category["item_id"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemName"] = $category["item_name"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemDescription"] = $category["short_description"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["itemType"] = $category["item_type"];
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["image"] = !empty($category["image"]) ? KAZA_SYSTEM_PATH.$category["image"] : "" ;
								$catItemsListDetails[$category["cat_category_id"]]['itemList'][$k]["price"] = $category["price"];
							} else {
								$catItemsListDetails[$category["cat_category_id"]]['itemList'] = array();
							}
							$j++;
							$k++;							
						}
					}
					$categoryItems = array_values($catItemsListDetails);
					$i++;					
					if (!empty($categoryItems) && count($categoryItems > 0)) {
						$responseDate["status"] = 1;
						$responseDate["message"] = "success";
						$responseDate["categoryList"] = $categoryItems;
					} else {
						$responseDate["status"] = 0;
						$responseDate["message"] = "No record(s) found!";
					}
				}
			}
		} else {
			$responseDate["status"] = 0;
			$responseDate["message"] = "No record(s) found!";
		}
		return $this->jsonResponse($responseDate);
	}

	// Update invoice
	public function updateProfile($request) {

		// Check $request variable is an array
		if (is_array($request) && count($request) > 0) {
			
			// Check required fields
			$requiredFields = array("userId", "userType");
			$errors = $this->funCheckRequiredFields($request, $requiredFields);
			$currentDate = $this->currentDate;
			if (count($errors) > 0) {
				return $this->jsonResponse($errors);
			}
			// Check vendor/customer is exist
			$selQryParams = array ( 
								":user_id" => $request["userId"], 
								":user_type" => strtolower($request["userType"])
							);
		    $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");   
		    $reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "count(user_id) as countRows",
				"tableName" => "tbl_users",
				"whereCondition" => $whereCondtn
			);
			$chekUserExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
			$responseDate = array();
			if (isset($chekUserExistRes["countRows"]) && $chekUserExistRes["countRows"] > 0) {

				$uploadImg = $request["userImg"];
				$uploadDocs = $request["userdocs"];
				$userImg = "";
				if (!empty($uploadImg)) {
					// Convert base64image to image
					$floderName="user_profile";
					$userImg = $this->base64toImage($uploadImg,$floderName);
				}
				if (!empty($uploadDocs)) {
					// Convert base64image to image
					$floderName="user_documents";
					$userDocs = $this->base64toImage($uploadDocs,$floderName);
					/*$type = pathinfo(base64_decode($uploadDocs), PATHINFO_EXTENSION);
            		$Arrfiletypes = array("png","jpg","jpeg");                
					$userDocs = (!in_array(strtolower($type), $Arrfiletypes))?$this->base64toDocs($uploadDocs,$floderName):$this->base64toImage($uploadDocs,$floderName) ;*/
				}
				$updateQryParams = array ( 
									":full_name" => $request["fullName"],
									// ":email" => $request["email"],
									// ":password" => $request["password"],
									":address" => $request["address"],
									":country" => $request["country"],
									":zip_code" => $request["zipCode"],
									":mobile_number" => $request["mobileNumber"],
									":user_img" => $userImg,
									":upload_documents" => $userDocs,
									":modified_date" => $currentDate
								);
				$setCondtn = $this->funParseQryParams($updateQryParams, "modified_date", ",");
			    $reqQryParams = array (
						"tableName" => "tbl_users",
						"setCondtn" =>$setCondtn,
						"whereCondition" => $whereCondtn
					);
				$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, array_merge($updateQryParams, $selQryParams));
				$reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "*",
					"tableName" => "tbl_users",
					"whereCondition" => "user_id=:user_id"
				);
				unset($selQryParams[':user_type']);
				$userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams);
				$responseDate["status"] = 1;
				$responseDate["message"] = "User updated successfully";
				$responseDate["image"] = isset($userInfo["user_img"]) ? KAZA_SYSTEM_PATH.$userInfo["user_img"] : "";
				$responseDate["userdocs"] = isset($userInfo["upload_documents"]) ? KAZA_SYSTEM_PATH.$userInfo["upload_documents"] : "";
			} else {
				$responseDate["status"] = 0;
				$responseDate["message"] = "User not exists";
			}			
			return $this->jsonResponse($responseDate);
		}
	}

	public function testPushNotification($request) {
		// echo "Test";exit;
		$responseDate = array();
		$responseDate["message"] = $this->sendIospushNotification("70cf571cee62317f7487277d7d9b2cd3bb3ed0644af6deb0af2c9095c38c3e91", "Hi Pushnotification");
		return $this->jsonResponse($responseDate);
	}

	public function updatePassword($request) { 
		// Check required fields
		$requiredFields = array("userId", "verificationCode","password");
		$errors = $this->funCheckRequiredFields($request, $requiredFields);
		$currentDate = $this->currentDate;
		if (count($errors) > 0) {
			return $this->jsonResponse($errors);
		}
		$selQryParams = array ( 
								":user_id" => $request["userId"], 
								":verification_code" => $request["verificationCode"]
							);
		$whereCondtn = $this->funParseQryParams($selQryParams, "verification_code", "AND"); 
		$reqQryParams = array (
				"fetchType" => "singleRow",
				"selectField" => "*",
				"tableName" => "tbl_users",
				"whereCondition" => $whereCondtn
			);
		$userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams); 
		$responseDate = array();
		if (is_array($userInfo) && count($userInfo) > 0) {
		    $updateQryParams = array ( 
									":password" => $this->encode($request["password"])
								);
			$setCondtn = $this->funParseQryParams($updateQryParams);
			$updateQryParams[":user_id"] =  $request["userId"];
			$updateQryParams[":verification_code"] = $request["verificationCode"];
		    $reqQryParams = array (
								"tableName" => "tbl_users",
								"setCondtn" =>$setCondtn,
								"whereCondition" => $whereCondtn
							);
		    //print_r($reqQryParams);
			$updateQryResponse = $this->funExeUpdateRecord($reqQryParams, $updateQryParams);
            $responseDate["status"] = 1;
		    $responseDate["message"] = "Password updated successfully";
		} else {				 
				$responseDate["status"] = 0;
				$responseDate["message"] = "Verification code not exists!";
		}	
		return $this->jsonResponse($responseDate);
	}

	public function getPackageList($request) {

		$requiredFields = array('userId');
		$errors = $this->funCheckRequiredFields($request, $requiredFields);
		$currentDate = $this->currentDate;
		if (count($errors) > 0) {
			return $this->jsonResponse($errors);
		}
	
		$selVendorQryParams = array(
		                    ":user_id" => $request["userId"],
							":user_type" => "vendor" 
						);
		$whereCondtn = $this->funParseQryParams($selVendorQryParams, "user_type", "AND");
		$reqQryParams = array (
				"fetchType" => "multipleRow",
				"selectField" => "",
				"tableName" => "tbl_users",
				"whereCondition" => $whereCondtn
			);
		$vendorsResponse = $this->funExeSelectQuery($reqQryParams, $selVendorQryParams);
		//print_r($vendorsResponse);
		$categoryRes = $responseDate = $categoryItems = array();
		if (!empty($vendorsResponse)) {
			$i=0;
			foreach ($vendorsResponse as $vendor) {
				$selCatQryParams = array(":vendor_id" => $vendor["user_id"]);
				$selCatWhereCondtn = "a.vendor_id=:vendor_id";
				$reqQryParams = array (
										"fetchType" => "multipleRow",
										"selectField" => "*, a.package_id as pack_package_id, a.created_date as cat_created_date, a.modified_date as cat_modified_date, b.package_id as pack_item_id, b.created_date as catitem_created_date, b.modified_date as catitem_modified_date",
										"tables" => array("tbl_packages as a", "tbl_package_items as b"),
										"onCondition" => "a.package_id = b.package_id",
										"whereCondition" => $selCatWhereCondtn,
									);
				$packageRes = $this->funExeTwoTblLeftJoinQuery($reqQryParams, $selCatQryParams);
				$packItemsDetails = array();
				if (!empty($packageRes) && count($packageRes > 0)) {
					$k = $j = 0;
					$prevPackId = "";
					foreach ($packageRes as $package) {

						if ($prevPackId != $package["pack_package_id"]) {
							$packItemsDetails[$package["pack_package_id"]]["packageId"] = $package["pack_package_id"];
							$packItemsDetails[$package["pack_package_id"]]["packageName"] = $package["package_name"]; 
							$packItemsDetails[$package["pack_package_id"]]["image"] = $package["image"];
							$packItemsDetails[$package["pack_package_id"]]["packageType"] = $package["package_type"];
							$packItemsDetails[$package["pack_package_id"]]["price"] = $package["rate"];
							$packItemsDetails[$package["pack_package_id"]]["status"] = $package["status"];
							$packItemsDetails[$package["pack_package_id"]]["packageDaysCount"] = $package["package_days_count"];
							$prevPackId = $package["pack_package_id"];
							$k = 0;								
						}
						if (!empty($package["item_id"])) {
							
							$vendorParams = array ( ":item_id" => $package["item_id"]);								
							$vendorWhereCondtn = " item_id = :item_id";
							$reqQryParams = array (
									"fetchType" => "singleRow",
									"selectField" => "",
									"tableName" => "tbl_category_items",
									"whereCondition" => $vendorWhereCondtn
								);
							$vendorDetails = $this->funExeSelectQuery($reqQryParams, $vendorParams);
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["packageItemId"] = $package["item_id"];
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemId"] = $vendorDetails["item_id"];
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemName"] = $vendorDetails["item_name"];
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemDescription"] = $vendorDetails["short_description"];
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["itemType"] = $vendorDetails["item_type"];
							$packItemsDetails[$package["pack_package_id"]]['itemList'][$k]["image"] = !empty($vendorDetails["image"]) ? KAZA_SYSTEM_PATH.$vendorDetails["image"] : "" ;
						} else {
							$packItemsDetails[$package["pack_package_id"]]['itemList'] = array();
						}
						$j++;
						$k++;
					}
				}
				$categoryItems = array_values($packItemsDetails);
				$i++;
			}
			if (!empty($categoryItems) && count($categoryItems > 0)) {
				$responseDate["status"] = 1;
				$responseDate["message"] = "success";
				$responseDate["package"] = $categoryItems;
			} else {
	          $responseDate["status"] = 0;
			  $responseDate["message"] = "No record(s) found";
			}
		} else {
	          $responseDate["status"] = 0;
			  $responseDate["message"] = "No record(s) found";
		}
		return $this->jsonResponse($responseDate);
	}
		
	

	// // Get list of categories for particular customer
	// public function getCategoryList($request) {

	// 	// Check $request variable is an array
	// 	if (is_array($request) && count($request) > 0) {
			
	// 		// Check required fields
	// 		$requiredFields = array("userId");
	// 		$errors = $this->funCheckRequiredFields($request, $requiredFields);
	// 		if (count($errors) > 0) {
	// 			return $this->jsonResponse($errors);
	// 		}
	// 		$currentDate = $this->currentDate;
	// 		$userId = isset($request["userId"]) ? $request["userId"] : "";
	// 		$responseDate = array();					
	// 		if (!empty($userId)) {

	// 			$reqestParams = array ( ":vendor_id" => $userId );
	// 		    $whereCondtn = $this->funParseQryParams($reqestParams);
	// 		    $reqQryParams = array (
	// 				"fetchType" => "singleRow",
	// 				"selectField" => "count(vendor_id) as countRows",
	// 				"tableName" => "tbl_category",
	// 				"whereCondition" => $whereCondtn,
	// 				"groupByCondition" => "vendor_id"
	// 			);			
	// 			$chekVendorExistRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
	// 			if (isset($chekVendorExistRes["countRows"]) && $chekVendorExistRes["countRows"] > 0) {					

	// 				// Check category exists
	// 			    $reqQryParams = array (
	// 					"fetchType" => "multipleRow",
	// 					"selectField" => "*",
	// 					"tableName" => "tbl_category",
	// 					"whereCondition" => $whereCondtn,
	// 				);				
	// 				$catListRes = $this->funExeSelectQuery($reqQryParams, $reqestParams);
	// 				$catListArr = array();
	// 				if (is_array($catListRes) && count($catListRes) > 0) {
	// 					$i = 0;
	// 					foreach ($catListRes as $category) {
	// 						$catListArr[$i]["categoryName"] = $category["category_name"];
	// 						$catListArr[$i]["description"] = $category["description"];
	// 						$catListArr[$i]["image"] = $category["image"];
	// 						$i++;
	// 					}	
	// 				}
	// 				$responseDate["status"] = 1;
	// 				$responseDate["invoiceOrderList"] = $catListArr;
	// 			} else {
	// 				$responseDate["status"] = 0;
	// 				$responseDate["message"] = "No category(s) found!";
	// 			}
	// 		}		
	// 		return $this->jsonResponse($responseDate);
	// 	}
	// }

	// // Fetch list of orders that customer ordered till now
	// public function createUpdateCategory($request) {

	// 	// Check $request variable is an array
	// 	if (is_array($request) && count($request) > 0) {

	// 		$currentDate = $this->currentDate;
	// 		// Check required fields
	// 		$requiredFields = array('vendorId', 'categoryName');
	// 		$errors = $this->funCheckRequiredFields($request, $requiredFields);
	// 		if (count($errors) > 0) {
	// 			return $this->jsonResponse($errors);
	// 		}
	// 		$qryParams = array ( 	
	// 						":category_id" => $request["categoryId"],
	// 						":category_order" => $request["categoryOrder"],
	// 						":vendor_id" => $request["vendorId"],
	// 						":category_name" => $request["categoryName"],
	// 						":description" => $request["description"],
	// 						":image" => $request["image"],
	// 						":created_date" => $currentDate,
	// 						":modified_date" => $currentDate
	// 					);
	// 		unset($qryParams[':created_date']);
	// 		$responseDate = array();
	// 		if (isset($request["categoryId"])) {	
	// 			$categoryId = $request["categoryId"];		
	// 			if (!empty($request["categoryId"])) {
					
	// 				// Check given category exists ?
	// 				$selQryParams = array ( ":category_id" => $request["categoryId"]);
	// 			    $whereCondtn = "category_id = :category_id";	
	// 			    $reqQryParams = array (
	// 					"fetchType" => "singleRow",
	// 					"selectField" => "count(category_id) as countRows",
	// 					"tableName" => "tbl_category",
	// 					"whereCondition" => $whereCondtn
	// 				);
	// 				$chekCatExistRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);	    
	// 				// $chekCatExistRes = $this->funExeCountRows("category_id", "tbl_category", $selQryParams, $whereCondtn);
	// 				if (isset($chekCatExistRes["countRows"]) && $chekCatExistRes["countRows"] > 0) {
	// 					// Update category
	// 				    $setWhereCondtn = $this->funParseQryParams($qryParams, "modified_date", ",");
	// 				    $setWhereCondtn .= " WHERE category_id = :category_id";
	// 					$updateQryResponse = $this->funExeUpdateRecord("tbl_category", $qryParams, $setWhereCondtn);
	// 					$responseDate["message"] = "Updated successfully";
	// 				} else {
	// 					$responseDate["status"] = 0;
	// 					$responseDate["message"] = "Category not exists!";
	// 				}

	// 			} else {			

	// 				// Create category
	// 				unset($qryParams[':category_id']);
	// 				$insQryResponse = $this->funExeInsertRecord("tbl_category", $qryParams);
	// 				$categoryId = $insQryResponse;
	// 				$responseDate["message"] = "Created successfully";
	// 			}
	// 			if (!empty($categoryId)) {
	// 				$selInsQryParams = array(":category_id" => $categoryId );
	// 				$selInsWhereCondtn = $this->funParseQryParams($selInsQryParams);
	// 			    // Select last inserted vendor/customer
	// 			    $reqQryParams = array (
	// 						"fetchType" => "singleRow",
	// 						"selectField" => "",
	// 						"tableName" => "tbl_category",
	// 						"whereCondition" => $selInsWhereCondtn
	// 					);
	// 				$selQryResponse = $this->funExeSelectQuery($reqQryParams, $selInsQryParams);

	// 				// $selQryResponse = $this->funExeSelectQuery("tbl_category", "singleRow", $selInsQryParams, $selInsWhereCondtn);
	// 				$responseDate["status"] = 1;					
	// 				$responseDate["categoryId"] = !empty($selQryResponse["category_id"]) ? $selQryResponse["category_id"] : "" ;
	// 				$responseDate["categoryOrder"] = !empty($selQryResponse["category_order"]) ? $selQryResponse["category_order"] : "" ;
	// 				$responseDate["vendorId"] = !empty($selQryResponse["vendor_id"]) ? $selQryResponse["vendor_id"] : "" ;
	// 				$responseDate["categoryName"] = !empty($selQryResponse["category_name"]) ? $selQryResponse["category_name"] : "" ;
	// 				$responseDate["description"] = !empty($selQryResponse["description"]) ? $selQryResponse["description"] : "" ;
	// 				$responseDate["image"] = !empty($selQryResponse["image"]) ? $selQryResponse["image"] : "" ;
	// 			} else {
	// 				$responseDate["status"] = 0;
	// 				$responseDate["message"] = "Something Issue in creating category!";
	// 				$responseDate["categoryId"] = "";
	// 			}
	// 		}	
	// 		return $this->jsonResponse($responseDate);
	// 	}
	// }	
}

function emailValidate($email) {
	if (filter_var($email, FILTER_VALIDATE_EMAIL))
		return true;
	else
		return false;
}