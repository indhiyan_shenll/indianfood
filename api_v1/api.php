<?php
    // include('../includes/configure.php');
    header('Content-Type: application/json');
	require_once('indianfoodapp.php');
	// $returnFoodAppObject = new indianfoodapp($dbconn);
	$returnFoodAppObject = new indianfoodapp();

	// Declaring API HTTP request type
	$apiRequestMethod = array(
		"login" => "POST",
		"register"=>"POST",
		"forgetpassword"=>"POST",
		"vendorlist"=>"GET",
		"myorder"=>"POST",
		"orderhistory"=>"POST",
		"updateorder"=>"POST",
		"createupdatecategory"=>"POST",
		"deletecategory"=>"POST",
		"deleteitem"=>"POST",
		"createupdateitem"=>"POST",
		"updateorderstatus"=>"POST",
		"customerdetails"=>"POST",
		"getreport"=>"POST",
		"getinvoice"=>"POST",
		"updateinvoice"=>"POST",
		"updaterating" =>"POST",
		"foodtype"=>"GET",
		"categorylist"=>"GET",
		"addpackageedit" =>"GET",
		"confirmorder"=>"GET",
		"updateprofile"=>"POST"
	);

	$errorReqTypeMsg = array("status" => "Please check request method!!");
	$requestMethod = $_SERVER['REQUEST_METHOD'];
    if ( isset($_GET["type"]) && trim($_GET["type"])!="") {
    	$gettype =   stripslashes($_GET["type"]);
        if (strtolower($gettype)=="login" && $requestMethod == $apiRequestMethod["login"] ) {
			$email    =  stripslashes($_REQUEST["email"]);
	        $password =  stripslashes($_REQUEST["password"]);
	        $type     =  stripslashes($_REQUEST["userType"]);
			echo $returnFoodAppObject->funLoginDetails($email,$password,$type); 
	        
        } elseif ( strtolower($gettype)=="register" && $requestMethod == $apiRequestMethod["register"] ) {    

	        $name       =  stripslashes($_REQUEST["fullName"]);
	        $mobile     =  stripslashes($_REQUEST["mobileNumber"]);
	        $address    =  stripslashes($_REQUEST["address"]);
	        $country    =  stripslashes($_REQUEST["country"]);
	        $zip        =  stripslashes($_REQUEST["zipCode"]);
	        $email      =  stripslashes($_REQUEST["email"]);
	        $password   =  stripslashes($_REQUEST["password"]);
	        $type       =  stripslashes($_REQUEST["userType"]);
            $deviceToken=  stripslashes($_REQUEST["deviceToken"]);
            $deviceInfo =  stripslashes($_REQUEST["userPlatform"]);
			echo $returnFoodAppObject->funRegisterDetails($name,$mobile,$address,$country,$zip,$email,$password,$type,$deviceToken,$deviceInfo); 
        } elseif ( strtolower($gettype)=="forgetpassword" && $requestMethod == $apiRequestMethod["forgetpassword"] ) {    
	        $email =  stripslashes($_REQUEST["email"]);
	        $type  =  stripslashes($_REQUEST["userType"]);
            echo $returnFoodAppObject->funForgetPasswordDetails($email,$type); 
        } elseif ( strtolower($gettype)=="vendorlist" && $requestMethod == $apiRequestMethod["vendorlist"] ) { 
	        // $gettype  =  stripslashes($_GET["type"]);
            echo $returnFoodAppObject->funVendorListDetails($gettype); 

        } elseif ( strtolower($gettype)=="myorder" && $requestMethod == $apiRequestMethod["myorder"] ) {    

	        $userid  =  stripslashes($_REQUEST["userId"]);
            echo $returnFoodAppObject->funMyOrderDetails($userid); 
        } elseif ( strtolower($gettype)=="orderhistory" && $requestMethod == $apiRequestMethod["orderhistory"] ) {    
	        $userid  =  stripslashes($_REQUEST["userId"]);
            echo $returnFoodAppObject->funOrderHistoryDetails($userid); 
        } elseif ( strtolower($gettype)=="updateorder" && $requestMethod == $apiRequestMethod["updateorder"] ) {    
	        $userid   =  stripslashes($_REQUEST["userId"]);
	        $orderid  =  stripslashes($_REQUEST["orderId"]);
	        $orderstatus =  stripslashes($_REQUEST["orderStatus"]);
	        echo $returnFoodAppObject->funUpdateOrderDetails($userid,$orderid,$orderstatus); 
        } elseif ( strtolower($gettype)=="createupdatecategory" && $requestMethod == $apiRequestMethod["createupdatecategory"] ) { 
            $userid        =  stripslashes($_REQUEST["userId"]); 
            $categoryid    =  stripslashes($_REQUEST["categoryId"]); 
        	$categoryorder =  stripslashes($_REQUEST["categoryOrder"]);
	        $vendorid      =  stripslashes($_REQUEST["vendorId"]);
	        $categoryname  =  stripslashes($_REQUEST["categoryName"]);
	        $description   =  stripslashes($_REQUEST["description"]);
	        $image         =  stripslashes($_REQUEST["image"]);
	        echo $returnFoodAppObject->funCreateUpdateCategoryDetails($userid,$categoryid,$categoryorder,$vendorid,$categoryname,$description,$image); 
        } elseif ( strtolower($gettype)=="deletecategory" && $requestMethod == $apiRequestMethod["deletecategory"] ) {
        	$userid        =  stripslashes($_REQUEST["userId"]); 
            $categoryid    =  stripslashes($_REQUEST["categoryId"]); 
             echo $returnFoodAppObject->funDeleteCategoryDetails($userid,$categoryid); 
        } elseif ( strtolower($gettype)=="createupdateitem" && $requestMethod == $apiRequestMethod["createupdateitem"] ) { 
            $userid        =  stripslashes($_REQUEST["userId"]); 
            $categoryid    =  stripslashes($_REQUEST["categoryId"]);
            $itemid        =  stripslashes($_REQUEST["itemId"]); 
        	$itemorder     =  stripslashes($_REQUEST["itemOrder"]);
	        $itemname      =  stripslashes($_REQUEST["itemName"]);
	        $description   =  stripslashes($_REQUEST["itemDesc"]);
	        $price         =  stripslashes($_REQUEST["itemPrice"]);
	        $itemtype      =  stripslashes($_REQUEST["itemType"]);
	        $image         =  stripslashes($_REQUEST["image"]);
	        echo $returnFoodAppObject->funCreateUpdateItemDetails($userid,$categoryid,$itemid,$itemname,$description,$price,$itemtype,$image); 
        } elseif ( strtolower($gettype)=="deleteitem" && $requestMethod == $apiRequestMethod["deleteitem"] ) {
        	$userid    =  stripslashes($_REQUEST["userId"]); 
            $itemId    =  stripslashes($_REQUEST["itemId"]); 
            echo $returnFoodAppObject->funDeleteItemDetails($userid,$itemId); 
        } elseif ( strtolower($gettype)=="updateorderstatus" && $requestMethod == $apiRequestMethod["updateorderstatus"] ) {
        	$userid      =  stripslashes($_REQUEST["userId"]); 
            $orderId     =  stripslashes($_REQUEST["orderId"]); 
            $orderStatus =  stripslashes($_REQUEST["orderStatus"]); 
            $reason      =  stripslashes($_REQUEST["reason"]); 
            echo $returnFoodAppObject->funUpdateOrderStatusDetails($userid,$orderId,$orderStatus,$reason); 
        } elseif ( strtolower($gettype)=="customerdetails" && $requestMethod == $apiRequestMethod["customerdetails"] ) {
        	$userid      =  stripslashes($_REQUEST["userId"]); 
            echo $returnFoodAppObject->funCustomerDetails($userid); 
        } elseif (strtolower($gettype)=="getreport" && $requestMethod == $apiRequestMethod["getreport"] ) {
        	$userid  =  stripslashes($_REQUEST["userId"]); 
        	$startdate   =  stripslashes($_REQUEST["startDate"]); 
        	$enddate     =  stripslashes($_REQUEST["endDate"]); 
			echo $returnFoodAppObject->funGetReport($userid,$startdate,$enddate); 
        } elseif (strtolower($gettype)=="getinvoice" && $requestMethod == $apiRequestMethod["getinvoice"] ) {
        	$userid  =  stripslashes($_REQUEST["userId"]); 
			echo $returnFoodAppObject->funGetInvoice($userid); 
        } elseif (strtolower($gettype)=="updateinvoice" && $requestMethod == $apiRequestMethod["updateinvoice"] ) {
        	$userid  =  stripslashes($_REQUEST["userId"]);
        	$invoiceId  =  stripslashes($_REQUEST["invoiceId"]); 
        	$invoiceStatus  =  stripslashes($_REQUEST["invoiceStatus"]); 
			echo $returnFoodAppObject->funUpdateInvoice($userid,$invoiceId,$invoiceStatus); 
        } elseif (strtolower($gettype)=="updaterating" && $requestMethod == $apiRequestMethod["updaterating"] ) {
        	$userid   =  stripslashes($_REQUEST["userId"]);
        	$orderId  =  stripslashes($_REQUEST["orderId"]); 
        	$vendorId =  stripslashes($_REQUEST["vendorId"]); 
        	$rate     =  stripslashes($_REQUEST["rate"]);
			echo $returnFoodAppObject->funUpdateRating($userid,$orderId,$vendorId,$rate); 
        } elseif (strtolower($gettype)=="foodtype" && $requestMethod == $apiRequestMethod["foodtype"] ) {
        	// $gettype  =  stripslashes($_GET["type"]);
            echo $returnFoodAppObject->funFoodTypeDetails($gettype); 
        } elseif (strtolower($gettype)=="categorylist" && $requestMethod == $apiRequestMethod["categorylist"] ) {
        	// $gettype  =  stripslashes($_GET["type"]);
            echo $returnFoodAppObject->funCategoryListDetails($gettype); 
        } elseif (strtolower($gettype)=="confirmorder" && $requestMethod == $apiRequestMethod["confirmorder"] ) {
        	// $gettype  =  stripslashes($_GET["type"]);
        	$confirmJson  =  stripslashes($_REQUEST["confirmJson"]);
            echo $returnFoodAppObject->funConfirmOrder($gettype,$confirmJson); 
        } elseif (strtolower($gettype)=="addpackageedit" && $requestMethod == $apiRequestMethod["addpackageedit"] ) {
        	// $gettype  =  stripslashes($_GET["type"]);
        	$packageJson  =  stripslashes($_REQUEST["packageJson"]);
            echo $returnFoodAppObject->funAddPackageEdit($gettype,$packageJson); 
        } elseif ( strtolower($gettype)=="updateprofile" && $requestMethod == $apiRequestMethod["updateprofile"] ) { 
            $userid     =  stripslashes($_REQUEST["userId"]);   
	        $name       =  stripslashes($_REQUEST["name"]);
	        $mobile     =  stripslashes($_REQUEST["mobile"]);
	        $address    =  stripslashes($_REQUEST["address"]);
	        $country    =  stripslashes($_REQUEST["country"]);
	        $zip        =  stripslashes($_REQUEST["zip"]);
	        $email      =  stripslashes($_REQUEST["email"]);
	        $password   =  stripslashes($_REQUEST["password"]);
	        $type       =  stripslashes($_REQUEST["type"]);
	        $documentupload=  stripslashes($_REQUEST["documentUpload"]);
            $deviceToken=  stripslashes($_REQUEST["deviceToken"]);
            $deviceInfo =  stripslashes($_REQUEST["deviceInfo"]);
			echo $returnFoodAppObject->funUpdateProfileDetails($userid,$name,$mobile,$address,$country,$zip,$email,$password,$type,$documentupload,$deviceToken,$deviceInfo); 
        }
        else {
			echo json_encode($errorReqTypeMsg);
		}
	}

?>