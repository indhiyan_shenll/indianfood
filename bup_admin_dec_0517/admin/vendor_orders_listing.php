<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
include("header.php");
/****Paging ***/
$Page = 1;$RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
$TotalPages = 0;
/*End of paging*/
$foodAppApi = new Common($dbconn);
$emptycondn = array();
$food_type = "";
$pstflg = false;
if (isset($_GET['vendor'])) 
    $vendor = trim(base64_decode($_GET['vendor']));
if(isset($_POST['vendors']) || isset($_POST['food_type'])) {
    $vendor     = $_POST['vendors'];
    $food_type  = $_POST['food_type'];
    $pstflg     = true;
}
?>
<link href="../assets/global/css/jquery.rateyo.css" rel="stylesheet" type="text/css" />
<form name="vendorderslisting_form" id="vendorderslisting_form" method="post" action="">
<div class="page-content" id="vendor-order-list-page-content">
    <div class="row food-orders">
        <div class="col-md-12">
           <div class="portlet light ">
                <div class="portlet-title vendor-order-list-portlet-title" >
                    <div class="caption font-dark vendor-order-list-caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">order list</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body search-body vendor-order-list-portlet-body">
                    <div class="row">                        
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-6 col-sm-6 col-xs-12 remove-left-right-padding">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Aunties:</label>
                                    <select name="vendors" id="vendors" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                            $Qry1="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by full_name asc";
                                            $qryParams1[":user_type"]   =   "vendor";
                                            $qryParams1[":status"]      =   "active";
                                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
                                            foreach ($getvendors as $getVendorsData) {
                                                $encrypted_vendor = base64_encode($getVendorsData["user_id"]);
                                                echo "<option value=".$getVendorsData["user_id"]." data-vendorid=".$encrypted_vendor.">".$getVendorsData["full_name"]."</option>";
                                            }
                                        ?>
                                    </select>
                                    <!-- <script>document.getElementById("vendors").value="<?php echo $vendor;?>";</script> -->
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Food Type:</label>
                                    <select name="food_type" id="food_type" class="form-control">
                                        <option value="" >Select</option>
                                        <option value="veg">Veg</option>
                                        <option value="nonveg">Non-veg</option>
                                    </select>
                                    <!-- <script>document.getElementById("food_type").value="<?php echo $food_type;?>";</script>                                 -->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <a type="button" class="btn yellow custombtn" id="searchbtn"><i class="fa fa-search"></i> Search</a>
                                    <a type="button" class="btn red custombtn" id="resetbtn"><i class="fa fa-times-circle"></i> Reset</a>
                                    <a class="btn dark custombtn" href="vendors_listing.php"><i class="fa fa-arrow-left"></i> Back</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <!-- AJAX response will be appending here -->
                <div id="vendorListTable">  
                    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                </div>                
            </div>
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>