<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$InsertInvoiceId="";
$ArrInvoiceId="";
$ArrInvoiceId=array();
if(isset($_REQUEST["ArrInvoiceJson"])) {
	$ArrInvoiceJson=$_REQUEST["ArrInvoiceJson"];
	$InvoiceJson=json_decode($ArrInvoiceJson);
    $ArrInvoiceId=explode(",",$InvoiceJson);
    $InsertInvoiceId=$_REQUEST["invoiceid"];
    $created_date = date("Y-m-d H:i:s");

    $invoiceHTMLStart='<style>
	    #order_tbl>tr>td {font-size:5px;}
	    #order_tbl>tr>td, #order_tbl>th>td {border:0.4px solid #ccc;}
    </style>
    <table width="100%" bgcolor="#F7F7F7" id="order_tbl" cellpadding="6">
	    <thead>
	        <tr>
	            <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">S.No</th>
                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Order #</th>
                <th width="20%" class="tableborderhead" color="#006679"  align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Package ?</th>
                <th width="20%" class="tableborderhead" color="#006679" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;"></th>
                <th width="20%" class="tableborderhead" color="#006679" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Amount</th>
	        </tr>
	    </thead>
    	<tbody>';
	    $qryParams=array();
		if (count($ArrInvoiceId)>0) {
        	$DiscountQry="SELECT * FROM tbl_settings WHERE setting_name='discount'";
			$getResDiscount = $foodAppApi->funBckendExeSelectQuery($DiscountQry);
        	$discountVal = $getResDiscount[0]['setting_value'];
        	$discountType = $getResDiscount[0]['setting_type'];
        	$subTot=0;$invoice_amount=0;$discount_amount=0;$sno=1;$invoiceHTML = "";
        	foreach ($ArrInvoiceId as $ArrInvoicekey => $ArrInvoiceValue) {

				$InsertPreviewQry="INSERT INTO tbl_invoice_orders (invoice_id, order_id, invoice_date) VALUES (:invoice_id, :order_id, :invoice_date);";
				$InsertPreviewParams['invoice_id'] = $InsertInvoiceId;
				$InsertPreviewParams['order_id'] = $ArrInvoiceValue;
				$InsertPreviewParams['invoice_date'] = $created_date;
				$getPreviewId = $foodAppApi->funBckendExeInsertRecord($InsertPreviewQry,$InsertPreviewParams);

        		$Qry="SELECT order_id,package_id,price from tbl_orders where order_id = :order_id";
        		$qryParams['order_id'] = $ArrInvoiceValue;
				$getResInvoice = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
					
				$proCount = count($getResInvoice);
				$subTotAmt=0;$discountText="";$discount=0;
				foreach ($getResInvoice as $key => $OrderDetails) {
					$invoiceHTML .= "<tr>"; 
					$OrderDetails["package_id"] = empty($OrderDetails["package_id"])?"No":"Yes";

					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$sno.'</td>';
					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$OrderDetails["order_id"].'</td>';
					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$OrderDetails["package_id"].'</td>';
			   		$invoiceHTML .= '<td width="20%" style="font-size:13px;"></td>';
			   		$invoiceHTML .= '<td width="20%" style="font-size:13px;">$ '.$OrderDetails["price"].'</td>';
			   		$invoiceHTML .= '</tr>';
			   		$subTotAmt += $OrderDetails["price"];
			   		// $proCount++;
				}
				$subTot += $subTotAmt;
				$sno ++;
        	}

        	$updateqry = "UPDATE tbl_invoices SET vendor_invoice_status = :vendor_invoice_status, admin_invoice_status = :admin_invoice_status, develop_status = :develop_status, created_date = :created_date, modified_date = :modified_date  WHERE invoice_id = :invoice_id";

			$qryParamsUpdate['invoice_id'] = $InsertInvoiceId;
			$qryParamsUpdate['vendor_invoice_status'] = "Pending";
			$qryParamsUpdate['admin_invoice_status'] = "Pending";
			$qryParamsUpdate['develop_status'] = "";
			$qryParamsUpdate['created_date'] = $created_date;
			$qryParamsUpdate['modified_date'] = $created_date;
			$getvendors = $foodAppApi->funBckendExeUpdateRecord($updateqry,$qryParamsUpdate);

        	if ($discountType=='percent') {
				$discount = ($subTot * $discountVal)/100;
				$discountText = $discountVal."%";
			} else {
				$discount = $discountVal;
				$discountText = "$ ".number_format($discountVal);
			}
			$totalAmount = $subTot - $discount;

			$invoiceHTML .= '<tr>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%" style="font-size:13px;">SubTotal</td>
			<td width="20%" style="font-size:13px;">$ '.number_format($subTot,2).'</td>
			</tr>';
        	$invoiceHTML .= '<tr>
        	<td width="20%"></td>
        	<td width="20%"></td>
        	<td width="20%"></td>
        	<td width="20%" style="font-size:13px;">Discount ('.$discountText.')</td>
        	<td width="20%" style="font-size:13px;">$ '.number_format($discount,2).'</td>
        	</tr>';
			$invoiceHTML .= '<tr>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%" style="font-size:13px;">Total</td>
			<td width="20%" style="font-size:13px;">$ '.number_format($totalAmount,2).'</td>
			</tr>';
		} else {
            $invoiceHTML .= '<tr><td colspan="5" style="text-align:center;">No order(s) found </td></tr>';
        }
   		$invoiceHTMLEnd='</tbody>
   	</table>';
   	// echo $invoiceHTML;
 	$htmlpdf=$invoiceHTMLStart.$invoiceHTML.$invoiceHTMLEnd;
	include("create_invoice_pdf.php");
}
?>