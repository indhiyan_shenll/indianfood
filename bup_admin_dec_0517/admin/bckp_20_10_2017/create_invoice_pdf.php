<?php
// error_reporting(E_ALL);
/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Removing Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
/**
* Set the height of the cell (line height) respect the font height.
     * @param $h (int) cell proportion respect font height (typical value = 1.25).
     * @public
     * @since 3.0.014 (2008-06-04)
*/

// Include the main TCPDF library (search for installation path).
include_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
// $pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Invoice');
// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set cell height
$pdf->setCellHeightRatio(1.5);
// set font
$pdf->SetFont('Helvetica');
// add a page
$pdf->AddPage();
//variable
$InvDate=date("d/m/Y");

$html='<table>
			<table>
				<tr>
					<td width="60%"><img src="../assets/layouts/layout2/img/logo.png" height="70" width="100"/></td>
			        <td width="40%" style="font-size:25.3px;" align="right"></td>
				</tr>
				<tr style="line-height: 60%;">
					<td width="5%"></td>
					<td width="70%"></td>
			        <td width="25%" style="font-size:13.7px;" align="right">Date: '.$InvDate.' </td>
				</tr>
			</table>
	<br/><br/>
	'.$htmlpdf.'
</table>
<table width="100%" bgcolor="#F9F9F9">
	<tr><td></td></tr>
    <tr style="line-height: 70%;">
   		<td width="5%"></td>
   		<td width="45%"><hr style="background-color:red;" width="90%"/></td>
   		<td width="50%"><hr bgcolor="#DEDEDE" width="90%"/></td>
   	</tr>
	<tr>
		<td width="5%"></td>
		<td width="90%" color="#666666" style="font-size:25px;text-align:center;">Kaza Food</td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="90%" color="#666666" style="text-align:center;">Level 123, ABC Road</td>
	</tr>  	
	<tr>
		<td width="5%"></td>
		<td width="90%" color="#666666" style="text-align:center;">South Melbourne  Victoria  3205</td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="90%" color="#666666" style="text-align:center;">Ph:1200 120 120</td>
	
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="90%" color="#666666" style="text-align:center;">mail@kazafood.org&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; www.kazafood.com</td>
	</tr>
	<tr><td></td></tr>  
</table>';
// reset pointer to the last page
$pdf->lastPage();
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
// reset pointer to the last page
$pdf->lastPage();
// ob_clean();
//Close and output PDF document
// $InvoiceFileName =$_SERVER['DOCUMENT_ROOT'].'kazafood/invoices/invoice_pdf/Invoice_'.date("ymd")."_".time().'.pdf';
// $InvoiceFileName =$_SERVER['DOCUMENT_ROOT'].'kazafood/invoices/invoice_pdf/test.pdf';
// $InvoiceFileName =$_SERVER['DOCUMENT_ROOT'].'kazafood/invoices/invoice_pdf/Invoice_'.date("ymd")."_".time().'.pdf';
// $pdf->Output($_SERVER['DOCUMENT_ROOT'].'kazafood/test/Invoice_'.date("ymd")."_".time().'.pdf', 'F', 'F');
// $InvoiceFileName ='Invoice_'.date("ymd")."_".time().'.pdf';
$InvoiceFileName = KAZA_ROOT_PATH.'invoices/invoice_pdf/Invoice_'.$InsertInvoiceId.'.pdf';
$pdf->Output($InvoiceFileName, 'F');
// $pdf->Output('name.pdf', 'I');