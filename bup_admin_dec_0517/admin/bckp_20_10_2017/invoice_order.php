<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');

$display="display:none;";
if(isset($_POST["vendors"]) && !empty($_POST["vendors"])) {
    $vendor=(empty($_POST["vendors"]))?"":$_POST["vendors"];
    $display="display:block;";
} else {
    $vendor="";
}


/****Paging ***/
$Page=1;$RecordsPerPage=25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;

/*End of paging*/
include("header.php");
$foodAppApi = new Common($dbconn);
?>
<style>
    /*#tbl_detail_order_item tr td:nth-child(3){
        text-align: right;
    }*/
    #previewchk, #confirmBtn {
        color: #FFFFFF;
    }
    #emailNotificationMsg{
        margin: auto;
        float: none;
        display: none;   
        margin-bottom: -17px;     
    }
    #emailNotificationMsg .customsuccessalert{
        margin-top: 18px !important;
        margin-bottom: 0px !important;
        color: #3c763d !important;
        background-color: #dff0d8 !important;
        border-color: #d6e9c6 !important;
        border: 1px solid transparent;
        border-radius: 4px !important;
    }
</style>
<form name="invoiceorder_form" id="invoiceorder_form" method="post" action="">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light customlistminheight">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-docs font-dark"></i>
                        <span class="caption-subject bold uppercase">Generate Invoice</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-12 col-sm-12 col-xs-12 remove-left-right-padding">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Aunties:</label>
                                    <select name="vendors" id="vendors" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                            $Qry1="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
                                            $qryParams1[":user_type"]="vendor";
                                            $qryParams1[":status"]="Active";
                                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
                                            if (count($getvendors,COUNT_RECURSIVE)>1) {
                                                foreach ($getvendors as $getVendorsData) {
                                                    if($getVendorsData["user_id"]==$vendor)
                                                        $selected="selected";
                                                    else
                                                        $selected="";
                                                    echo "<option value=".$getVendorsData["user_id"]." ".$selected.">".$getVendorsData["full_name"]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    <div class="error errormsg" style="display:none;">Please select aunty</div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                    <button type="button" class="btn yellow custombtn btn-md" id="Search"><i class="fa fa-search"></i> Search</button> 
                                    <button type="button" class="btn red custombtn reset" id="invoiceReset"><i class="fa fa-times-circle"></i> Reset</button> 
                                    <button type="button" name="preview" id="previewchk" class="btn invoicecustombtn customcreate" style="display:none;">Generate Invoice</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <div id="showtable" style="<?php echo $display ?>">
                    <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                    <input type="hidden" name="HdnRecordsPerPage" id="HdnRecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="modal fade bs-modal-lg" id="InvoiceOrderModel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <input type="hidden" name="HndChckvalue" id="HndChckvalue">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                   <h4 class="modal-title preview">Invoice</h4>
                </div>
                <div class="col-md-11 col-sm-12 col-xs-12 remove-left-right-padding" id="emailNotificationMsg">
                    <div class="alert alert-success fade in alert-dismissable customsuccessalert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Successfully sent email with Invoice</strong> 
                    </div>                
                    
                </div>
                <div class="modal-body" id="popuptable">
            
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn red custombtn reset" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</button>
                    <button type="button" class="btn custombtn customconfirm" id="confirmBtn"><i class="fa fa-check"></i> Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
</form>

<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/invoice_order.js" type="text/javascript"></script>
<style>
.dataTables_extended_wrapper .table.dataTable{
    margin:0px 0!important;;
}
</style>
<script>
$(document).ready(function() {
  $(".errormsg").css("display","none");
  $("#previewchk").hide();
  loading_sorting();
});
function loading_sorting(){
$('#tbl_invoice_list').DataTable( {
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength": false ,
       'aoColumnDefs': [
            {'bSortable': false,}
        ],
         "order": [[ 0, "asc" ]]  
    } );    
}
</script>
<script>
$(document).on("change","#vendors",function() {
    var vendorsId =$("#vendors").val();
    if(vendorsId=="") {
        $(".errormsg").css("display","block");
        return false;
    } else {
        $(".errormsg").css("display","none");
         return true;
    }
});

function checkAllExp(Element) {
    //alert(Element);
    chk_len=$('input[id=checkbox]').length;
    if(Element.checked){
        if(chk_len>0){
            $('input[id=checkbox]').prop('checked', true);
            $("#previewchk").show();
        }
    }
    else{
        if(chk_len>0){
            $('input[id=checkbox]').prop('checked', false);
            $("#previewchk").hide();
        }
    }
}
 
function eachChk() {
    var atLeastOneChecked = false;
    $('.checkbox').each(function () {
        if ($(this).is(":checked")) {
            atLeastOneChecked = true;
        }
    });
    if (atLeastOneChecked) {
       $("#previewchk").show();
        
    } else {
         $("#previewchk").hide();
        
    }
}  

$(document).on("click","#previewchk",function() {

    var allVals = [];
    $('.checkbox:checked').each(function () {
        allVals.push($(this).val());
    });
    var vendorId  =$("#vendors").val();
    $("#HndChckvalue").val(allVals);
    var myInvoiceJSON = JSON.stringify(allVals);
    ajax_invoice_order(myInvoiceJSON,vendorId);
    // setTimeout(function() {
    //     var preview_val = $('#preview_id').val();
    //     $('.preview').text('Invoice #'+preview_val); 
    // }, 500);
});  

function ajax_invoice_order(myInvoiceJSON,vendorId) {
    if (myInvoiceJSON!="") {
        $.ajax({
            url:"preview_invoice_order.php",
            method:'GET',
            data:"ArrInvoiceJson="+myInvoiceJSON+"&vendorid="+vendorId,
            success:function(data) {    
               document.getElementById('popuptable').innerHTML = data; 
               $('.table-header').remove();
               $('#confirmBtn').show();
               var preview_val = $('#preview_id').val();
                $('.preview').text('Invoice #'+preview_val);   
                     
                $('#emailNotificationMsg').hide();        
                $('#InvoiceOrderModel').modal('show');  
            }
        }); 
    } else {
        alert("Please select atleast one invoice");
    }
}

$(document).on("click","#confirmBtn",function() {
    var checkallVals=$("#HndChckvalue").val();
    var Invoice =$(".preview").text();
    var Invoice_id = Invoice.substr(Invoice.indexOf("#") + 1)
    // console.log(Invoice_id);
    var myInvoiceJSON = JSON.stringify(checkallVals);
    if (myInvoiceJSON!="") {
        $.ajax({
            url:"invoice_order_email.php",
            method:'GET',
            data:"ArrInvoiceJson="+myInvoiceJSON+"&invoiceid="+Invoice_id,
            success:function(data) {  
               //document.getElementById('popuptable').innerHTML = data; 
               $('.table-header').remove();
               $('#InvoiceOrderModel').modal('show');
               $("#emailNotificationMsg").show();
                // setTimeout(function() {
                //     $('#InvoiceOrderModel').modal('hide');
                // }, 2000);
               
                var kazaFood = new FoodApp();               
                var invoiceDetArr = {};
                if ($("#vendors").val() != "") {
                    invoiceDetArr["vendorsid"] = $("#vendors").val();
                    invoiceDetArr["HdnPage"] = $("#HdnPage").val();
                    invoiceDetArr["HdnMode"] = $("#HdnMode").val();
                    invoiceDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
                    // console.log(invoiceDetArr);
                    var invoiceDetJsonString = JSON.stringify(invoiceDetArr);
                    kazaFood.invoiceOrderList(invoiceDetJsonString);
                }
                $('#previewchk').hide();
            }
        });            
    } 
});

$(document).on("click","#viewOrderItemPopup",function() {
    var order_type = $(this).attr("data-order-type");
    var order_id = $(this).attr("data-order-id");
    viewOrderItemPopup(order_type, order_id);
});

function viewOrderItemPopup(order_type, order_id){
    $.ajax({
        url:"ajax_view_order_items.php",
        method:'POST',
        // data:"order_type="+order_type, "order_id="+order_id
        data:{order_type:order_type,order_id:order_id},
        success:function(data) {    
           document.getElementById('popuptable').innerHTML = data; 
           $('.preview').text('View Order Items');
           $("#emailNotificationMsg").hide();
           $('#confirmBtn').hide();
           $('#popuptable').find('.modal-footer').empty().append('<button type="button" class="btn red custombtn reset" ><i class="fa fa-times-circle"></i> Cancel</button>');
           $('#InvoiceOrderModel').modal('show');
            if (order_type == "package")
                $(".orderPackname").text($("#packageName").val());
            else
                $(".orderCatename").text($("#categoryName").val());
        }
    }); 
}
</script>