<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$Page = 1;$RecordsPerPage = 25;
$TotalPages = 0;
$foodAppApi = new Common($dbconn);
if (isset($_POST["invoiceorderSearchCriteria"])) {
    $invoiceSearch = json_decode($_POST["invoiceorderSearchCriteria"], true);    
    $vendorsid   = !empty($invoiceSearch["vendorsid"]) ? $invoiceSearch["vendorsid"] : "" ;
    if (isset($invoiceSearch['HdnPage']) && is_numeric($invoiceSearch['HdnPage']))
        $Page = $invoiceSearch['HdnPage'];
}  
?>
<?php
if (!empty($vendorsid)) {
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnRecordsPerPage" id="HdnRecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<div class="portlet-body flip-scroll">
    <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_order_invoice_list">
        <thead class="flip-content">
            <tr>
            	<th width="5%" style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="check_all" onclick="checkAllExp(this)"><span></span></label></th>
                <th nowrap>Order Id</th>
                <th>Customer Name</th>
                <th>Vendor Name</th>
                <th>Amount</th>
                <th>Invoice Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            	if (!empty($vendorsid)) {
                    $Qry="SELECT * FROM tbl_orders as ord left JOIN tbl_users as user ON user.user_id=ord.customer_id where vendor_id=:vendorsid and payment_status='Completed'";
                    $qryParams[":vendorsid"]=$vendorsid;
                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                        $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                        $Start=($Page-1)*$RecordsPerPage;
                        $sno=$Start+1;
                        $Qry.=" limit $Start,$RecordsPerPage";
                   		$getInvoices = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);

    			   	    if (count($getInvoices)>0) {
                            $pending_options="";
                            $generated_options="";
    			   		    foreach ($getInvoices as $invoicesListData) {
                                $OrderQry="SELECT * FROM tbl_orders as ord JOIN tbl_invoice_orders as inv_ord on ord.order_id=inv_ord.order_id where ord.order_id=:order_id";
                                $OrderqryParams[":order_id"]=$invoicesListData["order_id"];
                                $OrdergetResCnt = $foodAppApi->funBckendExeSelectQuery($OrderQry,$OrderqryParams);
                                if ($OrdergetResCnt>0) {
                                    foreach ($OrdergetResCnt as $orderListData) {
                                        $rowdisable=($orderListData["order_id"]==$invoicesListData["order_id"])?"disabled":"";
                                        $checkbox_id=($orderListData["order_id"]==$invoicesListData["order_id"])?"checkbox1":"checkbox";
                                        $generate = "Invoice Generated";
                                        $genColor = "green";
                                    }
                                } else {
                                    $rowdisable="";
                                    $checkbox_id="checkbox";
                                    $generate = "Pending";
                                    $genColor = "red";
                                }
                                $VendorQry="SELECT full_name FROM tbl_users where user_id=:vendor_id";
                                $VendorqryParams[":vendor_id"]=$invoicesListData["vendor_id"];
                                $VendorgetResCnt = $foodAppApi->funBckendExeSelectQuery($VendorQry,$VendorqryParams);
                                if ($VendorgetResCnt>0) {
                                    foreach ($VendorgetResCnt as $vendorListData) {
                                        $vendor_name = $vendorListData['full_name'];
                                    }
                                }
                                
                                if ($generate != "Pending") {
                                    $generated_options .= '<tr>
                                        <td style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="'.$checkbox_id.'" name="checkbox[]" id="'.$checkbox_id.'" value="'.$invoicesListData["order_id"].'" onclick="eachChk()" '.$rowdisable.'>
                                        <span></span></label></td>
                                        <td>'.$invoicesListData["order_id"].'</td>
                                        <td>'.$invoicesListData["full_name"].'</td>
                                        <td>'.$vendor_name.'</td>
                                        <td>'.$invoicesListData["price"].'</td>
                                        <td style="color:'.$genColor.'">'.$generate.'</td>
                                        <td><div class="actions"><div class="btn-group"><a class="btn dark btn-sm" href="order_details.php.php?id='.base64_encode($invoicesListData["order_id"]).'">View <i class="fa fa-angle-right"></i></a></div></div></td>
                                    </tr>';
                                } else {
                                    $pending_options .= '<tr>
                                        <td style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="'.$checkbox_id.'" name="checkbox[]" id="'.$checkbox_id.'" value="'.$invoicesListData["order_id"].'" onclick="eachChk()" '.$rowdisable.'>
                                        <span></span></label></td>
                                        <td>'.$invoicesListData["order_id"].'</td>
                                        <td>'.$invoicesListData["full_name"].'</td>
                                        <td>'.$vendor_name.'</td>
                                        <td>'.$invoicesListData["price"].'</td>
                                        <td style="color:'.$genColor.'">'.$generate.'</td>
                                        <td><div class="actions"><div class="btn-group"><a class="btn dark btn-sm" href="order_details.php.php?id='.base64_encode($invoicesListData["order_id"]).'">View <i class="fa fa-angle-right"></i></a></div></div></td>
                                    </tr>';
                                }
                                $sno++;		
                            }
                            echo $pending_options;
                            echo $generated_options;
        				} 
                    } 
                } 
            ?>
        </tbody>
    </table>
</div>
<div>
    <?php
        if ($TotalPages > 1) {
            echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
            $FormName = "invoiceorder_form";
            require_once ("paging.php");
            echo "</td></tr>";
        }
    ?>
</div>
<?php
} else {
 echo '<div class="portlet-body flip-scroll">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:40px;margin-bottom:40px">
               <p style="text-align:center;font-size:20px;color:green;">Please select vendor to generate invoice.</p>
            </div>
          </div>
       </div>';
}
?>
<script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>