<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');

$foodAppApi = new Common($dbconn);
$emptycondn = array();
// Getting Daily Order count
$dailyQry = "SELECT count(order_id) as today_order_count FROM `tbl_orders` where date_format(now(),'%Y-%m-%d') between  date_format(start_date,'%Y-%m-%d') and date_format(end_date,'%Y-%m-%d')";
$getOrderCount = $foodAppApi->funBckendExeSelectQuery($dailyQry,$emptycondn,'fetch');
$dailycount = getcount($getOrderCount["today_order_count"]);

// Getting Weekly Order count
$weekQry = "SELECT count(order_id) as weekly_order_count FROM `tbl_orders` where WEEK(date_format(start_date,'%Y-%m-%d')) = WEEK(date_format(now(),'%Y-%m-%d')) and  WEEK(date_format(end_date,'%Y-%m-%d')) = WEEK(date_format(now(),'%Y-%m-%d'))";
$getOrderCount = $foodAppApi->funBckendExeSelectQuery($weekQry,$emptycondn,'fetch');
$weekcount = getcount($getOrderCount["weekly_order_count"]);

// Getting Monthly Order count
$monthQry = "SELECT count(order_id) as monthly_order_count from `tbl_orders` where MONTH(date_format(start_date,'%Y-%m-%d')) = MONTH(date_format(now(),'%Y-%m-%d')) and MONTH(date_format(end_date,'%Y-%m-%d')) = MONTH(date_format(now(),'%Y-%m-%d')) and date_format(start_date,'%Y') = date_format(now(),'%Y')";
$getOrderCount = $foodAppApi->funBckendExeSelectQuery($monthQry,$emptycondn,'fetch');
$monthcount = getcount($getOrderCount["monthly_order_count"]);

// Getting Vendor and Customer count
$vendrcntQry = "SELECT count(vendor_id) as vendor_count,sum(cus_count) as customer_count from (SELECT count(customer_id) as cus_count,vendor_id FROM `tbl_orders` group by vendor_id) as p";
$getVendorCount = $foodAppApi->funBckendExeSelectQuery($vendrcntQry,$emptycondn,'fetch');
$vendor_count = getcount($getVendorCount["vendor_count"]);
$customer_count = getcount($getVendorCount["customer_count"]);
$AlertMessage = '';
$AlertClass = '';
$AlertFlag = false;
if (isset($_GET['msg'])) { 

    if ($_GET['msg'] == 1) {
        $AlertMessage = "You have successfully logged in";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
    }
}
function getcount($count) {
    return ($count>9)?$count:'0'.$count;
}
include("header.php");
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <form name="dashboard_form" id="dashboard_form" action="reports.php" method="post">
        <input type="hidden" name="dashboard_widget_type" id="dashboard_widget_type">
        <?php if ($AlertFlag == true) { ?>
            <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
                <button type="button" class="close" data-dismiss="alert"></button>
                <p> <?php echo $AlertMessage; ?> </p>
            </div>
        <?php } ?>
        <!-- BEGIN PAGE HEADER-->   
        <div class="row widget-row">
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <span class="dash_widget" data-class="daily">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Daily</h4>
                        <div class="widget-thumb-wrap">
                            <!-- <i class="widget-thumb-icon bg-green icon-bulb"></i> -->
                            <!-- <img src="images/test.png" class="img-responsive"> -->
                            <div class="widget-thumb-body">
                                <!-- <span class="widget-thumb-subtitle">USD</span> -->
                                <!-- <span class="widget-thumb-body-stat" data-counter="counterup" data-value="7,644">0</span> -->
                                <table>
                                    <tr>
                                        <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="daily-orders"></td>
                                        <td>&nbsp;</td>
                                        <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $dailycount?>">0</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </span>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <span class="dash_widget" data-class="weekly">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Weekly</h4>
                        <div class="widget-thumb-wrap">
                            <!-- <i class="widget-thumb-icon bg-red icon-layers"></i> -->
                            <div class="widget-thumb-body">
                                <table>
                                    <tr>
                                        <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="weekly-orders"></td>
                                        <td>&nbsp;</td>
                                        <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $weekcount;?>">0</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </a>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <span class="dash_widget" data-class="monthly">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                        <h4 class="widget-thumb-heading">Monthly</h4>
                        <div class="widget-thumb-wrap">
                            <!-- <i class="widget-thumb-icon bg-purple icon-screen-desktop"></i> -->
                            <div class="widget-thumb-body">
                                <table>
                                    <tr>
                                        <td><img src="../assets/layouts/layout2/img/order.png" class="img-responsive dasboard-order-img" alt="monthly-orders"></td>
                                        <td>&nbsp;</td>
                                        <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $monthcount;?>">0</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </span>
                <!-- END WIDGET THUMB -->
            </div>
            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                    <h4 class="widget-thumb-heading">Aunties / Customers</h4>
                    <div class="widget-thumb-wrap">
                        <!-- <i class="widget-thumb-icon bg-blue icon-bar-chart"></i> -->
                        <div class="widget-thumb-body">
                            <table>
                                <tr>
                                    <td><img src="../assets/layouts/layout2/img/vendor.png" class="img-responsive dasboard-order-img" alt="vendors-customers"></td>
                                    <td>&nbsp;</td>
                                    <td><span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $vendor_count ?>/<?php echo $customer_count?>">0</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>
        </div>  

        <div class="row food-orders">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light " style="min-height:350px;">
                    <div class="portlet-title" style="margin-bottom: 0px;">

                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Today Top 10 Orders</span>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body" style="padding-top: 0px;overflow-x:auto;">
                        <table class="table table-striped table-bordered table-hover" id="food-orders-list">
                            <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>Customer</th>
                                    <th>Aunty</th>
                                    <th>Order type</th>
                                    <th>Amount</th>
                                    <th align='center'>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                 // 
                                    $Qry = "SELECT order_id,full_name,order_type,price,ordr.status,package_id,vendor_id from tbl_orders as ordr join tbl_users as usr on ordr.customer_id = usr.user_id where usr.user_type = :user_type and date_format(now(),'%Y-%m-%d') between date_format(start_date,'%Y-%m-%d') and date_format(end_date,'%Y-%m-%d') order by price desc limit 10";
                                    $qryParams[":user_type"] = 'Customer';
                                    $getOrder = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                                    $ordertbl = ""; $i=1;
                                    $statusColor="";
                                    if(count($getOrder,COUNT_RECURSIVE)>1) {
                                        foreach($getOrder as $fetchorder) {
                                            $VendorQry = "SELECT * from tbl_users where user_id = :user_id";
                                            $VendorqryParams[":user_id"] = $fetchorder['vendor_id'];
                                            $getVendor = $foodAppApi->funBckendExeSelectQuery($VendorQry,$VendorqryParams);
                                            $ordertype =  ($fetchorder['package_id']=='0')?'Normal':'Package';
                                            if (strtolower($fetchorder["status"])=="pending")
                                                $statusColor="info";
                                            if (strtolower($fetchorder["status"])=="confirmed")
                                                $statusColor="warning";
                                            if (strtolower($fetchorder["status"])=="started")
                                                $statusColor="purple";
                                            if (strtolower($fetchorder["status"])=="paid")
                                                $statusColor="success";
                                            if (strtolower($fetchorder["status"])=="cancel")
                                                $statusColor="danger";
                                            if (strtolower($fetchorder["status"])=="completed")
                                                $statusColor="default";
                                            if (strtolower($fetchorder["status"])=="delivery")
                                                $statusColor="delivery";
                                            if (strtolower($fetchorder["status"])=="cooking")
                                                $statusColor="cooking";
                                            $ordertbl.="<tr>";
                                            $ordertbl.= "<td>".$fetchorder['order_id']."</td>";
                                            $ordertbl.= "<td>".$fetchorder['full_name']."</td>";
                                            $ordertbl.= "<td>".$getVendor[0]['full_name']."</td>";
                                            $ordertbl.= "<td>".$ordertype."</td>";
                                            $ordertbl.= "<td>"."$".$fetchorder['price']."</td>";
                                            $ordertbl.= "<td><span class='label label-".$statusColor."'>".$fetchorder['status']."</span></td>";
                                        }
                                    }
                                    if($ordertbl)
                                        echo $ordertbl;
                                    else 
                                        echo "<td colspan='6'> No order(s) found</td>";
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->                
            </div>
        </div>
    </form>
<?php include_once("footer.php"); ?>
<script type="text/javascript">
    $('.dash_widget').on('click', function(){
        var cls = $(this).data("class");
        console.log(cls);
        if (cls!="") {
            $('#dashboard_widget_type').val(cls);
            $('#dashboard_form').submit();
        }
    });
</script>