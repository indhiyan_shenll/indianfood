<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);
if (isset($_GET["msg"])) {
   $msg=$foodAppApi->decode($_GET["msg"]);
    if ($msg=="1") {
      $message="Settings updated successfully";
      $alertclass="success";
    }
}
$Qry="SELECT * FROM tbl_settings where setting_name=:setting_name";
$qryParams[":setting_name"]="discount";
$getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams,'fetch');
$discount=$getResCnt["setting_value"];
$setting_id=$getResCnt["setting_id"];

if(isset($_POST["discount"])) {
	$discount=$_POST["discount"];
	$Qry="update tbl_settings set setting_value=:setting_value where setting_name=:setting_name and setting_id=:setting_id";
	$qryParams[":setting_value"]=$discount;
    $qryParams[":setting_name"]="discount";
    $qryParams[":setting_id"]=$setting_id;
	$updateRes=$foodAppApi->funBckendExeUpdateRecord($Qry,$qryParams);
	if ($updateRes) {
        $msg=$foodAppApi->encode("1");
        header("Location:setting.php?msg=$msg");
        exit;
	}
}
include("header.php");
?>
<form name="customerlist_form" id="customerlist_form" method="post" action="" class="form-horizontal">
<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<div class="row food-orders">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	        	<?php
	                if (!empty($message)) { 
	                ?>
	                <div class="alert alert-<?php echo $alertclass ?>">
	                    <button class="close" data-close="alert"></button>
	                    <span><?php echo $message ?></span>
	                </div>
	                <?php
	                }
		         ?>
	            <!-- BEGIN EXAMPLE TABLE PORTLET-->
	            <div class="portlet light customlistminheight">
	                <div class="portlet-title" >
	                    <div class="caption font-dark caption-new">
	                        <i class="icon-settings font-dark"></i>
	                        <span class="caption-subject bold uppercase">settings</span>
	                    </div>
	                    <div class="tools"> </div>
	                </div>
	                <div class="portlet-body">
	                    <div class="row">
	                    	<div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Discount:</label>
                                	<div class="col-md-4">
                                    	<input type="text" name="discount" id="discount" class="form-control" placeholder="Discount" value="<?php echo $discount ?>">
                                	</div>
                                </div>
                            </div>
                            <div class="form-actions top">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green custombtn customupdate" id="Search"><i class="fa fa-floppy-o"></i> update</button>
                                    </div>
                                </div>
                            </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</form>
<?php include_once("footer.php"); ?>