<?php    
    include_once('../includes/configure.php');
    include_once('../api/Common.php');
    $foodAppApi = new Common($dbconn);

    if (isset($_GET["msg"])) {
       $msg= $_GET["msg"];
        if ($msg=="1") {
          $message="Email sent successfully";
          $alertclass="success";
          $displayshow="display:block";
          $displaynone="display:none";
        } elseif ($msg=="2") {
            $displayshow="display:none";
            $displaynone="display:block";
            $message2="Invalid email address";
            $alertclass="danger";
        }
    }
    $loginerror_message="";
    if (isset($_POST['email'])&&($_POST['email'])!="" && isset($_POST['password']) && $_POST['password']!="") {
        //Posted values of the user name and Password.
        $login_username =addslashes(trim($_POST['email']));
        $password =trim($_POST['password']);
        $login_password=$foodAppApi->encode($password);
        //Find wether given login name and password exists in the database.
        $AdminloginQuery="select * from tbl_users where email = :login_username and password = :login_password and user_type = :user_type and status = :status";
        $qryParams[":login_username"] = $login_username;
        $qryParams[":login_password"] = $login_password;
        $qryParams[":user_type"] = "admin";
        $qryParams[":status"] = "active";
        $getAdmin = $foodAppApi->funBckendExeSelectQuery($AdminloginQuery,$qryParams);        
        if(count($getAdmin,COUNT_RECURSIVE)>1) {            
            foreach($getAdmin as $fetchAdmin) {        
                //SETTING THE DETAILS ABOUT THE LOGGED IN USER IN THE SESSION                
                $_SESSION['admin_id']    =   $fetchAdmin['user_id'];
                $_SESSION['admin_name']  =   $fetchAdmin['full_name'];
                header("Location:dashboard.php?msg=1");
                exit;
            }
        }
        else {
            
            $loginerror_message= "Login failed invalid email or password";
        }
    }

    if (($_POST["Sendemail"]=="ForgotMail") && ($_POST['forgetemail']!="")) {

        $login_username =addslashes(trim($_POST['forgetemail']));
        //$login_password=$foodAppApi->encode($password);
        //Find wether given login name and password exists in the database.
        $AdminloginQuery="select * from tbl_users where email = :login_username and user_type = :user_type and status = :status";
        $qryParams[":login_username"] = $login_username;
        $qryParams[":user_type"] = "admin";
        $qryParams[":status"] = "active";
        $getAdmin = $foodAppApi->funBckendExeSelectQuery($AdminloginQuery,$qryParams);        
        if(count($getAdmin,COUNT_RECURSIVE)>1) {            
            foreach($getAdmin as $fetchAdmin) {        
                //SETTING THE DETAILS ABOUT THE LOGGED IN USER IN THE SESSION                
                $userid  =   $fetchAdmin['user_id'];
                $full_name  =   $fetchAdmin['full_name'];
                $email =   $fetchAdmin['email'];
                $password=$foodAppApi->decode($fetchAdmin["password"]);
                // Send email to Customer
                $mailParams = array(
                    "fromAddress" => "food@kazafood.com",
                                      "toAddress" => "karuna.shenll@gmail.com",
                                      "customerName" => "Indhiyan",
                                      "subject" => "Forget password",
                                      "bodyMsg" => "<p>Dear ".$fetchAdmin["full_name"]."</p>
                                                      <p>Username: ".$fetchAdmin["email"]."</p>
                                                      <p>Password for your account is: ".$password."</p>
                                                      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                                      <p style='line-height:5px;'>Best Regards,</p> 
                                                      <p style='line-height:0px;'>KazaFood support Team</p>"
                                  );
                $sendEmail = $foodAppApi->sendEmailNotification($mailParams);
            }
            header("Location:index.php?msg=1");
            exit;
        }
        else {
            header("Location:index.php?msg=2");
            exit;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Kaza | <?php echo (is_numeric(strpos($_SERVER['REQUEST_URI'], "admin"))?"Admin":"User"); ?> Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/pages/css/login-3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
        <style>
            .custombtn{
                width: 85px;
                border-radius: 6px !important;
                box-shadow: 0px 3px 1px #888888 !important;
            }
            .customlogin, .customlogin:hover{
                background-color: #4CAF50 !important;
                border-color: #4CAF50 !important;
                color: white;
            }
            .alert-success{
                color: #3c763d;
                background-color: #dff0d8;
                border-color: #d6e9c6;
            }
            
        </style>
    <!-- END HEAD -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo vendorlogin" >
            <a href="index.php">
                <img src="../assets/layouts/layout2/img/logo.png" alt="" /> 
                </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content logincontent">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" method="post" id="frm_login" style="<?php echo $displayshow?>"> 
                <!-- <h3 class="form-title">Admin Login</h3> -->
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Incorrect Email and Password. </span>
                </div>
                <?php
                 if (!empty($message)) { 
                ?>
                <div class="alert alert-<?php echo $alertclass ?>">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo $message ?></span>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label>
                    <button type="submit" class="btn pull-right customlogin custombtn"><i class="fa fa-sign-in"></i> Login </button>
                </div>
                
                <div class="forget-password">
                    <h4>Forgot your password ?</h4>
                    <p style="margin:5px 0;"> no worries, click
                        <a href="javascript:;" id="forget-password" > here </a> to reset your password. </p>
                </div>                
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="" id="frm_forgot" method="post" style="<?php echo $displaynone?>">
                <h3>Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                 <?php
                 if (!empty($message2)) { 
                ?>
                <div class="alert alert-<?php echo $alertclass ?>">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo $message2 ?></span>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="forgetemail" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn dark custombtn"><i class="fa fa-arrow-left"></i> Back </button>
                    <button type="submit" class="btn pull-right customlogin custombtn" name="Sendemail" value="ForgotMail"><i class="fa fa-arrow-right"></i> Submit </button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            
        </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/login4.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
<script>
    $(document).ready(function(){
        $("#back-btn").click(function(){
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1]+"/" +l.pathname.split('/')[2];
        window.location.href=base_url+"/index.php";
        });
    });
</script>