<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
include("header.php");

$foodAppApi = new Common($dbconn);
$emptycondn = array();
$pstflg = false;
if (isset($_GET['vendor']))
    //$vendor = trim(base64_decode($_GET['vendor']));
    $vendor =$foodAppApi->decode($_GET['vendor']);
// if(isset($_POST['item_type'])) {
// 	$item_type = trim($_POST['item_type']);
// 	$pstflg     = true;
// }
?>
<form name="vendcategryitms_form" id="vendcategryitms_form" method="post" action="">
<input type="hidden" name="HdnVendor" id="HdnVendor" value="<?php echo $vendor; ?>">
<input type="hidden" name="HdnItemType" id="HdnItemType" value="<?php echo $item_type; ?>">
<div class="page-content" id="vendor-category-item-content">
    <div class="row food-orders">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <!-- <div class="page-bar">
            </div> -->
            <div class="portlet light customlistminheight">
                <div class="portlet-title" >
                    <div class="caption font-dark vendor-order-cat-item-caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Category Item</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <!-- karuna -->
                <div class="portlet-body search-body vendor-category-item-portlet-body">
                    <div class="row">                     
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch vendor-category-items-search">                        
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 remove-left-right-padding">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Item Name:</label>
                                    <select name="item_name" id="item_name" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                            $ItemQry = "SELECT item_id,item_name FROM `tbl_category` as cate JOIN tbl_category_items as cateitm on cateitm.category_id = cate.category_id where vendor_id = :vendor_id group by item_name";
                                            $qryParams[":vendor_id"]=$vendor;               
                                            $getItemName = $foodAppApi->funBckendExeSelectQuery($ItemQry,$qryParams);
                                            if(count($getItemName,COUNT_RECURSIVE)>1){
                                                foreach($getItemName as $fetchItemName) {
                                                    echo "<option value='".$fetchItemName['item_id']."'>".$fetchItemName['item_name']."</option>";
                                                }
                                            }
                                        ?>
                                    </select>                    
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>Item Type:</label>
                                    <select name="item_type" id="item_type" class="form-control">
                                        <option value="">Select</option>
                                        <option value="veg">Veg</option>
                                        <option value="nonveg">Non-veg</option>
                                        <option value="halal">Halal</option>
                                    </select>
                                </div>                
                                
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                <a type="button" class="btn yellow custombtn" id="searchbtn"><i class="fa fa-search"></i> Search</a>
                                <a type="button" class="btn red custombtn" id="resetbtn"><i class="fa fa-times-circle"></i> Reset</a>
                                <a class="btn dark custombtn" id="bckbtn" href="vendors_listing.php"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- karuna -->
                <!-- testing table -->
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>               
                <!-- testing table ends here -->
                <!-- AJAX response will be appending here -->
                <div id="vendorCategoryItemTable"></div>
            </div>
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/custom_vendor_category_items.js" type="text/javascript"></script>
