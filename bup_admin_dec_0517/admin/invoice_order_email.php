<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$foodAppApi = new Common($dbconn);

$InsertInvoiceId="";
$ArrInvoiceId="";
$ArrInvoiceId=array();
if(isset($_REQUEST["ArrInvoiceJson"])) {
	$ArrInvoiceJson=$_REQUEST["ArrInvoiceJson"];
	$InvoiceJson=json_decode($ArrInvoiceJson);
    $ArrInvoiceId=explode(",",$InvoiceJson);
    $InsertInvoiceId=$_REQUEST["invoiceid"];
    $created_date = date("Y-m-d H:i:s");

    $invoiceHTMLStart='<style>
	    #order_tbl>tr>td {font-size:5px;}
	    #order_tbl>tr>td, #order_tbl>th>td {border:0.4px solid #ccc;}
    </style>
    <table width="100%" bgcolor="#F7F7F7" id="order_tbl" cellpadding="6">
	    <thead>
	        <tr>
	            <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">S.No</th>
                <th width="20%" class="tableborderhead" color="#006679" align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Order #</th>
                <th width="20%" class="tableborderhead" color="#006679"  align="center" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Package ?</th>
                <th width="20%" class="tableborderhead" color="#006679" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;"></th>
                <th width="20%" class="tableborderhead" color="#006679" style="font-size:13px;border-top:1px solid #ccc;border-bottom: 1px solid #ccc;">Amount</th>
	        </tr>
	    </thead>
    	<tbody>';
	    $qryParams=array();
		if (count($ArrInvoiceId)>0) {
        	$DiscountQry="SELECT * FROM tbl_settings WHERE setting_name='discount'";
			$getResDiscount = $foodAppApi->funBckendExeSelectQuery($DiscountQry);
        	$discountVal = $getResDiscount[0]['setting_value'];
        	$discountType = $getResDiscount[0]['setting_type'];
        	$subTot=0;$invoice_amount=0;$discount_amount=0;$sno=1;$invoiceHTML = "";$ArrOrderId="";
        	foreach ($ArrInvoiceId as $ArrInvoicekey => $ArrInvoiceValue) {
        		$ArrOrderId[] = $ArrInvoiceValue;
				$InsertPreviewQry="INSERT INTO tbl_invoice_orders (invoice_id, order_id, invoice_date) VALUES (:invoice_id, :order_id, :invoice_date);";
				$InsertPreviewParams['invoice_id'] = $InsertInvoiceId;
				$InsertPreviewParams['order_id'] = $ArrInvoiceValue;
				$InsertPreviewParams['invoice_date'] = $created_date;
				$getPreviewId = $foodAppApi->funBckendExeInsertRecord($InsertPreviewQry,$InsertPreviewParams);

        		$Qry="SELECT order_id,package_id,price from tbl_orders where order_id = :order_id";
        		$qryParams['order_id'] = $ArrInvoiceValue;
				$getResInvoice = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
					
				$proCount = count($getResInvoice);
				$subTotAmt=0;$discountText="";$discount=0;
				foreach ($getResInvoice as $key => $OrderDetails) {
					$invoiceHTML .= "<tr>"; 
					$OrderDetails["package_id"] = empty($OrderDetails["package_id"])?"No":"Yes";

					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$sno.'</td>';
					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$OrderDetails["order_id"].'</td>';
					$invoiceHTML .= '<td width="20%" style="font-size:13px;" align="center">'.$OrderDetails["package_id"].'</td>';
			   		$invoiceHTML .= '<td width="20%" style="font-size:13px;"></td>';
			   		$invoiceHTML .= '<td width="20%" style="font-size:13px;">$ '.$OrderDetails["price"].'</td>';
			   		$invoiceHTML .= '</tr>';
			   		$subTotAmt += $OrderDetails["price"];
			   		// $proCount++;
				}
				$subTot += $subTotAmt;
				$sno ++;
        	}

        	$updateqry = "UPDATE tbl_invoices SET vendor_invoice_status = :vendor_invoice_status, admin_invoice_status = :admin_invoice_status, develop_status = :develop_status, created_date = :created_date, modified_date = :modified_date WHERE invoice_id = :invoice_id";

			$qryParamsUpdate['invoice_id'] = $InsertInvoiceId;
			$qryParamsUpdate['vendor_invoice_status'] = "Pending";
			$qryParamsUpdate['admin_invoice_status'] = "Pending";
			$qryParamsUpdate['develop_status'] = "";
			$qryParamsUpdate['created_date'] = $created_date;
			$qryParamsUpdate['modified_date'] = $created_date;
			$getvendors = $foodAppApi->funBckendExeUpdateRecord($updateqry,$qryParamsUpdate);

        	if ($discountType=='percent') {
				$discount = ($subTot * $discountVal)/100;
				$discountText = $discountVal."%";
			} else {
				$discount = $discountVal;
				$discountText = "$ ".number_format($discountVal);
			}
			$totalAmount = $subTot - $discount;

			$invoiceHTML .= '<tr>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%" style="font-size:13px;">SubTotal</td>
			<td width="20%" style="font-size:13px;">$ '.number_format($subTot,2).'</td>
			</tr>';
        	$invoiceHTML .= '<tr>
        	<td width="20%"></td>
        	<td width="20%"></td>
        	<td width="20%"></td>
        	<td width="20%" style="font-size:13px;">Discount ('.$discountText.')</td>
        	<td width="20%" style="font-size:13px;">$ '.number_format($discount,2).'</td>
        	</tr>';
			$invoiceHTML .= '<tr>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%"></td>
			<td width="20%" style="font-size:13px;">Total</td>
			<td width="20%" style="font-size:13px;">$ '.number_format($totalAmount,2).'</td>
			</tr>';
		} else {
            $invoiceHTML .= '<tr><td colspan="5" style="text-align:center;">No order(s) found </td></tr>';
        }
   		$invoiceHTMLEnd='</tbody>
   	</table>';
   	// echo $invoiceHTML;
 	$htmlpdf=$invoiceHTMLStart.$invoiceHTML.$invoiceHTMLEnd;
	include("create_invoice_pdf.php");

	$VenQry="SELECT vendor_id FROM tbl_invoices WHERE invoice_id = :invoice_id";
	$VenParams['invoice_id'] = $InsertInvoiceId;
	$getResVenId = $foodAppApi->funBckendExeSelectQuery($VenQry,$VenParams);
	$vendor_id = $getResVenId[0]['vendor_id'];

	$VendorDetailsQry="SELECT * FROM tbl_users WHERE user_id = :user_id";
	$VendorDetailsParams['user_id'] = $vendor_id;
	$getResVendorDetails = $foodAppApi->funBckendExeSelectQuery($VendorDetailsQry,$VendorDetailsParams);
	$email = $getResVendorDetails[0]['email'];
	$full_name = $getResVendorDetails[0]['full_name'];
	// echo "ArrOrderId = > <br>";
	// print_r($ArrOrderId);
	$Img_Path = KAZA_SYSTEM_PATH."assets/layouts/layout2/img/logo.png";

	$EmailMessage = '<html>
	  <style>
	    @media screen and (min-width: 320px) {
	      .container1 {
	        width: 100% !important;
	      }
	    }
	  </style>
	  <table class="container1" width="100%" cellpadding="0" cellspacing="0"  width="600px" style="font-family: verdana;font-size:13px;">
	  <tr><td>Dear '.ucfirst($full_name).',</td></tr>
	  <tr style="height:15px"><td></td></tr>
	  <tr><td>We have generated invoice for these following orders. </td></tr>
	  <tr style="height:15px"><td></td></tr>';

	foreach ($ArrOrderId as $ArrOrderId_key => $ArrOrderId_value) {
		$EmailMessage .= '
		<tr><td><b>Order # :</b> '.$ArrOrderId_value.'</td></tr></tr>
	    <tr style="height:15px"><td></td></tr>';
	}

	$EmailMessage .= '<tr style="height:15px"><td></td></tr><tr><td>Regards,</td></tr>
	  <tr style="height:15px"><td></td></tr>
	  <tr><td>Kaza Food</td></tr>
	  <tr style="height:15px"><td></td></tr>
	  <tr><td><img src="'.$Img_Path.'" height="50" width="150"/></td></tr>
	  <tr style="height:15px"><td></td></tr>
	  <tr><td>W: www.kazafood.com</td></tr> 
	  <tr style="height:15px"><td></td></tr>
	  <tr><td>T: 1300 212 212</td></tr>
	  </table>
	  </body>
	  </html>';

	  // echo $EmailMessage;exit;
	  require '../includes/PHPmailer/PHPMailerAutoload.php';
	  $mail = new PHPMailer;
	  $mail->Host = 'shenll.net;shenll.net'; // Specify main and backup server
	  $mail->From = "mail@kazafood.org";
	  $mail->FromName = 'Kaza Food';
	  $mail->addAddress($email); 
	  $mail->addReplyTo('mail@kazafood.org','Kaza Food');     //,'Support'
	  $mail->WordWrap = 100;                                 // Set word wrap to 50 characters
	  $mail->isHTML(true);                                  // Set email format to HTML
	  $mail->Subject = "Congrats, We have generated your invoice";
	  $mail->Body    =  $EmailMessage;
	  $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	  // $attach = KAZA_SYSTEM_PATH.'invoices/invoice_pdf/'.$InvoiceFileName;
	  // echo $attach;
	  // $mail->addAttachment($attachment_path);
	  $mail->addAttachment($InvoiceFileName);
	  if (!$mail->send()) {
	    $message ="Invalid email address";
	    $alertclass="danger";
	  } else {
		$message ="An email has been sent with self-assessment results attachment";
		$alertclass="success";
	  }
}
?>