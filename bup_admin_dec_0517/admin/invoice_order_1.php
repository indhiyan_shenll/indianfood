<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');

$display="display:none;";
if(isset($_POST["vendors"]) && !empty($_POST["vendors"])) {
    $vendor=(empty($_POST["vendors"]))?"":$_POST["vendors"];
    $display="display:block;";
} else {
    $vendor="";
}


/****Paging ***/
$Page=1;$RecordsPerPage=4;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page=$_REQUEST['HdnPage'];
$TotalPages=0;

/*End of paging*/
include("header.php");
$foodAppApi = new Common($dbconn);
?>
<form name="invoiceorder_form" id="invoiceorder_form" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnRecordsPerPage" id="HdnRecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title" style="margin-bottom: 0px;">

                    <div class="caption font-dark">
                        <i class="icon-docs font-dark"></i>
                        <span class="caption-subject bold uppercase">Generate Invoice</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcustomersearch">
                            <div class="col-md-9 col-sm-9 col-xs-12 remove-left-right-padding">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>Vendors Name:</label>
                                    <select name="vendors" id="vendors" class="form-control">
                                        <option value="">Select</option>
                                        <?php
                                            $Qry1="SELECT * FROM tbl_users where user_type=:user_type and status=:status order by user_id desc";
                                            $qryParams1[":user_type"]="vendor";
                                            $qryParams1[":status"]="Active";
                                            $getvendors = $foodAppApi->funBckendExeSelectQuery($Qry1,$qryParams1);
                                            if (count($getvendors,COUNT_RECURSIVE)>1) {
                                                foreach ($getvendors as $getVendorsData) {
                                                    if($getVendorsData["user_id"]==$vendor)
                                                        $selected="selected";
                                                    else
                                                        $selected="";
                                                    echo "<option value=".$getVendorsData["user_id"]." ".$selected.">".$getVendorsData["full_name"]."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    <div class="error errormsg">Please select vendors</div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                   <button type="button" class="btn green custombtn btn-md" id="Search">Search</button>  
                                    <button type="button" name="preview" id="previewchk" class="btn dark btn-md custombtn">Preview</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="loadingreportsection">
                    <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                </div>
                <div id="showtable" style="<?php echo $display ?>">
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content" id="tbl_invoice_list">
                            <thead class="flip-content">
                                <tr>
                                    <th width="5%"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="check_all" onclick="checkAllExp(this)"><span></span></label></th>
                                    <th nowrap>Order Id</th>
                                    <th>Customer Name</th>
                                    <th>Vendor Name</th>
                                    <th>Amount</th>
                                    <!-- <th>Invoice Amount</th> -->
                                    <!-- <th>Discount</th> -->
                                    <!-- <th>Order Status</th> -->
                                    <th>Invoice Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                               <!--  <?php
                                if (!empty($vendor)) {
                                    $ArrStatus = array('pending','paid','generate');
                                    $statuslist = implode("','",$ArrStatus);
                                    $Qry="SELECT user.full_name,invoice.invoice_id,invoice.vendor_id,invoice.transaction_id,invoice.total_amount,invoice.invoice_amount,invoice.discount,invoice.vendor_invoice_status,invoice.admin_invoice_status FROM tbl_invoices as invoice INNER JOIN tbl_users as user ON user.user_id=invoice.vendor_id where user.status=:status and user_type=:user_type and user.user_id=:vendorsid and upload_documents<>'' ORDER BY FIELD(invoice.vendor_invoice_status,'".$statuslist."')"; 
                                    $qryParams[":vendorsid"]=$vendor;
                                    $qryParams[":user_type"]="vendor";
                                    $qryParams[":status"]="Active";
                                    $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                                    if (count($getResCnt,COUNT_RECURSIVE)>1) {
                                        $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                                        $Start=($Page-1)*$RecordsPerPage;
                                        $sno=$Start+1;
                                        $Qry.=" limit $Start,$RecordsPerPage";
                                        $getInvoices = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                                       //$sno=1;
                                        if (count($getInvoices)>0) {
                                            foreach ($getInvoices as $invoicesListData) {
                                               $statusColor=(strtolower($invoicesListData["vendor_invoice_status"])=="paid")?"green":"red";
                                               $rowdisable=(strtolower($invoicesListData["vendor_invoice_status"])=="paid")?"disabled":"";
                                               $checkbox_id=(strtolower($invoicesListData["vendor_invoice_status"])=="paid")?"checkbox1":"checkbox";

                                ?>
                                       <tr>
                                            <td style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="<?php echo $checkbox_id?>" name="checkbox[]" id="<?php echo $checkbox_id?>" value="<?php echo $invoicesListData["invoice_id"];?>" onclick="eachChk()" <?php echo $rowdisable;?>>
                                                <span></span></label></td>
                                            <td><?php echo $invoicesListData["invoice_id"];?></td>
                                            <td><?php echo $invoicesListData["full_name"];?></td>
                                            <td><?php echo $invoicesListData["transaction_id"];?></td>
                                            <td><?php echo $invoicesListData["total_amount"];?></td>
                                            <td><?php echo $invoicesListData["invoice_amount"];?></td>
                                            <td><?php echo $invoicesListData["discount"];?></td>
                                            <td style="color:<?php echo $statusColor?>"><?php echo $invoicesListData["vendor_invoice_status"];?></td>
                                       </tr>
                                <?php   
                                    $sno++;     
                                        }
                                    } else {
                                        echo "<tr><td colspan='9' style='text-align:center;'>No invoice(s) found </td></tr>";
                                    }
                                } else {
                                    echo "<tr><td colspan='9' style='text-align:center;'>No invoice(s) found </td></tr>";
                                }
                                } else {
                                    echo "<tr><td colspan='9' style='text-align:center;'>No invoice(s) found </td></tr>";
                                }
                                ?> -->
                                <?php
                                    if (!empty($vendorsid)) {
                                        // $ArrStatus = array('pending','paid','generate');
                                        // $statuslist = implode("','",$ArrStatus);
                                        $Qry="SELECT * FROM tbl_orders as ord left JOIN tbl_users as user ON user.user_id=ord.customer_id where vendor_id=:vendorsid and payment_status='Completed'";
                                        // $Qry="SELECT * FROM tbl_orders as ord INNER JOIN tbl_users as user ON user.user_id=ord.vendor_id where user.status=:status and user_type=:user_type and user.user_id=:vendorsid and upload_documents<>'' and ord.payment_status='Completed'";
                                        // $Qry="SELECT user.full_name,invoice.invoice_id,invoice.vendor_id,invoice.transaction_id,invoice.total_amount,invoice.invoice_amount,invoice.discount,invoice.vendor_invoice_status,invoice.admin_invoice_status FROM tbl_invoices as invoice INNER JOIN tbl_users as user ON user.user_id=invoice.vendor_id where user.status=:status and user_type=:user_type and user.user_id=:vendorsid and upload_documents<>'' ORDER BY FIELD(invoice.vendor_invoice_status,'".$statuslist."')"; 
                                        $qryParams[":vendorsid"]=$vendorsid;
                                        // $qryParams[":user_type"]="vendor";
                                        // $qryParams[":status"]="Active";
                                        $getResCnt = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                                        // print_r($getResCnt);
                                        if (count($getResCnt,COUNT_RECURSIVE)>1) {
                                            $TotalPages=ceil(count($getResCnt)/$RecordsPerPage);
                                            $Start=($Page-1)*$RecordsPerPage;
                                            $sno=$Start+1;
                                            $Qry.=" limit $Start,$RecordsPerPage";
                                            $getInvoices = $foodAppApi->funBckendExeSelectQuery($Qry,$qryParams);

                                            if (count($getInvoices)>0) {
                                                $pending_options="";
                                                $generated_options="";
                                                foreach ($getInvoices as $invoicesListData) {
                                                    $OrderQry="SELECT * FROM tbl_orders as ord JOIN tbl_invoice_orders as inv_ord on ord.order_id=inv_ord.order_id where ord.order_id=:order_id";
                                                    $OrderqryParams[":order_id"]=$invoicesListData["order_id"];
                                                    $OrdergetResCnt = $foodAppApi->funBckendExeSelectQuery($OrderQry,$OrderqryParams);
                                                    if ($OrdergetResCnt>0) {
                                                        foreach ($OrdergetResCnt as $orderListData) {
                                                            $rowdisable=($orderListData["order_id"]==$invoicesListData["order_id"])?"disabled":"";
                                                            $checkbox_id=($orderListData["order_id"]==$invoicesListData["order_id"])?"checkbox1":"checkbox";
                                                            $generate = "Invoice Generated";
                                                            $genColor = "green";
                                                        }
                                                    } else {
                                                        $rowdisable="";
                                                        $checkbox_id="checkbox";
                                                        $generate = "Pending";
                                                        $genColor = "red";
                                                    }
                                                    // $statusColor=(strtolower($invoicesListData["payment_status"])=="completed")?"green":"red";

                                                    $VendorQry="SELECT full_name FROM tbl_users where user_id=:vendor_id";
                                                    $VendorqryParams[":vendor_id"]=$invoicesListData["vendor_id"];
                                                    $VendorgetResCnt = $foodAppApi->funBckendExeSelectQuery($VendorQry,$VendorqryParams);
                                                    if ($VendorgetResCnt>0) {
                                                        foreach ($VendorgetResCnt as $vendorListData) {
                                                            $vendor_name = $vendorListData['full_name'];
                                                        }
                                                    }
                                                    
                                                    if ($generate != "Pending") {
                                                        $generated_options .= '<tr>
                                                            <td style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="'.$checkbox_id.'" name="checkbox[]" id="'.$checkbox_id.'" value="'.$invoicesListData["order_id"].'" onclick="eachChk()" '.$rowdisable.'>
                                                            <span></span></label></td>
                                                            <td>'.$invoicesListData["order_id"].'</td>
                                                            <td>'.$invoicesListData["full_name"].'</td>
                                                            <td>'.$vendor_name.'</td>
                                                            <td>'.$invoicesListData["price"].'</td>
                                                            <td style="color:'.$genColor.'">'.$generate.'</td>
                                                            <td><div class="actions"><div class="btn-group"><a class="btn dark btn-sm" href="order_details.php.php?id='.base64_encode($invoicesListData["order_id"]).'">View <i class="fa fa-angle-right"></i></a></div></div></td>
                                                        </tr>';
                                                    } else {
                                                        $pending_options .= '<tr>
                                                            <td style="text-align: center;"><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                            <input type="checkbox" class="'.$checkbox_id.'" name="checkbox[]" id="'.$checkbox_id.'" value="'.$invoicesListData["order_id"].'" onclick="eachChk()" '.$rowdisable.'>
                                                            <span></span></label></td>
                                                            <td>'.$invoicesListData["order_id"].'</td>
                                                            <td>'.$invoicesListData["full_name"].'</td>
                                                            <td>'.$vendor_name.'</td>
                                                            <td>'.$invoicesListData["price"].'</td>
                                                            <td style="color:'.$genColor.'">'.$generate.'</td>
                                                            <td><div class="actions"><div class="btn-group"><a class="btn dark btn-sm" href="order_details.php.php?id='.base64_encode($invoicesListData["order_id"]).'">View <i class="fa fa-angle-right"></i></a></div></div></td>
                                                        </tr>';
                                                    }
                                                    $sno++;     
                                                }
                                                echo $pending_options;
                                                echo $generated_options;
                                            } else {
                                                echo "<tr><td colspan='9' style='text-align:center;'>No order(s) found </td></tr>";
                                            }
                                        } else {
                                            echo "<tr><td colspan='9' style='text-align:center;'>No order(s) found </td></tr>";
                                        }
                                    } else {
                                        echo "<tr><td colspan='9' style='text-align:center;'>No order(s) found </td></tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <?php
                            if ($TotalPages > 1) {

                                echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
                                $FormName = "invoiceorder_form";
                                require_once ("paging.php");
                                echo "</td></tr>";
                            }
                        ?>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="modal fade bs-modal-lg" id="InvoiceOrderModel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <input type="hidden" name="HndChckvalue" id="HndChckvalue">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                   <h4 class="modal-title preview">Vendors Invoice</h4>
                </div>
                <div class="modal-body" id="popuptable">
        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn green" id="confirmEmail">Confirm</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
</form>

<?php include_once("footer.php"); ?>

<style>
.dataTables_extended_wrapper .table.dataTable{
    margin:0px 0!important;;
}
</style>
<script>
$(document).ready(function() {
  $(".errormsg").css("display","none");
  $("#previewchk").hide();
  loading_sorting();
});
function loading_sorting(){
$('#tbl_invoice_list').DataTable( {
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength": false ,
       'aoColumnDefs': [
            {'bSortable': false,}
        ],
         "order": [[ 0, "asc" ]]  
    } );    
}
</script>
<script>
$(document).on("change","#vendors",function() {
    var vendorsId  =$("#vendors").val();
    if(vendorsId=="") {
        $(".errormsg").css("display","block");
        return false;
    } else {
        $(".errormsg").css("display","none");
         return true;
    }
});
$(document).on("click","#Search",function(){
    var vendorsId  =$("#vendors").val();
    if(vendorsId=="") {
        $(".errormsg").css("display","block");
        return false;
    }
    var HdnPage    = $("#HdnPage").val();
    var HdnMode    = $("#HdnMode").val();
    var RecordsPerPage = $("#HdnRecordsPerPage").val();
    filter_by_invoice_reports(vendorsId,HdnPage,HdnMode,RecordsPerPage);
   
});
function filter_by_invoice_reports(vendorsId,HdnPage,HdnMode,RecordsPerPage){
    if (vendorsId!="") {
        $(".loadingreportsection").show();
        $.ajax({
            url:"filter_invoice.php",
            method:'GET',
            data:"vendorsid="+vendorsId+"&HdnPage="+HdnPage+"&HdnMode="+HdnMode+"&PerPage="+RecordsPerPage,
            success:function(data) { 
                //console.log(data);
                setTimeout(function(){ $(".loadingreportsection").hide(); }, 1000);
                $("#showtable").show();                           
                document.getElementById('showtable').innerHTML = data; 
                $('.table-header').remove();
                loading_sorting();
            }
        });            
    } else {
        $("#showtable").hide();
    } 
} 
function checkAllExp(Element) {
    //alert(Element);
    chk_len=$('input[id=checkbox]').length;
    if(Element.checked){
        if(chk_len>0){
            $('input[id=checkbox]').prop('checked', true);
            $("#previewchk").show();
        }
    }
    else{
        if(chk_len>0){
            $('input[id=checkbox]').prop('checked', false);
            $("#previewchk").hide();
        }
    }
}
 
    function eachChk()
    {
        var atLeastOneChecked = false;
        $('.checkbox').each(function () {
            if ($(this).is(":checked")) {
                atLeastOneChecked = true;
            }
        });
        if (atLeastOneChecked) {
           $("#previewchk").show();
            
        } else {
             $("#previewchk").hide();
            
        }
    }  

$(document).on("click","#previewchk",function() {

    var allVals = [];
    $('.checkbox:checked').each(function () {
        allVals.push($(this).val());
    });
    var vendorId  =$("#vendors").val();
    $("#HndChckvalue").val(allVals);
    var myInvoiceJSON = JSON.stringify(allVals);
    ajax_invoice_order(myInvoiceJSON,vendorId);
    setTimeout(function() {
        var preview_val = $('#preview_id').val();
        console.log(preview_val);
        $('.preview').text('Vendors Invoice #'+preview_val); 
    }, 500);
});   
function ajax_invoice_order(myInvoiceJSON,vendorId) {
    if (myInvoiceJSON!="") {
        $.ajax({
            url:"filter_invoice_order.php",
            method:'GET',
            data:"ArrInvoiceJson="+myInvoiceJSON+"&vendorid="+vendorId,
            success:function(data) {    
               document.getElementById('popuptable').innerHTML = data; 
               $('.table-header').remove();
               $('#InvoiceOrderModel').modal('show');
            }
        }); 
    } else {
        alert("Please select atleast one invoice");
    }
}

$(document).on("click","#confirmEmail",function() {
    var checkallVals=$("#HndChckvalue").val();
    var myInvoiceJSON = JSON.stringify(checkallVals);
    if (myInvoiceJSON!="") {
        $.ajax({
            url:"invoice_order_email.php",
            method:'GET',
            data:"ArrInvoiceJson="+myInvoiceJSON,
            success:function(data) {  
            console.log(data);  
               // document.getElementById('popuptable').innerHTML = data; 
               $('.table-header').remove();
               $('#InvoiceOrderModel').modal('show');
            }
        });            
    } 
})
</script>