function FoodApp () {        
}

FoodApp.prototype.vendorOrderList = function (vendorDetJsonString) {

    $("#vendorListTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_vendor_orders_listing.php",
        // data: {vendor_id: vendorDetArr["vendor_id"], order_type: vendorDetArr["order_type"]},
        data: {orderSearchCriteria: vendorDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#vendorListTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#vendorListTable").show();
            // console.log(response);
        }, 

    }) 
}

FoodApp.prototype.customerList = function (customerDetJsonString) {
    $("#customerListTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_customer_listing.php",
        data: {customerSearchCriteria: customerDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#customerListTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#customerListTable").show();
            //console.log(response);
        }, 

    }) 
}
// Vendor's Packages List
FoodApp.prototype.vendorPackagesList =  function (vendorDetJsonString) {
    $("#vendorPackagesTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_vendor_packages.php",
        // data: {vendor_id: vendorDetArr["vendor_id"], order_type: vendorDetArr["order_type"]},
        data: {orderSearchCriteria: vendorDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#vendorPackagesTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#vendorPackagesTable").show();
            // console.log(response);
        }, 

    })
}
// Vendor's Category and Items page
FoodApp.prototype.vendorCategoryItemList = function (vendorDetJsonString) {
    $("#vendorCategoryItemTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_vendor_category_items.php",
        data: {orderSearchCriteria: vendorDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#vendorCategoryItemTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#vendorCategoryItemTable").show();
            // console.log(response);
        }, 

    })
}
// Vendor Listing page
FoodApp.prototype.vendorList = function (vendorDetJsonString) {

    $("#vendorListingTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_vendor_listing.php",
        // data: {vendor_id: vendorDetArr["vendor_id"], order_type: vendorDetArr["order_type"]},
        data: {orderSearchCriteria: vendorDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#vendorListingTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#vendorListingTable").show();
            // console.log(response);
        },
    })
}
// Vendor status
FoodApp.prototype.vendorStatus = function (status,vendorid) {    
    $.ajax({
        method: "POST",
        url: "vendor_ajax_status.php",
        data: {status: status,vendorid:vendorid},
        error: function(){
        },
        success: function(response) {
            if(response!="failure") {
                var label="";
                if (status=="active")
                    label="../assets/layouts/layout2/img/active.png";
                if (status=="pending")
                    label="../assets/layouts/layout2/img/waiting.png";
                if (status=="block")
                    label="../assets/layouts/layout2/img/block.png";
                $("#vendorstatus").modal("hide");
                $("#status_"+vendorid).empty().append("<img data-status='"+status+"' src='"+label+"'>");
                $(".alert-success").empty().append('<div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Status updated successfully</div>');
                $(".alertmsg").css({"margin-bottom": "-15px"});
                $(".alert-success").show();
                setTimeout(function(){ $(".alert-success").hide(); }, 5000);
            } else {
                $("#vendorstatus").modal("hide");
                $(".alert-success").empty().append('<div class="custom-alerts alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Status not updated</div>');
                $(".alertmsg").css({"margin-bottom": "-15px"});
                $(".alertmsg").show();
                setTimeout(function(){ $(".alert-success").hide(); }, 5000);
            }
        }, 
    }) 
}

FoodApp.prototype.customerAjaxStatus= function (popstatus,customerID) {
    $.ajax({
        method: "POST",
        url: "filter_customer_status.php",
        data: {status: popstatus,customerid:customerID},
        error: function(){
        },
        success: function(response) {
            if(response!="failure") {
                if (popstatus=="Active")
                    var label="../assets/layouts/layout2/img/active.png";
                if (popstatus=="InActive")
                    var label="../assets/layouts/layout2/img/inactive.png";
                if (popstatus=="Block")
                    var label="../assets/layouts/layout2/img/block.png";
                $("#CustomerStatus").modal("hide");
                $(".status_"+customerID).empty().append("<span data-id='"+customerID+"' id='popupStatus'> <img src="+label+"></span>");
                $(".successmsg").empty().append('<div class="custom-alerts alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Status updated successfully</div>');
                setTimeout(function(){ $(".successmsg").hide(); }, 5000);
            } else {
                $("#CustomerStatus").modal("hide");
                $(".successmsg").empty().append('<div class="custom-alerts alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Status not updated</div>');
                setTimeout(function(){ $(".successmsg").hide(); }, 5000);
            }
        }, 
    }) 
}

FoodApp.prototype.orderReportList= function(reportsDetJsonString) {
    $("#reportsListTable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_report.php",
        data: {reportsSearchCriteria: reportsDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#reportsListTable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#reportsListTable").show();
        }, 

    }) 
}

FoodApp.prototype.viewOrderItem=function(orderID,packageID) {
    $.ajax({
        method: "POST",
        url: "view_order_items.php",
        data: {orderID: orderID,packageID:packageID},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#viweorderitemList").empty().append(response);
            $("#viweorderitem").modal("show");
        }, 

    }) 

}

FoodApp.prototype.invoiceViewOrderItem=function(invoiceID,vendorID) {
    $.ajax({
        method: "POST",
        url: "view_invoice_order_items.php",
        data: {invoiceID: invoiceID,vendorID:vendorID},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#invoiceviweorderitemList").empty().append(response);
            $("#invoiceviweorderitem").modal("show");
        }, 

    }) 
}

FoodApp.prototype.invoiceList = function (invoiceDetJsonString) {

    $("#invoicelist").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_invoice.php",
        // data: {vendor_id: vendorDetArr["vendor_id"], order_type: vendorDetArr["order_type"]},
        data: {orderSearchCriteria: invoiceDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#invoicelist").empty().append(response);
            $(".loadingreportsection").hide();
            $("#invoicelist").show();
            // console.log(response);
        }, 

    }) 
}

FoodApp.prototype.invoiceOrderList = function (invoiceorderDetJsonString) { 

    $("#showtable").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "filter_invoice_order.php",
        data: {invoiceorderSearchCriteria: invoiceorderDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#showtable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#showtable").show();
            // console.log(response);
        }, 

    }) 

}
