 $(document).ready(function(){

    var kazaFood = new FoodApp();

    var vendorDetArr = {};
 	vendorDetArr["vendor_id"]       = $("#HdnVendor").val();
    vendorDetArr["package_name"]    = $("#package_name").val();
    vendorDetArr["package_type"]    = $("#package_type").val();
    // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
    // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
    // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
    var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
    kazaFood.vendorPackagesList(vendorDetJsonString);
    
    $(document).on("click", "#searchbtn", function() {
        // var vendorDetArr = {};
        vendorDetArr["vendor_id"]        = $("#HdnVendor").val();
	    vendorDetArr["package_name"]     = $("#package_name").val();
	    vendorDetArr["package_type"]     = $("#package_type").val();
	    // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
	    // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
	    // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();	    
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorPackagesList(vendorDetJsonString);
    });

    $(document).on("click", "#resetbtn", function() {        
        $("#package_name").val("");
        $("#package_type").val("");
        // $("#HdnPage").val("1");
        // $("#HdnMode").val("1");
        // $("#RecordsPerPage").val("25");

        vendorDetArr["vendor_id"] 		= $("#HdnVendor").val();
        vendorDetArr["package_name"] 	= $("#package_name").val();
        vendorDetArr["package_type"] 	= $("#package_type").val();
        // vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
        // vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
        // vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorPackagesList(vendorDetJsonString);
    });
});