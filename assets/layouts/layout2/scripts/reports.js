$(document).ready(function(){

    var kazaFood = new FoodApp();
    
    var reportsDetArr = {};
    reportsDetArr["customer_id"] = $("#customer").val();
    reportsDetArr["vendors_id"] = $("#vendors").val();
    reportsDetArr["start_date"] = $("#start_date").val();
    reportsDetArr["end_date"] = $("#end_date").val();
    reportsDetArr["status"] = $("#status").val();
    reportsDetArr["HdnPage"] = $("#HdnPage").val();
    reportsDetArr["HdnMode"] = $("#HdnMode").val();
    reportsDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var reportsDetJsonString = JSON.stringify(reportsDetArr);    
    kazaFood.orderReportList(reportsDetJsonString);

    $(document).on("click", "#Search", function() {
        reportsDetArr["customer_id"] = $("#customer").val();
        reportsDetArr["vendors_id"] = $("#vendors").val();
        reportsDetArr["start_date"] = $("#start_date").val();
        reportsDetArr["end_date"] = $("#end_date").val();
        reportsDetArr["status"] = $("#status").val();
        reportsDetArr["HdnPage"] = $("#HdnPage").val();
        reportsDetArr["HdnMode"] = $("#HdnMode").val();
        reportsDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var reportsDetJsonString = JSON.stringify(reportsDetArr);
        kazaFood.orderReportList(reportsDetJsonString);
    });

    $(document).on("click", "#reportReset", function() {

        $("#customer").val("");
        $("#vendors").val("");
        $("#start_date").val("");
        $("#end_date").val("");
        $("#status").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        reportsDetArr["customer_id"] = $("#customer").val();
        reportsDetArr["vendors_id"] = $("#vendors").val();
        reportsDetArr["start_date"] = $("#start_date").val();
        reportsDetArr["end_date"] = $("#end_date").val();
        reportsDetArr["status"] = $("#status").val();
        reportsDetArr["HdnPage"] = $("#HdnPage").val();
        reportsDetArr["HdnMode"] = $("#HdnMode").val();
        reportsDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var reportsDetJsonString = JSON.stringify(reportsDetArr);
        kazaFood.orderReportList(reportsDetJsonString);
    });
    $(document).on("click", "#vieworderitems", function() {
        var orderID=$(this).attr("dataorder-id");
        var packageID=$(this).attr("datapackage-id");
        kazaFood.viewOrderItem(orderID,packageID);
    });
    $(document).on("change","#vendors,#customer,#start_date,#end_date,#status",function() {
        var vendors=$("#vendors").find(':selected').attr('data-id');
        if (vendors!="" && typeof (vendors) != "undefined") {
            $("#appendURL").attr("href", "invoice_order.php?vendor="+vendors);
        } else {
            $("#appendURL").attr("href", "invoice_order.php");
        }
    });
});