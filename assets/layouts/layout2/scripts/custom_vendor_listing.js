 $(document).ready(function(){

    var kazaFood = new FoodApp();

    var vendorDetArr = {};
 	vendorDetArr["vendor_id"]       = $("#vendors").val();
    vendorDetArr["vendor_mail"]     = $("#vendor_mail").val();
    vendorDetArr["status_fltr"]     = $("#status_fltr").val();
    vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
    vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
    vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
    var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
    kazaFood.vendorList(vendorDetJsonString);
    
    $(document).on("click", "#searchbtn", function() {
        // var vendorDetArr = {};
        vendorDetArr["vendor_id"]       = $("#vendors").val();
	    vendorDetArr["vendor_mail"]     = $("#vendor_mail").val();
        var status=$("#status_fltr").val();
        $( "#status_fltr" ).val(status);
	    vendorDetArr["status_fltr"]     = $("#status_fltr").val();
	    vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
	    vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
	    vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();	    
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorList(vendorDetJsonString);
    });

    $(document).on("click", "#resetbtn", function() {
        $("#vendors").val("");
        $("#vendor_mail").val("");
        $("#status_fltr").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        vendorDetArr["vendor_id"] 		= $("#vendors").val();
        vendorDetArr["vendor_mail"] 	= $("#vendor_mail").val();
        vendorDetArr["status_fltr"] 	= $("#status_fltr").val();
        vendorDetArr["HdnPage"] 		= $("#HdnPage").val();
        vendorDetArr["HdnMode"] 		= $("#HdnMode").val();
        vendorDetArr["RecordsPerPage"] 	= $("#RecordsPerPage").val();
        var vendorDetJsonString 		= JSON.stringify(vendorDetArr);
        kazaFood.vendorList(vendorDetJsonString);
    });
    
    $("#Savevendorstatus").on("click",function(){         
        var HdnVendor   =   $("#HdnVendor").val();
        var status      =   $("#status").val();        
        if(HdnVendor && status) {
            kazaFood.vendorStatus(status,HdnVendor);
        }
    });   
});