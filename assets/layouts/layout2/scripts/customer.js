$(document).ready(function(){

    var kazaFood = new FoodApp();
    
    var customerDetArr = {};
    customerDetArr["customer_id"] = $("#customer").val();
    customerDetArr["email"] = $("#email").val();
    customerDetArr["status"] = $("#status").val();
    customerDetArr["HdnPage"] = $("#HdnPage").val();
    customerDetArr["HdnMode"] = $("#HdnMode").val();
    customerDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var customerDetJsonString = JSON.stringify(customerDetArr); 

    kazaFood.customerList(customerDetJsonString);

    $(document).on("click", "#Search", function() {
        customerDetArr["customername"] = $("#customer").val();
        customerDetArr["email"] = $("#email").val();
        customerDetArr["status"] = $("#status").val();
        customerDetArr["HdnPage"] = $("#HdnPage").val();
        customerDetArr["HdnMode"] = $("#HdnMode").val();
        customerDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var customerDetJsonString = JSON.stringify(customerDetArr);
        kazaFood.customerList(customerDetJsonString);
    });

    $(document).on("click", "#customReset", function() {

        $("#customer").val("");
        $("#email").val("");
        $("#status").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        customerDetArr["customername"] = $("#customer").val();
        customerDetArr["email"] = $("#email").val();
        customerDetArr["status"] = $("#status").val();
        customerDetArr["HdnPage"] = $("#HdnPage").val();
        customerDetArr["HdnMode"] = $("#HdnMode").val();
        customerDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var customerDetJsonString = JSON.stringify(customerDetArr);
        kazaFood.customerList(customerDetJsonString);
    });

    $(document).on("click","#popupStatus",function() {
        var customerid = $(this).attr("data-id");
        $("#popstatus").val("");
        if (customerid!="") {
            $("#customerID").val(customerid);
            $("#CustomerStatus").modal("show");
        } else {
            $("#customerID").val("");
            $("#CustomerStatus").modal("hide");
        }

    });
    $(document).on("click","#PopupStatus",function() {

        var customerID=$("#customerID").val();
        var popstatus=$("#popstatus").val();

        if(popstatus==""){
            $(".statusmsg").show();
            return false;
        }
        if(popstatus!="" && customerID!="") {
             kazaFood.customerAjaxStatus(popstatus,customerID);
        }

    });
    $(document).on("change","#popstatus",function() {
         var popstatus=$("#popstatus").val();

        if(popstatus==""){
            $(".statusmsg").show();
            return false;
        } else {
            $(".statusmsg").hide();
            return true;
        }
    });
});