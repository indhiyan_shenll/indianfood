-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2017 at 05:05 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `indian_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
`category_id` int(11) NOT NULL,
  `category_order` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_order`, `vendor_id`, `category_name`, `description`, `image`, `created_date`, `modified_date`) VALUES
(1, 1, 6, 'Main dishes', 'Main dishes for Lunch', 'uploads/category/cate_main_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(2, 2, 6, 'Side dishes', 'Side dishes for Lunch', 'uploads/category/cate_side_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(3, 3, 6, 'Vegetarian Only/Halal Only', 'Best Soups after the Lunch', 'uploads/category/cate_soup.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(4, 4, 7, 'Main dishes', 'Main dishes for the Lunch', 'uploads/category/cate_main_dish.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(5, 5, 7, 'Side Dishes', 'Ice creams', 'uploads/category/cate_ice.png', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(6, 6, 7, 'Vegetarian Only/Halal Only', 'Vegetarian Only/Halal Only', 'uploads/category/cate_ice.png', '2017-10-24 00:00:00', '2017-10-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_items`
--

CREATE TABLE IF NOT EXISTS `tbl_category_items` (
`item_id` int(11) NOT NULL,
  `item_order` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_name` varchar(250) NOT NULL,
  `short_description` text NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_category_items`
--

INSERT INTO `tbl_category_items` (`item_id`, `item_order`, `category_id`, `item_name`, `short_description`, `item_type`, `image`, `price`, `created_date`, `modified_date`) VALUES
(1, 1, 1, 'item 1', 'This item 1 for Main dishes', 'veg', '', '100.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(2, 2, 1, 'item 2', 'This item 2 for Main dishes', 'veg', '', '120.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(3, 3, 1, 'item 3', 'This item 3 for Main dishes', 'nonveg', '', '150.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(4, 4, 1, 'item 4', 'This item 4 for Main dishes', 'nonveg', '', '130.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(5, 5, 2, 'Item 1', 'side dish item 1', 'veg', 'uploads/category_items/maindish-item1.jpg', '54.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(6, 6, 2, 'Item 2', 'side dish item 2', 'nonveg', 'uploads/category_items/sidedishes-item2.jpg', '86.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(7, 7, 2, 'Item 3', 'side dish item 3', 'nonveg', '', '32.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(8, 8, 3, 'Item 1', 'veg soup', 'Halal', 'uploads/category_items/halal-item1.jpg', '123.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(9, 9, 3, 'Item 2', 'Non-veg soup', 'Halal', 'uploads/category_items/halal-item2.jpg', '233.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(10, 10, 4, 'Item 1', 'item 1', 'nonveg', 'uploads/category_items/maindish-item1.jpg', '234.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(11, 10, 4, 'Item 2', 'item 2', 'nonveg', 'uploads/category_items/maindish-item2.jpg', '102.00', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(12, 11, 5, 'Item 1', 'venilla icecream large cup', 'veg', 'uploads/category_items/sidedishes-item1.jpg', '42.00', '2017-10-09 00:00:00', '2017-10-02 00:00:00'),
(13, 12, 5, 'Item 2', 'strawberry icecream large cup', 'veg', 'uploads/category_items/sidedishes-item2.jpg', '51.00', '2017-10-09 00:00:00', '2017-10-02 00:00:00'),
(14, 13, 6, 'Item 1', 'venilla icecream large cup', 'Halal', 'uploads/category_items/halal-item1.jpg', '55.00', '2017-10-24 00:00:00', '2017-10-24 00:00:00'),
(15, 13, 6, 'Item 2', 'strawberry icecream large cup', 'Halal', 'uploads/category_items/halal-item2.jpg', '55.00', '2017-10-24 00:00:00', '2017-10-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daywise_orders`
--

CREATE TABLE IF NOT EXISTS `tbl_daywise_orders` (
`days_order_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoices`
--

CREATE TABLE IF NOT EXISTS `tbl_invoices` (
`invoice_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `total_amount` decimal(16,2) NOT NULL,
  `invoice_amount` decimal(16,2) NOT NULL,
  `discount` decimal(16,2) NOT NULL,
  `vendor_invoice_status` varchar(25) NOT NULL,
  `admin_invoice_status` varchar(25) NOT NULL,
  `develop_status` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `tbl_invoices`
--

INSERT INTO `tbl_invoices` (`invoice_id`, `vendor_id`, `transaction_id`, `total_amount`, `invoice_amount`, `discount`, `vendor_invoice_status`, `admin_invoice_status`, `develop_status`, `created_date`, `modified_date`) VALUES
(34, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 7, '', '0.00', '540.00', '60.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 7, '', '0.00', '540.00', '60.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 7, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 7, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 7, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 7, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '2017-10-16 01:11:24', '2017-10-16 01:11:24'),
(102, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '2017-10-16 01:15:59', '2017-10-16 01:15:59'),
(103, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '2017-10-16 01:17:20', '2017-10-16 01:17:20'),
(108, 7, 'ACRAF23DB3C4', '0.00', '180.00', '20.00', 'Paid', 'Paid', '', '2017-10-16 03:08:24', '2017-10-16 03:08:24'),
(109, 6, '', '0.00', '360.00', '40.00', '', '', 'preview', '2017-10-19 22:40:22', '2017-10-19 22:40:22'),
(142, 7, '', '0.00', '540.00', '60.00', '', '', 'preview', '2017-10-23 04:41:20', '2017-10-23 04:41:20'),
(143, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '2017-10-23 05:52:45', '2017-10-23 05:52:45'),
(144, 6, '', '0.00', '270.00', '30.00', '', '', 'preview', '2017-10-23 05:52:53', '2017-10-23 05:52:53'),
(145, 6, 'ACRAF23DB3C5', '0.00', '270.00', '30.00', 'paid', 'Paid', '', '2017-10-23 05:53:02', '2017-10-23 05:53:02'),
(146, 7, '', '0.00', '180.00', '20.00', '', '', 'preview', '2017-10-23 05:57:32', '2017-10-23 05:57:32'),
(147, 7, 'ACRAF23DB3C6', '0.00', '180.00', '20.00', 'Pending', 'Pending', '', '2017-10-23 05:57:46', '2017-10-23 05:57:46'),
(148, 7, '', '0.00', '360.00', '40.00', '', '', 'preview', '2017-10-23 05:58:29', '2017-10-23 05:58:29'),
(149, 7, 'ACRAF23DB3C7', '0.00', '360.00', '40.00', 'Pending', 'Pending', '', '2017-10-23 05:58:31', '2017-10-23 05:58:31'),
(150, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '2017-10-23 07:00:28', '2017-10-23 07:00:28'),
(151, 6, 'ACRAF23DB3C8', '0.00', '90.00', '10.00', 'Pending', 'Pending', '', '2017-10-24 02:27:55', '2017-10-24 02:27:55'),
(152, 6, '', '0.00', '90.00', '10.00', '', '', 'preview', '2017-10-24 03:22:40', '2017-10-24 03:22:40'),
(153, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '2017-10-24 05:09:48', '2017-10-24 05:09:48'),
(154, 8, '', '0.00', '135.00', '15.00', '', '', 'preview', '2017-10-24 05:10:19', '2017-10-24 05:10:19'),
(155, 6, 'ACRAF23DB3C9', '0.00', '90.00', '10.00', 'Pending', 'Pending', '', '2017-10-24 07:01:56', '2017-10-24 07:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice_orders`
--

CREATE TABLE IF NOT EXISTS `tbl_invoice_orders` (
`invoice_order_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `invoice_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `tbl_invoice_orders`
--

INSERT INTO `tbl_invoice_orders` (`invoice_order_id`, `invoice_id`, `order_id`, `invoice_date`) VALUES
(124, 145, 1, '2017-10-23 05:53:02'),
(125, 147, 2, '2017-10-23 05:57:46'),
(126, 149, 5, '2017-10-23 05:58:31'),
(127, 151, 4, '2017-10-24 02:27:55'),
(128, 155, 6, '2017-10-24 07:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orders`
--

CREATE TABLE IF NOT EXISTS `tbl_orders` (
`order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `delivery_time` time DEFAULT NULL,
  `price` decimal(16,2) NOT NULL,
  `ratings` float NOT NULL,
  `order_type` varchar(10) NOT NULL,
  `status` varchar(25) NOT NULL,
  `paypal_transaction_id` varchar(100) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `payment_date` datetime NOT NULL,
  `reason` varchar(200) DEFAULT NULL,
  `feedback_msg` varchar(200) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_orders`
--

INSERT INTO `tbl_orders` (`order_id`, `customer_id`, `vendor_id`, `package_id`, `start_date`, `end_date`, `delivery_time`, `price`, `ratings`, `order_type`, `status`, `paypal_transaction_id`, `payment_status`, `payment_date`, `reason`, `feedback_msg`, `created_date`, `modified_date`) VALUES
(1, 2, 6, 1, '2017-10-06', '2017-10-10', NULL, '300.00', 3.5, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 12:20:26'),
(2, 2, 7, 2, '2017-10-06', '2017-10-09', NULL, '200.00', 4.3, 'veg', 'Pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 07:18:19', '2017-10-06 10:21:21'),
(3, 3, 8, 2, '2017-10-06', '2017-10-10', NULL, '150.00', 2.5, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 12:20:26'),
(4, 4, 6, 1, '2017-10-06', '2017-10-09', NULL, '100.00', 4, 'veg', 'Pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 07:18:19', '2017-10-06 10:21:21'),
(5, 5, 7, 1, '2017-10-06', '2017-10-10', NULL, '400.00', 3, 'Non veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-06 04:26:15', '2017-10-06 07:10:06'),
(6, 2, 6, 2, '2017-10-22', '2017-10-28', NULL, '100.00', 4, 'veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-23 19:26:43', '2017-10-23 19:26:47'),
(7, 2, 6, 1, '2017-10-23', '2017-10-31', NULL, '100.00', 4, 'veg', 'pending', '', 'Completed', '0000-00-00 00:00:00', NULL, NULL, '2017-10-23 19:26:43', '2017-10-23 19:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_items`
--

CREATE TABLE IF NOT EXISTS `tbl_order_items` (
`order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_order_items`
--

INSERT INTO `tbl_order_items` (`order_item_id`, `order_id`, `category_id`, `item_id`, `price`, `created_date`, `modified_date`) VALUES
(1, 3, 1, 6, '150.00', '2017-10-06 10:24:24', '2017-10-06 10:24:24'),
(2, 4, 1, 2, '150.00', '2017-10-06 08:21:25', '2017-10-06 11:28:27'),
(3, 5, 3, 3, '200.00', '2017-10-06 10:24:24', '2017-10-06 10:24:24'),
(4, 3, 2, 2, '150.00', '2017-10-06 08:21:25', '2017-10-06 11:28:27'),
(5, 4, 5, 4, '50.00', '2017-10-06 10:24:24', '2017-10-06 10:24:24'),
(6, 5, 5, 4, '50.00', '2017-10-06 08:21:25', '2017-10-06 11:28:27'),
(7, 5, 4, 7, '200.00', '2017-10-06 10:24:24', '2017-10-06 10:24:24'),
(8, 4, 4, 5, '200.00', '2017-10-06 08:21:25', '2017-10-06 11:28:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packages`
--

CREATE TABLE IF NOT EXISTS `tbl_packages` (
`package_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_days_count` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `package_type` varchar(50) NOT NULL,
  `rate` decimal(16,2) NOT NULL,
  `status` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_packages`
--

INSERT INTO `tbl_packages` (`package_id`, `vendor_id`, `package_name`, `package_days_count`, `image`, `package_type`, `rate`, `status`, `created_date`, `modified_date`) VALUES
(1, 6, 'pack 1', 3, '', 'both', '20.00', 'active', '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(2, 7, 'pack 2', 5, '', 'veg', '10.00', 'active', '2017-10-09 00:00:00', '2017-10-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_items`
--

CREATE TABLE IF NOT EXISTS `tbl_package_items` (
`package_item_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_package_items`
--

INSERT INTO `tbl_package_items` (`package_item_id`, `package_id`, `category_id`, `item_id`, `created_date`, `modified_date`) VALUES
(1, 1, 1, 1, '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(2, 1, 2, 6, '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(3, 2, 5, 12, '2017-10-09 00:00:00', '2017-10-09 00:00:00'),
(4, 2, 5, 13, '2017-10-09 00:00:00', '2017-10-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
`setting_id` int(11) NOT NULL,
  `setting_name` varchar(100) NOT NULL,
  `setting_type` varchar(100) NOT NULL,
  `setting_value` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`setting_id`, `setting_name`, `setting_type`, `setting_value`, `created_date`, `modified_date`) VALUES
(1, 'discount', 'percent', '10', '2017-10-15 23:47:54', '2017-10-15 23:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
`user_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(50) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `upload_documents` text NOT NULL,
  `status` varchar(25) NOT NULL,
  `device_token` varchar(100) NOT NULL,
  `user_platform` varchar(25) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `full_name`, `email`, `password`, `user_type`, `address`, `country`, `zip_code`, `mobile_number`, `upload_documents`, `status`, `device_token`, `user_platform`, `created_date`, `modified_date`) VALUES
(1, 'Kaza', 'admin@kaza.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'Admin', '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '324222', '543534534533', '', 'Active', '', '', '2017-10-05 09:22:22', '2017-10-05 08:24:25'),
(2, 'karunakaran', 'karuna.shenll@gmail.com', '123456', 'customer', '26/1, Mahathma Gandhi Road, Chennai  600034.', 'India', '635737', '90893887362', '', 'Active', '', '', '2017-10-06 09:22:24', '2017-10-06 09:22:28'),
(3, 'vicky', 'vigneshkumar.shenll@gmail.com', '123456', 'customer', '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '746373', '3232324322', '', 'Active', '', '', '2017-10-06 08:20:24', '2017-10-06 08:22:31'),
(4, 'indhiyan', 'indhiyan.shenll@gmail.com', '123456', 'customer', 'No 6 AMC Enclave,  Nungambakkam, Chennai - 600034, ', 'India', '324522', '43432322322', 'uploads/user_documents/auntya.pdf', 'active', '', '', '2017-10-06 10:24:33', '2017-10-06 09:24:27'),
(5, 'thiyagu', 'thiyagu.shenll@gmail.com', '123456', 'customer', 'No 6 AMC Enclave,KK.Nagar, Chennai - 600034, ', 'India', '324522', '43432322322', '', 'Active', '', '', '2017-10-06 10:24:33', '2017-10-06 09:24:27'),
(6, 'Saravanan', 'saravanakumar.shenll@gmail.com', '123456', 'vendor', '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '626765', '9095905095', 'uploads/user_documents/auntya.pdf', 'active', '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19'),
(7, 'Alan', 'alan@gmail.com', '123456', 'vendor', '692, MHU Complex, Nandanam, Anna Salai, Chennai 600035.', 'India', '626555', '9095905065', 'uploads/user_documents/auntya.pdf', 'active', '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19'),
(8, 'ino', 'ino@gmail.com', '123456', 'customer', '26/1, Mahathma Gandhi Road, Chennai 600034.', 'India', '626555', '9095905065', 'uploads/user_documents/auntya.pdf', 'active', '', '', '2017-10-06 09:25:24', '2017-10-06 07:21:19'),
(9, 'Shenll', 'shenllemail@gmail.com', '123456', 'customer', 'Chennai', 'India', '354637', '1234567894', '', 'block', '643834834834346384363', 'IOS', '2017-10-12 05:03:00', '2017-10-12 05:03:00'),
(10, 'preethi', 'test@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Address', 'india', '600900', '12345678', '', 'Active', '1234', 'IOS', '2017-10-23 01:22:56', '2017-10-24 05:12:13'),
(11, 'preethi', 'test1@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Address', 'india', '606096', '12323445', '', 'Active', '1234', 'IOS', '2017-10-23 02:22:18', '2017-10-23 02:22:18'),
(12, 'testing', 'testing@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Address', 'india', '606907', '12345', '', 'Active', '1234', 'IOS', '2017-10-23 02:23:31', '2017-10-23 02:23:31'),
(13, 'Testing1', 'testing1@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'customer', 'AddressAddress', 'india', '600000', '1234567', '', 'Active', '1234', 'IOS', '2017-10-23 02:30:25', '2017-10-23 03:08:08'),
(14, 'Testing', 'testing1@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Addressnew', 'india', '606060', '1234567890', '', 'Active', '1234', 'IOS', '2017-10-23 03:05:22', '2017-10-23 03:05:22'),
(15, 'Testing12', 'testing13@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Addressnew3', 'india', '606060', '1234567893', '', 'Active', '1234', 'IOS', '2017-10-23 03:06:12', '2017-10-23 03:06:12'),
(16, 'Testing123', 'testing135@test.com', 'ZW3mrSUDB1YWdZ9EKxxHOaw7WYjPUYWEBI6AjuMTsdY', 'customer', 'Addresss', 'india', '606909', '1234567833', '', 'Active', '1234', 'IOS', '2017-10-23 03:06:42', '2017-10-23 03:06:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_category_items`
--
ALTER TABLE `tbl_category_items`
 ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `tbl_daywise_orders`
--
ALTER TABLE `tbl_daywise_orders`
 ADD PRIMARY KEY (`days_order_id`);

--
-- Indexes for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
 ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `tbl_invoice_orders`
--
ALTER TABLE `tbl_invoice_orders`
 ADD PRIMARY KEY (`invoice_order_id`);

--
-- Indexes for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_items`
--
ALTER TABLE `tbl_order_items`
 ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
 ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `tbl_package_items`
--
ALTER TABLE `tbl_package_items`
 ADD PRIMARY KEY (`package_item_id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
 ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_category_items`
--
ALTER TABLE `tbl_category_items`
MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_daywise_orders`
--
ALTER TABLE `tbl_daywise_orders`
MODIFY `days_order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_invoices`
--
ALTER TABLE `tbl_invoices`
MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `tbl_invoice_orders`
--
ALTER TABLE `tbl_invoice_orders`
MODIFY `invoice_order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `tbl_orders`
--
ALTER TABLE `tbl_orders`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_order_items`
--
ALTER TABLE `tbl_order_items`
MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_packages`
--
ALTER TABLE `tbl_packages`
MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_package_items`
--
ALTER TABLE `tbl_package_items`
MODIFY `package_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
