<?php
session_start();
error_reporting(0);
global $dbconn;
define("LOCAL_SERVER_NAME","localhost");
define("DEMO_SERVER_NAME","demo.shenll.net");

//PDO connection starts here
try {
	if (substr_count($_SERVER["SERVER_NAME"], LOCAL_SERVER_NAME) > 0 ) {
		// Database Informations
		!defined("DB_HOST_NAME") ? define("DB_HOST_NAME","localhost") : "" ;
		!defined("DATABASE") ? define("DATABASE","indian_food") : "" ;
		!defined("USERNAME") ? define("USERNAME","root") : "" ;
		!defined("PASSWORD") ? define("PASSWORD","") : "" ;
		define("KAZA_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/indianfood/");
		define("KAZA_SYSTEM_PATH","http://localhost/indianfood/");
	} else if (substr_count($_SERVER["SERVER_NAME"], DEMO_SERVER_NAME) > 0 ) {
		// Database Informations
		!defined("DB_HOST_NAME") ? define("DB_HOST_NAME","kazafood.db.6877569.b43.hostedresource.net") : "" ;
		!defined("DATABASE") ? define("DATABASE","kazafood") : "" ;
		!defined("USERNAME") ? define("USERNAME","kazafood") : "" ;
		!defined("PASSWORD") ? define("PASSWORD","Shen!lK@z@2017") : "" ;
		define("KAZA_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/demo.shenll.net/kazafood/");
		define("KAZA_SYSTEM_PATH","http://demo.shenll.net/kazafood/");
	}
	// Mysql PDO connection
	$dbconn = new PDO("mysql:host=".DB_HOST_NAME.";dbname=".DATABASE,USERNAME,PASSWORD);
	$dbconn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
} catch (PDOException $e) {
   echo "Connection failed: " . $e->getMessage();
}

